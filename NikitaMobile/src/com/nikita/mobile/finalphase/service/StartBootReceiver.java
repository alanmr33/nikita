package com.nikita.mobile.finalphase.service;

 
import com.nikita.mobile.finalphase.utility.Utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartBootReceiver extends BroadcastReceiver {
	
     public void onReceive(Context context, Intent intent) {
    	Utility.i("StartBootReceiver", "onReceive");
    	
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {             
        	AlarmReceiver.setAlarm(context);
        	NikitaRz.restartNikitaWebSocket(context);        	
        }
        
        /*
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {}        
        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {} 
        if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {}
        */
    }
}
