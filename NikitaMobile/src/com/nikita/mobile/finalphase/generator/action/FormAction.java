package com.nikita.mobile.finalphase.generator.action;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Vector;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.activity.MenuActivity;
import com.nikita.mobile.finalphase.activity.SettingActivity;
import com.nikita.mobile.finalphase.activity.SplashScreen;
import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NFormActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.NikitaEngine;
import com.nikita.mobile.finalphase.generator.ui.ButtonUI;
import com.nikita.mobile.finalphase.generator.ui.CheckboxUI;
import com.nikita.mobile.finalphase.generator.ui.CombolistUI;
import com.nikita.mobile.finalphase.generator.ui.ListViewUI;
import com.nikita.mobile.finalphase.generator.ui.RadioboxUI;
import com.nikita.mobile.finalphase.generator.ui.TableGridUI;
import com.nikita.mobile.finalphase.generator.ui.TableGridUI.AdapterListener;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.nikita.mobile.finalphase.service.NikitaFuseLocation;
import com.nikita.mobile.finalphase.stream.NfData;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class FormAction  implements IAction{
	@Override
	public boolean OnAction(final Component comp, final Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
	    String c = (currdata.getData("args").getData("param1").toString());
	    String modal = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());  
	    String reqcode = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString());  
		
	    
	    if (code.equals("showform")||code.equals("showfinder")||code.equals("calllink")||code.equals("calltask")||code.equals("callfunction")||code.equals("showwindows")||code.equals("showcontent")) {
	    	//new 19/08/2016
	    	String param5 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param5").toString());  
	    	//new 20160401
            c = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
	    	
	    	Nset n = Nset.newObject();Nset parameter = Nset.newObject();
            int ilog = comp.getNikitaComponent().getCurrentRow();
            if (c.startsWith("{")) {
                n = Nset.readJSON(c);
                c=n.getData("formid").toString();
                if (!n.getData("formname").toString().equals("")) {
                    c=n.getData("formname").toString();
                }
                
                
                String[] keys = n.getData("args").getObjectKeys();               
                for (int i = 0; i < keys.length; i++) {
                	String data = n.getData("args").getData(keys[i]).toString();
                    if (keys[i].startsWith("[")&& keys[i].endsWith("]")) {
                        //Difinition Result      (finder)               
                        parameter.setData(keys[i].substring(1, keys[i].length()-1),  data  );      
                    }else{                         
                    	parameter.setData(keys[i], comp.getNikitaComponent().getVirtualString( data ) );      
                    }      
                    //request.setParameter(keys[i], comp.getNikitaComponent().getVirtualString(  n.getData("args").getData(keys[i]).toString() ) );                                
                	//parameter.setData(keys[i], comp.getNikitaComponent().getVirtualString(  n.getData("args").getData(keys[i]).toString() ) );                  
                }
                
                 
            }       
            //19/08/2016
            if (Utility.isNumeric(param5.trim())) {
				c  = c + NikitaEngine.NFID_ARRAY_INDEX + param5.trim();
            }       
            
            if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString()) instanceof Nset) {
                Nset arg = (Nset)comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString());
                String[] keys = arg.getObjectKeys();
                for (int i = 0; i < keys.length; i++) {
                   // request.setParameter(keys[i], arg.getData(keys[i]).toString() );  
                	parameter.setData(keys[i], arg.getData(keys[i]).toString() );  
                }
            } 
            if (code.equals("calllink")||code.equals("calltask")||code.equals("callfunction")) {
            	//comp.getNikitaComponent().runServletGen(comp, request, response , logic );
            	//comp.getNikitaComponent().callTask(c , parameter);
            	NikitaControler component = NikitaControler.getInstance(c).start();//name[instance]      
            	component.executeNikitaTask(parameter);
            }else if(code.equals("showwindows")||code.equals("showcontent")){
            	
            	/*
             	final NForm form = NCore.getInstanceForm(c,  "", parameter);	 
            	form.setCaller(comp.getNikitaComponent());
            	form.setRequestCode(reqcode);
            	comp.getNikitaComponent().runOnUI(new Runnable() {
					public void run() {
						if (form.getContent()!=null) {
							Component  component =   comp.getNikitaComponent().getComponent(currdata.getData("args").getData("param2").toString());
							View whilereplace =component.getView();
							if (whilereplace!=null && whilereplace.getParent()!=null) {
								// 
								final ViewGroup parent = (ViewGroup) whilereplace.getParent();
						    	int index = parent.indexOfChild(whilereplace);
							    parent.removeView(whilereplace);
							    
							    form.getContent().onCreate(comp.getNikitaComponent());
						    	parent.addView(form.getContent().getView(), index);	
							}							
							form.getContent().getNikitaComponent().setActivityListener(new NikitaActivityListener() {
								public NFormActivity getActivity() {						
									return comp.getNikitaComponent().getActivity();
								}
							});		
						    comp.getNikitaComponent().getContent().replaceComponentbyId(component.getId(), form.getContent());
						    form.getContent().setId(component.getId());
						    form.getContent().setName(component.getName());
						    	
						}
					}
				});	
				*/
            	
            	NikitaControler component = NikitaControler.getInstance(c).start();//name[instance]      
            	 
            	comp.setReplaceComponent(component);    
            	if (comp.getNikitaComponent().isCreated()) {
            		comp.getNikitaComponent().runOnUiThread(new Runnable() {
						public void run() {
							comp.replaceCreateUI();							
						}
					});
				}
            }else{
            	AppNikita.getInstance().setCurrentNikitaControler(comp.getNikitaComponent());//04/08/2016
              	NikitaControler component = NikitaControler.getInstance(c);
            	if (code.equals("showfinder")) {
            		component.showFinder(reqcode, parameter);
				}else{
					//****************************************************// 08-12-2015
					if (c.endsWith("Activity")) {
						if (Generator.isClassExist("com.nikita.mobile.finalphase.activity."+c)) {
							try {
								Intent intent = new Intent(comp.getNikitaComponent().getActivity(), Class.forName("com.nikita.mobile.finalphase.activity."+c));	
								String[] keys = parameter.getObjectKeys();
								for (int i = 0; i < keys.length; i++) {
									intent.putExtra(keys[i], parameter.getData(keys[i]).toString());
								}		
								if (parameter.containsKey("activity-top")) {
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
								}else if (parameter.containsKey("FLAG_ACTIVITY_CLEAR_TOP")) {
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);									
								}
								comp.getNikitaComponent().getActivity().startActivity(intent);
								comp.getNikitaComponent().setCurrentRow(ilog);   
								return true;
							} catch (Exception e) { }
						}
					}
					//****************************************************//
					
					component.show(reqcode, parameter);
				}            	 
            }
            comp.getNikitaComponent().setCurrentRow(ilog);   
            
	    }else if (code.equals("home")) { 
	    	String s = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
	    	
	    	Generator.clearNikitaForms();
	    	try {
				if (s.equals("")) {
					s = "MenuActivity";
				}
				if (Generator.isClassExist("com.nikita.mobile.finalphase.activity."+s)) {
					Intent intent = new Intent(comp.getNikitaComponent().getActivity(), Class.forName("com.nikita.mobile.finalphase.activity."+s));
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    	comp.getNikitaComponent().getActivity().startActivity(intent);	    
				}					
			} catch (Exception e) { 	}
	    	
	    }else if (code.equals("closeform")) {
	    	String s = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
	    	if (s.equals("*")||s.equals("[*]")) {
	    		String[] keys = AppNikita.getInstance().getNikitaControlerNames();	    		 
	    		for (int i = 0; i < AppNikita.getInstance().getNikitaControlerCount(); i++) {
	    			if (!keys[i].equals(comp.getNikitaComponent().getNikitaFormName())) {
	    				AppNikita.getInstance().getNikitaComponent(keys[i]).close();
					}
				}	    		
	    		Generator.clearNikitaForms();
	    		comp.getNikitaComponent().close();
			}else{
				comp.getNikitaComponent().close();
			}
	    }else if (code.equals("back")) {
	    	comp.getNikitaComponent().back();	    	
	    }else if (code.equals("calllogic")){
            int i = comp.getNikitaComponent().getCurrentRow();
            int l = comp.getNikitaComponent().getLoopCount();
            boolean b =comp.getNikitaComponent().expression;
            
            if (c.startsWith("$")) {
                //call route
                //NikitaService.loopdetect(response);
                if (comp.getNikitaComponent().getComponent(c).getId().equals("")) {
                    //link
                	/*
                    Nikitaset nikitaset = Generator.getConnection().QueryPage(1, 1, "SELECT * FROM web_component WHERE formid = ? ORDER BY compindex ASC",comp.getNikitaComponent().getContent().getId());
                    if (nikitaset.getRows()>=1) {
                    	comp.getNikitaComponent().getComponent(c).setId( nikitaset.getText(0, "compid") );
                    	comp.getNikitaComponent().getComponent(c).runRouteAction("click", false);
                        //new execLogic(request, response, response.getNikitaLogic(), nikitaset.getText(0, "compid"));
                    }    
                    */                        
                }else{
                	comp.getNikitaComponent().getComponent(c).runRouteAction("click", false);
                    //new NikitaServlet().execLogic(request, response, response.getNikitaLogic(), response.getComponent(comp).getId());
                }
            }else{
                //call action
            	//comp.getNikitaComponent().loopdetect(response);
                //NikitaService.runActionClass(Nset.newObject().setData("class", comp), request, response, logic);
            }
  
            //refill
            comp.getNikitaComponent().expression=b;
            comp.getNikitaComponent().setCurrentRow(i);
            comp.getNikitaComponent().setLoopCount(l);
            comp.getNikitaComponent().setVirtualRegistered("@+LOGICCOUNT", comp.getNikitaComponent().getLoopCount());
            comp.getNikitaComponent().setVirtualRegistered("@+LOGICCOUNTB1", comp.getNikitaComponent().getLoopCount()+1);  
            
	    }else if (code.equals("inflate")) {
            //equ = ComponentAction.settedata
            
            final Component component  = comp.getNikitaComponent().getComponent(currdata.getData("args").getData("param1").toString());  
            
            if (component instanceof TableGridUI||component instanceof ListViewUI) {
                if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString()) instanceof Nikitaset) {

                    ((TableGridUI)component).setData( (Nikitaset)comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString())  );
                }else{
                    component.setData(new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString())));
                }
                Nset header = Nset.newNull();
                if (currdata.getData("args").getData("param4").toString().trim().length()>=1) {    
                    Nset n  = new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param4").toString()));
                    if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param4").toString()) instanceof  String) {
                        String s = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString());
                        if((s.startsWith("[")|| s.startsWith("{") ) && !(s.startsWith("[*")|| s.startsWith("[#")|| s.startsWith("[v"))){
                            n = Nset.readJSON(s);
                        } else if(s.contains("|")){
                            n = Nset.readsplitString(s,"|");
                        } else if(s.contains(",")){
                            n = Nset.readsplitString(s,",");
                        }
                    }
                    if (n.getArraySize()>=1) {
                        ((TableGridUI)component).setDataHeader(n);
                    }                    
                     
                    if ( n.getData(0).toString().startsWith("[#]") ) {
                        ((TableGridUI)component).showRowIndex(true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }else if ( n.getData(0).toString().startsWith("[*]") ) {
                        ((TableGridUI)component).showRowIndex(true,true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }else if ( n.getData(0).toString().startsWith("[v]") ) {
                        ((TableGridUI)component).showRowIndex(false,true);
                        try {
                            ((Vector)n.getInternalObject()).setElementAt(n.getData(0).toString().substring(3), 0);
                        } catch (Exception e) { }
                    }
                    
                    header = new Nset(((Vector)n.getInternalObject()).clone());
                    for (int i = 0; i < n.getArraySize(); i++) {
                       ((TableGridUI)component).setColHide(i, n.getData(i).toString().equals(""));
                        if (n.getData(i).toString().contains("[*]")) {
                            ((TableGridUI)component).setRowCounter(true);                       
                        }else if (n.getData(i).toString().equals("[v]")) {
                            ((TableGridUI)component).setColType(i, TableGridUI.TYPE_CHECKBOK);
                        }else if (n.getData(i).toString().startsWith("[")&&n.getData(i).toString().endsWith("]") ) {
                            //((Tablegrid)component).setColType(i, Tablegrid.TYPE_CHECKBOK);
                           // ((Vector)n.getInternalObject()).setElementAt("", i);
                        }
                    }     
                    
                }
                
                int ir = comp.getNikitaComponent().getCurrentRow();
                int lr = comp.getNikitaComponent().getLoopCount();
                boolean b =comp.getNikitaComponent().expression;
                
                final Vector<NForm> xxxxxxxxxxx =  new Vector<NForm>();
                String fname = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()) ;
                for (int row = 0; row < component.getData().getArraySize(); row++) {
					
				 	Nset nq = Nset.newObject(); 
				 	nq.setData("INDEX", row+"");
				 	nq.setData("ROWDATA", component.getData().getData(row).toJSON());
	                                            

                    if (fname.startsWith("{")) {
                        Nset n = Nset.readJSON(fname);
                        fname=n.getData("formid").toString();
                        if (!n.getData("formname").toString().equals("")) {
                            fname=n.getData("formname").toString();
                        }
                        String[] keys = n.getData("args").getObjectKeys();               
                        for (int i = 0; i < keys.length; i++) {
                            nq.setData(keys[i], comp.getNikitaComponent().getVirtualString(  n.getData("args").getData(keys[i]).toString() ) );                                
                        }
                    }            
                    if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString()) instanceof Nset) {
                        Nset arg = (Nset)comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString());
                        String[] keys = arg.getObjectKeys();
                        for (int i = 0; i < keys.length; i++) {
                            nq.setData(keys[i], arg.getData(keys[i]).toString() );                    
                        }
                    } 
                    
                    NikitaControler nr =NikitaControler.getInstance(fname);
                    //nr.sta
                    //nr.show("", arg);
                    
                    nr.setVirtualRegistered("@+INDEX", row);
				 	nr.setVirtualRegistered("@+ROWDATA", component.getData().getData(row).toJSON());
				 	//nr.setCaller(component.getNikitaComponent());
				 	
				 	//xxxxxxxxxxx.addElement(nr);
				} 
                
                comp.getNikitaComponent().expression=b;
                comp.getNikitaComponent().setCurrentRow(ir);
                comp.getNikitaComponent().setLoopCount(lr);
                comp.getNikitaComponent().setVirtualRegistered("@+LOGICCOUNT", comp.getNikitaComponent().getLoopCount());
                comp.getNikitaComponent().setVirtualRegistered("@+LOGICCOUNTB1", comp.getNikitaComponent().getLoopCount()+1);  
                
                if (component instanceof ListViewUI) {
                	((ListViewUI)component).setAdapterListener(new TableGridUI.AdapterListener() {
                        private NikitaForm nf  ;
                        private Nset header;
                        private String id;
                        public AdapterListener get(String id, Nset header){
                            this.id=id;
                            this.header=header;
                            return this;
                        }
                        
                        public Component getViewItem(int row, int col, Component parent, Nset data) {                     
                        	//xxxxxxxxxxx.elementAt(row).internalonCreate(null);
                        	//nf = xxxxxxxxxxx.elementAt(row).getContent();                             
                             
                            return nf;
                        }
                    }.get("", header ));
                }else{
                	((TableGridUI)component).setAdapterListener(new TableGridUI.AdapterListener() {
                        private NikitaForm nf  ;
                        private Nset header;
                        private String id;
                        public AdapterListener get(String id, Nset header){
                            this.id=id;
                            this.header=header;
                            return this;
                        }
                        
                        public Component getViewItem(int row, int col, Component parent, Nset data) {                     
                            if (col==0 && comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()).trim().length()>=1 ) { 
                            	//xxxxxxxxxxx.elementAt(row).internalonCreate(null);
                            	//nf = xxxxxxxxxxx.elementAt(row).getContent(); 
                                                            
                            }  
                            
                            if (nf!=null) {
                                if (col>=nf.getComponentCount()) {
                                    return null;
                                }
                                Component cmp = nf.getComponent(col);     
                                if (cmp.isEnable() == false && cmp.isVisible() == false && cmp.getName().equals("")) {                               
                                    return null;
                                }
                                return cmp;
                            }
                            return null;
                        }
                    }.get("", header ));
                }
                
                
                
            }else{
                
                if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString()) instanceof Nikitaset) {
                    Nikitaset n =  (Nikitaset)comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString())  ;
                }else{
                    Nset n = new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString())) ;
                }
                int len = 0;
                for (int row = 0; row < len; row++) {
                    
                    
                    Nset nq = Nset.newObject();                    
                    nq.setData("INDEX", row+"");
                    
                    nq.setData("ROWDATA", "{}");


                    String fname = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()) ;
                    if (fname.startsWith("{")) {
                        Nset n = Nset.readJSON(fname);
                        fname=n.getData("formid").toString();

                        String[] keys = n.getData("args").getObjectKeys();               
                        for (int i = 0; i < keys.length; i++) {
                            nq.setData(keys[i], comp.getNikitaComponent().getVirtualString(  n.getData("args").getData(keys[i]).toString() ) );                                
                        }
                    }            
                    if (comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString()) instanceof Nset) {
                        Nset arg = (Nset)comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString());
                        String[] keys = arg.getObjectKeys();
                        for (int i = 0; i < keys.length; i++) {
                            nq.setData(keys[i], arg.getData(keys[i]).toString() );                    
                        }
                    }   
                    
                    /*
                    NForm nr = new NForm(fname);

                    nr.setVirtualRegistered("@+INDEX", row);
                    nr.setVirtual("@+ROWDATA", "{}");
                    nr.setVirtual("@NEW-INSTANCE",row);
                    
                    
                    comp.getNikitaComponent().setContent(nr.getContent());
                    */
                }
            }
            
            
            
            comp.getNikitaComponent().refreshComponent(component);
	    }else if (code.equals("validation")) {            
            if (comp.getNikitaComponent().getContent()!=null) {
                StringBuffer sb = new StringBuffer();
                Vector<Component>  components = comp.getNikitaComponent().getContent().populateAllComponents();
                for (int i = 0; i < components.size(); i++) {
                    Component component = components.get(i);
                    if (component.isVisible() && component.isEnable()&& component.isMandatory() && component.getName().length()>=1  ) {
                        if ((component instanceof TableGridUI)||(component instanceof ListViewUI)||(component instanceof ButtonUI)) {
                        }else if ((component instanceof ComponentGroup)) {                            
                        }else if ((component instanceof CombolistUI)||(component instanceof RadioboxUI)||(component instanceof CheckboxUI)) {
                             
                        }else if (component.getText().length()==0){
                            sb.append(sb.toString().length()>=1?",":"").append(component.getLabel());
                        }
                    }
                }
                comp.getNikitaComponent().setVirtual(currdata.getData("args").getData("result").toString(), sb.toString());
                
            }
        
	    }else if (code.equals("location")) { 	    	
	    	NikitaFuseLocation.requestLocationOnce(comp.getNikitaComponent().getActivity(), new NikitaFuseLocation.OnLocationListener() {
				public void onLocationChanged(final Location location) {}
 
				public void onLocationDone(Location location, int status) {
					// TODO Auto-generated method stub
					Generator.out("location",  NikitaFuseLocation.locationToNset(location).toJSON());
					String param = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
					comp.getNikitaComponent().onResult( param,  "LOCATION", NikitaFuseLocation.locationToNset(location));
				}
	    	});  
	    }else if (code.equals("localdate")) { 
	    	comp.getNikitaComponent().runOnActionThread(new Runnable() {
				public void run() {
					int i = TimeZone.getDefault().getRawOffset()/1000;
					String param = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
					Nset r = Nset.newObject();
					r.setData("date", Utility.Now());
					r.setData("time", new Date().getTime());
					r.setData("timezone", i/60/60);
					r.setData("utc", i/60);
					comp.getNikitaComponent().onResult( param,  "LOCALDATE", r );					
				}
			});
	    }else if (code.equals("datetime")) {
	    	comp.getNikitaComponent().runOnActionThread(new Runnable() {
				public void run() {
					String param = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());
					Nset r = Nset.newObject();
					r.setData("datetime", Utility.Now());
					comp.getNikitaComponent().onResult( param,  "DATETIME", r );					
				}
			});
	    	
        }
	   return true;
	}
    
}

