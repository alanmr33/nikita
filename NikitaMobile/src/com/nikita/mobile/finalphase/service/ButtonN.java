package com.nikita.mobile.finalphase.service;

import com.nikita.mobile.finalphase.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
 

public class ButtonN  extends Button {
    protected int width;
    protected View icon;
   

    // Cached to prevent allocation during onLayout
    Rect bounds;
 

    private enum DrawablePositions {
        NONE,
        LEFT,
        RIGHT
    }

    public ButtonN(Context context) {
        super(context);
        bounds = new Rect();
    }

    public ButtonN(Context context, AttributeSet attrs) {
        super(context, attrs);
        bounds = new Rect();
        applyAttributes(attrs);
    }

    public ButtonN(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        bounds = new Rect();
        applyAttributes(attrs);
    }

    protected void applyAttributes(AttributeSet attrs) {
        // Slight contortion to prevent allocating in onLayout
        if (null == bounds) {
            bounds = new Rect();
        }

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.IconButton);
        int paddingId = typedArray.getDimensionPixelSize(R.styleable.IconButton_iconPadding, 0);
   
        typedArray.recycle();
    }
    
    public void setIcon(View icon, int width) {
    	this.width=width;
    	this.icon=icon;
     }
 
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        Paint textPaint = getPaint();
        String text = getText().toString();
        textPaint.getTextBounds(text, 0, text.length(), bounds);

        int textWidth = bounds.width();
        int contentWidth = width + textWidth;

        int contentLeft = (int) ((getWidth() / 2.0) - (contentWidth / 2.0));        
        if (icon!=null) {
        	((MarginLayoutParams) icon.getLayoutParams()).leftMargin=contentLeft;
		}
    }
 
}