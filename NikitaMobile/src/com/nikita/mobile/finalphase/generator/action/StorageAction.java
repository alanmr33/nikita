/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.nikita.mobile.finalphase.generator.action;


import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.storage.NikitaStorage;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rkrzmail
 */
public class StorageAction implements IAction{
    @Override
    public boolean OnAction(Component comp, Nset currdata) {
        String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString()); //storage 
        String param1 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());//Storage
        String param2 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());//Name
        String param3 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString());//Mode
        String param4 = (currdata.getData("args").getData("param4").toString());//Data/Result
        String param5 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param5").toString());//Result file.exist;
        String param6 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param6").toString());//Target Storage
        String param7 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param7").toString());//Target Name
        
        
        if (code.equals("storage")) {
              
          
             
            if (param3.equals("exist")||param3.equals("")) {
                boolean result = NikitaStorage.getStorage(param1).existStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("read")) {
                String result = NikitaStorage.getStorage(param1).readStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result);
            }else if (param3.equals("readline")) {
                Nset result = NikitaStorage.getStorage(param1).readLineStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result);
            }else if (param3.equals("readxls")) {
                Nikitaset result = NikitaStorage.getStorage(param1).readXlsStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result);
            }else if (param3.equals("write")) {
                boolean result = NikitaStorage.getStorage(param1).writeStorage(param2, comp.getNikitaComponent().getVirtualString( param4) );
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("append")) {
                boolean result = NikitaStorage.getStorage(param1).appendStorage(param2,  comp.getNikitaComponent().getVirtualString( param4));
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("mkdir")) {                
                boolean result = NikitaStorage.getStorage(param1).makeStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("create")) {
                boolean result = NikitaStorage.getStorage(param1).createStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("copy")) {
                boolean result = NikitaStorage.getStorage(param1).copyStorage(param2, param6, param7);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("cut")) {
                boolean result = NikitaStorage.getStorage(param1).cutStorage(param2, param6, param7);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }else if (param3.equals("delete")) {
                boolean result = NikitaStorage.getStorage(param1).deleteStorage(param2);
                comp.getNikitaComponent().setVirtual(param4, result?"true":"false");
            }
        }else if (code.equals("copy")) {
            
        }
        
        
        return true;
    }
 
    
}
