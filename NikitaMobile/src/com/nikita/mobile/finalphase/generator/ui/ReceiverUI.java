package com.nikita.mobile.finalphase.generator.ui;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NikitaControler;

import android.view.View;
import android.widget.TextView;

public class ReceiverUI extends Component{

	public ReceiverUI(NikitaControler form) {
		super(form);
	}
	private TextView view;
 
	protected boolean defaultshowBusy() {
		return false;
	}
	public View onCreate(NikitaControler form) {
		view = new TextView(form.getActivity());
		view.setVisibility(View.GONE);
		
		return view;
	} 
}
