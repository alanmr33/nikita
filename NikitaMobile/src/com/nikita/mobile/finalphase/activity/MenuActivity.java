package com.nikita.mobile.finalphase.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.google.android.gms.drive.internal.u;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends AndroidActivity{
	private Nikitaset nikitaset;
	private Nset notifCount = Nset.newObject();
	private ListView listView;
	private GridView gridView;
	private boolean userGrid = false;
	
	private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
        	//Log.i("MenuActivity-BroadcastReceiver-onReceive", "BroadcastReceiver");
        	refreshCount();
            //Toast.makeText(getApplicationContext(), intent.getStringExtra("data")!=null?intent.getStringExtra("data"):"", Toast.LENGTH_SHORT).show();
        }
    };
    
    protected void onStart() {
		 IntentFilter filter = new IntentFilter();
	     filter.addAction("com.nikita.activity");
	     filter.addAction("com.nikita.generator");	 
	     filter.addAction("com.nikita.generator.service");	
	    
	     registerReceiver(receiver, filter);
         
    	super.onStart();    	
    };
    
    protected void onStop() {
    	unregisterReceiver(receiver);
    	super.onStop();
    }
    
   
    
    private String getDefaultValue(Nset params, Nset def, String cdoname, String key){
    	if (params.containsKey(cdoname)&& params.getData(cdoname).containsKey(key)) {
    		return params.getData(cdoname).getData(key).toString();
		}else if (def.containsKey(cdoname)&& def.getData(cdoname).containsKey(key)) {
			return def.getData(cdoname).getData(key).toString();
		}
    	return "";
    }
    
	protected void onCreate(Bundle savedInstanceState) {		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultorder);		
		
		((TextView)findViewById(R.id.txtTitle)).setText( "Menu" );
		findViewById(R.id.textView1).setVisibility(View.GONE);
		findViewById(R.id.frmCustom).setVisibility(View.GONE);
		findViewById(R.id.frmCustomFind).setVisibility(View.GONE);
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		
		gridView = (GridView) findViewById(R.id.grdView);
		listView = (ListView) findViewById(R.id.lstView);
		
		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));	
		
		String act = Generator.getNikitaParameters().getData("INIT-MENU-ACTIVITY").toString();
		nSetting = Nset.readJSON(act);
		
		UtilityAndroid.setButtonActivity(findViewById(R.id.lnrCnt), nSetting , new View.OnClickListener() {
			public void onClick(View v) {
				String tag = String.valueOf(v.getTag());
				Nset n = Nset.readJSON(tag);				
				
				Generator.setVirtual("@+CORE-MENUID", "MENU");
				Generator.setVirtual("@+CORE-STATUS", "0");										
				Generator.setVirtual("@+CORE-FORMID", n.getData("id").toString() );
				
				String name = n.getData("form").toString() ;
				
				Generator.clearNikitaForms();
				Generator.startNikitaForm(MenuActivity.this, name );
			}
		} );		
		
		if (nSetting.containsKey("gridview")) {
			Nset v = nSetting.getData("gridview");
			if (v.getData("visible").toString().equalsIgnoreCase("true")) {
				userGrid = true;
			}
			if (v.containsKey("verticalspacing")) {
				gridView.setVerticalSpacing(Utility.convertPixel(this, v.getData("verticalspacing").toString()));
			}
			if (v.containsKey("horizontalspacing")) {
				gridView.setHorizontalSpacing(Utility.convertPixel(this, v.getData("horizontalspacing").toString()));
			}
			if (v.containsKey("margin-top")) {
				gridView.setPadding(gridView.getPaddingLeft(), Utility.convertPixel(this, v.getData("margin-top").toString()) , gridView.getPaddingRight(), gridView.getPaddingBottom());
			}			
			if (v.containsKey("numcolumns")) {
				gridView.setNumColumns(v.getData("numcolumns").toInteger());
			}
		}
		 
		
		if (nSetting.containsKey("TITLE")) {
			if (nSetting.getData("TITLE").isNsetArray()) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < nSetting.getData("TITLE").getSize(); i++) {
					Nset v = Nset.readJSON( com.nikita.mobile.finalphase.utility.Utility.getSetting(AppNikita.getInstance(), "N-PROFILE", "") );
					if ( nSetting.getData("TITLE").getData(i).toString().startsWith("@+SPACE")) {
						sb.append(" ");
					}else if ( nSetting.getData("TITLE").getData(i).toString().startsWith("@+SETTING-N-PROFILE-")) {
						try {
							String s = nSetting.getData("TITLE").getData(i).toString().substring(20);
							sb.append(v.getData(s).toString());
						} catch (Exception e) { }
					}else if ( nSetting.getData("TITLE").getData(i).toString().startsWith("@+SETTING-")) {
						try {
							String s = nSetting.getData("TITLE").getData(i).toString().substring(10);
							sb.append(v.getData(s).toString());
						} catch (Exception e) { }
					}else{
						sb.append(nSetting.getData("TITLE").getData(i).toString());
					}
					
				}
				((TextView)findViewById(R.id.txtTitle)).setText( sb.toString() );
			}else{
				((TextView)findViewById(R.id.txtTitle)).setText( nSetting.getData("TITLE").toString() );
			}
			
		}
	
		
		findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {				 
				onBackPressed();
			}
		});
		
		refreshCount();
		
		Messagebox.showBusyDialog(MenuActivity.this, new Runnable() {					
			public void run() {
				String defmenuacc = "  {'order':{'name':'order','text':'Order','icon':'/drawable/n_order','badge':''},"
									+ " 'draft':{'name':'draft','text':'Data Draft','icon':'/drawable/n_order'},"
									+ " 'pending':{'name':'pending','text':'Data Pending','icon':'/drawable/n_order'},"
									+ " 'finish':{'name':'finish','text':'Data Pending','icon':'/drawable/n_order'},"
									+ " 'sent':{'name':'sent','text':'Data Sent','icon':'/drawable/n_order'},"
									+ " 'closebatch':{'name':'closebatch','text':'Close Batch','icon':'/drawable/n_closebatch'},"
									+ " 'setting':{'name':'setting','text':'Setting','icon':'/drawable/n_setting'},"
									+ " 'profile':{'name':'profile','text':'Profile','icon':'/drawable/n_setting'},"
									+ " 'change':{'name':'change','text':'Change Password','icon':'/drawable/n_order'},"
									+ " 'logout':{'name':'logout','text':'Logout','icon':'/drawable/n_logout'}}";
				Nset ndefmenuacc = Nset.readJSON(defmenuacc, true);
				Nset nparameters = null;
				nparameters = Nset.readJSON( Generator.getNikitaParameterValue("INIT-MENU-ACCESS", "") );
												
				Vector<String> header = new Vector<String>();
			    
		        header.addElement("name");
		        header.addElement("text");
		        header.addElement("icon");
		        header.addElement("form");
		        
		        Vector<Vector<String>> 
		        data = new Vector<Vector<String>>();
		        
				String maccess = Utility.getSetting(getApplicationContext(), "N-ACCESS",  "" );;
				Nset nacc = Nset.readJSON(maccess);
				
				if (nacc.getSize()>=1) {					 
					for (int i = 0; i < nacc.getSize(); i++) {
						Vector<String>
					        row = new Vector<String>();
							String code = nacc.getData(i).toString();
					        row.addElement(code);
					        row.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "text"));//text
					        row.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "icon"));//icon
					        row.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "badge"));//badge
					        row.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "form"));//form
				        data.addElement(row);
					}
				}else{
					String def = "['order','draft','pending','sent','setting','logout']";
					Nset ndef = Nset.readJSON(def, true) ;
					for (int i = 0; i < ndef.getSize(); i++) {
						Vector<String>
				        acc = new Vector<String>();
						String code = ndef.getData(i).toString();
						acc.addElement(code);
						acc.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "text"));//text
						acc.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "icon"));//icon
						acc.addElement(getDefaultValue(nparameters, ndefmenuacc, code, "badge"));//badge
						acc.addElement("");//form
						data.addElement(acc);
					}					 
				}
				
			    
				nikitaset = new Nikitaset(header, data);
				
				
				
				
				
			}
		}, new Runnable() {
			public void run() {
				
				if (userGrid){ 					
					gridView.setVisibility(View.VISIBLE);
					listView.setVisibility(View.GONE);
				} else {					
					gridView.setVisibility(View.GONE);
					listView.setVisibility(View.VISIBLE);
				}
				
				 
				ArrayAdapter<Vector<String>>  arrayAdapter = new ArrayAdapter<Vector<String>> (MenuActivity.this, (userGrid?R.layout.defaultorderitemgrid:R.layout.defaultmenuitem), nikitaset.getDataAllVector()){
 					public View getView(final int position, final View convertView, final ViewGroup parent) {
						final View v = Utility.getInflater(MenuActivity.this,  (userGrid?R.layout.defaultorderitemgrid:R.layout.defaultmenuitem));
						
						if (nikitaset.getText(position, 2).startsWith("/drawable/")) {					
							((ImageView)v.findViewById(R.id.imageView1)).setImageResource(UtilityAndroid.getDrawable(nikitaset.getText(position, 2)));
						}else if (nikitaset.getText(position, 2).startsWith("/")) {
							((ImageView)v.findViewById(R.id.imageView1)).setImageBitmap(BitmapFactory.decodeFile(nikitaset.getText(position, 2)));
						}else if (nikitaset.getText(position, 2).contains(":")) {
							
						}else{
							((ImageView)v.findViewById(R.id.imageView1)).setImageResource(Utility.getInt(nikitaset.getText(position, 2)));
						}				
		 
						((TextView)v.findViewById(R.id.txtView)).setText(nikitaset.getText(position, 1));
						if (nSetting.containsKey("badge") && nSetting.getData("badge").getData("visible").toString().equalsIgnoreCase("true") &&  notifCount.getData(nikitaset.getText(position, 0)).toInteger()>=1) {
							v.findViewById(R.id.frmBagde).setVisibility(View.VISIBLE);
							((TextView)v.findViewById(R.id.txtBagde)).setText(  notifCount.getData(nikitaset.getText(position, 0)).toString() );
						}else{
							v.findViewById(R.id.frmBagde).setVisibility(View.GONE);
						}				
						return v;
					}
				};
				
				
				if (userGrid) {
					gridView.setAdapter(arrayAdapter);
				}else{
					listView.setAdapter(arrayAdapter);
				}
				
				
				AdapterView.OnItemClickListener onItemClickListener =  new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView arg0, View view,final int pos, long arg3) {
						String cd = nikitaset.getText(pos, 0);
						if (cd.equals("order")||cd.equals("orderx")) {
							Intent intent = new Intent(MenuActivity.this, OrderCurrentActivity.class);	
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);
						}else if (cd.equals("draft")||cd.equals("draftx")) {
							Intent intent = new Intent(MenuActivity.this, OrderDraftActivity.class);
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);	
						}else if (cd.equals("pending")||cd.equals("pendingx")) {
							Intent intent = new Intent(MenuActivity.this, OrderPendingActivity.class);	
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);	
						}else if (cd.equals("finish")||cd.equals("finishx")) {//draft+pending
							Intent intent = new Intent(MenuActivity.this, OrderFinishActivity.class);
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);	
						}else if (cd.equals("sent")||cd.equals("sentx")) {							
							Intent intent = new Intent(MenuActivity.this, OrderSentActivity.class);	
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);
						}else if (cd.equals("setting")||cd.equals("profile")) {
							Intent intent = new Intent(MenuActivity.this, SettingActivity.class);
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);
						}else if (cd.equals("listdata")||cd.equals("datalist")) {
							Intent intent = new Intent(MenuActivity.this, ListDataActivity.class);
							intent.putExtra("code", nikitaset.getText(pos, 0));
							intent.putExtra("title", nikitaset.getText(pos, 1));
							intent.putExtra("icon", nikitaset.getText(pos, 2));
							startActivityForResult(intent, 1);
						}else if (cd.equals("change")||cd.equals("changepassword")) {
							Generator.setVirtual("@+CORE-MENUID", "MENU");
							Generator.setVirtual("@+CORE-STATUS", "0");										
							Generator.setVirtual("@+CORE-FORMID", nikitaset.getText(pos, 0));
							Generator.setVirtual("@+CORE-ICONID", nikitaset.getText(pos, 2));
							String name = Generator.getNikitaParameters().getData("INIT-MENU-ACTIVITY-CHANGE").toString();
								
							Generator.setVirtual("@+CORE-MENUCODE", cd);
							if ( findViewById(R.id.txtTitle)instanceof TextView ) {							
								Generator.setVirtual("@+CORE-MENUTITLE", ((TextView)findViewById(R.id.txtTitle)).getText().toString());
							}else{
								Generator.setVirtual("@+CORE-MENUTITLE", "");
							}	
							
							Generator.clearNikitaForms();
							Generator.startNikitaForm(MenuActivity.this, name.equals("")?nikitaset.getText(pos, 0):name);
						}else if (cd.equals("logout")||cd.equals("signout")) {
							showDialogLogout();
						}else if (cd.equals("closebatch")) {
							 if ( Utility.getInt( NikitaConnection.getConnection(NikitaConnection.MOBILE).Query("SELECT COUNT(status) FROM mobileactivity WHERE status=0  OR status=1 ").getText(0, 0)  )>=1) {
								 showDialog("Information", "Please resend all pending/draft data", "Done", "", "", "", Nset.newObject());
							 }else{
								 showDialog("Logout", "Are you sure to Closebatch ?", "No", "Yes", "", "closebatch", Nset.newObject());
							 }
						}else{
							
							Generator.setVirtual("@+CORE-MENUID", "MENU");
							Generator.setVirtual("@+CORE-STATUS", "0");		
							Generator.setVirtual("@+CORE-FORMID", nikitaset.getText(pos, 0));	
							Generator.setVirtual("@+CORE-ICONID", nikitaset.getText(pos, 2));
							String fname =  nikitaset.getText(pos, 4).equals("")? nikitaset.getText(pos, 0): nikitaset.getText(pos, 4);
							
							Generator.setVirtual("@+CORE-MENUCODE", cd);
							if ( findViewById(R.id.txtTitle)instanceof TextView ) {							
								Generator.setVirtual("@+CORE-MENUTITLE", ((TextView)findViewById(R.id.txtTitle)).getText().toString());
							}else{
								Generator.setVirtual("@+CORE-MENUTITLE", "");
							}
							
							Generator.clearNikitaForms();
							Generator.startNikitaForm(MenuActivity.this, fname);//form
						}
					}
				};
				
				if (userGrid) {
					gridView.setOnItemClickListener(onItemClickListener);
				}else{
					listView.setOnItemClickListener(onItemClickListener);
				}
				
				
			}
		});			
		
		
		refreshCount();
	}
    
	private void showDialogLogout(){
		showDialog("Logout", "Are you sure to Logout ?", "No", "Yes", "", "logout", Nset.newObject());
	}
	private void showDialogExit(){
		showDialog("Exit", "Are you sure to Exit", "No", "Yes", "", "exit", Nset.newObject());
	}
	private void execLogout(){

		Generator.clearNikitaForms();
		//fitur logout
		Utility.setSetting(getApplicationContext(), "TIME-LOGOUT", Utility.Now());
		insertHistoryLogout();
		Utility.setSetting(getApplicationContext(), "N-LOGON", "");
		
		//stop running service alarm scheduler
		AlarmReceiver.cancelAlarmBroadcast(getApplicationContext());
		
		Intent intent = new Intent(MenuActivity.this, LoginActivity.class);	
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}
	private void insertHistoryLogout() {
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		// insert activity 'logout' to table
		//Nikitaset nset = ncmobile.Query("SELECT name FROM sqlite_master WHERE type='table' AND name='historyactivity'");
		//check table in database
		//if (nset.getRows() == 0) {
		//	ncmobile.Query("CREATE TABLE historyactivity (id INTEGER NOT NULL," + "activity TEXT NOT NULL, " + "datetime TEXT NOT NULL, " + "status TEXT NOT NULL,"
		//			+ "PRIMARY KEY (id) );");
		//}

		String datetime = Utility.getSetting(getApplicationContext(), "TIME-LOGOUT", "");
		//if (datetime.equals(null) || datetime == "" || datetime.isEmpty()) {
			ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) VALUES ('logout','" + Utility.Now() + "','0')");
			Utility.setSetting(getApplicationContext(), "TIME-LOGOUT", Utility.Now());
		//} else {
		//	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//	Calendar now = Calendar.getInstance();
		//	Date lastlogin = new Date();
		//	try {
		//		lastlogin = dateformat.parse(datetime);
		//	} catch (ParseException e) {
		//}

			// if date and month is same
			//if (lastlogin.getDate() == now.getTime().getDate() && (lastlogin.getMonth() + 1) == (now.getTime().getMonth() + 1)) {
				// not insert
			//} else {
				//insert logout activity; data from shared pref
			//	if (Utility.getSetting(getApplicationContext(), "TIME-LOGOUT", "").isEmpty()) {
			//		String dateLogout = (lastlogin.getYear() + 1900) + "-" + (lastlogin.getMonth() + 1) + "-" + lastlogin.getDate() + " 23:59:00";
			//		ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) " + "VALUES ('logout','" + dateLogout + "','0')");
			//	}
			//	ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) VALUES ('login','" + Utility.Now() + "','0')");
			//	Utility.setSetting(getApplicationContext(), "TIME-LOGIN", Utility.Now());
			//}
		//}

	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {		 
		//if (resultCode==1) {
			refreshCount();
		//}
	}
	Handler handler = new Handler();
	
	private void refreshCount(){
		AppNikita.getInstance().getActionHandler().post(new Runnable() {
			public void run() {
				//mobileactivity
				NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
				notifCount.setData("order", 	 nc.Query("SELECT COUNT(*) FROM mobileorder ").getText(0, 0)) ;
				notifCount.setData("draft", 	 nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 0 ").getText(0, 0)) ;
				notifCount.setData("pending",	 nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 1 ").getText(0, 0)) ;
				notifCount.setData("sent", 		 nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 2 ").getText(0, 0)) ;
				
				notifCount.setData("finish",	(notifCount.getData("pending").toInteger()+notifCount.getData("draft").toInteger())+"" ) ;

				Log.i("refreshCount0",    nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 0 ").getText(0, 0));
				Log.i("refreshCount1",    nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 1 ").getText(0, 0));
				Log.i("refreshCount2",    nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 2 ").getText(0, 0));
				
				if (MenuActivity.this!=null) {
					MenuActivity.this.runOnUiThread(new Runnable() {
						public void run() {							
							try {
								if (listView!=null && listView.getAdapter() instanceof ArrayAdapter) {
									((ArrayAdapter)listView.getAdapter() ).notifyDataSetChanged();
									((ArrayAdapter)listView.getAdapter() ).notifyDataSetInvalidated();;
								}
							} catch (Exception e) { }
						}
					});
				}				
			}
		});
		 
		
		//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-ORDER-ACTIVITY-INFO",   nc.Query("SELECT COUNT(*) FROM mobileorder ").getText(0, 0)  )  ;
	  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-DRAFT-ACTIVITY-INFO",   nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 0 ").getText(0, 0) )  ;
	  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-PENDING-ACTIVITY-INFO", nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 1 ").getText(0, 0) )  ;
	  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-SENT-ACTIVITY-INFO",    nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 2 ").getText(0, 0) )  ;
	
		
		
	}
	public Nset nclosebatch = Nset.newObject();
	private void closebatch(){
		 Messagebox.showBusyDialog(MenuActivity.this, new Runnable() {					
			 	
			 public void run() {
				    Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
					hashtable.put("nauth" , Utility.getSetting(getApplicationContext(), "N-AUTH", "") );
					hashtable.put("batch" , Utility.getSetting(getApplicationContext(), "N-BATCH", "") );
					hashtable.put("userid" , Utility.getSetting(getApplicationContext(), "N-USER", "") );
					
					 
					nclosebatch = Nset.readJSON(  NikitaInternet.getString( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mclosebatch" , hashtable ))  );					
					 
				}
			}, new Runnable() {
				public void run() {
					
					if (nclosebatch.getData("error").toString().equals("") && nclosebatch.getData("status").toString().equals("OK")) {
						execLogout();	
					}else{
						showDialog("Closebatch Gagal", nclosebatch.getData("error").toString(), "Done", "",  "", "", Nset.newObject());
					}					
				}
			});		
	}
	private void populate(){
		 Messagebox.showBusyDialog(MenuActivity.this, new Runnable() {					
				public void run() {
								
				}
			}, new Runnable() {
				public void run() {
					 
				}
			});		
	}
	
	private AlertDialog alertDialog = null; 
	protected void hideMessage(){
		if (alertDialog!=null) {
			if (alertDialog.isShowing()) {
				alertDialog.dismiss();
			}			
		}
	}
	Nset nSetting = Nset.newObject();
	public void onBackPressed() {		
		Nset n = nSetting;
		if (n.containsKey("onback")) {
			String ob = n.getData("onback").toString();
			if (ob.equalsIgnoreCase("logout")) {
				showDialogLogout();
			}else if (ob.equalsIgnoreCase("autologout")) {
				execLogout();
			}else if (ob.equalsIgnoreCase("exit")) {
				showDialogExit();
			}else if (ob.equalsIgnoreCase("false")) {	
				//freeze
			}else{
				super.onBackPressed();
			}
		}else{
			super.onBackPressed();
		}		
	}
	
	public void showDialog(String title, String msg, String button1, String button2, String button3, String requestcode, Nset result){
		hideMessage();
		
		final Nset data = Nset.newObject();
		data.setData("title", title);
		data.setData("msg", msg);
		data.setData("button1", button1);
		data.setData("button2", button2);
		data.setData("button3", button3);
		data.setData("requestcode", requestcode);
		data.setData("result", result);
		
		Builder dlg = new AlertDialog.Builder(this);
		dlg.setTitle("");
		if (!data.getData("button1").toString().equals("")) {
			dlg.setPositiveButton(data.getData("button1").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if ( data.getData("logout").toString().equals("showdialog") ) {
						
					}
				}
			});
		}
		if (!data.getData("button2").toString().equals("")) {
			dlg.setNeutralButton(data.getData("button2").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if ( data.getData("requestcode").toString().equals("logout") ) {
						execLogout();
					}else if ( data.getData("requestcode").toString().equals("closebatch") ) {
						closebatch();
					}else if ( data.getData("requestcode").toString().equals("exit") ) {
						finish();
					}
				}
			});
		}
		if (!data.getData("button3").toString().equals("")) {
			dlg.setNegativeButton(data.getData("button3").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					 
				}
			});
		}			
		dlg.setCancelable(false);
		dlg.setTitle(data.getData("title").toString());
		dlg.setMessage(data.getData("msg").toString());
		alertDialog=dlg.create();
		alertDialog.show();
	}
	
	  
}
