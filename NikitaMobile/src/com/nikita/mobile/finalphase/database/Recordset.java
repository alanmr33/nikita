package com.nikita.mobile.finalphase.database;

import java.util.Vector;

public interface Recordset {
	public Vector<String> getAllHeaderVector();
	public Vector<Vector<String>> getAllDataVector();
	public Vector<String> getRecordFromHeader(String colname);
	public int getRows();
	public int getCols();
	
	public String getText(int row, int col);
	public String getText(int row, String colname);
	public String getHeader(int col);
}
