package com.nikita.mobile.finalphase.generator.ui;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.style.SuperscriptSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.AdapterView.OnItemSelectedListener;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.Global;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class ComboboxUI extends Component{

 
	public ComboboxUI(NikitaControler form) {
		super(form);
	}
	private int selectedindex = -1;
	private boolean selecteddummy = false;
	private View view;
	private android.widget.Spinner spin;
	private TextView lblText;
	private Vector<String> list = new Vector<String>();
	private Vector<String> code = new Vector<String>();
 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compcombobox);
		spin = (android.widget.Spinner) view.findViewById(R.id.combobox);
		lblText = (TextView) view.findViewById(R.id.lblLabel);
		brunFirst = false;
		bCreated = false;
		populatData(); 
		
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		 
		
		
		refreshViewOnUI();
		refreshStyleOnUI();
		fillDataOnUI();
		
		
		bCreated=true;
		return view;
	}
	 
	public void setData(Nset data) { 
		super.setData(data);
		if (spin!=null && spin.getAdapter() instanceof ArrayAdapter) {
			populatData();
			runOnUI(new Runnable() {
				public void run() {
					setActivateEvent(false);
				 	((ArrayAdapter)spin.getAdapter() ).notifyDataSetChanged();	
				 	setActivateEvent(true);
				}
			});
		}
	} 
	  
	private void populatData(){
		selecteddummy = false;
		code.removeAllElements();
		list.removeAllElements();
				
		if (getStyle()!=null && getStyle().getInternalStyle().containsKey("n-list-default")) {
			code.add("");
			list.add(getStyle().getInternalStyle().getData("n-list-default").toString());
		}else if (Generator.getNikitaParameters().containsKey("INIT-COMPONENT-COMBOBOX-DEFAULT")) {
			code.add("");
			list.add(Generator.getNikitaParameters().getData("INIT-COMPONENT-COMBOBOX-DEFAULT").toString());
		}
		//if (Global.getText("generator.ui.dropdown.default").length()>=1) {
			//list.add(0,Global.getText("generator.ui.dropdown.default"));
		//}
		for (int i = 0; i < getData().getArraySize(); i++) {
			if(getData().getData(i).getData("id").toString().length()>=1){
	        	//checkBox.setTag(getData().getData(i).getData("id").toString());
				code.add(getData().getData(i).getData("id").toString());
	        	list.add(getData().getData(i).getData("text").toString());
	        }else if(getData().getData(i).getArraySize()>=2){
	            //checkBox.setTag(getData().getData(i).getData(0).toString());
	        	code.add(getData().getData(i).getData(0).toString());
	        	list.add(getData().getData(i).getData(1).toString());
	        }else if(getData().getData(i).getArraySize()>=1){
	        	// checkBox.setTag(getData().getData(i).getData(0).toString());
	        	code.add(getData().getData(i).getData(0).toString());
	        	list.add(getData().getData(i).getData(0).toString());
	        }else{
	        	//checkBox.setTag(getData().getData(i).toString());
	        	code.add(getData().getData(i).toString());
	        	list.add(getData().getData(i).toString());
	        }  
		}
		
	}
	 
	private void fillDataOnUI(){
		if (view!=null) {
			
			selectedindex = -1;		
			//ArrayAdapter<String> adapter = new ArrayAdapter(getNikitaComponent().getActivity(), android.R.layout.simple_spinner_item, list);//simple_list_item_1
			//adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//simple_spinner_dropdown_item
			//spin.setAdapter(adapter);			
			spin.setAdapter(new ItemArray(getNikitaComponent().getActivity(), android.R.layout.simple_spinner_dropdown_item, list));
			
			setText(super.getText());
			 
			spin.setOnItemSelectedListener(new OnItemSelectedListener() {
				public void onItemSelected(AdapterView arg0, View arg1, int position, long arg3) {
					if ( selectedindex != position && selectedindex>=0 ) {//!getText().equals(getSuperText())
						selectedindex = position;
						cleardummy();						
						onSaveState();
						getNikitaComponent().runOnActionThread(new Runnable() {
							public void run() {
								ComboboxUI.this.runRoute();
							}
						});	
					}
					selectedindex = position;
					/*
					if (brunFirst && bCreated) {
						getNikitaComponent().runOnActionThread(new Runnable() {
							public void run() {
								ComboboxUI.this.runRoute();
							}
						});	
					}
					if (bCreated) {
						brunFirst=true;
					}	
					*/				
				}
				public void onNothingSelected(AdapterView<?> arg0) { }
			});
			
			
		}	
	} 
	private boolean brunFirst = false;
	private boolean bCreated = false;
	public View getView() {		 
		return view;
	}
		
	private String getSuperText(){
		return super.getText();
	}
	private void cleardummy(){
		if (selecteddummy) {
			selecteddummy=false;
			list.removeElementAt(0);
			code.removeElementAt(0);
			((ArrayAdapter)spin.getAdapter()).notifyDataSetChanged();
		}		
	}
	private void createdummy(){
		if (!selecteddummy) {
			selecteddummy = true ;
			list.insertElementAt(getSuperText(), 0);
			code.insertElementAt(getSuperText(), 0);
			((ArrayAdapter)spin.getAdapter()).notifyDataSetChanged();
		}		
	}
	public void setText(String text) {
		super.setText(text);
		
		if (spin != null) {	
			runOnUI(new Runnable() {				
				public void run() {
					/*
					if (spin.getSelectedItemPosition()>=0 && code.size() > spin.getSelectedItemPosition()) {
						if(code.get(spin.getSelectedItemPosition()).equals( getSuperText()  ))
							 return;
					}
					*/
					for (int i = 0; i < code.size(); i++) {
						if(code.get(i).equals( getSuperText()  )) {			
							selectedindex = i ;
							cleardummy();
							spin.setSelection(i);							
							return;
						}
					}					
					//not found[0]
					selectedindex = -1;
					if (!getSuperText().trim().equals("")) {	
						cleardummy();
						createdummy();
					}
				}
			});			
		}				
 	}

	 
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	public void onChangeViewUi() {	 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	 
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.combobox);
		if (v instanceof Spinner) {
			//((Spinner)v).getSelectedItem();
		}
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
		
		if (spin!=null) {
			spin.setEnabled(isEnable());
		}
		
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
		Nset style = Nset.newObject();
		if (getStyle()!=null) {
			style = getStyle().getInternalObject().getData("style");
		}
		
		String val =  style.getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
		if (val.equals("true")) {
			view.findViewById(R.id.combobox).setBackgroundResource(R.drawable.nfedittexterror);
		}else{
			view.findViewById(R.id.combobox).setBackgroundResource(R.drawable.nfedittext);
		}
	}
	public String getText() {
		if (spin != null) {
			try {
				return code.get(selectedindex);	//spin.getSelectedItemPosition()		 
			} catch (Exception e) {	} 
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
	}
	
	
	private class ItemArray extends ArrayAdapter{

		public ItemArray(Context context, int textViewResourceId, List objects) {
			super(context, textViewResourceId, objects);
			 
		}
 
		@Override
		public View getDropDownView(int position, View convertView, 	ViewGroup parent) {
			View v =  super.getDropDownView(position, convertView, parent);
			//v =new TextView(getContext());
			//((TextView)v).setText(""+position);
			if (position==0 && selecteddummy) {
				v.setVisibility(View.GONE);
			}else{
				v.setVisibility(View.VISIBLE);
			}
			return v;
		}
		
	}
	
}
