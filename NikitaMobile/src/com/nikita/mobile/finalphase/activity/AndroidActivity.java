package com.nikita.mobile.finalphase.activity;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.MotionEvent;

public class AndroidActivity extends Activity{
	public boolean dispatchKeyEvent(android.view.KeyEvent event) {
    	AppNikita.setUserInteraction();
    	return super.dispatchKeyEvent(event);
    }
    public boolean dispatchTouchEvent(MotionEvent ev) {
    	AppNikita.setUserInteraction();
    	return super.dispatchTouchEvent(ev);
    }
    
    public String getCode(){
		if (getIntent()!=null && getIntent().getStringExtra("code")!=null) {
			return getIntent().getStringExtra("code");
		}
		return "";
	}
	String resultdeleteOnline = "";
	private void deleteOnline(Context context, final String url, final String jsonarg){
		 Messagebox.showBusyDialog(context, new Runnable() {					
			 public void run() {
					Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
					hashtable.put("activitydata", jsonarg);
					resultdeleteOnline =   NikitaInternet.getString( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/"+url , hashtable ))   ;					
				}
			}, new Runnable() {
				public void run() {
					Nset v = Nset.readJSON(resultdeleteOnline);				
					if (v.getData("error").toString().equals("") && v.getData("status").toString().equals("OK")) {
						if (v.containsKey("action") && v.getData("action").toString().equalsIgnoreCase("update")) {
							NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
							nc.Query("UPDATE mobileactivity SET status=? WHERE activityid=?",v.getData("activitystatus").toString() , v.getData("activityid").toString());
						}else if (v.containsKey("action") && v.getData("action").toString().equalsIgnoreCase("delete")) {
							NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
							nc.Query("DELETE FROM mobileactivity WHERE activityid=?", v.getData("activityid").toString());
						}
						reloadActivityData();
					}else{
						showDialog("Closebatch Gagal", v.getData("error").toString(), "Done", "",  "", "", Nset.newObject());
					}					
				}
			});		
	}
	private AlertDialog alertDialog = null; 
	protected void hideMessage(){
		if (alertDialog!=null) {
			if (alertDialog.isShowing()) {
				alertDialog.dismiss();
			}			
		}
	}
	public void reloadActivityData(){		
		
	}
	public void sortMobileActivity(Nikitaset ns,final String fcompname){		
		Vector<String>[] v = ns.getDataAllVector().toArray(new Vector[ns.getRows()]);
		final int body = ns.getDataAllHeader().indexOf("body");
		Arrays.sort(v, new Comparator<Vector<String>>(){
		    public int compare(Vector<String> o1, Vector<String> o2) {
		    	try {
					Nset n1 = Nset.readJSON( o1.elementAt(body));
					Nset n2 = Nset.readJSON( o2.elementAt(body));
					boolean orderdesc = fcompname.toLowerCase().endsWith(":desc");//asc def
					String 	fname = fcompname.contains(".")? fcompname.substring(0, fcompname.indexOf("."))  :"";
					String 	fcomp = fcompname.contains(".")? fcompname.substring(fcompname.indexOf(".")+1)  :"";
							fcomp = fcomp.contains(":")? fcomp.substring(0, fcomp.indexOf(":"))  :"";
					if (!orderdesc) {
						//asc
						if (n1.getData("fname").getData("fcomp").isNumber()) {
							return Double.compare(n1.getData("fname").getData("fcomp").toNumber().doubleValue() ,  n2.getData("fname").getData("fcomp").toNumber().doubleValue());
						}else{
							return n1.getData("fname").getData("fcomp").toString().compareTo(n2.getData("fname").getData("fcomp").toString());
						}
					}else{
						//desc
						if (n1.getData("fname").getData("fcomp").isNumber()) {
							return -1*Double.compare(n1.getData("fname").getData("fcomp").toNumber().doubleValue() ,  n2.getData("fname").getData("fcomp").toNumber().doubleValue());
						}else{
							return -1*n1.getData("fname").getData("fcomp").toString().compareTo(n2.getData("fname").getData("fcomp").toString());
						}
					}
				} catch (Exception e) { }
		        return 0; 
		    }
		});		
	
		ns = new Nikitaset(ns.getDataAllHeader(), new Vector<Vector<String>>(Arrays.asList(v)), ns.getError(), ns.getInfo());
	}
	public void listDeleteAction(Nset n, Nset nSetting, int position, Nikitaset nikitaset){		 
		 n.setData("activityid", nikitaset.getText(position, "activityid"));
		 n.setData("orderid", nikitaset.getText(position, "orderid"));
		 n.setData("link", n.getData("list").getData("link").toString());
		 n.setData("list", "");
		 
		 String title = "Delete";
		 /*String message = "Are you sure delete ?";
		 String button1 = "No";
		 String button2 = "Yes";*/
		 String message = "Apakah Anda ingin menghapus data ini?";
		 String button1 = "Tidak";
		 String button2 = "Ya";
		 if (nSetting.getData("listdelete").getData("dialog").containsKey("title")) {
			 title = nSetting.getData("listdelete").getData("dialog").getData("title").toString();
		 }
		 if (nSetting.getData("listdelete").getData("dialog").containsKey("message")) {
			 message = nSetting.getData("listdelete").getData("dialog").getData("message").toString();
		 }
		 if (nSetting.getData("listdelete").getData("dialog").containsKey("button1")) {
			 button1 = nSetting.getData("listdelete").getData("dialog").getData("button1").toString();
		 }
		 if (nSetting.getData("listdelete").getData("dialog").containsKey("button2")) {
			 button2 = nSetting.getData("listdelete").getData("dialog").getData("button2").toString();
		 } 
		 showDialog(title, message, button1, button2, "", "deleteactivity", n);

	}
	public void showDialog(String title, String msg, String button1, String button2, String button3, String requestcode, Nset result){
		hideMessage();
		
		final Nset data = Nset.newObject();
		data.setData("title", title);
		data.setData("msg", msg);
		data.setData("button1", button1);
		data.setData("button2", button2);
		data.setData("button3", button3);
		data.setData("requestcode", requestcode);
		data.setData("result", result);
		
		Builder dlg = new AlertDialog.Builder(this);
		dlg.setTitle("");
		if (!data.getData("button1").toString().equals("")) {
			dlg.setPositiveButton(data.getData("button1").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if ( data.getData("logout").toString().equals("showdialog") ) {
						
					}
				}
			});
		}
		if (!data.getData("button2").toString().equals("")) {
			dlg.setNeutralButton(data.getData("button2").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if ( data.getData("requestcode").toString().equals("logout") ) {
					}else if ( data.getData("requestcode").toString().equals("closebatch") ) {
					}else if ( data.getData("requestcode").toString().equals("deleteactivity") ) {	
						NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
						nc.Query("DELETE FROM mobileactivity WHERE activityid=?", data.getData("result").getData("activityid").toString());
						reloadActivityData();
					}else if ( data.getData("requestcode").toString().equals("deleteactivityonline") ) {	
						deleteOnline(AndroidActivity.this,data.getData("result").getData("link").toString(),  data.getData("result").getData("activityid").toJSON());
					}else if ( data.getData("requestcode").toString().equals("deleteactivity") ) {	
						
					}else if ( data.getData("requestcode").toString().equals("exit") ) {
						finish();
					}
				}
			});
		}
		if (!data.getData("button3").toString().equals("")) {
			dlg.setNegativeButton(data.getData("button3").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					 
				}
			});
		}			
		dlg.setCancelable(false);
		dlg.setTitle(data.getData("title").toString());
		dlg.setMessage(data.getData("msg").toString());
		alertDialog=dlg.create();
		alertDialog.show();
	}
}
