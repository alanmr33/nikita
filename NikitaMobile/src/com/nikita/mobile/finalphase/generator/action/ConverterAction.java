package com.nikita.mobile.finalphase.generator.action;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class ConverterAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
        
		Nset data =currdata.getData("args");        
        String param1 = comp.getNikitaComponent().getVirtualString(data.get("param1").toString());
        String param2 = comp.getNikitaComponent().getVirtualString(data.get("param2").toString());
        String param3 =  data.get("param3").toString() ;
        if (param1.equals("toint")) {
        	comp.getNikitaComponent().setVirtual(param3, AUtility.getInt(param2));
        }else  if (param1.equals("todouble")) {
        	comp.getNikitaComponent().setVirtual(param3, AUtility.getDouble(param2));
        }else{
        	comp.getNikitaComponent().setVirtual(param3, "0");
        }
			
		return true;
	}
}
