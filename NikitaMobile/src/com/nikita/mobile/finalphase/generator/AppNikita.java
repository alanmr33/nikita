package com.nikita.mobile.finalphase.generator;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Enumeration;
import java.util.Hashtable;
 











import java.util.Locale;

import com.google.android.gcm.GCMRegistrar;
import com.nikita.mobile.finalphase.ServerUtilities;
import com.nikita.mobile.finalphase.printer.GlobalPrinter;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.service.LocationService;
import com.nikita.mobile.finalphase.service.NikitaRz;
import com.nikita.mobile.finalphase.service.SchedulingService;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class AppNikita extends android.app.Application{
	private static AppNikita singleton;	
	public GlobalPrinter printer;
	public static boolean PRODUCTION = true;
	public static boolean NIKITA_DEV = !PRODUCTION;	
	public static String  GCMID = "12568017616";
	
	public static void setUserInteraction(){
		
	}
	
	public static AppNikita getInstance(){
		return singleton;
	}  
	Handler lochandler;	
	public Handler getLocationHandler(){		 
		return lochandler;
	}
	Handler currhandler;	
	public Handler getActionHandler(){		 
		return currhandler;
	}
	Handler streamhandler;	
	public Handler getStreamHandler(){		 
		return streamhandler;
	}
	Handler mainhandler;	
	public Handler getMainHandler(){		 
		return mainhandler;
	}
	private NikitaActivity nikitaActivity;
	public NikitaActivity getCurrentNikitaActivity(){		 
		return nikitaActivity;
	}
	public void setCurrentNikitaActivity(NikitaActivity nikitaActivity){		 
		this.nikitaActivity=nikitaActivity;
	}	
	public String getBaseUrlProduction(){
		return "https://gpstdevqa.dipostar.com/dipo";
//		return "https://gpstdevqa.dipostar.com/dipo_kev";
//		return "https://gpst.dipostar.com/dipo";
//		return "http://10.100.10.104:8080/dipo_fase2"; 
	}
	public String getBaseUrl(){
		if (PRODUCTION) {
			 
			return getBaseUrlProduction();
		}
		return Utility.getSetting(getApplicationContext(), "BASE-URL", "");
	}
	public String getBaseUrl(String url){
		if (getBaseUrl().equals("/")) {
			return getBaseUrl()+url;
		}
		return getBaseUrl()+"/"+url;
	}
	
	private NikitaControler nikitaComponent;
	public NikitaControler getCurrentNikitaControler(){		 
		return nikitaComponent;
	}
	public void setCurrentNikitaControler(NikitaControler nikitaControler){		 
		this.nikitaComponent=nikitaControler;
	}
	
	private Hashtable<String, NikitaControler> manager = new Hashtable<String, NikitaControler> ();
	public NikitaControler getNikitaComponent(String name){
		//Log.i("getNikitaComponent", name);
		return manager.get(name);
	}
	public  Hashtable<String, NikitaControler> getNikitaControlers(){
		return manager;
	}
	public int getNikitaControlerCount(){
		return manager.size();
	}
	public void setNikitaControler(String name, NikitaControler nikitaControler){
		manager.put(name, nikitaControler);
	}
	public String[] getNikitaControlerNames(){
		return Utility.getObjectKeys(manager);
    }
	public void removeNikitaControler(String name){
		manager.remove(name);
	}
	public void removeNikitaControlerAll(){
		manager.clear();
	}
	private Hashtable<String, Object> virtual = new Hashtable<String, Object> (){
		public synchronized Object get(Object key) {			 
			return super.get(String.valueOf(key).toLowerCase());
		};	
		public synchronized Object put(String key, Object value) {
			return super.put(String.valueOf(key).toLowerCase(), value);
		};
	};
	public void setVirtual(String key, Object data){		 
		virtual.put(key.trim(), data); 
	}
	public Object getVirtual(String key){
		if (key.startsWith("@+CORE-MOBILE-ACTIVITY-")) { //@+CORE-MOBILE-ACTIVITY-FORMNAME-COMPONENTNAME [NSET]
			String[] keys = Utility.getObjectKeys(AppNikita.getInstance().getNikitaControlers());
			for (int i = 0; i < AppNikita.getInstance().getNikitaControlerCount() ; i++) {
				AppNikita.getInstance().getNikitaComponent(keys[i]).saveFileComponents();
			} 	
			key = key.substring("@+CORE-MOBILE-ACTIVITY-".length());
			String fname = "";
			String cname = "";
			String castx = "";
			if (key.contains("-")) {
				fname = key.substring(0, key.indexOf("-")).trim();
				cname =  key.substring(key.indexOf("-")+1).trim();
			}	
			if (cname.contains("[")) {
				castx =  cname.substring(cname.indexOf("[")).trim();
				cname = cname.substring(0, cname.indexOf("[")).trim();				
			}
			NikitaControler nf = AppNikita.getInstance().getNikitaComponent(fname);
			if (nf!=null) {
				return  nf.getComponent((cname.startsWith("$")?"":"$")+cname).getText();
			}
		}
		 return virtual.get(key.trim());
	}
	public void removeVirtual(String key){
		virtual.remove(key);
	}
	public boolean containVirtual(String key){
		
		return virtual.containsKey(key);
	}
	public void onCreate() {
		super.onCreate();
		this.printer=new GlobalPrinter(this);
		String appVersion = "";
		singleton = this;		
		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			appVersion = pInfo.versionName;
     		setVirtual("@+VERSION", appVersion );
		} catch (Exception e) { }
		
		Log.i("Nikita", "onCreate [AppNikita] :" + Thread.currentThread().getName());		
		mainhandler = new Handler();
		
		//need inisialize coonecction and parameter
		Generator.initializeNikita(this);
		
		GCMRegistrar.checkDevice(this);
		final String regId = GCMRegistrar.getRegistrationId(this);
		Utility.setSetting(getApplicationContext(), "PUSHID", regId);
		Log.i("GCMRegistrar", regId);
	    if (regId.equals("")) {
            GCMRegistrar.register(this, GCMID);            
        }   
	    
	     //extract from asset	    
	     new File(Utility.getDefaultPath("nv3")).mkdirs();
		 new File(Utility.getDefaultPath("temp")).mkdirs();
		 new File(Utility.getDefaultPath("image")).mkdirs();
		 new File(Utility.getDefaultPath("files")).mkdirs();
		 new File(Utility.getDefaultPath("profile")).mkdirs();
		 
		 Utility.deleteAllFileOnFolder(new File(Utility.getDefaultPath("files")));
		 
		 try {			  
			 if (! new File(Utility.getDefaultPath("zip.db")).exists() ) {
				 Utility.extrackZip(getApplicationContext().getAssets().open("zip.zip"), Utility.getDefaultPath());
			 }			 
		} catch (Exception e) {}
		 
		
	}
		
	public AppNikita(){
		 
		new Thread("actionNikitaThread"){//"nikita"
			public void run() {
				try { 
					Looper.prepare();
					currhandler = new Handler() {	
						public void handleMessage(Message msg) {
							if (msg!=null) {
								if (msg.what==-1) {
									Looper.myLooper().quit();
								}
							}
							super.handleMessage(msg);
						}
					};					
					Looper.loop();
				} catch (Exception e) { }
			}
			
		}.start();
		
		new Thread("streamNikitaThread"){//"nikita"
			public void run() {
				try { 
					Looper.prepare();
					streamhandler = new Handler() {	
						public void handleMessage(Message msg) {
							if (msg!=null) {
								if (msg.what==-1) {
									Looper.myLooper().quit();
								}
							}
							super.handleMessage(msg);
						}
					};					
					Looper.loop();
				} catch (Exception e) { }
			}
			
		}.start();
		 
		new Thread("locationNikitaThread"){ 
			public void run() {
				try { 
					Looper.prepare();
					lochandler = new Handler() {	
						public void handleMessage(Message msg) {
							if (msg!=null) {
								if (msg.what==-1) {
									Looper.myLooper().quit();
								}
							}
							super.handleMessage(msg);
						}
					};					
					Looper.loop();
				} catch (Exception e) { }
			}
			
		} ;
		
		 new HandlerThread("locationNikitaThread"){
			public void run() {
				lochandler = new Handler(getLooper()) {	
					public void handleMessage(Message msg) {
						if (msg!=null) {
							if (msg.what==-1) {
								Looper.myLooper().quit();
							}
						}
						super.handleMessage(msg);
					}
				};	
				super.run();
			}
		 }.start();
		 
		 
		/*
		HandlerThread 
		mHandlerThread = new HandlerThread("actionNikitaThread");
        mHandlerThread.start();
        currhandler = new Handler(mHandlerThread.getLooper());
        
        mHandlerThread = new HandlerThread("streamNikitaThread");
        mHandlerThread.start();
        lochandler = new Handler(mHandlerThread.getLooper());
        
        mHandlerThread = new HandlerThread("locationNikitaThread");
        mHandlerThread.start();
        lochandler = new Handler(mHandlerThread.getLooper());
        
        
        */
		 
	}
 
	public void onTerminate() {
		/*
		 if (currhandler!=null) {
			 currhandler.sendEmptyMessage(-1);
		}
		 if (streamhandler!=null) {
			 streamhandler.sendEmptyMessage(-1);
		}
		 */
	}
	public void startActivity(Intent intent) {
		Log.i("Nikita", "startActivity");
		super.startActivity(intent);
	}
	
	public static String BASE_URL = "";
	
	
	private void init (){
		//Thread.setDefaultUncaughtExceptionHandler(
        //       (UncaughtExceptionHandler) new CustomExceptionHandler( getApplicationContext() ));
		
		
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
		    @Override
		    public void uncaughtException(Thread thread, final  Throwable ex) {
		    	Log.i("Nikita crashed", ex.getMessage());
                //Toast.makeText( getApplicationContext(), "Application crashed", Toast.LENGTH_LONG).show();
		    	
		    	new Thread() {
		            @Override
		            public void run() { 
		                Looper.prepare();
		                Log.i("Application crashed", ex.getMessage());
		                Toast.makeText( getApplicationContext(), "Application crashed", Toast.LENGTH_LONG).show();
		                Looper.loop();
		            }
		        };

		        try{
		            Thread.sleep(1000); // Let the Toast display before app will get shutdown
		        }catch (InterruptedException e) {
		            // Ignored.
		        }
		    }
		});
		
		 
	}
	public String getApplicationId(){
		try {
			PackageInfo pInfo = AppNikita.getInstance().getPackageManager().getPackageInfo(getPackageName(), 0);
			return  pInfo.packageName  ;
		} catch (Exception e) {  } 
		return "nikitamobile";
	}
	public String getDeviceId(){
		return  ( (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase(Locale.ENGLISH);
	}
	public String getApplicationVersion(){		 
		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			return pInfo.versionName ;
		} catch (Exception e) { }
		return "";
	}
	public String getLogicVersion(){			  
		return Utility.getSetting(getApplicationContext(), "INIT-LOGIC-VERSION", "");
	}
	public Hashtable<String, String> getArgsData(){
		Hashtable<String, String> hashtable = new Hashtable<String, String>();
		hashtable.put("userid", 		Utility.getSetting(getApplicationContext(), "N-USER", ""));
		hashtable.put("username", 		Utility.getSetting(getApplicationContext(), "N-USER", ""));
		hashtable.put("imei", 			( (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase(Locale.ENGLISH));
		hashtable.put("localdate", 		Utility.Now());
		hashtable.put("session", 		Utility.getSetting(getApplicationContext(), "N-SESSION", ""));
		hashtable.put("auth", 			Utility.getSetting(getApplicationContext(), "N-AUTH", ""));
		hashtable.put("batch",			Utility.getSetting(getApplicationContext(), "N-BATCH", ""));
		hashtable.put("logicversion", 	Utility.getSetting(getApplicationContext(), "INIT-LOGIC-VERSION", "") );
		hashtable.put("branchid", 		Utility.getSetting(getApplicationContext(), "N-USER-BRANCH", "") );

		try {
			PackageInfo pInfo = AppNikita.getInstance().getPackageManager().getPackageInfo(AppNikita.getInstance().getPackageName(), 0);
			hashtable.put("version", pInfo.versionName );
		} catch (Exception e) { hashtable.put("version", ""); } 
		 
		
		return hashtable;
	}
	
	 
}
