package com.nikita.mobile.finalphase.generator;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.component.ComponentManager;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.forms.DefaultHeaderForm;
import com.nikita.mobile.finalphase.generator.action.StringAction;
import com.nikita.mobile.finalphase.generator.ui.Function;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaExpression;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.data.Nson;
import com.rkrzmail.nikita.utility.Base64Coder;

public class NikitaControler  {
	//defaut is on ui thread
	String uuid = "";
	public void setNikitaControlerID(String id){
		this.uuid=id!=null?id:"";		
	}
	public boolean isNikitaActivityOwner(String id){
		return this.uuid.equals(  (id!=null?id:"") );		
	}
	public void refreshComponent(Component comp){
		 
	}
	public void runOnActionThread(Runnable run){		
		if (AppNikita.getInstance().getActionHandler()!=null) {		
			AppNikita.getInstance().getActionHandler().post(run);
			/*
			if (AppNikita.getInstance().getActionHandler().getLooper().getThread().equals(Thread.currentThread())) {
				run.run();
			}else{
				AppNikita.getInstance().getActionHandler().post(run);
			}	
			*/
		}
	}
	public void runOnUiThread(Runnable run){
		if (getActivity()!=null) {
			getActivity().runOnUiThread(run);
		}else if(runuiwaiting!=null){
			runuiwaiting.addElement(run);
		}
	}
	 
	private Vector<Runnable> runuiwaiting = new Vector<Runnable> ();
	
	public void runOnAsynThread(Runnable run){
		AsyncTask.execute(new Runnable() {
			private Runnable run;
			private Runnable get(Runnable run){
				this.run=run;return this;
			}
			public void run() {
				if (getActivity()!=null) {
					try {
						this.run.run();
					} catch (Exception e) { }
				}				
			}
		}.get(run));
	}

	public NikitaControler start(String reqcode, Nset args){
		internalstart(reqcode, args );
		return this;
	}	
	public NikitaControler start(){
		internalstart();
		return this;
	}	
	public void startFromActivity(Activity activity){
		startFromActivity(activity, null);
	}
	public void startFromActivity(Activity activity, String requestcode){
		Intent intent = new Intent(activity, NikitaActivity.class);	
		intent.putExtra("nfid", "nikitaform");
		intent.putExtra("uuid", System.currentTimeMillis()+""+System.nanoTime());
		intent.putExtra("nikitaformname", getNikitaFormInstance());
		if (requestcode!=null && !requestcode.equals("")) {
			intent.putExtra("nikitarequestcode", requestcode); 
			activity.startActivityForResult(intent,Utility.getInt( Utility.getNumberOnly(requestcode) )  );
		}else{
			activity.startActivity(intent);	
		}		
	}	
	public boolean isOnMainThread(){
		return Thread.currentThread().getName().equalsIgnoreCase("main");
	}
	public void showBusywithDelay(int delayms){
		delayms = isbusy?0:delayms;
	
		setBusy(true);
		if (getActivity()!=null) {
			new Handler(getActivity().getMainLooper()).postDelayed(new Runnable() {
				public void run() {
					if (getView().findViewById(R.id.frmBusy)!=null && isbusy) {
						getView().findViewById(R.id.frmBusy).setVisibility( View.VISIBLE );
					}	
				}
			}, delayms);
		} 		
	}
	public void showBusy(final boolean show){
		setBusy(show);
		if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
			if (getView().findViewById(R.id.frmBusy)!=null) {
				getView().findViewById(R.id.frmBusy).setVisibility(show?View.VISIBLE:View.GONE);
			}			
		}else{
			runOnUiThread(new Runnable() {
				public void run() {
					if (getView().findViewById(R.id.frmBusy)!=null) {
						getView().findViewById(R.id.frmBusy).setVisibility(show?View.VISIBLE:View.GONE);
					}	
				}
			});
		}
	}
	 
	public void setBusy(boolean busy) {
		isbusy=busy;
	}
	public boolean isBusy() {
		return isbusy;
	}  
	public void saveFileComponents(){
		if (getContent()!=null) {	
			Vector<Component> all = getContent().populateAllComponents();
			for (int i = 0; i < all.size(); i++) {
				if (!all.elementAt(i).getName().equals("")) {					 
					if (all.elementAt(i).isFileComponent()) {
						all.elementAt(i).saveFileComponent();
					}				 
				}			
			}
		}
	}
	public void openStreamComponents(Nset fdata){
		//Log.i("aaaaaaaaaaa", "openStreamComponents");
		if (getContent()!=null) {	
			//Log.i("aaaaaaaaaaa", fdata.toJSON());
			Vector<Component> all = getContent().populateAllComponents();
			for (int i = 0; i < all.size(); i++) {
				if (!all.elementAt(i).getName().equals("")) {
					all.elementAt(i).setText( 	 fdata.getData(all.elementAt(i).getName()).getData(0).toString()  );
					all.elementAt(i).setVisible( fdata.getData(all.elementAt(i).getName()).getData(1).toString().equals("1")  );
					all.elementAt(i).setEnable(  fdata.getData(all.elementAt(i).getName()).getData(2).toString().equals("1")  );
				}			
			}
		}
		//String callinst = fdata.getData( Utility.MD5("init").toLowerCase(Locale.ENGLISH) ).getData("callerinstance").toString();
		//String callname = fdata.getData( Utility.MD5("init").toLowerCase(Locale.ENGLISH) ).getData("fname").toString();
		//setCallerNikitaComponent(AppNikita.getInstance().getNikitaComponent(callname));
	}
	public Nset getStreamComponents(){
		//{"compname":["text","visible[0,1]","enable[0,1]","[file,]"]}
		Nset nset = Nset.newObject();			
		if (getContent()!=null) {	
			Vector<Component> all = getContent().populateAllComponents();
			for (int i = 0; i < all.size(); i++) {
				if (!all.elementAt(i).getName().equals("")) {
					Nset compset = Nset.newArray();
					compset.addData(all.elementAt(i).getText());
					compset.addData(all.elementAt(i).isVisible()?"1":"0");
					compset.addData(all.elementAt(i).isEnable() ?"1":"0");
					if (all.elementAt(i).isFileComponent()) {
						compset.addData("file");//array[3]
					}
					compset.addData(all.elementAt(i).getType());//type
					nset.setData(all.elementAt(i).getName(), compset);
				}			
			}
		}
		nset.addData(Nset.newObject().setData(  Utility.MD5("init").toLowerCase(Locale.ENGLISH),   Nset.newObject().setData("version", "v2").setData("fname", getNikitaFormName()).setData("caller", getCallerName()).setData("callerinstance", getCallerInstance()) ));
		return nset;//{components}
	}
	public void reopen(){
		show(requestcode, Nset.newObject());//
	}
	public void show(){
		show(null, Nset.newObject());
	}
	public void show(String requestcode, Nset arg){
		callerinstance=AppNikita.getInstance().getCurrentNikitaControler().getNikitaFormInstance();
		Log.i("Text+setTextcaller++", callerinstance);
		
		internalstart(requestcode, arg);
		if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
			Intent intent = new Intent(AppNikita.getInstance(), NikitaActivity.class);	
			intent.putExtra("nfid", "nikitaform");
			intent.putExtra("uuid", System.currentTimeMillis()+""+System.nanoTime());
			intent.putExtra("nikitaformname", getNikitaFormInstance());
			if (requestcode!=null && !requestcode.equals("")) {
				intent.putExtra("nikitarequestcode", requestcode); 
			}
			if (arg.containsKey("FLAG_ACTIVITY_CLEAR_TOP")) {
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			}
			AppNikita.getInstance().getCurrentNikitaActivity().startActivity(intent);		
		}else{
			if (AppNikita.getInstance().getCurrentNikitaActivity()!=null) {
				Intent intent = new Intent(AppNikita.getInstance(), NikitaActivity.class);	
				intent.putExtra("nfid", "nikitaform");
				intent.putExtra("uuid", System.currentTimeMillis()+""+System.nanoTime());
				intent.putExtra("nikitaformname", getNikitaFormInstance());
				if (requestcode!=null && !requestcode.equals("")) {
					intent.putExtra("nikitarequestcode", requestcode); 
				}
				if (arg.containsKey("FLAG_ACTIVITY_CLEAR_TOP")) {
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				}
				AppNikita.getInstance().getCurrentNikitaActivity().startActivity(intent);	
			}   
		}  
	}
	public void showFinder(String requestcode, Nset arg){
		if (!arg.isNsetObject()) {
			arg = Nset.newObject();
		}
		arg.setData(Utility.MD5("CALLER"), "FINDER");
		show(requestcode, arg);
	}
	public void back(){
		if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
			if (getActivity()!=null) {
				prepareResultActivity();
				onSaveState();
				getActivity().finish();
			}	
		}else{
			runOnUiThread(new Runnable() {
				public void run() {
					if (getActivity()!=null) {						
						prepareResultActivity();
						onSaveState();
						getActivity().finish();
					}					
				}
			});
		}
	}
	public void close(){
		AppNikita.getInstance().removeNikitaControler(getNikitaFormInstance());
		back();
	}
	private Nset result = null;private String response;
	public void setResult(String response, Nset result){
		this.response=response;
		this.result=result;
		
		NikitaControler caller = AppNikita.getInstance().getNikitaComponent(getCallerInstance());
		if (caller!=null) {
			caller.onResult(requestcode, response, result);
		}
	}
	private void prepareResultActivity(){
		if (getActivity()!=null && result!=null && response!=null) {
			Intent intent = new Intent();
			if (result.isNsetObject()) {
				String[] keys = result.getObjectKeys();
				for (int i = 0; i < keys.length; i++) {
					if (result.getData(keys[i]).isBoolean()) {
						intent.putExtra(keys[i],  result.getData(keys[i]).toBoolean());
					}else if (result.getData(keys[i]).isNumber()) {
						intent.putExtra(keys[i],  result.getData(keys[i]).toNumber());
					}else{
						intent.putExtra(keys[i],  result.getData(keys[i]).toString());
					}						
				}
			}else if (result.isNsetArray()) {
				for (int i = 0; i < result.getArraySize(); i++) {
					if (result.getData(i).isBoolean()) {
						intent.putExtra(""+i,  result.getData(i).toBoolean());
					}else if (result.getData(i).isNumber()) {
						intent.putExtra(""+i,  result.getData(i).toNumber());
					}else{
						intent.putExtra(""+i,  result.getData(i).toString());
					}	
				}
				intent.putExtra("size",  result.getArraySize());
			}
			getActivity().setResult(Utility.getInt(response), intent);
		}
	}
	public static NikitaControler getInstance(String formname){
		if (AppNikita.getInstance().getNikitaComponent(formname)!=null) {
			return AppNikita.getInstance().getNikitaComponent(formname);
		}
		else{
			return new NikitaControler(formname);
		}
	}
	public static NikitaControler getInstance(String formname, int index){
		if (AppNikita.getInstance().getNikitaComponent(formname)!=null) {
			return AppNikita.getInstance().getNikitaComponent(formname);
		}
		else{
			return new NikitaControler(formname);
		}
	}
	public NikitaControler(){
		this.formname = getClass().getSimpleName();
		AppNikita.getInstance().setNikitaControler(getNikitaFormInstance(), this); 
	}
	private NikitaControler(String formname){
		if (formname!=null) {
			this.formname=formname;
		}else{
			this.formname = getClass().getSimpleName();
		}	
		AppNikita.getInstance().setNikitaControler(getNikitaFormInstance(), this); 
	}
	public NikitaControler(Activity activity){
		this.formname=activity.getClass().getSimpleName();
		internalstart("", Nset.newObject());
	}

	protected void internalstart(){ 
		internalstart("", Nset.newObject());
	}
	
	public String getNikitaFormName(){//formname[intance]
		if (getNikitaFormInstance().contains("[")) {
			return getNikitaFormName().substring(0, getNikitaFormName().indexOf("[")).trim();
		}
		return getNikitaFormInstance();
	}
	public String getNikitaFormInstance(){
		if (this.formname!=null) {
			return this.formname.trim() ;
		}else{
			return  getClass().getSimpleName();
		}	
	}
	
	protected void internalstart(String requestcode, Nset args){ 		
		if (isload) {
			return;
		}
		isload=true; 
		
		Log.i("nikita","internalstart:"+getNikitaFormInstance());
		this.args = args;	
		this.requestcode=requestcode;	
		
		
		if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
			 runOnActionThread(new Runnable() {
				public void run() {
					onLoad();
					Log.i("nikita","isinitialized"+getNikitaFormInstance());
					isloaded=true;
				}
			});
		}else{
			onLoad();
			Log.i("nikita","isinitialized"+getNikitaFormInstance());
			isloaded=true;
		}  
	}
	public void executeNikitaTask( Nset args){
		this.args = args;	//no need activity
		if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
			 runOnActionThread(new Runnable() {
				public void run() {
					onRunNikitaTask();	
				}
			});
		}else{
			onRunNikitaTask();	
		}  
	}
	
	private Component componentLink;
	public void onRunNikitaTask(){
		if (componentLink!=null) {
			componentLink.runRouteAction("link", true);
		}
	}
	
	private Thread getThreadByName(String threadName) {
	    for (Thread t : Thread.getAllStackTraces().keySet()) {
	        if (t.getName().equals(threadName)) return t;
	    }
	    return null;
	}
	
	private static class InternalHandler extends Handler {
        public InternalHandler() {
            super(Looper.getMainLooper());
        }
 
        public void handleMessage(Message msg) {
                        
        }
    }
	
	private Runnable onLoadListener;
	public void setOnLoadListener(Runnable loadlistener){
		this.onLoadListener=loadlistener;
	}
	
	private Vector<Runnable> onLoadListeners = new Vector<Runnable>();
	public void addOnLoadListener(Runnable loadlistener){
		onLoadListeners.addElement(loadlistener);
	}
	
	boolean iscreated = false;
	boolean isbusy = false;//action
	boolean isloaded = false;
	boolean isload = false;
	
	public boolean isLoaded(){
		return isloaded;
	}
	public boolean isCreated(){
		return iscreated;
	}	
	
	public void onCreateUI() {
		iscreated=true;
		if (header!=null) {
			LinearLayout parent = ((LinearLayout)getView().findViewById(R.id.frmHeader)  );
			header.setActivity((NikitaActivity)getActivity(), parent);
			header.onCreateUI();
		}
		if (getContent()!=null) {	
			if (getView() instanceof ViewGroup &&  getView().findViewById(R.id.frmContentScroll) instanceof LinearLayout ) {
				LinearLayout vcontent = ((LinearLayout)getView().findViewById(R.id.frmContentScroll)  );
				if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-scroll:false")) {
					vcontent = ((LinearLayout)getView().findViewById(R.id.frmContent));
				}
				if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-orientation:potrait")) {
					getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				}else if (getContent().getStyle()!=null &&  getContent().getStyle().getViewStyle().contains("n-orientation:landscape")) {
					getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				}
				if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-softinputmode:hidden")) {
					getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
				}
				View child = getContent().onCreate(this);		
				vcontent.addView(child, Utility.getMarginLayoutParams(child, MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.MATCH_PARENT) );	

				getView().findViewById(R.id.frmOnTop).setVisibility(View.GONE);
				if (getContent().getStyle()!=null && getContent().getStyle().getViewStyle().contains("n-vtop:true")) {
					viewOnTop();
				}				
			}else if (getView() instanceof ViewGroup) {
				View child = getContent().startCreateUI(this);		
				((ViewGroup)getView()).addView(child, Utility.getMarginLayoutParams(child, MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT) );	
			}
		}
		
		if (AppNikita.getInstance().getVirtual(NIKITA_SHOWDIALOG_KEY_NAME) instanceof Nset && saved) {
			Nset data =  (Nset)AppNikita.getInstance().getVirtual(NIKITA_SHOWDIALOG_KEY_NAME);
			if (data.getData("nfname").toString().equals(getNikitaFormInstance())  ) {
				showDialog(data.getData("title").toString(), data.getData("message").toString(), data.getData("button1").toString(), data.getData("button2").toString(), data.getData("button3").toString(), data.getData("reqcode").toString(), data.getData("data"));
			}
		}		
		
	}
	private void viewOnTop(){
		getView().findViewById(R.id.frmOnTop).setVisibility(View.VISIBLE);
		if (getContent()!=null) {
			Vector<Component>  conComponents = getContent().populateAllComponents();
			for (int i = 0; i < conComponents.size(); i++) {
				if (conComponents.elementAt(i).getStyle()!=null) {
					if (conComponents.elementAt(i).getStyle().getInternalStyle().getData("n-ontop").toString().equalsIgnoreCase("true")) {
						if (conComponents.elementAt(i).getView().getParent() instanceof ViewGroup) {
							((ViewGroup)conComponents.elementAt(i).getView().getParent()).removeView(conComponents.elementAt(i).getView());
							((ViewGroup)getView().findViewById(R.id.frmOnTop) ).addView(conComponents.elementAt(i).getView());
						}
					}
				}
			}
		}
	}
	 
	private boolean saved = false;
	private String formname;
	private String requestcode  =  "" ;
	private Nset args = Nset.newObject() ;	
	
	private Vector<Component> navLoad  = new Vector<Component>();
	private Vector<Component> navBack  = new Vector<Component>();
	private Vector<Component> navResult  = new Vector<Component>();
	
	public String getParameterString(String name){
		return args.getData(name).toString();
	}
	public Nset getParameters(){
		return args;
	}	
	
	private NikitaEngine nikitaEngine;
	public NikitaEngine getNikitaEngine() {
		return nikitaEngine;
	}
	public void setNikitaEngine(NikitaEngine nikitaEngine) {
		this.nikitaEngine = nikitaEngine;
	}
	
	
	public void onLoad() {  //actionthread	 
		Log.i("nikita","initialize:"+formname);	 
		
		NikitaConnection nc = Generator.getConnection(NikitaConnection.LOGIC);
	 
		
		String fname = getNikitaFormInstance();
		//boolean isID = Utility.isNumeric(fname);
		
		
		NikitaEngine engine = new NikitaEngine(nc, fname, nc.isNikitaConnection(), nc.isNikitaConnection());
		setNikitaEngine(engine);
		
		
	    //Nikitaset logicform = nc.QueryPage(1, 1, "SELECT formid,formname,formtitle,formtype,formstyle FROM web_form WHERE "+(isID?"formid":"formname")+" = ?",fname);
	    //Nikitaset nikitaset = nc.Query("SELECT comp.*,MAX(logic.compid) AS countid, COUNT(*) AS count FROM web_component comp LEFT JOIN web_route logic ON (comp.compid=logic.compid) WHERE comp.formid = ?  GROUP BY comp.compid ORDER BY comp.compindex ASC ;", logicform.getText(0, 0) );
	   
	    
	    Nikitaset logicform = engine.getForm();
	    Nikitaset nikitaset = engine.getComponents();
	    	    
	    Log.i("nikita","initialize-");
	    
	   
	    
	    
	    NikitaForm nf = new NikitaForm(this);
        nf.setText(logicform.getText(0, "formtitle"));  
	    if (NikitaEngine.getFormIndex(formname).trim().length()>=1) {
	    	nf.setText(logicform.getText(0, "formtitle")+ " ["+NikitaEngine.getFormIndex(formname).trim()+"]");  
		}
        
        //mobile only
        nf.setId(logicform.getText(0, "formid"));
        nf.setName(logicform.getText(0, "formname"));
        nf.setType(logicform.getText(0, "formtype"));
                
        if (logicform.getText(0, "formstyle").contains(":")||logicform.getText(0, "formstyle").contains("=")) {
            Style style = Style.createStyle(logicform.getText(0, "formstyle")) ;              
            nf.setStyle(style);
        }     
        
        Hashtable<String, Component> hashtable = new Hashtable<String, Component>();
        Vector<Component> components = new Vector<Component>(); 
        for (int i = 0; i < nikitaset.getRows(); i++) {

            Component comp = ComponentManager.createComponent( this , nikitaset, i);       
            components.addElement(comp);
            hashtable.put(comp.getName(), comp);
            //if (nc.QueryPage(1, 1, "SELECT routeindex from web_route WHERE compid = ? ;", comp.getId()).getRows()>=1 ) {
                
            //}
            
            //fill default
            if (nikitaset.getText(i, "compdefault").startsWith("@")||nikitaset.getText(i, "compdefault").startsWith("&")||nikitaset.getText(i, "compdefault").startsWith("$")) {
                comp.setText(getParameter(nikitaset.getText(i, "compdefault").substring(1)));
            }  
            if (nikitaset.getText(i, "comptext").startsWith("@")||nikitaset.getText(i, "comptext").startsWith("&")||nikitaset.getText(i, "comptext").startsWith("$")) {
                comp.setText(getParameter(nikitaset.getText(i, "comptext").substring(1)));
            } 
            
            //fill data
            if (nikitaset.getText(i, "complist").startsWith("@")||nikitaset.getText(i, "complist").startsWith("&")||nikitaset.getText(i, "complist").startsWith("$")) {
                comp.setData(Nset.readJSON(getParameter(nikitaset.getText(i, "complist").substring(1))));
            } else if(nikitaset.getText(i, "complist").startsWith("[")|| nikitaset.getText(i, "complist").startsWith("{")){
                comp.setData(Nset.readJSON(nikitaset.getText(i, "complist")));
            } else if(nikitaset.getText(i, "complist").contains("|")){
                comp.setData(Nset.readsplitString(nikitaset.getText(i, "complist"),"|"));
            } else if(nikitaset.getText(i, "complist").contains(",")){
                comp.setData(Nset.readsplitString(nikitaset.getText(i, "complist"),","));
            }
            
            //fill style
            if (nikitaset.getText(i, "compstyle").contains(":")||nikitaset.getText(i, "compstyle").contains("=")) {
                Style style = Style.createStyle(nikitaset.getText(i, "compstyle")) ;    
                comp.setStyle(style);
            }
            
            if (nikitaset.getText(i, "comptype").equals("navload")) {
                navLoad.addElement(comp);
            }else if (nikitaset.getText(i, "comptype").equals("navback")) {
                navBack.addElement(comp);
            }else if (nikitaset.getText(i, "comptype").equals("navresult")) {   
                navResult.addElement(comp);
            }
            
        }  
        
        //flat to tree[1]
        for (int i = 0; i < components.size(); i++) {
            String parent =components.elementAt(i).getParentName().trim();
            parent=parent.startsWith("$") ? parent.substring(1):parent;
            
            if (parent.equals(components.elementAt(i).getName())) {
                 components.elementAt(i).clearParentName();
            }else if (!parent.equals("")) {
                if ( hashtable.get(parent) instanceof ComponentGroup) {
                    ((ComponentGroup)hashtable.get(parent)).addComponent(components.elementAt(i));
                }else{
                    components.elementAt(i).clearParentName();
                }
            }else{
                components.elementAt(i).clearParentName();
            }
        }            
        //flat to tree[2]
        for (int i = 0; i < components.size(); i++) {
            if (components.elementAt(i).getParentName().equals("")) {
                nf.addComponent(components.elementAt(i));
            }                
        }
             
        //add for mobile header
        if (nf.getStyle()!=null) {
        	String fheader =nf.getStyle().getInternalObject().getData("style").getData("n-header").toString();
        	if (fheader.equals("default")) {    
        		NikitaControler nikitaComponent  = new DefaultHeaderForm().start();
        		NikitaForm nikitaForm = nikitaComponent.getContent();
        		nikitaForm.setText(nf.getText());
        		nikitaComponent.setCallerNikitaComponent(this);
        		setHeader(nikitaComponent);
        	}else if (fheader.length()>=1) {
        		
				//NForm nheader = new NForm(fheader, Nset.newObject().setData("+FORMTITLE", logicform.getText(0, "formtitle")).setData("+FORMNAME", logicform.getText(0, "formname"))  );
				//nheader.setCaller(this);
				//nheader.setRequestCode("HEADER");
				//setHeader(nheader.getContent());	
				
				
			}
        	
		}       
        
        
        /*
       //add for mobile drawer
        if (nf.getStyle()!=null) {
        	String fheader =nf.getStyle().getInternalObject().getData("style").getData("n-drawer").toString();
        	if (fheader.equals("default")) {
        		
        	}else if (fheader.length()>=1) {
				NForm nheader = new NForm(fheader, Nset.newObject().setData("+FORMTITLE", logicform.getText(0, "formtitle")).setData("+FORMNAME", logicform.getText(0, "formname"))  );
				nheader.setCaller(this);
				nheader.setRequestCode("DRAWER");
				setDrawer(nheader.getContent());			
			}
		}   
         */
        
            
               
        /*
        //mobileActivity
        String[]  keys = NCore.currMobileActivity.getData(getContent().getName()).getObjectKeys();
        for (int i = 0; i < keys.length; i++) {
			Component comp = getContent().findComponentbyName(keys[i]);
			if (comp!=null) {
				Nset n = NCore.currMobileActivity.getData(getContent().getName());
				comp.setText(    n.getData(keys[i]).getData(0).toString() );//array 0 text
				comp.setVisible( n.getData(keys[i]).getData(1).toString().equals("1") );//array 1 Visible
				comp.setEnable(  n.getData(keys[i]).getData(2).toString().equals("1") );//array 2 Enable
			}
		}
        //==================================//       
        */   
        Log.i("nikita","inisialized-");
        
        if (logicform.getText(0, "formtype").equals("link")||logicform.getText(0, "formtype").equals("scheduler")) {
        	Log.i("nikita","inisialized-link:"+nikitaset.getText(0, "compid"));
        	componentLink = new Component(this, nikitaset.getText(0, "compid"));
	    	nf.addComponent(componentLink);
		}
        
        setContent(nf);   
        
        //after setcontent before onload
        for (int i = 0; i < onLoadListeners.size(); i++) {
        	onLoadListeners.elementAt(i).run();
		}           
        
        if (navLoad.size()>=1) {
        	runOnUiThread(new Runnable() {
    			public void run() {
    				runOnActionThread(new Runnable() {
    					public void run() {
    						for (int i = 0; i < navLoad.size(); i++) {
    				        	Log.i("nikita","navLoad-");
    					    	navLoad.elementAt(i).runRoute();
    						}   						
    					}
    				});
    			}
    		});    
		}
        
        if (onLoadListener!=null) {
        	if (Thread.currentThread().getName().equalsIgnoreCase("main")) {
        		onLoadListener.run();
			}else{
				runOnUiThread(onLoadListener);
			}        	
		}	
        
        
        
	}
	
	public void onResult(String requestCode, String resultCode, Nset data) {
		setVirtualRegistered("@+RESULT", data);
		setVirtualRegistered("@+REQUESTCODE", requestCode);
		setVirtualRegistered("@+RESPONSECODE", resultCode);
		
		
		//add for finder 13/02/2015  //add 30102015
        if (resultCode.equals("FINDER")) {
           String[]  keys = data.getObjectKeys();
           for (int i = 0; i < keys.length; i++) {
                String key = keys[i];//key/component/variable
                String res = data.getData(key).toString();//data
                if (key.equals("")) {                                
                }else if (key.startsWith("@")) {
                    setVirtual(key, res);
                }else{
                    key = (key.startsWith("$")?"":"$")+key;
                    Component c = getComponent(key);
                    c.setText(res);
                    if (key.contains(c.getName())) {
                        refreshComponent(c);
                    }
                }        
            }
        } //end   
		
        
        if (isOnMainThread()) {
			runOnActionThread(new Runnable() {
				public void run() {
					if (getContent()!=null) {
						for (int i = 0; i < navResult.size(); i++) {
							navResult.elementAt(i).runRoute();
						}
					}				
				}
			});
		}else{
			if (getContent()!=null) {
				for (int i = 0; i < navResult.size(); i++) {
					navResult.elementAt(i).runRoute();
				}
			}
		}	      
		
	}
	
	public void onBack() {		
		if (isOnMainThread()) {
			runOnActionThread(new Runnable() {
				public void run() {
					onBackNow();					
				}
			});
		}else{
			onBackNow();
		}		 	
	}	
	
	private void onBackNow() {
		if (navBack.size()!=0) {
			for (int i = 0; i < navBack.size(); i++) {
				navBack.elementAt(i).runRoute();
			}			
		}else{
			//close();//2016-01-26 ????????????????????????????????
			runOnUiThread(new Runnable() {
				public void run() {
					getActivity().finish();					
				}
			});
		}		
	}
	
	private Activity activity;
	private View root;
	public Activity getActivity(){		
		return activity;
	}
	public View getView(){
		return root;
	}
	public void setActivity(Activity activity, View root){
		this.activity=activity;this.root=root;
		runuiwaitingAction();
	}
	
	protected void runuiwaitingAction(){
		if (runuiwaiting!=null && activity!=null) {
			for (int i = 0; i < runuiwaiting.size(); i++) {
				this.activity.runOnUiThread(runuiwaiting.elementAt(i));
			}
			runuiwaiting.clear();
		}
	}
	public void onSaveState() {
		if (getContent()!=null) {
			getContent().onSaveState();
		}
		saved =true;
	}
	
	public Nset getMobileActivityStream(){
		//{"compname":["text","visible[0,1]","enable[0,1]","[file,]"]}
		Nset nset = Nset.newObject();	
		
		if (getContent()!=null) {	
			Vector<Component> all = getContent().populateAllComponents();
			for (int i = 0; i < all.size(); i++) {
				if (!all.elementAt(i).getName().equals("")) {
					Nset compset = Nset.newArray();
					compset.addData(all.elementAt(i).getText());
					compset.addData(all.elementAt(i).isVisible()?"1":"0");
					compset.addData(all.elementAt(i).isEnable() ?"1":"0");
					if (all.elementAt(i).isFileComponent()) {
						compset.addData("file");//array[3]
					}
					nset.setData(all.elementAt(i).getName(), compset);
				}			
			}
		}
		nset.addData(Nset.newObject().setData(Utility.MD5("init").toLowerCase(Locale.ENGLISH), Nset.newObject().setData("caller", getCallerName()).setData("callerinstance", getCallerInstance()) ));
		return nset;//{components}
	}
 
	private String callerinstance = "";		
	private String getCallerName(){
		if (getCallerInstance().contains("[")) {
			return getCallerInstance().substring(0, getCallerInstance().indexOf("["));
		}
		return getCallerInstance();
	}
	public String getCallerInstance(){
		return callerinstance;
	}
	public void setCallerNikitaComponent(NikitaControler caller){
		this.callerinstance=caller.getNikitaFormInstance();
	}
	
	
	private NikitaForm content;
	public NikitaForm getContent(){
		return content;
	}	
	public  void setContent(NikitaForm content){
		this.content=content;
	}
	
	private NikitaControler header;
	public  void setHeader(NikitaControler header){
		this.header=header;
	}
	
	
	private Hashtable virtual = new Hashtable(){
		public synchronized Object get(Object key) {
			if (String.valueOf(key).equals("@ninfo")) {
				int i = TimeZone.getDefault().getRawOffset()/1000;
				Nset ld = Nset.newObject();
				ld.setData("date", Utility.Now());
				ld.setData("time", new Date().getTime());
				ld.setData("timezone", i/60/60);
				ld.setData("utc", i/60);
				
				
				
				Nset r = Nset.newObject();
				r.setData("height",  AppNikita.getInstance().getResources().getDisplayMetrics().heightPixels);
				r.setData("width",   AppNikita.getInstance().getResources().getDisplayMetrics().widthPixels);
				r.setData("localdate", ld);
				r.setData("version", String.valueOf(AppNikita.getInstance().getVirtual("@+VERSION")));
				
				return r.toJSON();
			}
			return super.get(key);
		};
	};
	 
	private Nset param ;
	public String getParameter(String key){
        if (param!=null) {
        	if (param.containsKey(key)) {
        		return param.getData(key).toString();
			}        	
		}
        if (args.containsKey(key)) {
        	return args.getData(key).toString();
		}
        return "";
	}
	public void updateParameter(String key, String data){
        if (param==null) {
        	param=Nset.newObject();
		}
        param.setData(key, data);
	}
	 
	public boolean expression;
	public void setInterupt(boolean b){
	   setVirtualRegistered("@+INTERUPT", b?"true":"false");
	}
	public void setLogicClose(boolean b){
		getVirtualString("@+INTERUPT").equals("true");
	}
	
	public boolean isInterupted(){
	   return getVirtualString("@+INTERUPT").equals("true"); 
	}
	private int irow = 0;
    private int lcount = 0;
    public void setCurrentRow(int i){
        irow=i;
    }
    public void setLoopCount(int m){
        lcount=m;
    }    
    public int getCurrentRow(){
        return irow;
    }
    public int getLoopCount(){
        return lcount;
    }
    public Component getComponent(String key){
        if (key.contains("[")) {
            key=key.substring(0,key.indexOf("["));
        }
        Component comp=null;        
        if (getContent()==null) {
        	comp=null;
        }else if (key.startsWith("$#")) {
            comp = getContent().findComponentbyId(key.substring(2));
        }else if (key.startsWith("$")) {  
            comp = getContent().findComponentbyName(key.substring(1));
        }
        
        
        if (header!=null &&  header.getContent()!=null && comp==null) {
        	if (key.startsWith("$#")) {
                comp = header.getContent().findComponentbyId(key.substring(2));
            }else if (key.startsWith("$")) {  
                comp = header.getContent().findComponentbyName(key.substring(1));
            }
        }        
        return comp!=null?comp:new Component(this);
        
    }
    private String getCookieOrSessionKey(String key){
        if (key.startsWith("@+SESSION")) {
            if (key.startsWith("@+SESSION.")) {
                key = key.substring(10);
            }else if (key.startsWith("@+SESSION-")) {
                key = key.substring(10);
            }else{
                key = key.substring(2);
            }  
        }else if (key.startsWith("@+COOKIE")) {  
            if (key.startsWith("@+COOKIE.")) {
                key = key.substring(9);
            }else if (key.startsWith("@+COOKIE-")) {
                key = key.substring(9);
            }else{
                key = key.substring(2);
            }       
        }          
        return  key;
    }
    public void setVirtualRegistered(String key, Object data){
        if (key.startsWith("@+SESSION")) {
        		String sessionname = "@+SESSION-" + getCookieOrSessionKey(key);
        	    Utility.getSetting(AppNikita.getInstance(), sessionname, String.valueOf(data) );      
        	    return;
        }else if (key.startsWith("@+COOKIE")) {
        	 	Utility.getSetting(AppNikita.getInstance(), getCookieOrSessionKey(key), String.valueOf(data) );      
        	 	return;
        }else if (key.startsWith("@+")||key.startsWith("@-")) {
        		AppNikita.getInstance().setVirtual(key, data);  
        }
    }
    public void setVirtual(String key, Object data){
        if (key.equals("")) {
        }else if (key.startsWith("@+SESSION")||key.startsWith("@+COOKIE") ) {
        	setVirtualRegistered(key, data);
            return;
        }else  if (key.startsWith("@+STATIC-")||key.startsWith("@+LOCAL-")) {
            Utility.setSetting(AppNikita.getInstance(), key, String.valueOf(data) ); 
            return;
        }else if (key.startsWith("@+")||key.startsWith("@-")) {
        	setVirtualRegistered(key, data);
            return;
        }else  if (key.startsWith("@")||key.startsWith("!")) {
            virtual.put(String.valueOf(key).toLowerCase().trim(), data);  
        }        
    }  
    
	private Object getVirtualStream(String key){
		String var = key;
		if ( key.startsWith("!") ) {
            if (!key.contains("[")) {
                key=key+"[\"data\",0,\""+key.substring(1).trim()+"\"]";
            }else if (key.contains("[")&& key.contains("[")) {
                String index = key.substring(key.indexOf("[")+1);
                key=key.substring(0,key.indexOf("["));
                if (index.contains("]")) {
                    index=index.substring(0,index.indexOf("]"));
                }
                key=key+"[\"data\","+index+",\""+key.substring(1).trim()+"\"]";
            }
        }
		
		if ( key.startsWith("@#") ) {
            return key.substring(2);
		}else if (key.startsWith("@+FUNCTION")) {    
			return NikitaFunction.evaluate(key, new NikitaFunction.OnVariableListener() {
				public Object getValue(String var) {					
					return getVirtual(var);
				}
			});
		}else if (key.startsWith("@+CORE")) {    
			return AppNikita.getInstance().getVirtual(key);
		 }else if (key.startsWith("@+SETTING-")) {
			return Utility.getSetting(AppNikita.getInstance(), key.substring(10), ""); 
        }else if (key.startsWith("@+SESSION")) {
              String sessionname = "@+SESSION-" + getCookieOrSessionKey(key);
              return Utility.getSetting(AppNikita.getInstance(), sessionname, "");           
        }else  if (key.startsWith("@+STATIC-")||key.startsWith("@+LOCAL-")) {
        	return Utility.getSetting(AppNikita.getInstance(), key, "");               
        }else if (key.startsWith("@+COOKIE")) {    
        	  return Utility.getSetting(AppNikita.getInstance(), getCookieOrSessionKey(key), "");       
        }else if (key.startsWith("@+CHECKEDROWS")) {  
            /*
            key=key.substring(13);
            if (key.startsWith(".")) {
                key=key.substring(1);
            }
            if (key.startsWith("[")) {
                key=key.substring(1);
            }
            if (key.endsWith("]")) {
                key=key.substring(0, key.length()-1);
            }
            if (key.startsWith("$#")) {
                Component cm = getContent().findComponentbyId(key.substring(2));
                if (cm instanceof Tablegrid) {                    
                    return ((Tablegrid)cm).getRowChecked();
                }
            }else if (key.startsWith("$")) {
                Component cm = getContent().findComponentbyName(key.substring(1));
                if (cm instanceof Tablegrid) {                    
                    return ((Tablegrid)cm).getRowChecked();
                }
            }else if (key.length()>=2) {
                Component cm = getContent().findComponentbyId(key);
                if (cm instanceof Tablegrid) {                    
                    return ((Tablegrid)cm).getRowChecked();
                }    
            }else if (key.equals("")) {
                for (int i = 0; i < getContent().getComponentCount(); i++) {
                    if (getContent().getComponent(i) instanceof Tablegrid) {                    
                        return ((Tablegrid)getContent().getComponent(i)).getRowChecked();
                    }
                }     
            }
            */
        }else if (key.equals("@+FORMINDEX")||key.equals("@+ENGINE_FORMINDEX")) {
            return NikitaEngine.getFormIndex(formname);
        }else if (key.equals("@+ENGINE_FORMNAME")) {
            return formname;   
        }else if (key.startsWith("@+MOBILEARGSDATA")) {    
			return new Nset(AppNikita.getInstance().getArgsData());
        }else if (key.equals("@+COREMYSQL")) {
            return NikitaConnection.CORE_MYSQL;
        }else if (key.equals("@+COREORACLE")) {
            return NikitaConnection.CORE_ORACLE;
        }else if (key.equals("@+CORESQLSERVER")) {
            return NikitaConnection.CORE_SQLSERVER;
        }else if (key.equals("@+CORESQLITE")||key.equals("@+CORESQLLITE")) {
            return NikitaConnection.CORE_SQLITE; 
        }else if (key.equals("@+BUTTON1")) {
            return "button1";
        }else if (key.equals("@+BUTTON2")) {
            return "button2";
        }else if (key.equals("@+BASEURL")||key.equals("@+BASE")) {
            return AppNikita.getInstance().getBaseUrl();            
        }else if (key.startsWith("@+BUTTONGRID")) {
            try {
                String sact = getParameter("action");
                if (sact.startsWith("item-")) {
                    sact=sact.substring(5);
                    if (sact.contains("-")) {
                        sact=sact.substring(sact.indexOf("-")+1);
                    }else{
                        sact="";
                    }
                }else{
                    sact="";
                }
                return sact;    
            } catch (Exception e) {}
            return "" ;
        }else if (key.equals("@+ENTER")) {
            return "\r\n";
        }else if (key.equals("@+SPACE")) {
            return " ";
        }else if (key.equals("@+SPACE32")) {
            return "                                ";
        }else if (key.equals("@+TAB")) {
            return "\t";
        }else if (key.equals("@+EMPTYSTRING")) {
            return "";
        }else if (key.equals("@+VERSION")) {
            return "MOBILE 1.0.13 Beta";//MOBILE DEKSTOP
        }else if (key.equals("@+FORMTITLE")) { 
        	
        	return virtual.get(String.valueOf(key).toLowerCase().trim());
        }else if (key.equals("@+FORMNAME")) {         
        	return virtual.get(String.valueOf(key).toLowerCase().trim());
        }else if (key.equals("@+DEVICEOS")) {
              
            return "ANDROID";    
        }else if (key.equals("@+DEVICENAME")) {  
            
            return "MOBILE-ANDROID" ;
        }else if (key.equals("@+DEVICEINFO")) {
        	return "MOBILE-ANDROID" ;
        }else if (key.equals("@+NOW")) {
            return Utility.Now();
        }else if (key.equals("@+TIME")) {
            return System.currentTimeMillis();    
        }else if (key.equals("@+RANDOM")) {
            StringBuffer sb = new StringBuffer();
            Random randomGenerator = new Random(System.currentTimeMillis());
            for (int idx = 1; idx <= 16; ++idx){
                sb.append( randomGenerator.nextInt(100)  );
            }
            return sb.toString();
        }else if (key.equals("@+FILESEPARATOR")) {    
            return  "/";
        }else if (key.equals("@+UNIQUE")) {    
            return  System.currentTimeMillis()+"";
        }else if (key.startsWith("$")) {
            if (getContent()!=null) {
                Component comp ; String ky = "";
                if (key.contains("[")&& key.contains("]")) {
                    ky=key.substring( key.indexOf("["));
                    key=key.substring(0,  key.indexOf("["));
                    
                    ky=Utility.replace(ky, "[", "");
                    ky=Utility.replace(ky, "]", "");
                    ky=Utility.replace(ky, "\"", "");
                }
                
                ky=ky.trim();
                key=key.trim();
                        
                if (key.startsWith("$#")) {
                    comp = getContent().findComponentbyId(key.substring(2));
                }else{   
                    comp = getContent().findComponentbyName(key.substring(1));
                }
                
                if (comp==null) {
                    comp= new Component(this);
                }
                
                if (ky.equals("tag")) {
                    return  comp.getTag();
                }else if (ky.equals("visible")) {
                    return comp.isVisible()?"true":"false";
                }else if (ky.equals("enable")) {
                    return comp.isEnable()?"true":"false";   
                }else if (ky.equals("id")) {
                    return comp.getId();  
                }else if (ky.equals("name")) {
                    return comp.getName();  
                }else if (ky.equals("data")) {
                    return comp.getData()!=null?comp.getData():Nset.newObject(); //new  
                }else if (ky.equals("datajson")) {
                    return comp.getData()!=null?comp.getData().toJSON():"";  
                }else if (ky.equals("datanset")) {
                    return comp.getData()!=null?comp.getData():Nset.newObject();        
                }else if (ky.equals("style")) {
                    return comp.getViewStyle();  
                }else if (ky.equals("class")) {
                    return comp.getViewClass();
                }else if (ky.equals("attribut")) {
                    return comp.getViewAttribut();
                }else if (ky.equals("gps")) {
                	return comp.getExtraTag("gps");
                }
                return comp.getText();
            }
        }else if (key.startsWith("@+")||key.startsWith("@-")) {
        	 return AppNikita.getInstance().getVirtual(key);
        }else if (key.startsWith("@")||key.startsWith("!")) {
            String stream ="";
            if ( key.contains("[")) {                
                stream=key.substring( key.indexOf("["));
                key=key.substring(0,  key.indexOf("["));
            } 
             
            key=key.trim();
            stream=stream.trim();
            Object obj = virtual.get(String.valueOf(key).toLowerCase().trim());
            if (key.equals("@")) {
            	obj = "";
			}            
            if (obj==null) {
                try {
                    return getParameter(key.substring(1));    
                } catch (Exception e) {} 
            }  else if (obj instanceof  Nson && !stream.equals("")) {
                stream=runArrayStream(stream);
                return ((Nson)obj).get(stream);
            }  else if (obj instanceof  Nset && !stream.equals("")) {
                stream=runArrayStream(stream);
                return ((Nset)obj).get(stream);
            }  else if (obj instanceof  Nikitaset && !stream.equals("")) {
                stream=runArrayStream(stream);
                return ((Nikitaset)obj).getStream(stream);
            }  else if (obj instanceof  String && !stream.equals("")) {
                stream=runArrayStream(stream);
                return StringAction.getStringStream(((String)obj), stream);
            }          
            return obj;        
        }else if (key.startsWith("&")) {
            try {
                return getParameter(key.substring(1));    
            } catch (Exception e) {}
        }else if (key.startsWith("!")||key.startsWith("'")) {
            return key.substring(1);
        }
        return var;
	}
	private Nset fillStreamNset(Nset stream){
        if (stream.isNsetArray()) {
            Nset out = Nset.newArray();
            for (int i = 0; i < stream.getArraySize(); i++) {
                out.addData(  fillStreamNset(stream.getData(i))  );                
            }
            return out;
        }else if (stream.isNsetObject()) {
            Nset out = Nset.newObject();
            String[] keys = stream.getObjectKeys();
            for (int i = 0; i < keys.length; i++) {
                out.setData(keys[i], fillStreamNset(stream.getData(keys[i])) );
            }
            return out;
        }else{
            if (stream.toString().startsWith("@")||stream.toString().startsWith("$")||stream.toString().startsWith("!")) {
                Object obj = getVirtual(stream.toString());
                if (obj instanceof Nset) {
                    obj =  ((Nset)obj)  ;                        
                }else if (obj instanceof String) {
                    obj =  (String)obj  ;
                }else if (obj instanceof Double) {
                    obj =    (Double)obj  ;
                }else if (obj instanceof Integer) {
                    obj =   (Integer)obj  ;
                }else if (obj instanceof Long) {
                    obj =  (Long)obj  ;
                }else if (obj instanceof Boolean) {
                    obj =  (Boolean)obj  ;
                }else if (obj instanceof Vector) {  
                    obj = (((Vector)obj))   ;   
                }else if (obj instanceof Hashtable) {  
                    obj =  (((Hashtable)obj))  ;
                }else{
                    obj = "";
                }       
                return new Nset(obj);
            }else {  
                return stream;
            }     
        }        
    }
	private String runArrayStream(String stream){
        if (stream.contains("@")||stream.contains("$")||stream.contains("!")) {
            Nset n = Nset.readJSON(stream);
            Nset out = Nset.newArray();
            for (int i = 0; i < n.getArraySize(); i++) {
                if (n.getData(i).toString().startsWith("@")||n.getData(i).toString().startsWith("$")||n.getData(i).toString().startsWith("!")) {
                    Object obj = getVirtual(n.getData(i).toString());
                    if (obj instanceof Nset) {
                        out.addData(  (Nset)obj  );                        
                    }else if (obj instanceof String) {
                        out.addData(  (String)obj  );
                    }else if (obj instanceof Double) {
                        out.addData(  (Double)obj  );
                    }else if (obj instanceof Integer) {
                        out.addData(  (Integer)obj  );
                    }else if (obj instanceof Long) {
                        out.addData(  (Long)obj  );
                    }else if (obj instanceof Boolean) {
                        out.addData(  (Boolean)obj  );
                    }else if (obj instanceof Vector) {  
                        out.addData(  (Vector)obj  );   
                    }else if (obj instanceof Hashtable) {  
                        out.addData(  (Hashtable)obj  );
                    }else if (obj == null) {  
                        out.addData(  n.getData(i).toString()  );
                    }else{
                        out.addData(  obj.toString()  );
                    }
                }else {  
                    out.addData(n.getData(i).toString());
                }                
            }      
            return out.toJSON();
        }
        return stream;
    }
	public Object nsonCodeScript(String key){
		if (key.startsWith("@+NSON-CODE")||key.startsWith("@+NSON-SCRIPT")) {
            //cari ( sampai }
            String arr = "";
            String exp = "";
            int i = key.indexOf("]");
            
            if (i>=10) {
                arr = key.substring(11, i);
                exp = key.substring(i+1);
            }
            arr = key.substring(key.startsWith("@+NSON-CODE")?11:13, i);
            if (arr.startsWith("@")||arr.trim().startsWith("$")) {
                exp = getVirtualString(arr);
            }
            if (exp.trim().length()>=1) {
            	 HashMap var =  new HashMap(){
                     public Object put(Object key, Object value) {
                    	 String s = String.valueOf(key);
                    	 if (s.startsWith("$")) {
                             getComponent(s).setText( String.valueOf(value));
                         }else{
                             setVirtual(String.valueOf(key), value);
                         }   
                         return true;
                     }
                     public Object get(Object key) {
                         return getVirtual(String.valueOf(key)); 
                     }                    
                 };
                 if (key.startsWith("@+NSON-CODE")) {
                     return new NikitaExpression().expression(exp, var);
                 }else{
                     new NikitaExpression().evaluate(exp, var);
                     return key;
                 }
            }          
        }              
        return null;
    }
	public Object nsonExpression(String exp){
		HashMap var =  new HashMap(){
            public Object put(Object key, Object value) {
                String s = String.valueOf(key);
                if (s.startsWith("$")) {
                    getComponent(s).setText( String.valueOf(value));
                }else{
                    setVirtual(String.valueOf(key), value);
                }                     
                return true;
            }
            public Object get(Object key) {
                return getVirtual(String.valueOf(key)); 
            }                    
        };
        return new NikitaExpression().expression(exp, var);
	}
	public Object nsonEvaluateScript(String exp){
		HashMap var =  new HashMap(){
            public Object put(Object key, Object value) {
                String s = String.valueOf(key);
                if (s.startsWith("$")) {
                    getComponent(s).setText( String.valueOf(value));
                }else{
                    setVirtual(String.valueOf(key), value);
                }                     
                return true;
            }
            public Object get(Object key) {
                return getVirtual(String.valueOf(key)); 
            }                    
        };
        new NikitaExpression().evaluate(exp, var);
        return "";
	}
	public Object getVirtual(String key){
		String name = key;
		if ( (key.startsWith("@[") && key.endsWith("]") ) || (key.startsWith("@{") && key.endsWith("}"))) {
			return Nset.readJSON(key.substring(1));
		} else if (key.startsWith("@#")) {
            return key.substring(2);
		}
		 
		
		String f = "";
        if ( (key.startsWith("!") && key.contains("(") ) || (key.startsWith("@") && key.contains("(") ) || (key.startsWith("$") && key.contains("("))) {
            f = key.substring(key.lastIndexOf("(")+1);//yang terakhir
            key=key.substring(0, key.lastIndexOf("("));
            if (f.contains(")")) {
                f=f.substring(0,f.indexOf(")"));
            }
        }   
        f=f.trim();
         
       //NSON EXPRESSION  @+NSON-EXP ()
        Object nsonObject = nsonCodeScript(  key) ;
        if (nsonObject!=null) {
             return nsonObject;
        }
        
        if (key.startsWith("@?")) {
        	 if (name.contains("(")) {
                 name=name.substring(0, name.lastIndexOf("(")); 
             }
             key = "@#"+ name.substring(2);          
        }        
        
        Object reObject = getVirtualStream(key);
        if (f.startsWith("$")) {//"$"
            //new function
            Component component = getComponent(f);
            if (component instanceof Function) {
                //component.setOn               
                Function function = ((Function)component);
                if (reObject instanceof Nikitaset) {
                    function.setText("");
                }else if (reObject instanceof Nset) {
                    function.setText("");
                }else if (reObject instanceof String) { 
                    function.setText((String)reObject);
                }else if (reObject instanceof Long) {   
                    function.setText("");
                }else if (reObject instanceof Integer) {
                    function.setText("");
                }else if (reObject instanceof Double) {  
                    function.setText("");
                }else if (reObject instanceof Boolean) {  
                    function.setText("");
                }else{
                    function.setText("");
                }                
                function.runRouteAction("function");
                return function.getText();
            }                    
        }else if (f.equals("abs")) {
        	return Math.abs(Utility.getNumber(reObject).doubleValue());
        }else if (f.equals("floor")) {
        	return Math.floor(Utility.getNumber(reObject).doubleValue());
        }else if (f.equals("round")) {
        	return Math.round(Utility.getNumber(reObject).doubleValue());
        }else if (f.equals("ceil")) {
        	return Math.ceil(Utility.getNumber(reObject).doubleValue());	
        }else if (f.equals("cbrt")) {
        	return Math.cbrt(Utility.getNumber(reObject).doubleValue());
        }else if (f.equals("ulp")) {
        	return Math.ulp(Utility.getNumber(reObject).doubleValue());
        }else if (f.equals("integer")||f.equals("int")||f.equals("inc")||f.equals("dec")) {
            int inc = f.equals("inc")?1:0;  inc = f.equals("dec")?-1:inc;
            return Utility.getNumber(reObject).intValue()+inc;
        }else if (f.equals("curr")||f.equals("fcurrview")) {              
            return Utility.formatCurrency( String.valueOf(reObject) ) ;
        }else if (f.equals("numonly")) {
        	return Utility.getNumberOnly( String.valueOf(reObject) ) ;
        }else if (f.equals("numpointonly")) {
        	return Utility.getNumberPointOnly(String.valueOf(reObject) ) ;
        }else if (f.equals("num")||f.equals("fnumview")) { 
            if (f.equals("fnumview")) {
                return Utility.formatNumber(String.valueOf(reObject));
            }else if (String.valueOf(reObject).contains(".")) {
                return Utility.getNumber(Utility.formatNumber(String.valueOf(reObject))).doubleValue();
            }                    
            return Utility.getNumber(Utility.formatNumber(String.valueOf(reObject))).longValue();
        }else if (f.equals("long")) { 
        	return Utility.getNumber(reObject).longValue();
        }else if (f.equals("double")||f.equals("single")||f.equals("float")) { 
        	return Utility.getNumber(reObject).doubleValue();
        }else if (f.equals("decimal")||f.equals("fdecimal")||f.startsWith("fdecimal")) { 
        	String result = BigDecimal.valueOf( Utility.getNumber(reObject).doubleValue() ).toPlainString();
        	if (f.equals("fdecimal")) {         		
        	}else if (f.equals("decimal")) { 
        		if (result.contains(".")) {        			
        			return Utility.getNumber( result.substring(result.indexOf(".")+1)  ).longValue() ;
        		}else{
        			return 0 ;
        		}
        	}else{			
				int sig = Utility.getInt(f.substring(8)) ;
				String num = result;
				String dec = "";
				if (result.contains(".")) {
					num = result.substring(0, result.indexOf("."));
					dec = result.substring(result.indexOf(".")+1);
				}
				StringBuilder stringBuilder = new StringBuilder(num);
				stringBuilder.append(".");
				for (int i = 0; i < sig; i++) {
					if (dec.length()>i) {
						stringBuilder.append(dec.substring(i,i+1));
					}else{
						stringBuilder.append("0");
					}
				}
				result = stringBuilder.toString();
			}
        	return  result;
        }else if (f.equals("name")) {
            return key;
        }else if (f.equals("exec")) {
        	String key2 = getVirtualString(key);
            if (key2.startsWith("@")||key2.startsWith("$")) {
                return getVirtual(key2);
            }
            return "";        
        }else if (f.equals("boolean")) {
            if (reObject instanceof  Boolean) {
                return (Boolean) reObject;
            }else {
                return  Boolean.valueOf(String.valueOf(reObject));
            }  
        }else if (f.equals("nsonscript")) { 
            return nsonEvaluateScript(String.valueOf(reObject));
        }else if (f.equals("expression")) { 
            return nsonExpression(String.valueOf(reObject));
        }else if (f.equals("nson")) {
            if (reObject instanceof Nikitaset) {
                return Nson.readNset( ((Nikitaset)reObject).toNset() );
            }else if (reObject instanceof Nset) {   
                return Nson.readNset( (Nset)reObject );
            }else{
                return Nson.readNson(String.valueOf(reObject), new Nson.OnVariableListener() {
                    public Object get(String name, boolean singlequote, boolean doublequote) {
                        if (singlequote) {
                            return NikitaControler.this.getVirtualString(name);
                        }else if (!singlequote && !doublequote) {
                            return NikitaControler.this.getVirtualString(name);
                        }else{
                            return name;
                        }                                
                    }
                });
            }
        }else if (f.equals("json")) {
            if (reObject instanceof String) {
                return  Nset.newArray().addData((String)reObject).toJSON();
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).toNset().toJSON() ;
            }else if (reObject instanceof Nset) {
                return  ((Nset)reObject).toJSON() ;
            }else if (reObject instanceof Nson) {
                return  ((Nson)reObject).toJson();
            }
        }else if (f.equals("arrayview")||f.equals("arraydb")) {
        	Nset v = null;
            if (reObject instanceof Nikitaset) {
            	v =((Nikitaset)reObject).toNset() ;
            }else if (reObject instanceof Nset) {
            	v =((Nset)reObject);
            }else if (String.valueOf(reObject).trim().startsWith("[")){
                v = Nset.readJSON(String.valueOf(reObject));
            }
            if (f.equals("arrayview")) {
            	if (v!=null) {
                	StringBuilder builder = new StringBuilder();
                	for (int i = 0; i < v.getArraySize(); i++) {
                		builder.append(i>=1?",":"").append(v.getData(i).toString());
        			}    
                	return builder.toString();
    			}else{
    				return "";
    			}
			}else{
				if (v!=null) {
	            	StringBuilder builder = new StringBuilder("(");
	            	for (int i = 0; i < v.getArraySize(); i++) {
	            		builder.append(i>=1?",":"").append("'").append(Component.escapeSql(v.getData(i).toString())).append("'");
	    			}    
	            	builder.append(")");
	            	return builder.toString();
				}else{
					return "()";
				}
			}
        }else if (f.equals("arrayjson")) {
            if (reObject instanceof Nikitaset) {
                return ((Nikitaset)reObject).toNset().toJSON() ;
            }else if (reObject instanceof Nset) {
                return ((Nset)reObject).toJSON() ;
            }else if (String.valueOf(reObject).contains(",")){
                return new Nset(Utility.splitVector(String.valueOf(reObject), ",")).toJSON();
            }else{
            	return Nset.newArray().addData(String.valueOf(reObject)).toJSON();
            }
        }else if (f.equals("arrayselected")) {
        	if (reObject instanceof Nikitaset) {
        		Nset n = ((Nikitaset)reObject).toNset().getData("data");            	
            	Nset b = Nset.newArray();
            	for (int i = 0; i < n.getArraySize(); i++) {
					b.addData(n.getData(i).getData(0).toString());
				}
                return b.toJSON() ;
            }else if (reObject instanceof Nset) {
            	Nset n = ((Nset)reObject);
            	Nset b = Nset.newArray();
            	for (int i = 0; i < n.getArraySize(); i++) {
					b.addData(n.getData(i).getData(0).toString());
				}
                return b.toJSON() ;
            }
        	return "[]";
        }else if (f.equals("csv")||f.equals("comma")) {
            if (reObject instanceof String) {
                return  Nset.readJSON((String)reObject).toCsv();
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).toNset().toCsv() ;
            }else if (reObject instanceof Nset) {
                return  ((Nset)reObject).toCsv() ;
            }  
        }else if (f.equals("quote")||f.equals("string")||f.equals("lcase")||f.equals("ucase")) { 
        	 String buffer = "";
            if (reObject instanceof Nikitaset) {
                 buffer = ((Nikitaset)reObject).toNset().toString() ;
            }else if (reObject instanceof Nset) {
                 buffer =  ((Nset)reObject).toString() ;
             }else{
                 buffer = String.valueOf(reObject);
            }  
             if (f.equals("lcase")) {  
                 return buffer.toLowerCase();
             }else if (f.equals("ucase")) {  
                 return buffer.toUpperCase();
             }else if (f.equals("quote")) {  
                 return "\""+buffer+"\"";
             }            
             return buffer;
        }else if (f.equals("trim")) {  
            if (reObject instanceof String) {
                return  ((String)reObject).trim();
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).toNset().toString().trim() ;
            }else if (reObject instanceof Nset) {
                return  ((Nset)reObject).toString().trim() ;
            }else{
                return  String.valueOf(reObject).trim();
            }
        }else if (f.startsWith("escape")||f.startsWith("unescape")||f.startsWith("encode")||f.startsWith("decode")) {  
            String buffer = "";
            if (reObject instanceof String) {
                buffer =  StringEscapeUtils.escapeSql((String)reObject );
            }else if (reObject instanceof Nikitaset) {
                buffer =  StringEscapeUtils.escapeSql(((Nikitaset)reObject).toNset().toString()) ;
            }else if (reObject instanceof Nset) {
                buffer =  StringEscapeUtils.escapeSql(((Nset)reObject).toString()) ;
            }   
            if (f.equals("escapesql")) {
               return  StringEscapeUtils.escapeSql(buffer) ; 
            }else if (f.equals("escapehtml")) {
               return  StringEscapeUtils.escapeHtml(buffer) ; 
            }else if (f.equals("escapejs")) {
               return  StringEscapeUtils.escapeJavaScript(buffer) ; 
            }else if (f.equals("escapejava")) {
               return  StringEscapeUtils.escapeCsv(buffer) ; 
            }else if (f.equals("escapejava")) {
                return  StringEscapeUtils.escapeCsv(buffer) ;    
            }else if (f.equals("escapecsv")) {
               return  StringEscapeUtils.escapeJava(buffer) ; 
            }else if (f.equals("escapejson")) {
            	 String s = buffer;
                 s = String.valueOf(Nset.newArray().addData(s)).trim();   
                 s = s.substring(1,s.length()-1).trim();
                 if (s.startsWith("\"") && s.endsWith("\"")) {
                     s = s.substring(1,s.length()-1);
                 }      
                return  s ;       
            //==============================================//
            }else if (f.equals("unescapehtml")) {
               return  StringEscapeUtils.unescapeHtml(buffer) ; 
            }else if (f.equals("unescapejs")) {
               return  StringEscapeUtils.unescapeJava(buffer) ; 
            }else if (f.equals("unescapejava")) {
               return  StringEscapeUtils.unescapeJavaScript(buffer) ; 
            }else if (f.equals("unescapecsv")) {
               return  StringEscapeUtils.unescapeCsv(buffer) ; 
            }else if (f.equals("unescapejson")) {
            	return Nset.readJSON("["+buffer+"]").getData(0).toString(); 
            //==============================================//
            }else if (f.equals("encodeurl")) {
               return URLEncoder.encode( buffer   )   ; 
            }else if (f.equals("decodeurl")) {
                return URLDecoder.decode(buffer   )   ; 
            }else if (f.equals("encodebase64")) {                 
               return  Base64Coder.encodeString(buffer)  ; 
            }else if (f.equals("decodebase64")) {
               return   Base64Coder.decodeString(buffer)  ; 
            }
        }else if (f.startsWith("expression")) {  
            if (getVirtualString("@"+f).equalsIgnoreCase("true")) {
                return reObject;
            }else{
                return "";
            }  
        }else if (f.equals("md5")) {  
            if (reObject instanceof String) {
                return  Utility.MD5((String)reObject);
            }else if (reObject instanceof Nikitaset) {
                return  Utility.MD5(((Nikitaset)reObject).toNset().toString()) ;
            }else if (reObject instanceof Nset) {
                return  Utility.MD5(((Nset)reObject).toString()) ;
            }else{
            	Utility.MD5( String.valueOf(reObject) );
            }
        }else if (f.equals("nset")) {
            if (reObject instanceof String) {
                return  Nset.readJSON((String)reObject);
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).toNset() ;
            }else if (reObject instanceof Nset) {
                return   ((Nset)reObject)  ;
            }else if (reObject instanceof Nson) {
                return   ((Nson)reObject).toNset()  ;
            }else{
            	return  Nset.readJSON( String.valueOf(reObject) );
            }
        }else if (f.equals("filltonset")) {            
            if (reObject instanceof String) {
                return   fillStreamNset(Nset.readJSON((String)reObject));
            }else if (reObject instanceof Nikitaset) {
                return   fillStreamNset(((Nikitaset)reObject).toNset()) ;
            }else if (reObject instanceof Nset) {
                return   fillStreamNset((Nset)reObject)  ;
            }        
        }else if (f.equals("nikitaset")) {
            if (reObject instanceof String) {
                Nset n = Nset.readJSON((String)reObject);
                if (Nikitaset.isNikitaset(n)) {
                    return new Nikitaset(n);
                }else{
                    return  n;
                }
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).toNset() ;
            }else if (reObject instanceof Nset) {
                Nset n =  ((Nset)reObject) ;
                if (Nikitaset.isNikitaset(n)) {
                    return new Nikitaset(n);
                }else{
                    return  n;
                }
            }else if (reObject instanceof Nson) {
                Nson n =  ((Nson)reObject) ;
                if (Nikitaset.isNikitaset(n.toNset())) {
                    return new Nikitaset(n.toNset());
                }else{
                    return  n;
                }
            }else{
            	return  new Nikitaset(  Nset.readJSON( String.valueOf(reObject) ) ) ;
            }
        }else if (f.equals("nhtml")) {        	
            return Utility.nhtmlNikitaParse(String.valueOf(reObject), new Utility.NikitaParse() {
                public Object getVirtual(String name) {
                    return NikitaControler.this.getVirtual(name);
                }
            });  
        }else if (f.equals("njson")) {        	
            return Utility.nhtmlNikitaParse(String.valueOf(reObject), new Utility.NikitaParse() {
                public Object getVirtual(String name) {
                    String s =  "null";
                    Object buff = NikitaControler.this.getVirtual(name);
                    if (buff == null) {
                    }else if (buff instanceof Boolean) {
                        s = String.valueOf(buff);
                    }else if (buff instanceof Number) {
                        s = String.valueOf(buff);
                    }else if (buff instanceof Nset) {
                        s = ((Nset)buff).toJSON();
                    }else if (buff instanceof Nson) {
                        s = ((Nson)buff).toJson();    
                    }else if (buff instanceof Nikitaset) {
                        s = ((Nikitaset)buff).toNset().toJSON();
                    }else if (buff instanceof Vector||buff instanceof Hashtable) {  
                        s = new Nset (buff).toJSON();                        
                    }else{
                        if (name.trim().endsWith("(ustring)")) {
                            s = String.valueOf(buff);
                            s = String.valueOf(Nset.newArray().addData(s)).trim();   
                            s = s.substring(1,s.length()-1);    
                            if (s.startsWith("\"")&& s.endsWith("\"")) {
                                s = s.substring(1,s.length()-1);    
                            }
                        }else if (name.trim().endsWith("(nstring)")) {
                            s = String.valueOf(buff);    
                        }else{ //include qstring
                            s = String.valueOf(buff);
                            s = String.valueOf(Nset.newArray().addData(s)).trim();   
                            s = s.substring(1,s.length()-1);    
                        }                                                      
                    }     
                    return  s;
                }
            });
        }else if (f.equals("nujson")) {        	
            return Utility.nhtmlNikitaParse(String.valueOf(reObject), new Utility.NikitaParse() {
                public Object getVirtual(String name) {
                    String s =  "null";
                    Object buff = NikitaControler.this.getVirtual(name);
                    if (buff == null) {
                    }else if (buff instanceof Boolean) {
                        s = String.valueOf(buff);
                    }else if (buff instanceof Number) {
                        s = String.valueOf(buff);
                    }else if (buff instanceof Nset) {
                        s = ((Nset)buff).toJSON();
                    }else if (buff instanceof Nikitaset) {
                        s = ((Nikitaset)buff).toNset().toJSON();
                    }else if (buff instanceof Vector||buff instanceof Hashtable) {  
                        s = new Nset (buff).toJSON();                        
                    }else{
                        if (name.trim().endsWith("(qstring)")) {
                            s = String.valueOf(buff);
                            s = String.valueOf(Nset.newArray().addData(s)).trim();   
                            s = s.substring(1,s.length()-1);    
                        }else if (name.trim().endsWith("(nstring)")) {
                            s = String.valueOf(buff);                          
                        }else{//include ustring
                            s = String.valueOf(buff);
                            s = String.valueOf(Nset.newArray().addData(s)).trim();   
                            s = s.substring(1,s.length()-1);                           
                            if (s.startsWith("\"")&& s.endsWith("\"")) {
                                s = s.substring(1,s.length()-1);    
                            }
                        } 
                    }     
                    return  s;
                }
            });
        }else if (f.equals("type")||f.equals("ntype")) {        	
            if (reObject instanceof String) {
                return  "string";
            }else if (reObject instanceof Nikitaset) {
                return  "nikitaset" ;
            }else if (reObject instanceof Nset) {
                return  "nset"  ;
            }else if (reObject instanceof Nson) {
            	if (f.equals("ntype")) {
            		return "nson-" + ((Nson)reObject).asType();
				}else{
					return  "nson" ;  
				}                  
            }else if (reObject instanceof Integer) {
                return  "integer"  ;
            }else if (reObject instanceof Long) {
                return  "long"  ;
            }else if (reObject instanceof Double) {
                return  "double"  ;     
            }else {
                return  ""  ;     
            }
        }else if (f.equals("newarray")) {
            Nset n =Nset.newArray();
            setVirtual(key, n);
            return n;
        }else if (f.equals("newobject")) { 
            Nset n =Nset.newObject();
            setVirtual(key, n);
            return n;
        }else if (f.equals("newstring")) { 
            setVirtual(key, "");
            return "";
        }else if (f.equals("newint")||f.equals("newlong")||f.equals("newfloat")||f.equals("newdecimal")||f.equals("newdouble")) { 
            setVirtual(key, 0);
            return 0;
        }else if (f.equals("fdate")||f.equals("fdatenumber")||f.equals("fdateint")||f.equals("fdatelong")) {
            if (reObject instanceof String) {
                return Utility.getDateTime((String)reObject);
            }else if (reObject instanceof Long) {
                return  (Long)reObject;
            }
        }else if (f.equals("fdateview")) {
            if (reObject instanceof String) {
                long l = Utility.getDate((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "dd/MM/yyyy");
                }
                //return DateFormatAction.FormatDate(-1, (String)reObject, "");
            }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "dd/MM/yyyy");
                }
            }
        }else if (f.equals("fdatetimeview")) { 
            if (reObject instanceof String) {
                long l = Utility.getDateTime((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "dd/MM/yyyy HH:mm:ss");
                }
            }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "dd/MM/yyyy HH:mm:ss");
                }
            }
            //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
        }else if (f.equals("fdatedb")||f.equals("todate")) {   
            if (reObject instanceof String) {
                long l = Utility.getDate((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd");
                }
            }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd");
                }
            }
            //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
        }else if (f.equals("fdatetimedb")||f.equals("todatetime")) {   
            //yyyy-mm-dd hh:nn:ss
            if (reObject instanceof String) {
                long l = Utility.getDateTime((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd HH:mm:ss");
                }
             }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd HH:mm:ss");
                }
            }
       //return DateFormatAction.FormatDate(-1, reObject.toString(), Utility.NowTime());
        }else if (f.equals("fdatetmaxdb")||f.equals("todatetmax")) {   
            //yyyy-mm-dd hh:nn:ss
            if (reObject instanceof String) {
                long l = Utility.getDateTime((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + " 23:59:59";
                }
             }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + " 23:59:59";
                }
            }
        }else if (f.equals("fdatetnowdb")||f.equals("todatetnow")) {   
            //yyyy-mm-dd hh:nn:ss
            if (reObject instanceof String) {
                long l = Utility.getDateTime((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + Utility.Now().substring(10);
                }
             }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + Utility.Now().substring(10);
                }
            }
        }else if (f.equals("fdatetmmindb")||f.equals("todatetmin")) {   
            //yyyy-mm-dd hh:nn:ss
            if (reObject instanceof String) {
                long l = Utility.getDateTime((String)reObject);
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + " 00:00:00";
                }
             }else if (reObject instanceof Long) {
                long l = (Long)reObject;
                if (l!=0) {
                    return Utility.formatDate(l, "yyyy-MM-dd") + " 00:00:00";
                }
            }        
        }else if (f.equals("length")||f.equals("rows")) {
            if (reObject instanceof String) {
                return  ((String)reObject).length();
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).getRows();
            }else if (reObject instanceof Nset) {
                return  ((Nset)reObject).getArraySize()>=0 ?((Nset)reObject).getArraySize():((Nset)reObject).getObjectKeys().length  ;
            }else if (reObject instanceof Integer) {
                return  (""+((Integer)reObject) ).length();
            }else if (reObject instanceof Long) {
                return  (""+((Long)reObject) ).length();
            }else if (reObject instanceof Double) {
                return  (""+((Double)reObject) ).length();    
            }else {
                return  0  ;     
            }
        }else if (f.equals("error")) {
            if (reObject instanceof String) {
                return  "";
            }else if (reObject instanceof Nikitaset) {
                return  ((Nikitaset)reObject).getError();
            }else if (reObject instanceof Nset) {
                return "";
            }else{
                return  ""  ;      
            }
        }else if (f.equals("header")||f.equals("headernset")) {
            if (reObject instanceof Nikitaset) {
                return  new Nset(((Nikitaset)reObject).getDataAllHeader());
            }else{
                return  Nset.newArray() ;      
            }
        }else if (f.equals("data")||f.equals("datanset")) {
            if (reObject instanceof Nikitaset) {
                return  new Nset(((Nikitaset)reObject).getDataAllVector());
            }else{
                return  Nset.newArray()  ;      
            }
        }
        return reObject;
	}
	public String getVirtualString(String key){
        Object obj = getVirtual(key);
        return obj!=null?obj.toString():"";
	}
	public boolean isComponent(String key){
	        return key.startsWith("$");
    }
    public boolean isVirtual(String key){
        return key.startsWith("@");
    }
    public boolean isVirtualComponnet(String key){
        return key.startsWith("@$");
    }
    public static final String NIKITA_SHOWDIALOG_KEY_NAME = "NIKITA-SHOWDIALOG-DATA";
    
    private Nset getBufferDialogData(){
    	if (AppNikita.getInstance().getVirtual(NIKITA_SHOWDIALOG_KEY_NAME) instanceof Nset) {
    		return (Nset)AppNikita.getInstance().getVirtual(NIKITA_SHOWDIALOG_KEY_NAME) ;
		}
    	return Nset.newObject();
    }
	public void showDialog(String title, String message, String button1, String button2, String button3, String reqcode, Nset data) {
		if (getActivity()!=null ) {
			AppNikita.getInstance().setVirtual(NIKITA_SHOWDIALOG_KEY_NAME, Nset.newObject().setData("nfname", getNikitaFormInstance()).setData("title", title).setData("message", message).setData("button1", button1).setData("button2", button2).setData("button3", button3).setData("reqcode", reqcode).setData("data", data));
			boolean cancelable = true;
			Builder dlg = new AlertDialog.Builder(getActivity());
			
			if (button1!=null && !button1.equals("")) {
				dlg.setPositiveButton(button1, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String requestCode = getBufferDialogData().getData("reqcode").toString();
						String resultCode = "button1";
						Nset data =  getBufferDialogData().getData("data");
						
						AppNikita.getInstance().removeVirtual(NIKITA_SHOWDIALOG_KEY_NAME);
						dialog.dismiss();  
						
						onResult(requestCode, resultCode, data);
					}
				});
				cancelable=false;
			}
			if (button2!=null && !button2.equals("")) {
				dlg.setNegativeButton(button2, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String requestCode = getBufferDialogData().getData("reqcode").toString();
						String resultCode = "button2";
						Nset data =  getBufferDialogData().getData("data");
						
						AppNikita.getInstance().removeVirtual(NIKITA_SHOWDIALOG_KEY_NAME);
						dialog.dismiss();
						
						onResult(requestCode, resultCode, data);
					}
				});
				cancelable=false;
			}
			if (button3!=null && !button3.equals("")) {
				dlg.setNegativeButton(button3, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String requestCode = getBufferDialogData().getData("reqcode").toString();
						String resultCode = "button3";
						Nset data =  getBufferDialogData().getData("data");
						
						AppNikita.getInstance().removeVirtual(NIKITA_SHOWDIALOG_KEY_NAME);
						dialog.dismiss() ; 
						
						onResult(requestCode, resultCode, data);
					}
				});
				cancelable=false;
			}
			
			
			dlg.setCancelable(true);//cancelable
			dlg.setTitle(title);
			dlg.setMessage(message);
						 
			AlertDialog alertDialog = dlg.create();			
			alertDialog.setCanceledOnTouchOutside(false);
						
			alertDialog.setOnCancelListener(new OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					AppNikita.getInstance().removeVirtual(NIKITA_SHOWDIALOG_KEY_NAME);		
				}
			});
			 
			 
			if (getActivity() instanceof NikitaActivity) {
				((NikitaActivity)getActivity()).showDialog(alertDialog);
			}else{
				alertDialog.show();	
			}	
		}		
	}
   
}
