package com.nikita.mobile.finalphase.activity;

import java.util.Hashtable;

import org.apache.commons.lang.StringEscapeUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.action.MobileAction;
import com.nikita.mobile.finalphase.generator.action.ShowDialogAction;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class SettingActivity  extends AndroidActivity {
	private static Thread timer = null;
	 
	private String result = "";
	private String server = "";
	public static boolean pause = false;
	private String getTitle(String def){
		if (getIntent()!=null && getIntent().getStringExtra("title")!=null) {
			return getIntent().getStringExtra("title");
		}
		return def;
	}

	protected void onCreate(Bundle savedInstanceState) {		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultsetting);	
		
		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));	
		
//		add [auto complete]
//		AutoCompleteTextView autoCompleteTextView = ((AutoCompleteTextView)findViewById(R.id.txtServer));
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,  android.R.layout.simple_dropdown_item_1line, DevActivity.getListServer(getApplicationContext()));
//		autoCompleteTextView.setAdapter(adapter);
		
		 	
		((TextView)findViewById(R.id.txtTitle)).setText( getTitle("Pengaturan")  );
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				finish();				
			}
		});
		
		
		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			((EditText)findViewById(R.id.txtVersion)).setText(pInfo.versionName );
		} catch (Exception e) { }
//		((EditText)findViewById(R.id.txtServer)).setText(   AppNikita.getInstance().getBaseUrl());
		((EditText)findViewById(R.id.txtServer)).setText(   "Dipostar GPS Tracking" );
		((EditText)findViewById(R.id.txtDevice)).setText(    ( (TelephonyManager)getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase());
		((EditText)findViewById(R.id.txtStorage)).setText(   Utility.getDefaultPath() );		 
		((EditText)findViewById(R.id.txtLogic)).setText(     Utility.getSetting(getApplicationContext(), "INIT-LOGIC-VERSION", ""));
		 
		((EditText)findViewById(R.id.txtUserId)).setText(    Utility.getSetting(getApplicationContext(), "N-USER", ""));
 		
		if (AppNikita.PRODUCTION) {
			(findViewById(R.id.lblServer)).setVisibility(View.GONE);
			(findViewById(R.id.txtServer)).setVisibility(View.GONE);			
			(findViewById(R.id.tblSave)).setVisibility(View.GONE);
			
			((EditText)findViewById(R.id.txtStorage)).setText( "Production" );
		}
		
		(findViewById(R.id.tblSave)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				server = AppNikita.getInstance().getBaseUrl();
				if (server.toLowerCase().startsWith("http://")||server.toLowerCase().startsWith("https://")) {
				}else{
					server="http://"+server;
				}
				if (server.endsWith("/")) {		
					server=server.substring(0,server.length()-1);
				}
			 
				Utility.setSetting(getApplicationContext(), "BASE-URL", server);	
				finish(); 
			}
		});
		
		
		//change to TesCOnn
		(findViewById(R.id.tblTestConn)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				 //===========================================================================//
				 Messagebox.showBusyDialog(SettingActivity.this, new Runnable() {					
						public void run() {
							server = AppNikita.getInstance().getBaseUrl();
							if (server.toLowerCase().startsWith("http://")||server.toLowerCase().startsWith("https://")) {
							}else{
								server="https://"+server;
							}
							if (server.endsWith("/")) {
								server=server+"mobile.mcheckuser";
							}else{
								server=server+"/mobile.mcheckuser";
							}							
							result = NikitaInternet.getString(NikitaInternet.postHttp(server)) ;
						}
					}, new Runnable() {
						public void run() {
							if (result.contains("Hello Nikita")) {
								DevActivity.saveListServer(getApplicationContext(), server);
								Messagebox.showInfo(SettingActivity.this, "Connection Success");
							}else{
								Messagebox.showInfo(SettingActivity.this, "Connection Error/Timeout");
							}
						}
					});
				  //===========================================================================//
			}
		});
			 
 
	}
	
	public static int parseVersion(String s) {
        s = s.trim();
        if (s.contains(" ")) {
            s = s.substring(0, s.indexOf(" "));
        }
        try {
            String[] ver = Utility.split(s + "..", ".");
            int i = Utility.getInt(ver[0]) * 1000000 + Utility.getInt(ver[1]) * 1000 + Utility.getInt(ver[2]);
            return i;
        } catch (Exception e) {}

        return 0;
    }
	 
}
