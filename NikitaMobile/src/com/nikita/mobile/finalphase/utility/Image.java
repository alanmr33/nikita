package com.nikita.mobile.finalphase.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Image {
	public static Bitmap OpenImage(Context context,String path){
		int scale=1;//int height=1;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds=true;	
		BitmapFactory.decodeFile(path,options); 
		if (options.outWidth>context.getResources().getDisplayMetrics().widthPixels||options.outHeight>context.getResources().getDisplayMetrics().heightPixels) {
			scale=options.outWidth/context.getResources().getDisplayMetrics().widthPixels;//960px
			scale=scale+((options.outWidth-context.getResources().getDisplayMetrics().widthPixels*scale>=1)?1:0);
			scale=(scale!=1?scale:2);
//			height=options.outHeight*context.getResources().getDisplayMetrics().widthPixels/options.outWidth;
		}else{
//			height=context.getResources().getDisplayMetrics().heightPixels;
		}
		options = new BitmapFactory.Options();
		options.inSampleSize=scale;
		return BitmapFactory.decodeFile( path, options );
	}
}
