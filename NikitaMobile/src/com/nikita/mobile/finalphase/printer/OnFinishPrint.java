package com.nikita.mobile.finalphase.printer;

public interface OnFinishPrint {
	public void OnFinish(Boolean status,String message, String battery, String deviceName, String deviceMAC);
}
