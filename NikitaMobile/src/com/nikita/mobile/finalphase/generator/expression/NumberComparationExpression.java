package com.nikita.mobile.finalphase.generator.expression;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class NumberComparationExpression implements IExpression {
	@Override
	public boolean OnExpression(Component comp, Nset currdata) {
		Nset data =currdata.getData("args");
        String code = currdata.getData("code").toString().trim();
        
        if (code.equalsIgnoreCase("=")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = Integer.parseInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = Integer.parseInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val == val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("!=")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = Integer.parseInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = Integer.parseInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val != val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase(">")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val > val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase(">=")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val >= val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("<")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val < val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("<=")) {
            
            String key1 = data.getData("param1").toString().trim();
            int val = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key1));

            String key2 = data.getData("param2").toString().trim();
            int val2 = AUtility.getInt(comp.getNikitaComponent().getVirtualString(key2));

            if (val <= val2) {
                return true;
            }
        }else if (code.equalsIgnoreCase("evaluate")) {
            
        }
        
		return false;
	}
}
