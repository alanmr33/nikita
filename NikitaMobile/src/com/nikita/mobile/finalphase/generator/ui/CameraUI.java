package com.nikita.mobile.finalphase.generator.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.location.Location;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.bs;
import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NFormActivity;
import com.nikita.mobile.finalphase.generator.NikitaActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.service.LocationService;
import com.nikita.mobile.finalphase.service.NikitaFuseLocation;
import com.nikita.mobile.finalphase.utility.Capture;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;
 
 

public class CameraUI extends FileUploadUI{
	static final int STARTDATE = 1;
	
	Intent gallery, capture;
	
	public boolean isFileComponent() {
		return true;
	}
	 
	public void saveFileComponent() {
		if (getText().endsWith(".tmp")) {
			String fname = AppNikita.getInstance().getDeviceId() + "." + getText().substring(0, getText().length()-4) +".cam";
			Utility.copyFile(Utility.getDefaultTempPath(getText()), Utility.getDefaultImagePath(fname) );
			new File(Utility.getDefaultTempPath(getText())).delete();
			setText(fname);
		}
	}
	 
	private boolean isSign = false;
	public CameraUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	private int requestcode   ; 
	 
	public View onCreate(NikitaControler form) {
		requestcode=(requestcode!=0?requestcode:hashCode());
		
		view=Utility.getInflater(form.getActivity(), R.layout.compcamera);		
		view.findViewById(R.id.imgClear).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setDefaultImage();
				view.findViewById(R.id.imgClear).setVisibility(View.INVISIBLE);
				view.findViewById(R.id.imgCamera).setVisibility(View.VISIBLE);
				setText("");//clear
			}
		});
		
		if (form.getActivity() instanceof NikitaActivity) {
			((NikitaActivity) form.getActivity()).addOnActivityResultListener(new NikitaActivity.OnActivityResultListener() {
				public void onActivityResult(int requestCode, int resultCode, Intent data) {
					if (requestCode==requestcode && resultCode==Activity.RESULT_OK) {
						File photo = new File(Environment.getExternalStorageDirectory(), "image");
						String fname =   Long.toHexString(System.currentTimeMillis()) +"." + Long.toHexString(System.nanoTime()) +"." + getId() +".tmp";
						Utility.copyFile( photo.getAbsolutePath(), Utility.getDefaultTempPath(fname) );
						setText(fname);	
						//camera
						if (!isSign) {
							onCompressImage( Utility.getDefaultTempPath(fname) );	
						}								
						viewImage( (ImageView)view.findViewById(R.id.imgView)  ,  Utility.getDefaultTempPath(fname) );//photo.getAbsolutePath()
						view.findViewById(R.id.imgClear).setVisibility(View.VISIBLE);
						view.findViewById(R.id.imgCamera).setVisibility(View.INVISIBLE);
					
					}						
				}
			});
		}
		
		
		view.findViewById(R.id.imgRotate).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!getImagePath().equals("")) {
					Messagebox.showBusyDialog(getNikitaComponent().getActivity(), new Runnable() {					
						public void run() {
							rotate(getImagePath(), 90);
						}
					}, new Runnable() {
						public void run() {
							viewImage( (ImageView)view.findViewById(R.id.imgView) , getImagePath());
						}
					});	
				}
			}
		});
		view.findViewById(R.id.imgCamera).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {			 
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRoute();							
					}
				});	
				
									
				
				if (CameraUI.this.getStyle()==null) {
					CameraUI.this.setStyle(new Style());
				}
				String mimeaccept = CameraUI.this.getStyle().getInternalObject().getData("attr").getData("n-accept").toString();
				String action = CameraUI.this.getStyle().getInternalObject().getData("attr").getData("n-action").toString();
//				String action = CameraUI.this.getStyle().getInternalObject().getData("style").getData("n-action").toString(); //danamon
				String camtype = CameraUI.this.getStyle().getInternalObject().getData("style").getData("n-camera-type").toString();
				
				//danamon 23-06-2016
				/*capture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				File photo_capture = new File(Environment.getExternalStorageDirectory(), "image");
				capture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo_capture));
				
				List<Intent> cameraIntent = new ArrayList<Intent>();
				PackageManager pm = getNikitaComponent().getActivity().getPackageManager();
				List<ResolveInfo> listCam = pm.queryIntentActivities(capture, 0);
				for(ResolveInfo res : listCam){
					String packageName = res.activityInfo.packageName;
					Intent intent = new Intent (capture);
					intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
					intent.setPackage(packageName);
					cameraIntent.add(intent);
				}
				
				
				gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				if (mimeaccept.equals("")) {
					gallery.setType("image/*");
				}else{
					gallery.setType(mimeaccept);
				}*/
				
				if (camtype.toLowerCase().trim().equals("sign")) {
					isSign=true;
					Intent intent = new Intent(getNikitaComponent().getActivity(), Capture.class);		
					File photo = new File(Environment.getExternalStorageDirectory(), "image");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, photo.getAbsolutePath());
				 
			    	
					getNikitaComponent().getActivity().startActivityForResult(intent, requestcode);					     
				}else if (!action.toLowerCase().trim().equals("image")) {
					Intent capture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					File photo = new File(Environment.getExternalStorageDirectory(), "image");
					capture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
					getNikitaComponent().getActivity().startActivityForResult(capture, requestcode);					
				}else {
					Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					if (mimeaccept.equals("")) {
						gallery.setType("image/*");
					}else{
						gallery.setType(mimeaccept);
					}
					getNikitaComponent().getActivity().startActivityForResult(gallery, requestcode);
				}
				//danamon 23-06-2016
				/*else{
					List<Intent> options = new ArrayList<Intent>();
					options.add(gallery);
					options.add(capture);
					
					Intent chooserIntent = Intent.createChooser(gallery, "Select Source");
//					chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, options.toArray(new Parcelable[options.size()]));
					chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntent.toArray(new Parcelable[cameraIntent.size()]));
					getNikitaComponent().getActivity().startActivityForResult(chooserIntent, requestcode);
				}*/	
				if (CameraUI.this.getStyle().getInternalObject().getData("style").containsKey("n-location") ) {
					
					new NikitaFuseLocation(getNikitaComponent().getActivity().getApplicationContext(), new NikitaFuseLocation.OnLocationListener() {
						public void onLocationChanged(Location location) {
							setExtraTag("gps", NikitaFuseLocation.locationToNset(location, 0).toJSON());
						}
						public void onLocationDone(Location location, int status) {
							setExtraTag("gps", NikitaFuseLocation.locationToNset(location, status).toJSON());
						}
					});	   
				}				
				
			}
		});
		if (Generator.getNikitaParameters().getData("INIT-CAMERA-ROTATE").toString().toLowerCase().equals("visible") && isSign) {
			view.findViewById(R.id.imgRotate).setVisibility( View.VISIBLE );
		}else{
			view.findViewById(R.id.imgRotate).setVisibility( View.INVISIBLE );
		}
		if (getStyle()!=null) {
			if (getStyle().getInternalObject().getData("style").containsKey("n-height")) {
				String val = getStyle().getInternalObject().getData("style").getData("n-height").toString();
				if (val.equals("")||val.equals("AUTO")||val.equals("WRAP")||val.equals("WRAP_CONTENT")) {
					getStyle().setStyle("n-height", "120dp");
				}
			}else{
				getStyle().setStyle("n-height", "120dp");
			}
		}else{
			setStyle(new Style());
			getStyle().setStyle("n-height", "120dp");
		}
		 
		setDefaultImage();
		setImage(); 
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	private void setDefaultImage(){		
		if (CameraUI.this.getStyle()!=null) {
			String camtype = CameraUI.this.getStyle().getInternalObject().getData("style").getData("n-camera-type").toString();
			if (camtype.toLowerCase().trim().equals("sign")) {
				((ImageView)view.findViewById(R.id.imgView)).setImageResource(R.drawable.sign);
				
				return;
			}			
		}
		((ImageView)view.findViewById(R.id.imgView)).setImageResource(R.drawable.noimage);
	}
	
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v  = view.findViewById(R.id.editText1);
		if (v instanceof EditText) {
			((EditText)v).setText(getText());
		}
		
		v  = view.findViewById(R.id.txtText);
		if (v instanceof Button) {
			((Button)v).setText(getText());
		}
		
		v  = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (view!=null) {
			//View v = view.findViewById(R.id.txtText);
			//if (v instanceof EditText) {
			//	return ((EditText)v).getText().toString();
			//}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	public static void viewImage(ImageView img, String absolutePath){
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds=true;	
		BitmapFactory.decodeFile(absolutePath, options); 
		int scale=options.outWidth/128; 
		
		
		options = new BitmapFactory.Options();
		options.inSampleSize=scale;
		Bitmap bmp = BitmapFactory.decodeFile(absolutePath,  options);
		
		img.setImageBitmap(bmp); 
	}
	
	public static void onCompressImage(String file){
		String cam = Generator.getNikitaParameters().getData("INIT-UI-CAMERAUI").toString();
	    Nset ncam = Nset.readJSON(cam);
		if (!ncam.getData("compress").toString().equalsIgnoreCase("true")) {
		   return;
		}
		
		// awal
		/*int quality = ncam.containsKey("compress.quality")? ncam.getData("compress.quality").toInteger() : 80 ;  
		int width =  ncam.containsKey("compress.width")? ncam.getData("compress.width").toInteger() :540 ; 
		int maxpx =  ncam.containsKey("compress.maxpx")? ncam.getData("compress.maxpx").toInteger() : 540 ;*/
		
		int quality = ncam.containsKey("compress.quality")? ncam.getData("compress.quality").toInteger() : 90 ;  
		int width =  ncam.containsKey("compress.width")? ncam.getData("compress.width").toInteger() : 470 ; 
		int maxpx =  ncam.containsKey("compress.maxpx")? ncam.getData("compress.maxpx").toInteger() : 470 ; 
		
		String format = ncam.containsKey("compress.format")? ncam.getData("compress.format").toString() : "png" ; 
		 
		width=width<=10?540:width;
		maxpx=maxpx<=10?540:maxpx;
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds=true;	
			BitmapFactory.decodeFile(file, options); 
			int scale= options.outWidth / width ; 
			if ( ncam.containsKey("compress.maxpx") ) {
				scale=Math.max(options.outWidth, options.outHeight) / maxpx; 
			}
			
			options = new BitmapFactory.Options();
			options.inSampleSize=scale+1;
			Bitmap bmp = BitmapFactory.decodeFile(file,  options);
			 
		    FileOutputStream fos = new FileOutputStream(file);
		    bmp.compress(format.equalsIgnoreCase("jpg")?Bitmap.CompressFormat.JPEG:Bitmap.CompressFormat.PNG, quality, fos);
		    
		    fos.flush();
			fos.close();		    
		} catch (Exception e) { }
	}
	public static  void rotate(String file, final int move){
		//mmust on other thread
		try {				 
			 Bitmap bitmap = BitmapFactory.decodeFile(file);
			 Matrix matrix = new Matrix();
			 matrix.postRotate(move);
			 bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

			 FileOutputStream fos = new FileOutputStream(file);
			 bitmap.compress(CompressFormat.PNG, 100, fos);
			 fos.flush();
			 fos.close(); 		
		} catch (Exception e) { }		
	}
	 
	
	private void setImage(){
		if (getText().trim().equals("")) {
			view.findViewById(R.id.imgClear).setVisibility(View.INVISIBLE);
			view.findViewById(R.id.imgCamera).setVisibility(View.VISIBLE);
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			//Utility.OpenImage(img, Utility.getDefaultTempPath(getText()), R.drawable.ic_launcher);
			viewImage( (ImageView)view.findViewById(R.id.imgView)  , Utility.getDefaultTempPath(getText()));
			view.findViewById(R.id.imgClear).setVisibility(View.VISIBLE);
			view.findViewById(R.id.imgCamera).setVisibility(View.INVISIBLE);
		}else if (new File(Utility.getDefaultImagePath(getText())).exists()) {
			//Utility.OpenImage(img, Utility.getDefaultPath(getText()), R.drawable.ic_launcher);
			viewImage( (ImageView)view.findViewById(R.id.imgView)  , Utility.getDefaultImagePath(getText()));
			view.findViewById(R.id.imgClear).setVisibility(View.VISIBLE);
			view.findViewById(R.id.imgCamera).setVisibility(View.INVISIBLE);
		}	
	}
	
	private String getImagePath(){
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			return Utility.getDefaultTempPath(getText());
		}else if (new File(Utility.getDefaultImagePath(getText())).exists()) {
			return Utility.getDefaultImagePath(getText());
		}	
		return "";
	}
	
	 
	  
}
