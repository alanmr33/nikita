package com.nikita.mobile.finalphase.service;

import java.util.Vector;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.utility.Messagebox;

public class Position extends Service{

	private String mode ="GPS";
	private Location location ;
 	private LocationManager locationManager;
 
 	private GeoUpdateHandlerGPS geoUpdateHandlerGPS = new GeoUpdateHandlerGPS();
 	private GeoUpdateHandlerNET geoUpdateHandlerNET = new GeoUpdateHandlerNET();
 	
 	private static double latitude=0;
 	private static double longitude=0;
 	private static boolean isGPS=true;
	private GeoPoint px =new GeoPoint(0, 0);
	
	public int onStartCommand(Intent intent, int flags, int startId) {	
		Log.i("GeoUpdateHandler", "onStartCommand");
		start();
		return super.onStartCommand(intent, flags, startId);
	}
	public void onCreate() {
		super.onCreate();
		Log.i("GeoUpdateHandler", "onCreate");
		start();
	}
	public void removeListener() {
		if (geoUpdateHandlerGPS!=null) {
			try {
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				locationManager.removeUpdates(geoUpdateHandlerGPS);
			} catch (Exception e) { }
		}
		if (geoUpdateHandlerNET!=null) {
			try {
				locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
				locationManager.removeUpdates(geoUpdateHandlerNET);
			} catch (Exception e) { }
		}
	}
	public void onDestroy() {
		Log.i("GeoUpdateHandler", "onDestroy");
		super.onDestroy();
		removeListener();
	}
	public void start() {
		
		Log.i("GeoUpdateHandler", "Started");
		removeListener();	 
		
	
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		//location =  locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,  1000, 10.0f, (LocationListener) geoUpdateHandlerGPS);
		
		if (location!=null) {
			mode="GPS";			
			//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,  30000, 50.0f, (LocationListener)geoUpdateHandlerNET);
			//location =  locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}else{
			mode="NET";
			//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,  30000, 50.0f, (LocationListener) geoUpdateHandlerNET);
			//location =  locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if(location!=null){
			double lat = location.getLatitude();
			double lng = location.getLongitude();
			px = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
			Log.i("NikitaWilly", mode);
			if (latitude==lat && longitude ==lng) {
			}else{
				sendLocation(mode, lat, lng);
			}
		}	 
		
	}
	private void sendLocation(final String mode, final double lat, final double lng){
 		PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
		final PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "sendLocation");
		wl.acquire();
		latitude=lat;
		longitude=lng;
		
		Intent intent = new Intent();
		intent.setAction("mylocation");
		intent.putExtra("AUTO", "AUTO");
		sendBroadcast(intent);
		
		Log.i("NikitaWilly", "sendLocation");
		Messagebox.newTask(new Runnable() {
			public void run() {				
				
				
				
				wl.release();
			}
		});
	}
 

	public class GeoUpdateHandlerGPS implements LocationListener {
		public void onLocationChanged(Location location) {
			double lat = (double) (location.getLatitude()  );
			double lng = (double) (location.getLongitude()  );
			px = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));

			Log.i("NikitaWilly", px.toString());
			sendLocation("GPS", lat, lng);
			//start();
			
			for (int i = 0; i < locationListeners.size(); i++) {
				locationListeners.elementAt(i).onLocationChanged(location);
			}
		}
		public void onProviderDisabled(String provider) {
			isGPS=false;
			Log.i("NikitaWilly", "onProviderDisabled");
			for (int i = 0; i < locationListeners.size(); i++) {
				locationListeners.elementAt(i).onProviderDisabled(provider);
			}
		}
		public void onProviderEnabled(String provider) { 
			isGPS=true;
			Log.i("NikitaWilly", "onProviderEnabled");
			for (int i = 0; i < locationListeners.size(); i++) {
				locationListeners.elementAt(i).onProviderEnabled(provider);
			}
		}
		public void onStatusChanged(String provider, int status, Bundle extras) { 
			for (int i = 0; i < locationListeners.size(); i++) {
				locationListeners.elementAt(i).onStatusChanged(provider,status,extras);
			}
		}
 
	}
	public class GeoUpdateHandlerNET implements LocationListener {
		public void onLocationChanged(Location location) {
			double lat = (double) (location.getLatitude()  );
			double lng = (double) (location.getLongitude()  );
			px = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));

			if (!isGPS) {
				Log.i("NikitaWilly", px.toString());
				sendLocation("NET" ,lat, lng);
			}			
		}
		public void onProviderDisabled(String provider) { }
		public void onProviderEnabled(String provider) { }
		public void onStatusChanged(String provider, int status, Bundle extras) { }
 
	}
	private static Vector<LocationListener> locationListeners = new Vector<LocationListener> ();
	
	public static void setLocationListener(LocationListener listener){
		if (listener!=null) {
			locationListeners.add(listener);
		}else{
			locationListeners.removeAllElements();
		}		
	}
	
	public static void startService(NForm form){
		try {
			//form.getActivity().startService(new Intent(form.getActivity(), Position.class));
		} catch (Exception e) { }
	}
	public static void stopService(NForm form){
		try {
			//form.getActivity().stopService(new Intent(form.getActivity(), Position.class));
		} catch (Exception e) { }
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	public static double getLatitude(){
		return latitude;
	}
	public static double getLongitude(){
		return longitude;
	}
	public static boolean isGPSactive(){
		return isGPS;
	}
	
}
