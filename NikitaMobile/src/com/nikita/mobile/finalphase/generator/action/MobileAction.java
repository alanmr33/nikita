package com.nikita.mobile.finalphase.generator.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.bixolon.printer.BixolonPrinter;
import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.activity.OrderCurrentActivity;
import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NikitaActivity;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.printer.BxlPrinter;
import com.nikita.mobile.finalphase.printer.OnFinishPrint;
import com.nikita.mobile.finalphase.printer.OnGetBatteryStatus;
import com.nikita.mobile.finalphase.printer.TemplatePrinter;
import com.nikita.mobile.finalphase.service.NikitaFuseLocation;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class MobileAction implements IAction{
	public static void drop(NikitaConnection nc){   	
		nc.Query("drop mobileactivity");
	}
    public static void createifneed(NikitaConnection nc){   	
    	//nc.Query( convertcreate("mobileactivity", "activityid=", "orderid=", "userid=", "threadid=",  "username=",  "modeid=", "content=", "body=", "read=INTEGER",  "status=INTEGER", "createdate=", "modifydate=") ); 
    	//new data activity
    	nc.Query( convertcreate("mobileactivity",
	 			"activityid=", "orderid=", "userid=", "threadid=",
	 			"username=",  "modeid=", "content=", "body=", "read=INTEGER", 
	 			"status=INTEGER", "createdate=", "modifydate=",
	 			"mobileresult=",
	 			"mobiletext=","mobilebody=","mobilecontent=",
	 			"mobileint=INTEGER","mobilenumber=INTEGER","mobilecount=INTEGER",
	 			"mobilereal=REAL") );     	
    	checkaltermobileactivty(nc);
    }
    private static void checkaltermobileactivty(NikitaConnection nc ){
		Nikitaset check = nc.QueryPage(1, 1, "SELECT * FROM mobileactivity", "");
		String cString = check.getDataAllHeader().toString();
		if (!cString.contains("mobileresult")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobileresult TEXT", "");
		}
		if (!cString.contains("mobiletext")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobiletext TEXT", "");
		}
		if (!cString.contains("mobilebody")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobilebody TEXT", "");
		}
		if (!cString.contains("mobilecontent")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobilecontent TEXT", "");
		}
		if (!cString.contains("mobileint")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobileint INTEGER", "");
		}
		if (!cString.contains("mobilenumber")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobilenumber INTEGER", "");
		}
		if (!cString.contains("mobilecount")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobilecount INTEGER", "");
		}
		if (!cString.contains("mobilereal")) {		 
			nc.Query("ALTER TABLE mobileactivity add column mobilereal REAL", "");
		}
    }
    public static String convertupdate(String tablename, String...fields){
    	StringBuffer sbuff = new StringBuffer();
		sbuff.append("UPDATE ").append(tablename).append(" SET ");
		for (int i = 0; i < fields.length; i++) {
			String fname = fields[i];			 
			if (fname.contains("=")) {				
				fname = fields[i].substring(0, fields[i].indexOf("="));		 
				String fval =  fields[i].substring( fields[i].indexOf("=") +1 );;
				sbuff.append(i>=1?",":"").append(fname).append("='").append(StringEscapeUtils.escapeSql(fval)).append("'");		
			}			
			
		}		 
		sbuff.append(" ");
		Generator.out("SQL", sbuff.toString());
		return  sbuff.toString();
    }
    public static String convertinsert(String tablename, String...fields){
    	StringBuffer sbuff = new StringBuffer();
		sbuff.append("INSERT INTO ").append(tablename).append(" ( ");
		for (int i = 0; i < fields.length; i++) {
			String fname = fields[i];			 
			if (fname.contains("=")) {				
				fname = fields[i].substring(0, fields[i].indexOf("="));		 
				 
				sbuff.append(i>=1?",":"").append(fname);		
			}			
			
		}
		sbuff.append(") VALUES ( ");
		for (int i = 0; i < fields.length; i++) {
			String fname = fields[i];		
			String fval = fields[i];			 
			if (fname.contains("=")) {				
				fname = fields[i].substring(0, fields[i].indexOf("="));
				fval =  fields[i].substring( fields[i].indexOf("=") +1 );
				 
				sbuff.append(i>=1?",'":"'").append(StringEscapeUtils.escapeSql(fval)).append("'");	
			}			
			
		}
		sbuff.append(") ");
		Generator.out("SQL", sbuff.toString());
		return  sbuff.toString();
    }
    public static String convertcreate(String tablename, String...fields){
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("CREATE table ").append(tablename).append(" (id  INTEGER PRIMARY KEY AUTOINCREMENT, ");
		for (int i = 0; i < fields.length; i++) {
			String fname = fields[i];
			String ftype = "TEXT";
			if (fname.contains("=")) {				
				fname = fields[i].substring(0, fields[i].indexOf("="));
				ftype =  fields[i].substring( fields[i].indexOf("=") +1 );
				if (ftype.equals("")) {
					ftype = "TEXT";
				}
			}else if (fname.trim().contains(" ")) {				
				ftype = "";
			}			
			sbuff.append(fname).append(" ").append(ftype).append((i!=fields.length-1)?",":");");		
		}
		Generator.out("SQL", sbuff.toString());
		return  sbuff.toString();
	}
    
	public synchronized boolean OnAction(final Component comp, Nset data) {
		NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
		
		String code = comp.getNikitaComponent().getVirtualString(data.getData("code").toString());  
		if (code.equals("save")||code.equals("saveandsend")) {			
			createifneed(nc);
			String reqcode = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).toLowerCase();
			String status = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param5").toString()).toLowerCase();
			String curform = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param6").toString()).toLowerCase();
			if (curform.startsWith("{")) {
                Nset n = Nset.readJSON(curform);
                curform=n.getData("formid").toString();
                if (!n.getData("formname").toString().equals("")) {
                	curform=n.getData("formname").toString();
                }
			}
			if (code.equals("saveandsend")) {
				status= "1"; //lock
			}else if (status.equals("0")||status.equals("draft")) {
				status= "0";
			}else if (status.equals("1")||status.equals("queue")||status.equals("finish")||status.equals("lock")) {
				status= "1";
			}else if (status.equals("2")||status.equals("sent")) {
				status= "2";
			}else if (Utility.isNumeric(status)) {
				//none
			}else{
				status= "0";
			}			
			String activityid = Generator.getVirtualString("@+CORE-ACTIVITYID");
		 
			if (Generator.getVirtualString("@+CORE-STATUS").equals("2") && code.equals("saveandsend")) {
				if (Generator.getNikitaParameters().getData("INIT-MOBILE-ACTIVITY-STATUS-SENT-COUNT").toInteger()>=1) {
					
				}else{
					String message = Generator.getNikitaParameters().getData("INIT-MOBILE-ACTIVITY-STATUS-SENT-MESSAGE").toString() ;
					data.getData("args").setData("message", message.equals("")?"Activity Has been Sent":message );
					new ShowAlertAction().OnAction(comp, data);
					return false;
				}
			}else if (Generator.getVirtualString("@+CORE-ACTIVITYID").equals("")) {				 
				Generator.saveOpenForms();
				activityid = AppNikita.getInstance().getDeviceId()+"."+Long.toHexString(System.currentTimeMillis()) +"."+Long.toHexString(System.nanoTime());
				//nc.Query(convertinsert("mobileactivity", "activityid="+activityid, "orderid="+Generator.getVirtualString("@+CORE-ORDERID"), "threadid=",  "username=", "content=", "body="+Generator.getStreamForms().toJSON(), "read="+Generator.getVirtualString("@+CORE-VIEW"),  "status="+status, "createdate="+Utility.Now(), "modifydate="+Utility.Now()));
				nc.Query(convertinsert("mobileactivity",  "modifydate="+Utility.Now(), "activityid="+activityid, "orderid="+Generator.getVirtualString("@+CORE-ORDERID"), "threadid=",  "username=", "content=", "body="+Generator.getStreamFormsWithCurrentForm(curform).toJSON(), "read="+Generator.getVirtualString("@+CORE-VIEW"),  "status="+status, "createdate="+Utility.Now()));
				Generator.setVirtual("@+CORE-ACTIVITYID", activityid);								
			}else{
				Generator.saveOpenForms();
				//update
				String sql = convertupdate("mobileactivity", "modifydate="+Utility.Now(),   "content=", "body="+Generator.getStreamForms().toJSON(), "read="+Generator.getVirtualString("@+CORE-VIEW"),  "status="+status ) +" WHERE activityid='"+ StringEscapeUtils.escapeSql(Generator.getVirtualString("@+CORE-ACTIVITYID")) +"' ";
				Nikitaset n = nc.Query(sql);
			}	
			
		 	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-ORDER-ACTIVITY-INFO",   nc.Query("SELECT COUNT(*) FROM mobileorder ").getText(0, 0)  )  ;
		  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-DRAFT-ACTIVITY-INFO",   nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 0 ").getText(0, 0) )  ;
		  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-PENDING-ACTIVITY-INFO", nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 1 ").getText(0, 0) )  ;
		  	//Utility.setSetting(comp.getNikitaComponent().getActivity().getApplicationContext(), "MAIN-SENT-ACTIVITY-INFO",    nc.Query("SELECT COUNT(*) FROM mobileactivity WHERE status = 2 ").getText(0, 0) )  ;
			
			if (code.equals("saveandsend")) {//actionthread	 
				Nikitaset ns = nc.Query("SELECT * FROM mobileactivity WHERE activityid = '"+activityid+"' ");
				if (ns.getRows()>=1) {
					boolean b = comp.getNikitaComponent().isBusy();
					if (!b) {
						comp.getNikitaComponent().showBusy(true);					
					}
					Nset n = sendMobileActivityData(comp.getNikitaComponent().getActivity(), ns, 0);
					if (b) {
						comp.getNikitaComponent().showBusy(false);	
					}
					comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), n);
					//comp.getNikitaComponent().onActionResult(reqcode, "MOBILE", n);
					
					//Intent intent = new Intent();					 
					//intent.putExtra("origin", "mobile-activity-send");
					//comp.getNikitaComponent().getActivity().sendBroadcast(intent, "com.nikita.activity");
					
					Intent   filter= new Intent();
				    filter.setAction("com.nikita.generator");
				    filter.putExtra("uuid", "mobileaction");
				    filter.putExtra("provider", "saveandsend");
				    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
				}				
			}else{
				//Intent intent = new Intent();					 
				//intent.putExtra("origin", "mobile-activity-save");
				//comp.getNikitaComponent().getActivity().sendBroadcast(intent, "com.nikita.activity");
				
				Intent   filter= new Intent();
			    filter.setAction("com.nikita.generator");
			    filter.putExtra("uuid", "mobileaction");
			    filter.putExtra("provider", "save");
			    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
			}
		}else if (code.equals("triger")) {
			String link = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).trim();
			Generator.saveOpenForms();
			Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
			hashtable.put("body", Generator.getStreamForms().toJSON());//stream data
			hashtable.put("mobileactioncode", "triger"); 
     		String x = NikitaInternet.getString( NikitaInternet.postHttp(  AppNikita.getInstance().getBaseUrl()+ (link.equals("")?"/mobile.mtriger":"/"+link), hashtable ) );
     		comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), Nset.readJSON(x));	
		}else if (code.equals("requestmobileactivity")) {
		}else if (code.equals("open")||code.equals("openmobileform")) {
			String activityid = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).toLowerCase();
			Generator.setVirtual("@+CORE-ACTIVITYID", activityid);
			Nikitaset ns = nc.Query("SELECT body FROM mobileactivity WHERE activityid = '"+activityid+"' ");
			Generator.openStreamForms(Nset.readJSON(ns.getText(0, 0)), comp.getNikitaComponent());
		}else if (code.equals("read")||code.equals("getmobileform")) {
			String forms = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).toLowerCase();
			 
			if (forms.trim().equals("")) {
				comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), Generator.getStreamForms());
			}else{
				comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), Generator.getStreamForms(Utility.split(forms, ",")));
			}	
			 
		}else if ( code.equals("setmobileform")) { 
			String forms = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).toLowerCase();
			Nset formData = new Nset(comp.getNikitaComponent().getVirtual(data.getData("args").getData("param2").toString()));
			if (forms.trim().equals("")) {
				 
				//comp.getNikitaComponent().setVirtual(data.getData("result").toString(), Generator.getStreamForms().toJSON());
			}else{
				//comp.getNikitaComponent().setVirtual(data.getData("result").toString(), Generator.getStreamForms(Utility.split(forms, ",")).toJSON());
			}	
		}else if (code.equals("mandatory")) {	
			//String forms = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString()).toLowerCase();
			//comp.getNikitaComponent().getComponent(key)
			StringBuffer sb = new StringBuffer();
			int comps = comp.getNikitaComponent().getContent().getComponentCount();
			for (int i = 0; i < comps; i++) {
				Component component = comp.getNikitaComponent().getContent().getComponent(i);
				if (component.isEnable() && component.isVisible()) {
					if (component.isMandatory() && (component.getText().equals("")||component.getText().equals("[]"))) {
						component.setStyle(Style.createStyle(component.getStyle()).setStyle("n-error", "true")  );
						if (!sb.toString().equals("") && !component.getLabel().equals("")) {
							sb.append("\r\n");
						}
						sb.append(component.getLabel());
						continue;
					}
				}
				Style style = Style.createStyle(component.getStyle());
				if (style.getInternalStyle().containsKey("n-error")) {
					component.setStyle(style.setStyle("n-error", "false")  );
				}		
				//component.setStyle(Style.createStyle(component.getStyle()).setStyle("n-error", "true")  );
			}	
			comp.getNikitaComponent().refreshComponent(comp);
			comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), sb.toString());
			comp.getNikitaComponent().setVirtual(data.getData("args").getData("param9").toString(), sb.toString());
		}else if (code.equals("insert")) {
		 
			createifneed(nc);
			Nikitaset ns = 	nc.Query("mobileactivity", "activityid=", "orderid=", "threadid=",  "username=", "content=", "body=", "read=",  "status=0", "createdate=", "modifydate=");
			
			comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), ns);
		}else if (code.equals("synlogic")) {
		}else if (code.equals("view")) {			 
			Nikitaset ns = 	nc.Query("mobileactivity", "activityid=", "orderid=", "threadid=",  "username=", "content=", "body=", "read=",  "status=0", "createdate=", "modifydate=");
			comp.getNikitaComponent().setVirtual(data.getData("args").getData("result").toString(), ns);
			
			
			
		}else if (code.equals("setactivity")) {	
			String param1 = data.getData("args").getData("param1").toString();  //formname			 
			if (param1.equals("")||param1.startsWith("$")) {
//				???????Generator.setMobileActivityStream(comp.getNikitaComponent());
			}else{
				param1 = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param1").toString());  
				//???????		Generator.setMobileActivityStream(param1);
			}
		}else if (code.equals("getactivity")) {		
			//comp.getNikitaComponent().setVirtual(data.getData("result").toString(), Generator.getMobileActivityStream().toJSON());
		}else if (code.equals("clearactivity")) {	
			//Generator.clearMobileActivityStream();
		}else if (code.equals("replaceactivity")) {	
			 		
		}else if (code.equals("synctable")) {	
			synctable(comp, data);
		}
		return true;
	}
	
	public static void deleteifneed(){
		
	}
 
	public static synchronized void deleteMobileActivity(Context context, Nikitaset ns, int i){

		NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
		Nset stream = Nset.readJSON(ns.getText(i, "body").toString());
	 
		String[] forms = stream.getObjectKeys(); Nset result = Nset.newObject();boolean berror = false;
		for (int f = 0; f < forms.length; f++) {
			String[]  comps = stream.getData(forms[f]).getObjectKeys();				
			for (int comp = 0; comp < comps.length; comp++) {
				Nset nform = stream.getData(forms[f]);				
				if ( nform.getData(comps[comp]).getData(3).toString().equals("file") ) {
					new File(Utility.getDefaultImagePath(nform.getData(comps[comp]).getData(0).toString() )).delete() ;					 
				}	
			}
			 
		}
		nc.Query("DELETE  FROM mobileactivity WHERE id='"+ns.getText(i, "id")+"'");
	}
	public void saveMobileActivityData(Context context){
		//moving data from temp to defaultpath (andpakage for resumablesupport)
		
	}
	private static boolean isImageExist(String fname){
		try {
			return new File(fname).exists();
		} catch (Exception e) { }
		return false;
	}
	private void sendMobilActivityResumable(){
		//resumeid, resumeoffset, resumesize, resumehash, resumeexpired, resumeinfo
		Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
		hashtable.put("resumeid", "");
		hashtable.put("resumeoffset", "");
		hashtable.put("resumesize", "");
		hashtable.put("resumehash", "");
		hashtable.put("resumeexpired", "");	
		hashtable.put("resumeinfo", "");	
	}
	
	public static Nset sendMobileActivityData(Context context, Nikitaset ns, int i){	
		Nset stream = Nset.readJSON(ns.getText(i, "body").toString());
		Generator.out("MOBILE", "sendMobileActivityData");	
		String[] forms = stream.getObjectKeys(); Nset result = Nset.newObject();boolean berror = false;boolean cerror = false;
		for (int f = 0; f < forms.length; f++) {
			String[]  comps = stream.getData(forms[f]).getObjectKeys();				
			for (int comp = 0; comp < comps.length; comp++) {
				Nset nform = stream.getData(forms[f]);				
				Generator.out("MOBILE", nform.toJSON());		
				if ( nform.getData(comps[comp]).getData(3).toString().equals("file") && !nform.getData(comps[comp]).getData(0).toString().trim().equals("") ) {
					//check image(att) is exist 	
				 
					if (isImageExist(Utility.getDefaultImagePath(nform.getData(comps[comp]).getData(0).toString()))) {
						Generator.out("MOBILEs", "prepare");
						Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
						hashtable.put("imagename", nform.getData(comps[comp]).getData(0).toString());

						hashtable.put("username", ns.getText(i, "username"));
						hashtable.put("activityid", ns.getText(i, "activityid"));
						hashtable.put("orderid", ns.getText(i, "orderid"));
						hashtable.put("threadid", ns.getText(i, "threadid"));
						hashtable.put("status", ns.getText(i, "status"));

						String x = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimagecheck", hashtable));
						result = Nset.readJSON(x);

						if (!result.getData("error").toString().equals("")) {
							berror = true;
							break;
						}

						if (!result.getData("status").toString().equalsIgnoreCase("exist")) {
							// send image(att)
							if (!getCurrentNikitaCode().contains("noimageupload")) {
								try {
									x = NikitaInternet.getString(NikitaInternet.multipartHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimageupload", hashtable,
											nform.getData(comps[comp]).getData(0).toString(),
											new FileInputStream(Utility.getDefaultImagePath(nform.getData(comps[comp]).getData(0).toString()))));
									result = Nset.readJSON(x);
								} catch (Exception e) {
								}
							}
						}
						
						//check image again 23-12-2016
						String z = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimagecheck", hashtable));
						Nset resultx = Nset.readJSON(z);
						
						//if (!result.getData("error").toString().equals("")) {							
						//	berror=true;
						//	break;
						//}
						
						//Edit Kevin
						if (!resultx.getData("status").toString().equalsIgnoreCase("exist")) {
							cerror = true;
							break;
						}
					}
				}				
				
			}//comps.length	
			//if (!result.getData("error").toString().equals("")) {							
			//	berror=true;
			//	break;
			//}
		}//forms.length		
		
	
		
		if (!berror && !cerror) {
			//send activity			
//			{branchid=16, threadid=, session=, create_date=2016-11-21 13:21:10, activityid=354599060368910.158858b981c.131e16b1c568, body={"e37f0136aa3ffaf149b351f6a4c948e9":{"nikitageneratorcurrentform":"dipo_mtodolist_child"},"dipo_mtodolist_child":{"ch_kelurahan":["","0","0",""],"lh26":["","1","0",""],"lbl_net_receivable":["OS Net Receivable :","1","0",""],"lh4":["","1","0",""],"lbl_id":["","0","0",""],"agreement_id":["0006045/1/17/03/2012","1","0",""],"lh21":["","1","0",""],"lh3":["","1","0",""],"current_status":["ICP","1","0",""],"alamat_lama_help":["JL. EMMY SAELAN NO 16,#UJUNG PANDANG#SAWERIGADING#MAKASSAR#90113","0","0",""],"cmb_status_hp":["1","1","0",""],"customer_name":["FRANS SALIM KALALO","1","0",""],"lh20":["","1","0",""],"result":["","1","0",""],"lbl_alamat_perubahan":["Alamat Perubahan :","1","0",""],"priority_flag":["0","1","0",""],"remark_adm_coll":["","1","0",""],"lh9":["","1","0",""],"expired_date":["2016-08-30","1","0",""],"lbl_alamat":["Alamat :","1","0",""],"lbl_jth_tempo":["Tgl Jatuh Tempo :","1","0",""],"lbl_prior":["1","0","0",""],"txt_ptp_amount":["","0","0",""],"lh5":["","1","0",""],"overdue_days":["214","1","0",""],"lh1":["","1","0",""],"lbl_total_period":["Total Period :","1","0",""],"lh10":["","1","0",""],"lbl_lat":["-6.165662","0","0",""],"ch_home_phone":["","0","0",""],"lh27":["","1","0",""],"lbl_no_kontrak":["No. Kontrak :","1","0",""],"ch_city":["","0","0",""],"lbl_mobile_old":["Mobile :","1","0",""],"lbl_unit":["Brand :","1","0",""],"cmb_visit_result":["10","1","0",""],"jlh_bayar":["150000000","1","0",""],"foto_1":["354599060368910.158858b6f3b.131ba6ee4ceb.30453.cam","1","0","file",""],"lh24":["","1","0",""],"lbl_nama_cust":["Nama Customer :","1","0",""],"lh6":["","1","0",""],"cmb_status_alamat":["1","1","0",""],"os_net_receivable":["1.374.860.186","1","0",""],"lh19":["","1","0",""],"lbl_telp_old":["Telp :","1","0",""],"func_separator_digit":["","0","0",""],"ptp":["","1","0",""],"foto_2":["","1","0","file",""],"lbl_no_polisi":["No Plat Kendaraan :","1","0",""],"btn_save_prior":["Save Prioritas","1","0",""],"lbl_balance":["Balance :","1","0",""],"lh7":["","1","0",""],"lbl_ptp_date":["PTP Date :","1","0",""],"lh15":["","1","0",""],"lbl_usl_paid":["USL Paid :","1","0",""],"modified_date":["2016-11-21 13:19:34","0","0",""],"tgl_bayar":["","0","0",""],"lh14":["","1","0",""],"ptp_date":["","1","0",""],"lbl_ptp_amount":["PTP Amount :","1","0",""],"lh23":["","1","0",""],"lh2":["","1","0",""],"tgl_janji_bayar":["","0","0",""],"lbl_overdue_days":["Overdue Days :","1","0",""],"brand":["10 UNIT KOBELCO EXCAVATOR HYDRAULIC SK200-8 ACERA GEOSPEC SUPER","1","0",""],"due_date":["2016-02-29","1","0",""],"h_btn":["","1","0",""],"lbl_remark_adm_coll":["Remark Admin Coll :","1","0",""],"curr_balance":["1.374.860.186","1","0",""],"hob_action":["FOLLOW UP","1","0",""],"visit_result":["","0","0",""],"txt_digit_separator":["","0","0",""],"ch_addr":["","0","0",""],"lh25":["","1","0",""],"lh8":["","1","0",""],"license_plate_no":["","1","0",""],"lbl_hob_action":["HOB Action :","1","0",""],"load":["","1","0",""],"lh16":["","1","0",""],"ptp_amount":["","0","0",""],"lbl_current_status":["Current Status :","1","0",""],"lh17":["","1","0",""],"cmb_status_pembayaran":["1","1","0",""],"jlh_angsuran":["196.408.598","1","0",""],"lh11":["","1","0",""],"telp_perubahan":["","1","0",""],"lbl_telp_perubahan":["Telp Perubahan :","1","0",""],"total_periods":["14","1","0",""],"bertemu_dengan":["1","1","0",""],"lh22":["","1","0",""],"txt_flag":["send","0","0",""],"alamat_perubahan":["","1","0",""],"telp_old":["0411330555, 0411858398, ","1","0",""],"other":["","0","0",""],"generate_date":["2016-09-30 00:00:00.0","0","0",""],"alamat_baru_help":["","0","0",""],"lbl_lon":["106.7785258","0","0",""],"btn_save":["SAVE","1","0",""],"broadcast":["com.nikita.generator","1","0",""],"ch_mobile_phone":["","0","0",""],"lh12":["","1","0",""],"btn_send":["SUBMIT","1","0",""],"remark":["bayar","1","0",""],"lbl_curr_unit":["Current Unit :","1","0",""],"cmb_status_telp":["1","1","0",""],"contract_no":["","0","0",""],"func_separator_bayar":["","0","0",""],"lbl_mobile_perubahan":["Mobile Perubahan :","1","0",""],"lh13":["","1","0",""],"mobile_perubahan":["","1","0",""],"lbl_expired_date":["Expired Date :","1","0",""],"unit":["10","1","0",""],"func_validasi":["","0","0",""],"mobile_old":["081241018818, 081524333018, ","1","0",""],"lbl_ptp":["PTP :","1","0",""],"lh18":["","1","0",""],"lbl_curr_balance":["Current Balance :","1","0",""],"alamat":["JL. EMMY SAELAN NO 16,, UJUNG PANDANG, SAWERIGADING, MAKASSAR, 90113","1","0",""],"created_date":["2016-11-21 13:19:34","0","0",""],"ch_kecamatan":["","0","0",""],"usl_paid":["7","1","0",""],"balance":["1.374.860.186","1","0",""],"current_unit":["9","1","0",""],"lbl_flag":["","0","0",""],"ch_zip_code":["","0","0",""],"lbl_jlh_angsuran":["Jumlah Angsuran :","1","0",""],"onback":["","1","0",""]},"DefaultHeaderForm":{"righticon":["","1","1",""],"title":["EDIT TO DO LIST","1","1",""],"lefticon":["","1","1",""]}}, mobileactioncode=save, userid=field.coll3, logicversion=2.0.352, status=1, imei=354599060368910, username=, version=2.3.166, localdate=2016-11-21 13:31:49, batch=, auth=, orderid=0006045/1/17/03/2012}
			String bodyx = Utility.encodeBase64(ns.getText(i, "body"));
			Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
			hashtable.put("body", bodyx);//stream data
			//agreement id dan fc id
//			hashtable.put("userid", Utility.getSetting(context, "N-USER", ""));
			hashtable.put("username", ns.getText(i, "username"));
			hashtable.put("activityid", ns.getText(i, "activityid"));
			hashtable.put("orderid", ns.getText(i, "orderid"));
			hashtable.put("threadid", ns.getText(i, "threadid"));
			hashtable.put("status", ns.getText(i, "status"));
			hashtable.put("create_date", ns.getText(i, "createdate"));
			hashtable.put("modifydate", ns.getText(i, "modifydate"));
			hashtable.put("mobileactioncode", "save");
			
			String baseUrl = AppNikita.getInstance().getBaseUrl();
			String x = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl()+"/mobile.mactivity", hashtable));			
			result = Nset.readJSON(  x  );
			if (result.getData("status").toString().equalsIgnoreCase("OK") && result.getData("error").toString().equals("")) {
				NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
				if (result.containsKey("mobilesatatus")) {
					nc.Query("UPDATE mobileactivity SET status = ? WHERE id = ?", result.getData("mobilesatatus").toString(), ns.getText(i, "id"));
				}else{
				nc.Query("UPDATE mobileactivity SET status = 2 WHERE id = ?", ns.getText(i, "id"));
				}	
				if (result.containsKey("mobileresult")) {
					checkaltermobileactivty(nc);
					nc.Query("UPDATE mobileactivity SET mobileresult = ? WHERE id = ?", result.getData("mobileresult").toString(), ns.getText(i, "id"));
				}
			}			
		}	
		return result;
	}
	public static String getCurrentNikitaCode(){
		return  Utility.getSetting(AppNikita.getInstance().getApplicationContext(), "NIKITACODE", "");
	}
	//send table history activity (login and logout)
	public void synctable(Component comp, Nset data) {
		String conname = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param2").toString());
		String tname = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param3").toString());
		String tmode = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param4").toString());
		Object syncdata = comp.getNikitaComponent().getVirtual(data.getData("args").getData("param5").toString());
		String fieldspk = comp.getNikitaComponent().getVirtualString(data.getData("args").getData("param6").toString());
		NikitaConnection nc = Generator.getConnection(conname);
		Nikitaset nsData ;
		if (syncdata instanceof Nikitaset) {
			nsData = (Nikitaset)syncdata;
		}else{
			nsData = new Nikitaset("Nikitaset not found");
		}
		
		if (tmode.contains("drop")) {
			DevActivity.dropTable(nc, tname);
		}	
		if (tmode.contains("delete") ) {
			StringBuffer sb = new StringBuffer("DELETE FROM "+ tname + " WHERE ");
			Nset pk ;
			if (fieldspk.startsWith("[") && fieldspk.equals("]")) {
				pk = Nset.readJSON(fieldspk);
			}else{
				pk = Nset.readsplitString(fieldspk);
			}
			for (int row = 0; row < nsData.getRows(); row++) {
				try {
					for (int i = 0; i < pk.getSize(); i++) {
						sb.append(i>=1?" AND ":" ");
						sb.append(pk.getData(i).toString());
						sb.append("=");
						sb.append("'").append(StringEscapeUtils.escapeSql( nsData.getText(row, pk.getData(i).toString())) ).append("'");
			}
					Generator.out("DEBUG", sb.toString());
					nc.Query( sb.toString()  );
				} catch (Exception e) { 
					Generator.err("DEBUGX", e.getMessage());  }
		}	
	}
		DevActivity.insertTable(nc, nsData, tname);		
	}
	
	public static void sendMobileActivityData(Context context){
		NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
		Nikitaset ns = nc.Query("SELECT * FROM mobileactivity WHERE status = 1"); //0=draft,1=unsent(finish),2=sent, 
		//Log.i("MobileAction-sendMobileActivityData",   ns.getRows() +":" + ns.toNset().toJSON());
		
		for (int i = 0; i < ns.getRows(); i++) {			
			if (!sendMobileActivityData(context, ns, i).getData("status").toString().equalsIgnoreCase("OK")) {							
				break;
			}
		}//endrows	
		
//		NikitaConnection nc_old = Generator.getConnection(NikitaConnection.MOBILE_OLD);
//		Nikitaset ns_old = nc_old.Query("SELECT * FROM mobileactivity WHERE status = 1"); //0=draft,1=unsent(finish),2=sent, 
//		//Log.i("MobileAction-sendMobileActivityData",   ns.getRows() +":" + ns.toNset().toJSON());
//		
//		for (int i = 0; i < ns_old.getRows(); i++) {			
//			if (!sendMobileActivityDataOld(context, ns_old, i).getData("status").toString().equalsIgnoreCase("OK")) {							
//				break;
//			}
//		}//endrows	
	}
	 
	public static void sendHistoryActivity(Context context) {
		NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE);
		Nikitaset ns = nc.Query("SELECT * FROM historyactivity WHERE status = '0'");
		for (int j = 0; j < ns.getRows(); j++) {
			Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
			hashtable.put("userid", Utility.getSetting(context, "N-USER", ""));
			hashtable.put("id", ns.getText(j, "id"));
			hashtable.put("activity", ns.getText(j, "activity"));
			hashtable.put("datetime", ns.getText(j, "datetime"));
//			hashtable.put("status", ns.getText(j, "status"));
			
			String x = NikitaInternet.getString( NikitaInternet.postHttp(  AppNikita.getInstance().getBaseUrl()+"/mobile.mhistory", hashtable ) );
			Nset result = Nset.readJSON(  x  );
			
			if (result.getData("status").toString().equalsIgnoreCase("OK") && result.getData("error").toString().equals("")) {
				nc.Query("UPDATE historyactivity SET status = '1' WHERE id='" + ns.getText(j, "id") + "'");
			}
		}
		
	}
	
//	public static Nset sendMobileActivityDataOld(Context context, Nikitaset ns, int i){	
//		Nset stream = Nset.readJSON(ns.getText(i, "body").toString());
//		Generator.out("MOBILE", "sendMobileActivityData");	
//		String[] forms = stream.getObjectKeys(); Nset result = Nset.newObject();boolean berror = false;boolean cerror = false;
//		for (int f = 0; f < forms.length; f++) {
//			String[]  comps = stream.getData(forms[f]).getObjectKeys();				
//			for (int comp = 0; comp < comps.length; comp++) {
//				Nset nform = stream.getData(forms[f]);				
//				Generator.out("MOBILE", nform.toJSON());		
//				if ( nform.getData(comps[comp]).getData(3).toString().equals("file") && !nform.getData(comps[comp]).getData(0).toString().trim().equals("") ) {
//					//check image(att) is exist 	
//				 
//					if (isImageExist(Utility.getDefaultImagePath(nform.getData(comps[comp]).getData(0).toString()))) {
//						Generator.out("MOBILEs", "prepare");
//						Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
//						hashtable.put("imagename", nform.getData(comps[comp]).getData(0).toString());
//
//						hashtable.put("username", ns.getText(i, "username"));
//						hashtable.put("activityid", ns.getText(i, "activityid"));
//						hashtable.put("orderid", ns.getText(i, "orderid"));
//						hashtable.put("threadid", ns.getText(i, "threadid"));
//						hashtable.put("status", ns.getText(i, "status"));
//
//						String x = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimagecheck", hashtable));
//						result = Nset.readJSON(x);
//
//						if (!result.getData("error").toString().equals("")) {
//							berror = true;
//							break;
//						}
//
//						if (!result.getData("status").toString().equalsIgnoreCase("exist")) {
//							// send image(att)
//							if (!getCurrentNikitaCode().contains("noimageupload")) {
//								try {
//									x = NikitaInternet.getString(NikitaInternet.multipartHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimageupload", hashtable,
//											nform.getData(comps[comp]).getData(0).toString(),
//											new FileInputStream(Utility.getDefaultImagePath(nform.getData(comps[comp]).getData(0).toString()))));
//									result = Nset.readJSON(x);
//								} catch (Exception e) {
//								}
//							}
//						}
//						
//						//check image again 23-12-2016
//						String z = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mimagecheck", hashtable));
//						Nset resultx = Nset.readJSON(z);
//						
//						//if (!result.getData("error").toString().equals("")) {							
//						//	berror=true;
//						//	break;
//						//}
//						
//						//Edit Kevin
//						if (!resultx.getData("status").toString().equalsIgnoreCase("exist")) {
//							cerror = true;
//							break;
//						}
//					}
//				}				
//				
//			}//comps.length	
//			//if (!result.getData("error").toString().equals("")) {							
//			//	berror=true;
//			//	break;
//			//}
//		}//forms.length		
//		
//	
//		
//		if (!berror && !cerror) {
//			//send activity			
////			{branchid=16, threadid=, session=, create_date=2016-11-21 13:21:10, activityid=354599060368910.158858b981c.131e16b1c568, body={"e37f0136aa3ffaf149b351f6a4c948e9":{"nikitageneratorcurrentform":"dipo_mtodolist_child"},"dipo_mtodolist_child":{"ch_kelurahan":["","0","0",""],"lh26":["","1","0",""],"lbl_net_receivable":["OS Net Receivable :","1","0",""],"lh4":["","1","0",""],"lbl_id":["","0","0",""],"agreement_id":["0006045/1/17/03/2012","1","0",""],"lh21":["","1","0",""],"lh3":["","1","0",""],"current_status":["ICP","1","0",""],"alamat_lama_help":["JL. EMMY SAELAN NO 16,#UJUNG PANDANG#SAWERIGADING#MAKASSAR#90113","0","0",""],"cmb_status_hp":["1","1","0",""],"customer_name":["FRANS SALIM KALALO","1","0",""],"lh20":["","1","0",""],"result":["","1","0",""],"lbl_alamat_perubahan":["Alamat Perubahan :","1","0",""],"priority_flag":["0","1","0",""],"remark_adm_coll":["","1","0",""],"lh9":["","1","0",""],"expired_date":["2016-08-30","1","0",""],"lbl_alamat":["Alamat :","1","0",""],"lbl_jth_tempo":["Tgl Jatuh Tempo :","1","0",""],"lbl_prior":["1","0","0",""],"txt_ptp_amount":["","0","0",""],"lh5":["","1","0",""],"overdue_days":["214","1","0",""],"lh1":["","1","0",""],"lbl_total_period":["Total Period :","1","0",""],"lh10":["","1","0",""],"lbl_lat":["-6.165662","0","0",""],"ch_home_phone":["","0","0",""],"lh27":["","1","0",""],"lbl_no_kontrak":["No. Kontrak :","1","0",""],"ch_city":["","0","0",""],"lbl_mobile_old":["Mobile :","1","0",""],"lbl_unit":["Brand :","1","0",""],"cmb_visit_result":["10","1","0",""],"jlh_bayar":["150000000","1","0",""],"foto_1":["354599060368910.158858b6f3b.131ba6ee4ceb.30453.cam","1","0","file",""],"lh24":["","1","0",""],"lbl_nama_cust":["Nama Customer :","1","0",""],"lh6":["","1","0",""],"cmb_status_alamat":["1","1","0",""],"os_net_receivable":["1.374.860.186","1","0",""],"lh19":["","1","0",""],"lbl_telp_old":["Telp :","1","0",""],"func_separator_digit":["","0","0",""],"ptp":["","1","0",""],"foto_2":["","1","0","file",""],"lbl_no_polisi":["No Plat Kendaraan :","1","0",""],"btn_save_prior":["Save Prioritas","1","0",""],"lbl_balance":["Balance :","1","0",""],"lh7":["","1","0",""],"lbl_ptp_date":["PTP Date :","1","0",""],"lh15":["","1","0",""],"lbl_usl_paid":["USL Paid :","1","0",""],"modified_date":["2016-11-21 13:19:34","0","0",""],"tgl_bayar":["","0","0",""],"lh14":["","1","0",""],"ptp_date":["","1","0",""],"lbl_ptp_amount":["PTP Amount :","1","0",""],"lh23":["","1","0",""],"lh2":["","1","0",""],"tgl_janji_bayar":["","0","0",""],"lbl_overdue_days":["Overdue Days :","1","0",""],"brand":["10 UNIT KOBELCO EXCAVATOR HYDRAULIC SK200-8 ACERA GEOSPEC SUPER","1","0",""],"due_date":["2016-02-29","1","0",""],"h_btn":["","1","0",""],"lbl_remark_adm_coll":["Remark Admin Coll :","1","0",""],"curr_balance":["1.374.860.186","1","0",""],"hob_action":["FOLLOW UP","1","0",""],"visit_result":["","0","0",""],"txt_digit_separator":["","0","0",""],"ch_addr":["","0","0",""],"lh25":["","1","0",""],"lh8":["","1","0",""],"license_plate_no":["","1","0",""],"lbl_hob_action":["HOB Action :","1","0",""],"load":["","1","0",""],"lh16":["","1","0",""],"ptp_amount":["","0","0",""],"lbl_current_status":["Current Status :","1","0",""],"lh17":["","1","0",""],"cmb_status_pembayaran":["1","1","0",""],"jlh_angsuran":["196.408.598","1","0",""],"lh11":["","1","0",""],"telp_perubahan":["","1","0",""],"lbl_telp_perubahan":["Telp Perubahan :","1","0",""],"total_periods":["14","1","0",""],"bertemu_dengan":["1","1","0",""],"lh22":["","1","0",""],"txt_flag":["send","0","0",""],"alamat_perubahan":["","1","0",""],"telp_old":["0411330555, 0411858398, ","1","0",""],"other":["","0","0",""],"generate_date":["2016-09-30 00:00:00.0","0","0",""],"alamat_baru_help":["","0","0",""],"lbl_lon":["106.7785258","0","0",""],"btn_save":["SAVE","1","0",""],"broadcast":["com.nikita.generator","1","0",""],"ch_mobile_phone":["","0","0",""],"lh12":["","1","0",""],"btn_send":["SUBMIT","1","0",""],"remark":["bayar","1","0",""],"lbl_curr_unit":["Current Unit :","1","0",""],"cmb_status_telp":["1","1","0",""],"contract_no":["","0","0",""],"func_separator_bayar":["","0","0",""],"lbl_mobile_perubahan":["Mobile Perubahan :","1","0",""],"lh13":["","1","0",""],"mobile_perubahan":["","1","0",""],"lbl_expired_date":["Expired Date :","1","0",""],"unit":["10","1","0",""],"func_validasi":["","0","0",""],"mobile_old":["081241018818, 081524333018, ","1","0",""],"lbl_ptp":["PTP :","1","0",""],"lh18":["","1","0",""],"lbl_curr_balance":["Current Balance :","1","0",""],"alamat":["JL. EMMY SAELAN NO 16,, UJUNG PANDANG, SAWERIGADING, MAKASSAR, 90113","1","0",""],"created_date":["2016-11-21 13:19:34","0","0",""],"ch_kecamatan":["","0","0",""],"usl_paid":["7","1","0",""],"balance":["1.374.860.186","1","0",""],"current_unit":["9","1","0",""],"lbl_flag":["","0","0",""],"ch_zip_code":["","0","0",""],"lbl_jlh_angsuran":["Jumlah Angsuran :","1","0",""],"onback":["","1","0",""]},"DefaultHeaderForm":{"righticon":["","1","1",""],"title":["EDIT TO DO LIST","1","1",""],"lefticon":["","1","1",""]}}, mobileactioncode=save, userid=field.coll3, logicversion=2.0.352, status=1, imei=354599060368910, username=, version=2.3.166, localdate=2016-11-21 13:31:49, batch=, auth=, orderid=0006045/1/17/03/2012}
//			String bodyx = Utility.encodeBase64(ns.getText(i, "body"));
//			Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
//			hashtable.put("body", bodyx);//stream data
//			//agreement id dan fc id
////			hashtable.put("userid", Utility.getSetting(context, "N-USER", ""));
//			hashtable.put("username", ns.getText(i, "username"));
//			hashtable.put("activityid", ns.getText(i, "activityid"));
//			hashtable.put("orderid", ns.getText(i, "orderid"));
//			hashtable.put("threadid", ns.getText(i, "threadid"));
//			hashtable.put("create_date", ns.getText(i, "createdate"));
//			hashtable.put("status", ns.getText(i, "status"));
//			hashtable.put("mobileactioncode", "save"); 
//			
//			String x = NikitaInternet.getString( NikitaInternet.postHttp(  AppNikita.getInstance().getBaseUrl()+"/mobile.mactivity", hashtable ) );
//			
//			result = Nset.readJSON(  x  );
//			if (result.getData("status").toString().equalsIgnoreCase("OK") && result.getData("error").toString().equals("")) {
//				NikitaConnection nc = Generator.getConnection(NikitaConnection.MOBILE_OLD);
////				if (result.containsKey("mobilesatatus")) {
////					nc.Query("UPDATE mobileactivity SET status = ? WHERE id = ?", result.getData("mobilesatatus").toString(), ns.getText(i, "id"));
////				}else{
//				nc.Query("UPDATE mobileactivity SET status = 2 WHERE id = ?", ns.getText(i, "id"));
////				}	
////				if (result.containsKey("mobileresult")) {
////					checkaltermobileactivty(nc);
////					nc.Query("UPDATE mobileactivity SET mobileresult = ? WHERE id = ?", result.getData("mobileresult").toString(), ns.getText(i, "id"));
////				}
//			}			
//		}	
//		return result;
//	}
	
 
	 
}
