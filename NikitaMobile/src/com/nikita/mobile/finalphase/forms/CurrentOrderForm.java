package com.nikita.mobile.finalphase.forms;

import android.os.Bundle;
import android.widget.Toast;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.Component.OnClickListener;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.generator.ui.ButtonUI;
import com.nikita.mobile.finalphase.generator.ui.TextAutoCompleteUI;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.rkrzmail.nikita.data.Nset;

public class CurrentOrderForm extends NikitaControler{
	 
		
	public void onCreateUI() {	
		 Toast.makeText(getActivity(), getParameterString("name"), Toast.LENGTH_LONG).show();
		 super.onCreateUI();
	}
  
	public void onLoad() {		 
		 NikitaForm nf = new NikitaForm(this);
		 nf.setStyle(Style.createStyle("n-width","MATCH"));
		 
		 TextAutoCompleteUI autoCompleteUI = new TextAutoCompleteUI(this);
		 autoCompleteUI.setText("");
		 autoCompleteUI.setData(Nset.readJSON("['asasasas', 'asasa','asasasa']",true));
		 autoCompleteUI.setStyle(Style.createStyle("n-width","MATCH"));
		 nf.addComponent(autoCompleteUI);
		 
		 for (int i = 0; i < 30; i++) {
			 ButtonUI buttonUI = new ButtonUI(this);
			 buttonUI.setText("aaaaaaaaaaaaaaa:"+i);
			 buttonUI.setStyle(Style.createStyle("n-width","MATCH"));
			 nf.addComponent(buttonUI);
			 buttonUI.setOnClickListener(new OnClickListener() {
				public void OnClick(Component component) {
					SentOrderForm orderForm = new SentOrderForm();
					orderForm.show();					
				}
			});			 
		 }
		 
		 
		 
		 
		 
		 setContent(nf);		  
	}
	
	public void onBack() {
		close();
		// if (getActivity()!=null) {
		//	 getActivity().finish();
		//}
	}
	 
}
