package com.nikita.mobile.finalphase.activity;

import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.R;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
 

public class SplashScreen extends AndroidActivity { 
    protected void onCreate(Bundle savedInstanceState) {    	 
    	super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen); 
		AppNikita.getInstance().setVirtual("@+SETTING+LAUNCHERICONID", R.drawable.generator+"");
		
		TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
		String imei=tm.getDeviceId().toUpperCase();
		String startupform =Generator.getNikitaParameters().getData("INIT").toString();
		if (Generator.getNikitaParameters().containsKey("INIT-STARTUP-"+imei)) {
			startupform = Generator.getNikitaParameters().getData("INIT-STARTUP-"+imei).toString();
		}
		startupNikita(startupform);
		//Generator.startNikitaForm(this, CurrentOrderForm.class);/
    }
	private void startupNikita(String startupform){
		if (startupform.endsWith("Activity")) {
			if (Generator.isClassExist("com.nikita.mobile.activity."+startupform)) {
				try {
					Intent intent = new Intent(SplashScreen.this, Class.forName("com.nikita.mobile.activity."+startupform));	
					startActivity(intent);
					finish();
					return;
				} catch (Exception e) { }
			}
		}		
		Generator.startNikitaForm(this, startupform);
		finish();
	}


}
