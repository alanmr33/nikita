package com.nikita.mobile.finalphase.generator;

import java.util.Vector;

import com.nikita.mobile.finalphase.generator.action.BluetoothPrintAction;
import com.nikita.mobile.finalphase.generator.action.SystemAction;
import com.nikita.mobile.finalphase.generator.ui.FileUploadUI;
import com.nikita.mobile.finalphase.utility.BusyDialog;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ListView;

public class NFormActivity extends Activity{
//	public static final String NIKITA_GENERATOR_NAME = "NIKITAFRAMEWORK-NIKITAGENERATOR-NFORM-OWNER";
//	public static final String NIKITA_GENERATOR_NFID = "NIKITAFRAMEWORK-NIKITAGENERATOR-NFORM-NFID";
//	  
//	
//	private void shortcutAdd(String name, int number) {
//	    // Intent to be send, when shortcut is pressed by user ("launched")
//	    Intent shortcutIntent = new Intent(getApplicationContext(), NFormActivity.class);
////shortcutIntent.setAction(Constants.ACTION_PLAY);
//
//	    // Create bitmap with number in it -> very default. You probably want to give it a more stylish look
//	    Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
//	    Paint paint = new Paint();
//	    paint.setColor(0xFF808080); // gray
//	    paint.setTextAlign(Paint.Align.CENTER);
//	    paint.setTextSize(50);
//	    new Canvas(bitmap).drawText(""+number, 50, 50, paint);
////((ImageView) findViewById(R.id.icon)).setImageBitmap(bitmap);
//
//	    // Decorate the shortcut
//	    Intent addIntent = new Intent();
//	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
//	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);
//
//	    // Inform launcher to create shortcut
//	    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
//	    getApplicationContext().sendBroadcast(addIntent);
//	}
//
//	private void shortcutDel(String name) {
//	    // Intent to be send, when shortcut is pressed by user ("launched")
//	    Intent shortcutIntent = new Intent(getApplicationContext(), NFormActivity.class);
////shortcutIntent.setAction(Constants.ACTION_PLAY);
//
//	    // Decorate the shortcut
//	    Intent delIntent = new Intent();
//	    delIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//	    delIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
//
//	    // Inform launcher to remove shortcut
//	    delIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
//	    getApplicationContext().sendBroadcast(delIntent);
//	}
//	
//	
//	private String fnameOwner = "";
//	public String getOwnerForm(){
//		return fnameOwner;
//	}
//
//	private NForm nFormOwner;
//	public NForm getCurrentForm(){
//		return nFormOwner;
//	}
//	public void switchtodrawer() {
//		setContentDrawer(root);
//	}
//	private boolean showDrawer = false;
//	private long nfid = 0;
//	private View root ;
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);		
//		activityResultListeners = new Vector<OnActivityResultListener>();
//		if ( getIntent()!=null) {
//			nfid= getIntent().getLongExtra(NIKITA_GENERATOR_NFID, 0);
//			fnameOwner = getIntent().getStringExtra(NIKITA_GENERATOR_NAME);
//		}					 
//		//super.setContentView(R.layout.nformactivity);	
//		root = Utility.getInflater(this, R.layout.nformactivity);
//		setContentView(root);
//		
//		
//		NCore.setCurrentActivity(this);
//		
//		nFormOwner = NCore.findInstanceForm(fnameOwner); 
//		if (nFormOwner!=null && nfid==nFormOwner.getNikitaGeneratorNfid())  {
//			if (nFormOwner.getNikitaGeneratorNfid()!=getIntent().getLongExtra(NIKITA_GENERATOR_NFID, 0)) {
//				finish();
//			}
//			nFormOwner.setActivityListener(new NForm.NFormActivityListener() {
//				public NFormActivity getActivity() {
//					return NFormActivity.this;
//				}
//			});
//			
//			
//			getCurrentForm().onCreate(savedInstanceState);
//			
//			showBusy(getCurrentForm().isBusy());
//			showDialogBox();
//		}else{
//			finish();
//		}
//		//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//	}
//	public View getViewHeader(){
//		if (root!=null) {
//			return root.findViewById(R.id.frmHeader);
//		}
//		return findViewById(R.id.frmHeader);
//	}
//	public View getViewContent(){
//		if (root!=null) {
//			return root.findViewById(R.id.frmContent);
//		}
//		return findViewById(R.id.frmContent);
//	}
//	public View getViewContentScroll(){
//		if (root!=null) {
//			return root.findViewById(R.id.frmContentScroll);
//		}
//		return findViewById(R.id.frmContentScroll);
//	}
//	private BusyDialog dialogBusy = null;
//	public void showBusy(boolean show){
//		if (dialogBusy==null) {
//			dialogBusy = new BusyDialog(this );
//		}
//		if (show) {
//			//dialogBusy.setMessage("-");
//			//dialogBusy.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//			dialogBusy.setCancelable(false);
//			 
//			dialogBusy.show();
//		}else{
//			dialogBusy.dismiss();
//		}		
//	}
//	public void showInfo(String title, String info) {
//		hideMessage();
//		Builder dlg = new AlertDialog.Builder(this);
//		dlg.setTitle(title);
//		dlg.setMessage(info);
//		alertDialog=dlg.create();
//		alertDialog.show();
//	}
//	public void showDialogBox() {
//		hideMessage();
//		
//		Nset data = getCurrentForm().getDialogData();
//		if (data!=null) {
//			Builder dlg = new AlertDialog.Builder(this);
//			dlg.setTitle("");
//			if (!data.getData("button1").toString().equals("")) {
//				dlg.setPositiveButton(data.getData("button1").toString(), new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						NForm nForm = getCurrentForm() ;
//						nForm.onActionResultNewThread( nForm.getDialogData().getData("requestcode").toString(), "button1",  nForm.getDialogData().getData("result"));
//						nForm.clearDialogData();
//						hideMessage();
//					}
//				});
//			}
//			if (!data.getData("button2").toString().equals("")) {
//				dlg.setNeutralButton(data.getData("button2").toString(), new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						NForm nForm = getCurrentForm() ;
//						nForm.onActionResultNewThread( nForm.getDialogData().getData("requestcode").toString(), "button2",  nForm.getDialogData().getData("result"));
//						nForm.clearDialogData();
//						hideMessage();
//					}
//				});
//			}
//			if (!data.getData("button3").toString().equals("")) {
//				dlg.setNegativeButton(data.getData("button3").toString(), new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int which) {
//						NForm nForm = getCurrentForm() ;
//						nForm.onActionResultNewThread( nForm.getDialogData().getData("requestcode").toString(), "button3",  nForm.getDialogData().getData("result"));
//						nForm.clearDialogData();
//						hideMessage();
//					}
//				});
//			}			
//			dlg.setCancelable(false);
//			dlg.setTitle(data.getData("title").toString());
//			dlg.setMessage(data.getData("msg").toString());
//			alertDialog=dlg.create();
//			alertDialog.show();
//		}
//	}
//	public void hideMessage(){
//		if (alertDialog!=null) {
//			if (alertDialog.isShowing()) {
//				alertDialog.dismiss();
//			}			
//		}
//	}
//	private AlertDialog alertDialog = null;
//	
//	
//	
// 	protected void onSaveInstanceState(Bundle outState) {
// 		getCurrentForm().onSaveState(outState);
//		//super.onSaveInstanceState(outState);
//	}
//	 
//	protected void onRestoreInstanceState(Bundle savedInstanceState) {
//		getCurrentForm().onRestoreState(savedInstanceState); 
//		super.onRestoreInstanceState(savedInstanceState);
//	}
//
//	public interface OnActivityResultListener{
//		public void onActivityResult(int requestCode, int resultCode, Intent data) ;
//	}
//	private Vector<OnActivityResultListener> activityResultListeners = new Vector<OnActivityResultListener>();
//	public void addOnActivityResultListener(OnActivityResultListener activityResultListener){
//		activityResultListeners.addElement(activityResultListener);
//	}
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		for (int i = 0; i < activityResultListeners.size(); i++) {
//			if (activityResultListeners.elementAt(i)!=null) {
//				activityResultListeners.elementAt(i).onActivityResult(requestCode, resultCode, data);
//			}
//		}
//		
//		String sRequesCode = "";
//		if (requestCode==SystemAction.RESULT_NATIVE_CALL) {
//			sRequesCode = "NATIVE";
//		}else if (requestCode==BluetoothPrintAction.RESULT_BLUETOOTH_SCAN) {
//			sRequesCode = "BLUETOOTH";
//		}else if (requestCode==FileUploadUI.RESULT_CAMERA_CAPTURE) {
//			sRequesCode = "CAMERA";
//		}else if (requestCode==FileUploadUI.RESULT_FILE_BROWSER) {
//			sRequesCode = "FILE";
//		}
//		
//		if (data==null) {
//			getCurrentForm().onActionResultNewThread(sRequesCode, resultCode+"",  Nset.newArray());
//		}else if (data.getExtras()!=null) {
//			 
//			Nset n = Nset.newObject();
//			String[] keys = data.getExtras().keySet().toArray(new String[data.getExtras().keySet().size()]);
//			for (int i = 0; i < keys.length; i++) {
//				if (data.getExtras().get(keys[i]) instanceof String) {
//					n.setData(keys[i], data.getExtras().getString(keys[i]) );
//				}else if (data.getExtras().get(keys[i]) instanceof Integer) {
//					n.setData(keys[i], data.getExtras().getInt(keys[i]) );
//				}else if (data.getExtras().get(keys[i]) instanceof Long) {
//					n.setData(keys[i], data.getExtras().getLong(keys[i]) );
//				}else if (data.getExtras().get(keys[i]) instanceof Boolean) {
//					n.setData(keys[i], data.getExtras().getBoolean(keys[i]) );
//				}else if (data.getExtras().get(keys[i]) instanceof Double) {
//					n.setData(keys[i], data.getExtras().getDouble(keys[i]) );
//				}else{
//				
//				}
//				
//			}
//			
//			getCurrentForm().onActionResultNewThread(sRequesCode, resultCode+"",  n);
//		}else{
//			getCurrentForm().onActionResultNewThread(sRequesCode, resultCode+"",  Nset.newArray());
//		}
//	} 
//		
//	public void closeActivity(){
//		setResult(1);
//		finish();
//	}
//	 
// 
//	public void onBackPressed() {	
//		if (getCurrentForm()!=null) {
//			if (!getCurrentForm().onActionBack()) {				
//				getCurrentForm().closeForm();
//			}
//		}else{
//			super.onBackPressed();
//		}
//	}
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//	
//
//
//
//
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//		if (actionBarDrawerToggle!=null) {
//			actionBarDrawerToggle.syncState();
//		}
//	}
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		if (actionBarDrawerToggle!=null) {
//			actionBarDrawerToggle.onConfigurationChanged(newConfig);
//		}
//	}
//	public void openListDrawer() {
//		if (drawerLayout!=null) {
//			drawerLayout.openDrawer(Gravity.LEFT);
//		}
//	}
//	 public void setListDrawer( int resource,  View vlist) {	
//		 View v = findViewById(R.id.drawer_list);	;
//		 if (v instanceof ListView) {
//			 ListView lstMenu = (ListView) v;	 Vector data = new Vector(); data.addElement("");
//			 lstMenu.setAdapter(new ArrayAdapter(this, 0 , data){
//				View vlist;
//				public ArrayAdapter get (View vlist){
//					this.vlist= vlist;;
//					return this;
//				}
//				public View getView(int position, View convertView, ViewGroup parent) {
//					if (vlist.getParent() instanceof ViewGroup) {
//						((ViewGroup)vlist.getParent()).removeView(vlist);
//					}
//					FrameLayout layout = new FrameLayout(vlist.getContext());
//					layout.addView(this.vlist);
//					
//					return layout;
//				}
//			 }.get(vlist));
//		 }
//	 }
//	
//	private ActionBarDrawerToggle actionBarDrawerToggle;
//	private DrawerLayout drawerLayout;
//	private  void setContentDrawer( View view) {		 
//  
//		
//		
//		super.setContentView(R.layout.nformdrawer);//must super
//		FrameLayout parent = (FrameLayout) findViewById(R.id.drawer_content_frame);
//		
//		//View view = Utility.getInflater(this, resource);
//		parent.addView(view, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//		 
//		
//		
//		//activity.getActionBar().setDisplayShowTitleEnabled(false);
//		//activity.getActionBar().setDisplayHomeAsUpEnabled(true);
//		//activity.getActionBar().setHomeButtonEnabled(true);
//
//		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.clear, R.string.icon, R.string.icon) {
//			public void onDrawerClosed(View drawerView) {
//				NFormActivity.this.invalidateOptionsMenu();
//			}
//			public void onDrawerOpened(View drawerView) {
//				NFormActivity.this.invalidateOptionsMenu();
//			}
//		};
//
//		drawerLayout.setDrawerListener(actionBarDrawerToggle);
//		 
//	}
//	
	
}
