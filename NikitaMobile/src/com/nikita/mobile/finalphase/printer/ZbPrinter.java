package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.bixolon.printer.BixolonPrinter;
import com.zebra.zq110.ZQ110;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
/*
 *
 */
public class ZbPrinter extends MobilePrinter {
	public static String deviceName;
	public static String deviceMAC;
	public static String battery;
	private Activity activity;
	static ZQ110 printer;
	private String  company;
	private int paper=2;
	private HashMap<String, String> printText;
	private boolean isConnected=false;
	private Bitmap logo;
	private final Handler mHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
				case ZQ110.MESSAGE_STATE_CHANGE:
					switch (msg.arg1) {
						case ZQ110.STATE_CONNECTED:
//					hideLoading();
							Toast.makeText(activity, deviceName+" is connected", Toast.LENGTH_LONG).show();
							printer.automateStatusBack(true);
							isConnected=true;
							break;

						case ZQ110.STATE_CONNECTING:
//					showLoading(deviceName+" is connecting");
							Log.i("Printer", deviceName+" is connecting");
							break;

						case ZQ110.STATE_NONE:
//					hideLoading();
							if(isConnected){
								isConnected=false;
								finishMessage(false, deviceName+" is disconnected",battery, deviceName, deviceMAC);
							}
							break;
					}
					return true;

				case ZQ110.MESSAGE_READ:
					dispatchMessage(msg);
					return true;

				case ZQ110.MESSAGE_DEVICE_NAME:
					deviceName = msg.getData().getString(ZQ110.KEY_STRING_DEVICE_NAME);
					if(deviceName.contains("2")){
						paper=2;
					}else{
						paper=3;
					}
					return true;

				case ZQ110.MESSAGE_TOAST:
					Toast.makeText(activity, msg.getData().getString(ZQ110.KEY_STRING_TOAST), Toast.LENGTH_SHORT).show();
					return true;

				case ZQ110.MESSAGE_BLUETOOTH_DEVICE_SET:
					if (msg.obj == null) {
						Toast.makeText(activity, "No paired device", Toast.LENGTH_SHORT).show();
					} else {
						showBluetoothDialog(activity, (Set<BluetoothDevice>) msg.obj);
					}
					return true;

				case ZQ110.MESSAGE_PRINT_COMPLETE:
					finishMessage(true, "Print Complete",battery, deviceName, deviceMAC);
					return true;
				case ZQ110.MESSAGE_ERROR_INVALID_ARGUMENT:
					finishMessage(false, "Invalid Argument",battery, deviceName, deviceMAC);
					return true;
				case ZQ110.MESSAGE_ERROR_OUT_OF_MEMORY:
					finishMessage(false, "Out of Memory",battery, deviceName, deviceMAC);
					return true;
			}
			return false;
		}
	});
	static void showBluetoothDialog(Context context, final Set<BluetoothDevice> pairedDevices) {
		final String[] items = new String[pairedDevices.size()];
		int index = 0;
		for (BluetoothDevice device : pairedDevices) {
			items[index++] = device.getAddress();
		}

		new AlertDialog.Builder(context).setTitle("Paired Bluetooth printers")
				.setItems(items, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						deviceName=items[which];
						deviceMAC=items[which];
						printer.connect(items[which]);

					}
				}).show();
	}
	public ZbPrinter(Activity activity){
		this.activity=activity;
		this.printer=new ZQ110(activity, mHandler, null);
	}
	public void setupPrintJob(String company,HashMap<String, String> printText){
		this.company=company;
		this.printText=printText;
		Log.i("Prinjob ", "for "+company);
	}
	public void startPrinting(){
		if(!isConnected){
			this.printer.findBluetoothPrinters();
		}else{
			printSJB();
		}
	}
	public void checkBattery(){
		if(!isConnected){
			this.printer.findBluetoothPrinters();
		}else{
			this.printer.getBatteryStatus();
		}
	}
	private void dispatchMessage(Message msg) {
		switch (msg.arg1) {
			case ZQ110.PROCESS_GET_PRINTER_ID:
				Bundle data = msg.getData();
				Toast.makeText(activity, data.getString(ZQ110.KEY_STRING_PRINTER_ID), Toast.LENGTH_SHORT).show();
				break;
			case ZQ110. PROCESS_GET_BATTERY_STATUS:
				switch (msg.arg2) {
					case ZQ110.STATUS_BATTERY_LOW:
						setBatteryStatus("LOW");
						break;
					case ZQ110.STATUS_BATTERY_MIDDLE:
						setBatteryStatus("MEDIUM");
						break;
					case ZQ110.STATUS_BATTERY_HIGH:
						setBatteryStatus("FULL");
					case ZQ110.STATUS_BATTERY_FULL:
						setBatteryStatus("FULL");
						break;
				}
				break;
			case ZQ110.PROCESS_AUTO_STATUS_BACK:
				StringBuffer buffer = new StringBuffer(0);
				if ((msg.arg2 & printer.AUTO_STATUS_COVER_OPEN) == printer.AUTO_STATUS_COVER_OPEN) {
					buffer.append("Cover is open.\n");
				}
				if ((msg.arg2 & printer.AUTO_STATUS_NO_PAPER) == printer.AUTO_STATUS_NO_PAPER) {
					buffer.append("Paper end sensor: no paper present.\n");
				}

				if (buffer.capacity() > 0) {
					finishMessage(false, buffer.toString(),battery, deviceName, deviceMAC);
				} else {
					printer.getBatteryStatus();
				}
				break;
			case ZQ110.PROCESS_EXECUTE_DIRECT_IO:
				buffer = new StringBuffer();
				data = msg.getData();
				byte[] response = data.getByteArray(ZQ110.KEY_STRING_DIRECT_IO);
				for (int i = 0; i < response.length && response[i] != 0; i++) {
					buffer.append(Integer.toHexString(response[i]) + " ");
				}

				Toast.makeText(activity, buffer.toString(), Toast.LENGTH_SHORT).show();
				break;
		}
	}
	public void setLogo(Bitmap logo){
		this.logo=logo;
	}
	public void printSJB(){
		TemplatePrinter template=new TemplatePrinter(activity, company,paper);
		//print logo
		printer.printBitmap(logo,ZQ110.ALIGNMENT_CENTER,ZQ110.BITMAP_WIDTH_FULL,75,true);
		//print title
		int attribute = 0;
//        attribute |= printer.TEXT_ATTRIBUTE_FONT_A;
//        attribute |= printer.TEXT_ATTRIBUTE_UNDERLINE1;
		int size = 0;
//        if(paper>2){
//        	size = printer.TEXT_SIZE_HORIZONTAL2;
//            size |= printer.TEXT_SIZE_VERTICAL2;
//        }else{
//        	size = printer.TEXT_SIZE_HORIZONTAL1;
//            size |= printer.TEXT_SIZE_VERTICAL1;
//        }
//        printer.printText(template.getSJBTitle(),ZQ110.ALIGNMENT_CENTER,attribute,size,false);
//        //print content
		String title="";
		String toPrint=printText.get("body");
		String[] printLines=toPrint.split("\n");
		toPrint=null;
		for(int i=0;i<printLines.length;i++){
			if(i>1){
				toPrint+=printLines[i]+" \n";
			}else{
				title=printLines[i]+"\n";
			}
		}
		paper=Integer.valueOf(printText.get("paper"));
		attribute |= ZQ110.TEXT_ATTRIBUTE_FONT_A;
		attribute |= ZQ110.TEXT_ATTRIBUTE_UNDERLINE1;
		if(paper>2){
			size = ZQ110.TEXT_SIZE_HORIZONTAL2;
			size |= ZQ110.TEXT_SIZE_VERTICAL2;
		}else{
			size = ZQ110.TEXT_SIZE_HORIZONTAL1;
			size |= ZQ110.TEXT_SIZE_VERTICAL1;
		}
		printer.printText(title,ZQ110.ALIGNMENT_CENTER,attribute,size,false);

		attribute = 0;
		size = 0;
		if(paper==3){
        	attribute |= ZQ110.TEXT_ATTRIBUTE_FONT_B;
        }else if(paper==1){
        	attribute |= ZQ110.TEXT_ATTRIBUTE_FONT_C;
        }else{
        	attribute |= ZQ110.TEXT_ATTRIBUTE_FONT_A;
        }
		size = ZQ110.TEXT_SIZE_HORIZONTAL1;
		size |= ZQ110.TEXT_SIZE_VERTICAL1;
//        String toPrint=template.generateSJBResult(printText);
        TextView txt=new TextView(activity);
        Typeface tf = Typeface.createFromAsset(activity.getAssets(), "tahoma.otf");
        txt.layout(0,0,256,40);
        txt.setTypeface(tf);
        txt.setTextSize(12);
        txt.setGravity(Gravity.CENTER_HORIZONTAL);
        txt.setDrawingCacheEnabled(true);
        txt.setText("---"+printText.get("footer")+"---");
		printer.printText(toPrint,ZQ110.ALIGNMENT_LEFT,attribute,size,false);
		printer.printText("---"+printText.get("footer")+"---",ZQ110.ALIGNMENT_CENTER,attribute,size,false);
//        printer.printBitmap(txt.getDrawingCache(),printer.ALIGNMENT_CENTER,printer.BITMAP_WIDTH_FULL,75,false,false,true);
		printer.lineFeed(3, false);
	}
	public void disconnect(){
		if(isConnected){
			isConnected=false;
			printer.automateStatusBack(false);
			printer.disconnect();
		}
	}

}
