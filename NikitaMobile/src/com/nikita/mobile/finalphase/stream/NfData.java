package com.nikita.mobile.finalphase.stream;

import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.utility.Utility;

public class NfData {
	private Hashtable<String, Hashtable<String, String>> master = new Hashtable<String, Hashtable<String, String>> ();
	public Hashtable getInternalObject(){
		return master;
	}
	public NfData(String stream){
		 Vector<String> rows  = Utility.splitVector(stream, "\r\n");
		 Hashtable<String, String> data = null;String header = null;
		 for (int i = 0; i < rows.size(); i++) {
			 if (rows.elementAt(i).startsWith("[") && rows.elementAt(i).endsWith("]")) {
				 if (data!=null && header!=null) {
					 master.put(header, data);
				 }
				 data = new Hashtable<String, String> ();
				 header=rows.elementAt(i);
			 }else{
				 int l = rows.elementAt(i).indexOf("=");
				 if (l>=0) {
					 data.put(rows.elementAt(i).substring(0,l), NfString.get(rows.elementAt(i).substring(l+1)));
				 }
			 }
		 }
		 if (data!=null && header!=null) {
			 master.put(header, data);
		 }
	}	
	 
	public String getData(String header, String key){
		header="["+header+"]";
		if (master.get(header)!=null) {
			if (master.get(header).get(key)!=null) {
				return master.get(header).get(key);
			}
		}
		return "";
	}
	public String getText(String header, String key){
		String s = getData(header, key);
		if (s.startsWith("!") && s.length()>=5) {
			return s.substring(5);
		}
		return s;
	}
	public boolean getVisible(String header, String key){
		String s = getData(header, key);
		if (s.startsWith("!") && s.length()>=5) {
			if (s.substring(1,2).equals("1")) {
				return true;
			}
		}
		return false;
	}
	public boolean getEnable(String header, String key){
		String s = getData(header, key);
		if (s.startsWith("!") && s.length()>=5) {
			if (s.substring(3,4).equals("1")) {
				return true;
			}
		}
		return false;
	}
}
