package com.nikita.mobile.finalphase.utility;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.BitmapFactory;
import android.graphics.YuvImage;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UtilityAndroid {
	private static Nset drw = Nset.newObject();
    static {
    	drw.setData("/drawable/n_order", R.drawable.n_order);
    	drw.setData("/drawable/n_change", R.drawable.n_change);
    	drw.setData("/drawable/n_closebatch", R.drawable.n_closebatch);
    	drw.setData("/drawable/n_draft", R.drawable.n_draft);
    	drw.setData("/drawable/n_home", R.drawable.n_home);
    	drw.setData("/drawable/n_back", R.drawable.n_back);
    	drw.setData("/drawable/n_information", R.drawable.n_information);
    	drw.setData("/drawable/n_menu", R.drawable.n_menu);
    	drw.setData("/drawable/n_sent", R.drawable.n_sent);
    	drw.setData("/drawable/n_order_blank", R.drawable.n_order_blank);
    	drw.setData("/drawable/n_pending", R.drawable.n_pending);
    	drw.setData("/drawable/n_setting", R.drawable.n_setting);
    	drw.setData("/drawable/n_logout", R.drawable.n_logout);
    	
    	drw.setData("/drawable/h", R.drawable.h);
    	drw.setData("/drawable/h_red", R.drawable.h_red);
    	
    	drw.setData("/drawable/d_changepswrd", R.drawable.d_changepswrd);
    	drw.setData("/drawable/d_delete", R.drawable.d_delete);
    	drw.setData("/drawable/d_logout", R.drawable.d_logout);
    	drw.setData("/drawable/d_pencil", R.drawable.d_pencil);
    	drw.setData("/drawable/d_setting", R.drawable.d_setting);
    	drw.setData("/drawable/d_todolist", R.drawable.d_todolist);
    	drw.setData("/drawable/d_pending", R.drawable.d_pending);
    	drw.setData("/drawable/d_delivered", R.drawable.d_delivered);
    	drw.setData("/drawable/d_data_todolist", R.drawable.d_data_todolist);
    	
    	drw.setData("/asset/n_logout", R.drawable.n_logout);
    }  
    
    public static int getDrawable(String drawname){
    	return drw.getData(drawname).toInteger();
    }
    public static void setListOrderIcon( int pos ,View v, Nset nSetting, String status, OnClickListener delete){
    	//lefticon 
		if (nSetting.containsKey("liststatusicon")&& nSetting.containsKey(status)) {
			((ImageView)v.findViewById(R.id.imageView1)).setImageResource(UtilityAndroid.getDrawable( nSetting.getData("liststatusicon").getData(status).toString() ));
		}else if (nSetting.containsKey("listicon")) {
			((ImageView)v.findViewById(R.id.imageView1)).setImageResource(UtilityAndroid.getDrawable( nSetting.getData("listicon").toString()  ));
		}else{
			
		}	
		//bagde[delete]		 
		if (nSetting.containsKey("listdelete")) {			
			v.findViewById(R.id.frmBagde).setVisibility(View.VISIBLE);
			((TextView)v.findViewById(R.id.txtBagde)).setVisibility(View.INVISIBLE);
			
			 Nset n = Nset.newObject().setData("position", pos).setData("list", nSetting.getData("listdelete").toJSON());
			((ImageView)v.findViewById(R.id.imgBagde)).setTag( n.toJSON());
			((ImageView)v.findViewById(R.id.imgBagde)).setImageResource(UtilityAndroid.getDrawable( nSetting.getData("listdelete").getData("icon").toString()  ));
			((ImageView)v.findViewById(R.id.imgBagde)).setOnClickListener(delete);
		}else{
			v.findViewById(R.id.frmBagde).setVisibility(View.GONE);
		}
		
    }
    public static void setHeaderBackground(Context context, View header){
    	String act = Generator.getNikitaParameters().getData("INIT-UI-HEADER").toString();
		Nset n = Nset.readJSON(act);
		if (n.containsKey("background") && header instanceof View) {
 
			header.setBackgroundResource(getDrawable(n.getData("background").toString()));
		}
    }
    public static boolean isMockSettingsON(Context context) {
    	boolean isMock = false;
    	if (android.os.Build.VERSION.SDK_INT >= 18) {
    	    //isMock = location.isFromMockProvider();
    	} else {
    	    //isMock = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION).equals("0");
    	}
    	
    	
    	  // returns true if mock location enabled, false if not enabled.
    	  if (Settings.Secure.getString(context.getContentResolver(),
    	                                Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
    	     return false;
    	  else
    	     return true;
	 }
    
    
    public static boolean areThereMockPermissionApps(Context context) {

    	  int count = 0;

    	  PackageManager pm = context.getPackageManager();
    	  List<ApplicationInfo> packages =
    	     pm.getInstalledApplications(PackageManager.GET_META_DATA);

    	  for (ApplicationInfo applicationInfo : packages) {
    	     try {
    	        PackageInfo packageInfo = pm.getPackageInfo(applicationInfo.packageName,
    	                                                    PackageManager.GET_PERMISSIONS);

    	        // Get Permissions
    	        String[] requestedPermissions = packageInfo.requestedPermissions;

    	        if (requestedPermissions != null) {
    	           for (int i = 0; i < requestedPermissions.length; i++) {
    	              if (requestedPermissions[i]
    	                  .equals("android.permission.ACCESS_MOCK_LOCATION")
    	                  && !applicationInfo.packageName.equals(context.getPackageName())) {
    	                 count++;
    	              }
    	           }
    	        }
    	     } catch (NameNotFoundException e) {
    	        //Log.e("Got exception " + e.getMessage());
    	     }
    	  }

    	  if (count > 0)
    	     return true;
    	  return false;
    	  }
    public static void setButtonActivity(View lnCnt, Nset n, OnClickListener listener){     
		if (n.containsKey("newbutton") && lnCnt instanceof LinearLayout) {
			LinearLayout lnr = (LinearLayout) lnCnt;
			for (int i = 0; i < n.getData("newbutton").getSize(); i++) {
				Button button = new Button(lnCnt.getContext());
				button.setText(n.getData("newbutton").getData(i).getData("text").toString());
				lnr.addView(button,new  LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				button.setTag(n.getData("newbutton").getData(i).toJSON());				
				button.setOnClickListener( listener );
			}
		}
    }
    
    public static int getAsset(String assetname){
    	ExifInterface exif;
		try {
			exif = new ExifInterface(assetname);
			exif.getAttribute(ExifInterface.TAG_DATETIME);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return 0;
    }
    
    public static void setImage(ImageView imageView, String src){    
    	Context context = AppNikita.getInstance();
    	if (src.startsWith("/drawable/")) {					
			imageView.setImageResource(getDrawable(src));
    	}else if (src.startsWith("/asset/")) {
    		//imageView.setImageDrawable(  context.getAssets().open(src.substring(7)));
    	}else if (src.contains("/static/")||src.contains("static/")) {	
    		
		}else if (src.startsWith("/")) {
			imageView.setImageBitmap(BitmapFactory.decodeFile(src));
		}else if (src.contains(":")) {
			 
		}else{
			  
		}	
    }
    
    
	
	public static void imageDownload(String url){
		 
	}
	
	
	public static void imageDownload(String url, ImageView imageView){
		
	}
	public static void saveDataLocal(String key, String json){
		
	}
	
	public static void viewDataLocal(String key, String json){
		
	}
	
	public static void InstallAPK(String filename){
	    File file = new File(filename); 
	    if(file.exists()){
	        try {   
	            String command;
	            filename =  (filename);//escape
	            command = "adb install -r " + filename;
	            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
	            proc.waitFor();
	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
	     }
	  }
	
	public void installApk(String filename) {
	    File file = new File(filename); 
	    if(file.exists()){
	        try {   
	            final String command = "pm install -r " + file.getAbsolutePath();
	            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", command });
	            proc.waitFor();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	     }
	}
}
