package com.rkrzmail.nikita.data;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
 
public class Encryptor {
    public static String encrypt(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            

            return new String(encrypted);//Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String decrypt(String key, String initVector, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(encrypted.getBytes());//Base64.decodeBase64(encrypted)

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        String key = "Bar12345Bar12345"; // 128 bit key
        String initVector = "RandomInitVector"; // 16 bytes IV

        System.out.println(decrypt(key, initVector,
                encrypt(key, initVector, "Hello World")));
    }
    
    
    
    
    
    
    
    
    
    static String IV = "AAAAAAAAAAAAAAAA";
	  static String plaintext = "test text 123\0\0\0"; /*Note null padding*/
	  static String encryptionKey = "0123456789abcdef";
    public class AES {
    	  
    	  public   void main(String [] args) {
    	    try {
    	      
    	      System.out.println("==Java==");
    	      System.out.println("plain:   " + plaintext);

    	      byte[] cipher = encrypt(plaintext, encryptionKey);

    	      System.out.print("cipher:  ");
    	      for (int i=0; i<cipher.length; i++)
    	        System.out.print(new Integer(cipher[i])+" ");
    	      System.out.println("");

    	      String decrypted = decrypt(cipher, encryptionKey);

    	      System.out.println("decrypt: " + decrypted);

    	    } catch (Exception e) {
    	      e.printStackTrace();
    	    } 
    	  }

    	  public   byte[] encrypt(String plainText, String encryptionKey) throws Exception {
    	    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
    	    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
    	    cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
    	    return cipher.doFinal(plainText.getBytes("UTF-8"));
    	  }

    	  public   String decrypt(byte[] cipherText, String encryptionKey) throws Exception{
    	    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
    	    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
    	    cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
    	    return new String(cipher.doFinal(cipherText),"UTF-8");
    	  }
    	}
}