package com.nikita.mobile.finalphase.generator.action;

import java.lang.reflect.Method;
import java.text.DecimalFormat;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;


public class SystemAction implements IAction {
	public static int RESULT_NATIVE_CALL = 13234567;
	
	public boolean OnAction(Component comp, Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        String data = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
        if (code.equals("out")) {
            Generator.out("SystemAction", data);
        }else if (code.equals("filternext")) {
        	//comp.getNikitaComponent().filterNext(data);          
        }else if (code.equals("break")) {
           return false;
        }else if (code.equals("close")||code.equals("exit")||code.equals("stop")) {
        	comp.getNikitaComponent().setLogicClose(true); 
        	comp.getNikitaComponent().setInterupt(true);
        }else if (code.equals("loop")) {           	
        	comp.getNikitaComponent().setLoopCount(comp.getNikitaComponent().getLoopCount()+1);
            int i = AUtility.getInt(data) ;            
            if ( i>=100000 ) {
                return false;//break
            }
            if (comp.getNikitaComponent().getLoopCount()>=i ) {              
                return true;//loop finish (next logic)
            } else{
            	comp.getNikitaComponent().setCurrentRow(0);//reset to 0
            }   
            
        }else if (code.equals("native")) {   
            String param1 = comp.getNikitaComponent().getVirtualString(currdata.getData("param1").toString()); 
            try {
                Object cls = Class.forName(param1).newInstance();                
                
            } catch (Exception e) {  }
        }else if (code.equals("property")){
            String result = "";
            if (data.equals("displaysize")||data.equals("size")) {
            	DecimalFormat df = new DecimalFormat("#.#");
            	result=df.format( Utility.getDisplaySizeInchi( comp.getNikitaComponent().getActivity() ));
            }else if (data.equals("width")||data.equals("widthpx")) {
            	DisplayMetrics dm = comp.getNikitaComponent().getActivity().getResources().getDisplayMetrics();
        		result=dm.widthPixels+"";
            }else if (data.equals("height")||data.equals("heightpx")) {
            	DisplayMetrics dm = comp.getNikitaComponent().getActivity().getResources().getDisplayMetrics();
        		result=dm.heightPixels+"";
            }else if (data.equals("resolution")||data.equals("resolutionpx")) {
            	DisplayMetrics dm = comp.getNikitaComponent().getActivity().getResources().getDisplayMetrics();
        		result=dm.widthPixels+"x"+dm.heightPixels;
            }else if (data.equals("resolutiondp")) {
            	Configuration con = comp.getNikitaComponent().getActivity().getResources().getConfiguration();
        		result=con.screenWidthDp +"x"+con.screenHeightDp;
            }else if (data.equals("density")) {
            	DisplayMetrics dm = comp.getNikitaComponent().getActivity().getResources().getDisplayMetrics();
        		result=dm.density+"";
            }else if (data.equals("dp")||data.equals("dip")) {
            	DisplayMetrics dm = comp.getNikitaComponent().getActivity().getResources().getDisplayMetrics();
        		result=(dm.density*160)+"";
            }else if (data.equals("gadget")) {//smartphone,table,dekstop,web
                if (Utility.isTablet(comp.getNikitaComponent().getActivity())) {
                	result="TABLET";
				}else{
					result="SMARTPHONE";
				}
            }else if (data.equals("deviceos")||data.equals("devicename")) {
            	result = "MOBILE-ANDROID "+Build.VERSION.SDK_INT+"";
            }else if (data.equals("merk")) {  
            	result = Build.MANUFACTURER;
            }else if (data.equals("model")) {
            	result = Build.MODEL;
            }else if (data.equals("imei"))  {
            	TelephonyManager tm = (TelephonyManager) comp.getNikitaComponent().getActivity().getSystemService(android.content.Context.TELEPHONY_SERVICE);
            	result=tm.getDeviceId().toUpperCase();
            }else if (data.equals("bluetooth"))  {
            	BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
                if (myDevice==null) {
                	result="NONE";
				} else  if ( myDevice.isEnabled()){
					result="ON";
				} else {	
					result="OFF";
				}
            }else if (data.equals("wifi")) {
            	WifiManager wifi = (WifiManager)comp.getNikitaComponent().getActivity().getSystemService(Context.WIFI_SERVICE);
            	if (wifi==null) {
                	result="NONE";
				} else  if ( wifi.isWifiEnabled()){
					result="ON";
				} else {	
					result="OFF";
				}
            }else if (data.equals("gps")) {
            	 LocationManager manager = (LocationManager)comp.getNikitaComponent().getActivity().getSystemService    (Context.LOCATION_SERVICE );
            	 if (manager==null) {
                    result="NONE";
 				 } else  if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER )  ){
 					result="GPS";
 				 } else  if ( manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER )  ){
  					result="NETWORK";
 				 } else {	
 					result="OFF";
 				 }
            }else if (data.equals("mobilenetwork")) { 
            	ConnectivityManager cm = (ConnectivityManager) comp.getNikitaComponent().getActivity() .getSystemService(Context.CONNECTIVITY_SERVICE);
                if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable()) {
                	result="ON";
				}else{
					result="OFF";
				}                 
            }else if (data.equals("location")) { 
            	 
            }else if (data.equals("version")) { 
            	PackageInfo pInfo;
				try {
					pInfo = comp.getNikitaComponent().getActivity().getPackageManager().getPackageInfo(comp.getNikitaComponent().getActivity().getPackageName(), 0);
					result = "MOBILE-ANDROID "+pInfo.versionName;
				} catch (NameNotFoundException e) {
					result = "MOBILE-ANDROID ";
				}
               
            }
 
                     
            comp.getNikitaComponent().setVirtual(currdata.getData("args").getData("result").toString(), result);       
        }else if (code.equals("call"))  {
        	String mode = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("mode").toString());  
        	
        	String sIntent = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
    		String sClass = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());  
    		String sMethode = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString());  
    		Object xParam = comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param4").toString());  
    		Object sNative = comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param5").toString());  
    		
    		comp.getNikitaComponent().setVirtualRegistered("@ERROR", "");
    		
        	if (mode.equals("intent")) {    
        		try {
        			Intent intent = new Intent(sIntent);
         	        intent.setPackage(sClass);
         	        Nset param = new Nset(xParam);
         	        String keys[] =param.getObjectKeys();
         	        for (int i = 0; i < keys.length; i++) {
						if (param.getData(keys[i]).isNumber()) {
							intent.putExtra(keys[i], param.getData(keys[i]).toLong());
						}else{
							intent.putExtra(keys[i], param.getData(keys[i]).toString());
						}
					}         	        
         	        comp.getNikitaComponent().getActivity().startActivityForResult(intent, RESULT_NATIVE_CALL);//native
				} catch (Exception e) { comp.getNikitaComponent().setVirtualRegistered("@ERROR", e.getMessage());  }
        	 }else if (mode.equals("static")) {  
        		 try {         			 
         			Class<?> cls = Class.forName("MyClass");
         			Method m = cls.getMethod(sMethode, String[].class);
         			String[] params = null; 
         			m.invoke(null, (Object) params); 
 				} catch (Exception e) { comp.getNikitaComponent().setVirtualRegistered("@ERROR", e.getMessage());  }
        	 }
        	
        	
        	
        }
        return true;
	}
}
