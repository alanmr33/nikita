package com.nikita.mobile.finalphase.generator.ui;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.Global;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class ContentUI extends Component{

	static final int STARTDATE = 1;
	
	public ContentUI(NikitaControler form) {
		super(form);
	}
	
	private List<String> list;
	private List<String> code;
 
	private View view;
	 
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compcontent);

		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		
		
		
		view.findViewById(R.id.tblContent).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (getData().getArraySize()>1 || getData().getObjectKeys().length>1) {
				
					code = new Vector<String>();
					list = new Vector<String>();
					for (int i = 0; i < getData().getArraySize(); i++) {
						if(getData().getData(i).getData("id").toString().length()>=1){
							code.add(getData().getData(i).getData("id").toString());
				        	list.add(getData().getData(i).getData("text").toString());
				        }else if(getData().getData(i).getArraySize()>=2){
				        	code.add(getData().getData(i).getData(0).toString());
				        	list.add(getData().getData(i).getData(1).toString());
				        }else if(getData().getData(i).getArraySize()>=1){
				        	code.add(getData().getData(i).getData(0).toString());
				        	list.add(getData().getData(i).getData(0).toString());
				        }else{
				        	code.add(getData().getData(i).toString());
				        	list.add(getData().getData(i).toString());
				        }  
					}
					
					String[] items = list.toArray(new String[list.size()]); 
					AlertDialog.Builder builder = new AlertDialog.Builder(getNikitaComponent().getActivity());  
					builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, final int which) {
							dialog.dismiss();
							getNikitaComponent().runOnActionThread(new Runnable() {
								public void run() {
									runRouteAction("click-"+code.get(which))	;
								}
							});							
						}
					});
					 
					builder.show(); 
				}else{
					getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							runRoute();							
						}
					});	
				}
				
				
				
				
				 
			}
		});
		
		return view;
	}	 
	 
	    
	
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	
	@Override
	public void setData(Nset data) {
		super.setData(data);
		refreshView();
	}

	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	 
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		 
		if (getData().getArraySize()>1 || getData().getObjectKeys().length>1) {
			((Button)view.findViewById(R.id.tblContent)).setText("  ::::  ");
		}		
		
		View v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		
		
		((Button)view.findViewById(R.id.tblContent)).setEnabled(getEnable());	
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.lstView);
			if (v instanceof ListView) {
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	

	
}
