package com.nikita.mobile.finalphase.connection;

import java.io.InputStream;



public class NikitaMultipart {
	private InputStream is ;
	private StringBuffer stringBuffer = new StringBuffer(); 
	
	public NikitaMultipart (){
		 
	}
	public NikitaMultipart (InputStream is){
		this.is=is;
	}
	
	public boolean isNikitaMultipart(){
		return false;
	}
	 
	public String getInputStreamString(){
		try {
			int current = 0;
			while ((current = is.read()) != -1) {
				stringBuffer.append((char) current);
			}
		} catch (Exception e) {}
		return stringBuffer.toString();
	}
	
	 
	
	
}
