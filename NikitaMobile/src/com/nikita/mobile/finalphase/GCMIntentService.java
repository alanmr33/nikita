
package com.nikita.mobile.finalphase;

 
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.nikita.mobile.finalphase.activity.SplashScreen;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
 
public class GCMIntentService extends GCMBaseIntentService {
    private static final String TAG = "Nikita [GCM]";

    public GCMIntentService() {
        super( AppNikita.GCMID );
    }

    protected void onRegistered(Context context, String registrationId) {
    	Utility.setSetting(getApplicationContext(), "PUSHID", registrationId);
        Log.i(TAG, "Device registered: regId = " + registrationId);
    }
   
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
 
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            //ServerUtilities.unregister(context, registrationId);
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }
    }
 
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        // notifies user
        //generateNotification(context, message);
        
        
        Intent   filter= new Intent();
	    filter.setAction("com.nikita.generator");
	    filter.putExtra("uuid", "pushmessage");
	    filter.putExtra("provider", "gcm");
	    filter.putExtra("methode", "onMessage");
	    filter.putExtra("data", intent.getExtras().getString("data")!=null?intent.getExtras().getString("data"):"");	    
	    filter.putExtra("message", intent.getExtras().getString("message")!=null?intent.getExtras().getString("message"):"");	    
	    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
 
    }

 
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        // notifies user
        //generateNotification(context, message);
        
        Intent   filter= new Intent();
	    filter.setAction("com.nikita.generator");
	    filter.putExtra("uuid", "pushmessage");
	    filter.putExtra("provider", "gcm");
	    filter.putExtra("methode", "onDeletedMessages");
	    filter.putExtra("total", total);	    
	    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
       
        Intent   filter= new Intent();
	    filter.setAction("com.nikita.generator");
	    filter.putExtra("uuid", "pushmessage");
	    filter.putExtra("provider", "gcm");
	    filter.putExtra("methode", "onError");
	    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        Log.i(TAG, "Received recoverable error: " + errorId);
        //displayMessage(context, "");
        return super.onRecoverableError(context, errorId);
    }

 
    private static void generateNotification(Context context, String message) {
        int icon = R.drawable.generator;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.appname);
        Intent notificationIntent = new Intent(context, SplashScreen.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

}
