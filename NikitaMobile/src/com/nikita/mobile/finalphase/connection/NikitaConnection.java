/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikita.mobile.finalphase.connection;

import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.NikitaCursor;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * created by 13k.mail@gmail.com
 */

public class NikitaConnection {
	public static final String LOGIC      = "logic";
    public static final String MOBILE     = "mobile";
    //public static final String MOBILE_OLD    = "mobile_old";
    public static final String NIKITA     = "nikita";
    public static final String DEFAULT    = "default";
	    
    
    public static NikitaConnection getConnection(){
        return getConnection("default");
    }
    public static NikitaConnection getConnection(String name)  {
    	try {
			String s=defLogin.toString();
		} catch (Exception e) {}

        String cls = classDefault(defLogin.getData(name).getData("class").toString());
        if (cls.startsWith("nikitaconnection")) {
           return new NikitaWillyConnection().openConnection(cls, defLogin.getData(name).getData("url").toString(), defLogin.getData(name).getData("user").toString(), defLogin.getData(name).getData("pass").toString());      
        }else  if (cls.startsWith("androidconnection")||cls.startsWith("com.sqlite")||cls.startsWith("org.sqlite")||cls.contains("sqlite")||cls.contains("sqllite")) {
            return new NikitaLiteConnection().openConnection(cls, defLogin.getData(name).getData("url").toString(), defLogin.getData(name).getData("user").toString(), defLogin.getData(name).getData("pass").toString());      
         }
        return new NikitaConnection().openConnection(cls, defLogin.getData(name).getData("url").toString(), defLogin.getData(name).getData("user").toString(), defLogin.getData(name).getData("pass").toString()); 
    } 
    
    public Connection getInternalConnection(){
        return conn;
    }
    
    private NikitaConnection openConnection(String cname, String url, String user, String pass){
    	 user=user!=null?user:"";pass=pass!=null?pass:"";   error=null;     
         if (cname.contains("com.mysql.jdbc.Driver") ) {
             core = CORE_MYSQL;
         }else if (cname.contains("com.microsoft.sqlserver.jdbc.SQLServerDriver") ) {
             core = CORE_SQLSERVER;
         }else if (cname.contains("org.sqlite.JDBC") ) {
             core = CORE_SQLITE;
         }else if (cname.contains("oracle.jdbc.driver.OracleDriver") ) {
             core = CORE_ORACLE;
         }else if (cname.contains("nikitaconnection") ) {
             core = -1;
         }        
        try {
            
            Class.forName(cname); 
            if (user.trim().length()>=1 || pass.trim().length()>=1) {
                conn = DriverManager.getConnection(url,user,pass);
            }else{
                conn = DriverManager.getConnection(url);
            }            
        } catch (Exception e) { error=e.getMessage(); }
        return this;
    }
    private static String classDefault(String cls){
        if (cls.toLowerCase().equals("mysql")) {
            return "com.mysql.jdbc.Driver";
        }else if (cls.toLowerCase().equals("sqlserver")) {
            return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        }else if (cls.toLowerCase().equals("sqllite")) {
            return cls;
        }else if (cls.toLowerCase().equals("oracle")) {
            return "oracle.jdbc.driver.OracleDriver";
        }else if (cls.toLowerCase().equals("nikita")) {
            return "nikitaconnection";
        }
        return cls;
    }
    public String getError(){
        return error!=null?error:"";
    }
    private String error;
    private Connection conn ;
    
    public void closeConnection(){
        try {
            conn.close();
        } catch (Exception e) {}
    }
    public void clearError(){
        error="";
    }
    public void setAutoCommit(boolean auto){
        try {
            conn.setAutoCommit(auto);
        } catch (Exception e) { error = e.getMessage(); }
    }
    public void setSavepoint(){
        try {
            conn.setSavepoint();
        } catch (Exception e) { error = e.getMessage(); }
    }
    public void rollback(){
        try {
            conn.rollback();
        } catch (Exception e) { error = e.getMessage(); }
    }
    public void commit(){
        try {
            conn.commit();
        } catch (Exception e) { error = e.getMessage();  }
    }
    public boolean isClosed(){
        try {
        	if (conn !=null) {
                return conn.isClosed();
            }
        } catch (Exception e) {}
        return true;
    }
    public NikitaCursor QueryCursor(String sql, String...param){
         return new NikitaCursor("");
    }
    public Nikitaset Query(String sql, String...param){
        return QueryPage(-1, -1, sql, param);
    } 
    public Nikitaset QueryNF(String sql, String...param){
        return QueryPageNF(-1, -1, sql, param);
    }
    public Nikitaset QueryPageNF(int page, int rowperpage, String sql, String...param){
        Nset narr = Nset.newArray();
        if (param!=null) {
            for (int i = 0; i < param.length; i++) {
                int ipos =param[i].indexOf("|");
                if (ipos>=0 && ipos<=10) {
                    String var = param[i].substring(0, ipos).toLowerCase();
                    if (var.equals("i")||var.equals("int")||var.equals("(int)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("int")  );
                    }else if (var.equals("l")||var.equals("long")||var.equals("(long)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("long")  );
                    }else if (var.equals("dt")||var.equals("datetime")||var.equals("(datetime)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("datetime")  );
                    }else if (var.equals("t")||var.equals("time")||var.equals("(time)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("time")  );
                    }else if (var.equals("d")||var.equals("date")||var.equals("(date)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("date")  );
                    }else if (var.equals("f")||var.equals("float")||var.equals("(float)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("float")  );
                    }else if (var.equals("b")||var.equals("boolean")||var.equals("(boolean)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("boolean")  );
                    }else if (var.equals("double")||var.equals("(double)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("double")  );
                    }else if (var.equals("decimal")||var.equals("(decimal)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("decimal")  );
                    }else if (var.equals("#")||var.equals("n")||var.equals("number")||var.equals("(number)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("number")  );    
                    }else if (var.equals("noescape")||var.equals("(noescape)")) {
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("noescape")  );
                    }else{
                        narr.addData(Nset.newArray().addData(param[i].substring(ipos+1)).addData("")  );
                    }                   
                }else{
                    narr.addData(param[i]);
                }
            }
        } 
        return QueryPage(sql, narr, page, rowperpage);
    }
    public Nikitaset QueryPage(int page, int rowperpage, String sql, String...param){
        Nset narr = Nset.newArray();
        if (param!=null) {
            for (int i = 0; i < param.length; i++) {
               narr.addData(param[i]);
            }
        } 
        return QueryPage(sql, narr, page, rowperpage);
    }
    protected Nset specialQuery(String sql, Nset array, int page, int rowperpage){
        sql=sql.trim();
        Nset nset = Nset.newObject();
        if (sql.startsWith("{") && sql.endsWith("}")) {
            Nset n = Nset.readJSON(sql);
            if (n.getData("nfid").toString().equals("nikitaconnection")) {
                sql = n.getData("sql").toString();
                array = n.getData("arg");
            }
        }else if (sql.startsWith("[") && sql.endsWith("]")) {
        	 Nset n = Nset.readJSON(sql);
             if (n.getData(0).toString().equals("create")) {
                 StringBuffer sbuff = new StringBuffer();
                 sbuff.append("CREATE table ").append(n.getData(1).toString()).append("(");//id  INTEGER PRIMARY KEY AUTOINCREMENT
                 for (int i = 2; i < n.getArraySize(); i++) {
                       if (n.getData(i).isNset()) {
                             sbuff.append((i>2)?",":"").append(n.getData(i).getData(0).toString()).append(" ").append(n.getData(i).getData(1).toString());
			 			}else{
                             sbuff.append((i>2)?",":"").append(n.getData(i).toString()).append(" TEXT");
			 			}                   
	                 }  
	                 sbuff.append(")"); 
	                 sql = sbuff.toString();
	                 array = n.getData("arg");
             }
        }else{
            
        }           
        nset.setData("sql", sql);
        nset.setData("arg", array);
        return nset;
    }
    public Nikitaset QueryPage(String sql, Nset array, int page, int rowperpage){
    	sql=sql.trim();
        if (sql.endsWith(";")) {
            sql=sql.substring(0, sql.length()-1);
        }   
         
        String fname = null;
        String tname = null;
        if (sql.startsWith("::")) {
            sql = sql.substring(2);
            if (sql.contains("::")) {
                fname = sql.substring(0, sql.indexOf("::")) ;
                sql = sql.substring(sql.indexOf("::")+2) ;
                if (sql.contains("::")) {
                    tname = sql.substring(0, sql.indexOf("::")) ;
                    sql = sql.substring(sql.indexOf("::")+2) ;
                }
            }           
        }
        Nset nset = specialQuery(sql, array, page, rowperpage);
        sql = nset.getData("sql").toString();
        array = nset.getData("arg");
        
        
        Nikitaset rst;
        boolean useArg = (array.getArraySize()>=1);
        
        
        if (getError().length()==0) {
            try {          
                if (useArg) {
	                	Nset n = Nset.newObject();
	                    if (sql.contains("@?")) {
	                        String _searchStr = "@?";                            
	                        StringBuffer sb = new StringBuffer();
	                        int searchStringPos = sql.indexOf(_searchStr);
	                        int startPos = 0; int count = 0;
	                        int searchStringLength = _searchStr.length();
	                        while (searchStringPos != -1) {
	                            String _replacementStr = "";
	                            if (array.getData(count).isNsetObject()) {
	                                _replacementStr = array.getData(count).getData("val").toString() ;
	                            }else if (array.getData(count).isNsetArray()) { 
	                                _replacementStr = array.getData(count).getData(0).toString() ;
	                            }else{
	                                _replacementStr = array.getData(count).toString();
	                            }
	                            n.setData(""+count, "");
	                            sb.append(sql.substring(startPos, searchStringPos)).append(_replacementStr);
	                            startPos = searchStringPos + searchStringLength;
	                            searchStringPos = sql.indexOf(_searchStr, startPos);count++;
	                        }
	                        sb.append(sql.substring(startPos,sql.length()));
	                        sql = sb.toString();
	                    }
	                   
                        PreparedStatement statement = conn.prepareStatement(sql, java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY );
                        if (!sql.trim().toLowerCase().startsWith("select")) {
                            statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                        }
                        int esc = 0;
                        for (int l = 0; l < array.getArraySize(); l++) {              
                            int i = l-esc;
                            if (n.containsKey(l+"")) {
                                //abaikan
                                esc++;                        	
                            }else if (array.getData(i).isNsetObject()) {
                                if (array.getData(i).getData("var").toString().equals("date")||array.getData(i).getData("var").toString().equals("todate")) {
                                    statement.setDate(i+1, new java.sql.Date(Utility.getDate( array.getData(i).getData("val").toString()  )));
                                }else if (array.getData(i).getData("var").toString().equals("time")) {
                                    statement.setTime(i+1,  new java.sql.Time( Utility.getTime(array.getData(i).getData("val").toString()  )));
                                }else if (array.getData(i).getData("var").toString().equals("datetime")||array.getData(i).getData("var").toString().equals("todatetime")) {
                                    statement.setTimestamp(i+1,  new java.sql.Timestamp( Utility.getDateTime(array.getData(i).getData("val").toString()  )));
                                }else if (array.getData(i).getData("var").toString().equals("timestamp")) {
                                    statement.setTimestamp(i+1,  new java.sql.Timestamp(System.currentTimeMillis()) );
                                }else if (array.getData(i).getData("var").toString().equals("int")) {
                                    statement.setInt(i+1, array.getData(i).getData("val").toInteger());
                                }else if (array.getData(i).getData("var").toString().equals("boolean")) {
                                    statement.setBoolean(i+1, array.getData(i).getData("val").toBoolean());
                                }else if (array.getData(i).getData("var").toString().equals("long")) {
                                    statement.setLong(i+1, array.getData(i).getData("val").toLong());
                                }else if (array.getData(i).getData("var").toString().equals("number")) {
                                    statement.setLong(i+1, array.getData(i).getData("val").toLong());
                                }else if (array.getData(i).getData("var").toString().equals("double")) {
                                    statement.setDouble(i+1, array.getData(i).getData("val").toDouble());
                                }else if (array.getData(i).getData("var").toString().equals("decimal")) {
                                    statement.setDouble(i+1, array.getData(i).getData("val").toDouble());
                                }else if (array.getData(i).getData("var").toString().equals("float")) {
                                    statement.setFloat(i+1, Utility.getFloat(array.getData(i).getData("val").toString()));
                                }else if (array.getData(i).getData("var").toString().equals("noescape")) {
                                    //abaikan
                                }else{
                                    statement.setString(i+1, array.getData(i).getData("val").toString());
                                }         
                            }else if (array.getData(i).isNsetArray()) { 
                                if (array.getData(i).getData(1).toString().equals("date")||array.getData(i).getData(1).toString().equals("todate")) {
                                    statement.setDate(i+1, new java.sql.Date(Utility.getDate( array.getData(i).getData(0).toString()  )));
                                }else if (array.getData(i).getData(1).toString().equals("time")) {
                                    statement.setTime(i+1,  new java.sql.Time( Utility.getTime(array.getData(i).getData(0).toString()  )));
                                }else if (array.getData(i).getData(1).toString().equals("datetime")||array.getData(i).getData(1).toString().equals("todatetime")) {
                                    statement.setTimestamp(i+1,  new java.sql.Timestamp( Utility.getDateTime(array.getData(i).getData(0).toString()  )));
                                }else if (array.getData(i).getData(1).toString().equals("timestamp")) {
                                    statement.setTimestamp(i+1,  new java.sql.Timestamp(System.currentTimeMillis()) );
                                }else if (array.getData(i).getData(1).toString().equals("int")) {
                                    statement.setInt(i+1, array.getData(i).getData(0).toInteger());
                                }else if (array.getData(i).getData(1).toString().equals("boolean")) {
                                    statement.setBoolean(i+1, array.getData(i).getData(0).toBoolean());
                                }else if (array.getData(i).getData(1).toString().equals("long")) {
                                    statement.setLong(i+1, array.getData(i).getData(0).toLong());
                                }else if (array.getData(i).getData(1).toString().equals("number")) {
                                    statement.setLong(i+1, array.getData(i).getData(0).toLong());
                                }else if (array.getData(i).getData(1).toString().equals("double")) {
                                    statement.setDouble(i+1, array.getData(i).getData(0).toDouble());
                                }else if (array.getData(i).getData(1).toString().equals("decimal")) {
                                    statement.setDouble(i+1, array.getData(i).getData(0).toDouble());
                                }else if (array.getData(i).getData(1).toString().equals("float")) {
                                    statement.setFloat(i+1, Utility.getFloat(array.getData(i).getData(0).toString()));
                                }else if (array.getData(i).getData(1).toString().equals("noescape")) {
                                    //abaikan
                                }else{
                                    statement.setString(i+1, array.getData(i).getData(0).toString());
                                }      
                            }else if (array.getData(i).isNumber()) {
                                if (array.getData(i).toDouble()==array.getData(i).toLong()) {
                                     statement.setLong(i+1, array.getData(i).toLong());
                                }else{
                                     statement.setDouble(i+1, array.getData(i).toDouble());
                                }                               
                            }else{
                                statement.setString(i+1, array.getData(i).toString());
                            }                           
                        }                
                        ResultSet rs = null;
                        if (sql.trim().toLowerCase().startsWith("select")) {
                            rs = statement.executeQuery();
                        }else{
                            statement.execute();
                            try {
                                rs=statement.getGeneratedKeys();
                            } catch (Exception e) {  }
                        }
                        rst=new Nikitaset(rs, page, rowperpage);
                        if (rs!=null)   
                            rs.close();         
                        if (statement!=null) {
							statement.close();
						}
                }else{
                		
                        Statement statement = conn.createStatement(java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE, java.sql.ResultSet.CONCUR_READ_ONLY );
                        String queryString = sql;

                        ResultSet rs = null;
                        if (queryString.trim().toLowerCase().startsWith("select")) {
                            rs = statement.executeQuery(queryString);
                        }else{
                            statement.execute(queryString, Statement.RETURN_GENERATED_KEYS);
                            try {
                                rs=statement.getGeneratedKeys();
                            } catch (Exception e) {  }
                        }
                
                        rst=new Nikitaset(rs, page, rowperpage);
                        if (rs!=null)   
                            rs.close();
                        if (statement!=null) {
							statement.close();
						}
               }
            } catch (Exception e) {  rst =  new Nikitaset(e.getMessage());  }
        }else{
             rst =  new Nikitaset(getError());
        }   
        if (rst.getInfo() instanceof Nset) {
            ((Nset)rst.getInfo() ).setData("core", getDatabaseCore());
        }else{
            rst.setInfo(Nset.newObject());
            ((Nset)rst.getInfo() ).setData("core", getDatabaseCore());
        }
       return rst;
   }  
   private static Nset defLogin = Nset.readJSON((""
           + "{"
           + "'default':{'class':'sqlite','url':'def.db','user':'','pass':''},"
           + "'logic'  :{'class':'sqlite','url':'data.db','user':'','pass':''},"
           + "'mobile' :{'class':'sqlite','url':'mobile.db','user':'','pass':''}"
           //+ "'mobile_old' :{'class':'sqlite','url':'mobile_old.db','user':'','pass':''}"
           + "}"
           + ""),true);    
   static  {
       NikitaConnection.setDefaultConnectionSetting();
   }
   public static void setDefaultConnectionSetting() {
       //setDefaultConnectionSetting(new NikitaProperty("nikita.ini"));
	   Generator.out("DEBUG-N", defLogin.toJSON());
   }
   public static Nset getDefaultPropertySetting(){
       return defLogin;
   } 
   public static void setDefaultConnectionSetting(InputStream inputStream){
       setDefaultConnectionSetting(new NikitaProperty(inputStream));
   }
   public static void setDefaultConnectionSetting(NikitaProperty nikitaProperty){
       defLogin = nikitaProperty.read();
       if (defLogin.getData("init").getData("nikitaonline").toString().equals("true")) {
           Nikitaset nikitaset = new NikitaConnection().getConnection(NikitaConnection.LOGIC).Query("SELECT connname,connusername,connpassword,connclass,connurl FROM web_connection;", null);
           for (int i = 0; i < nikitaset.getRows(); i++) {
               if (!nikitaset.getText(i, 0).equals("")) {
                    Nset n = Nset.newObject();
                    n.setData("user",nikitaset.getText(i, 1).toString() );
                    n.setData("pass",nikitaset.getText(i, 2).toString() );
                    n.setData("class",nikitaset.getText(i, 3).toString() );
                    n.setData("url",nikitaset.getText(i, 4).toString() );
                    defLogin.setData(nikitaset.getText(i, 0).toString(), n);       
               }    
           }
       }       
   } 
   
   public boolean isNikitaConnection(){
       return false;
   }
   protected int core = -1;
   public int getDatabaseCore(){
       return core;
   }   
          
   public static final int CORE_MYSQL = 0;
   public static final int CORE_SQLSERVER = 1;
   public static final int CORE_ORACLE = 2;
   public static final int CORE_SQLITE = 3;
   public static String convertType(String type,  int toDB){
       type=type.toUpperCase();//CORE_MYSQL[0],CORE_SQLSERVER[1],CORE_ORACLE[2],CORE_SQLLITE[3]
       String[] ret= new String[]{"","","",""};
       if (type.equals("STRING")) {
           ret= new String[]{"VARCHAR","VARCHAR","VARCHAR2","TEXT"};
       }else if (type.startsWith("VARCHAR")) {
           ret= new String[]{"VARCHAR","VARCHAR","VARCHAR2","TEXT"};
       }else if (type.startsWith("TEXT")) { 
           ret= new String[]{"TEXT","TEXT","BLOB","TEXT"};            
           
       }else if (type.startsWith("DATE")) {  
           ret= new String[]{"DATE","DATE","DATE","TEXT"};
       }else if (type.startsWith("DATETIME")) {        
           ret= new String[]{"DATETIME","DATETIME","DATETIME","TEXT"};
       }else if (type.startsWith("TIMESTAMP")) {        
           ret= new String[]{"TIMESTAMP","TIMESTAMP","TIMESTAMP","TEXT"};    
           
       }else if (type.startsWith("BLOB")) {  
           ret= new String[]{"BLOB","BLOB","BLOB","BLOB"}; 
       }else if (type.startsWith("NULL")) {     
           ret= new String[]{"VARCHAR","VARCHAR","VARCHAR2","TEXT"};
       }else if (type.startsWith("BOOLEAN")) {    
           ret= new String[]{"BOOLEAN","BOOLEAN","VARCHAR2","TEXT"};
       }else if (type.startsWith("ENUM")) {       
           ret= new String[]{"STRING","STRING","STRING","TEXT"};
           
       }else if (type.startsWith("INT")) { 
           ret= new String[]{"INT","INT","INT","INTEGER"};
       }else if (type.startsWith("BIGINT")) {  
           ret= new String[]{"BIGINT","BIGINT","BIGINT","INTEGER"};
       }else if (type.startsWith("TINYINT")) {      
           ret= new String[]{"TINYINT","TINYINT","NUMBER","INTEGER"};
       }else if (type.startsWith("LONG")) { 
           ret= new String[]{"BIGINT","BIGINT","NUMBER","INTEGER"};
       }else if (type.startsWith("FLOAT")) { 
           ret= new String[]{"FLOAT","FLOAT","NUMBER","REAL"};
       }else if (type.startsWith("DECIMAL")) {   
           ret= new String[]{"DECIMAL","DECIMAL","NUMBER","REAL"};
       }else if (type.startsWith("REAL")) {   
           ret= new String[]{"DECIMAL","REAL","NUMBER","REAL"};
       }else if (type.startsWith("NUMBER")) {   
           ret= new String[]{"DECIMAL","DECIMAL","NUMBER","REAL"};
       }
       
       try {
           return ret[toDB];
       } catch (Exception e) { return ""; }        
   }
}
