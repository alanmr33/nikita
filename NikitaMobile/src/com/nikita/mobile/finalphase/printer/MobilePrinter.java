package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.util.Log;

public class MobilePrinter implements MobilePrinterInterface {
	public String deviceName;
	public String deviceMAC;
	public Activity activity;
	public OnFinishPrint onFinishPrint;
	public OnGetBatteryStatus onGetBatteryStatus;
	
	@Override
	public void setOnGetBatteryStatus(OnGetBatteryStatus onGetBatteryStatus) {
		// TODO Auto-generated method stub
		this.onGetBatteryStatus = onGetBatteryStatus;
	}
	@Override
	public void setBatteryStatus(String message) {
		// TODO Auto-generated method stub
		if (onGetBatteryStatus!=null) {
			onGetBatteryStatus.OnGet(message);
		}
	}
	public void setOnFinishPrint(OnFinishPrint onFinishPrint){
		this.onFinishPrint = onFinishPrint;
	}
	public void finishMessage(Boolean status, String message, String battery, String deviceName, String deviceMAC){
		if (onFinishPrint!=null) {
			onFinishPrint.OnFinish(status, message, battery, deviceName, deviceMAC);
		}
		Log.i("On Finish Print", status+" : "+message);
	}
	public void setupPrintJob(String company,HashMap<String, String> printText){
	
	}
	public void startPrinting(){
		
	}
	public void disconnect(){
	
    }
	public void setLogo(Bitmap logo){
		
	}
	@Override
	public void checkBattery() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void finishMessage(Boolean status, String message) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void testConnection() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void printTest() {
		// TODO Auto-generated method stub
		
	}
	public void setDeviceName(String deviceName){
		this.deviceName = deviceName;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceMAC(String deviceMAC){
		this.deviceMAC = deviceMAC;
	}
	public String getDeviceMAC() {
		return deviceMAC;
	}
	@Override
	public void setActivity(Activity activity) {
		// TODO Auto-generated method stub
		this.activity=activity;
	}
}
