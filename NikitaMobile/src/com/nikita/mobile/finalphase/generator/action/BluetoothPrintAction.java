package com.nikita.mobile.finalphase.generator.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
 
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.printer.BxlPrinter;
import com.nikita.mobile.finalphase.printer.OnFinishPrint;
import com.nikita.mobile.finalphase.printer.OnGetBatteryStatus;
import com.nikita.mobile.finalphase.utility.DeviceListActivity;
import com.nikita.mobile.finalphase.utility.PrintGraphics;
import com.rkrzmail.nikita.data.Nset;
import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;

public class BluetoothPrintAction implements IAction{
	public static int RESULT_BLUETOOTH_SCAN = 1234567890;
	
	public BluetoothService mService;
	public String mDeviceAdress;
	public BluetoothSocket mSocket;
	public Component comp;
	String image1 ="";
	String image2 ="";
	String data1 ="" ;
	String data2 ="" ;
	
	public boolean OnAction(final Component comp, final  Nset currdata) {
		this.comp=comp;
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        final String address= comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()); 
        image1 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()); 
        data1  = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()); 
        image2 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString()); 
        data2  = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param5").toString()); 
        //NCore.out("BluetoothPrintAction", code);
        
        if (code.equals("print")) {
        	comp.getNikitaComponent().showBusy(true);
        	try {
        		openBT(address);
        		
        		if (!image1.equals("")) {
        			dataWithImage(mmOutputStream,comp.getNikitaComponent(), image1, data1);
				}else if (!data1.equals("")) {
					sendData((data1).getBytes());
				}
				if (!image2.equals("")) {
					dataWithImage(mmOutputStream, comp.getNikitaComponent(), image2, data2);
				}else if (!data2.equals("")) {
					sendData((data2).getBytes());
				}
				 
				sendData(new byte[]{0x0A, 0x00,0x00});
        		closeBT();
        		sleepFlush();
			} catch (Exception e) { }
        	comp.getNikitaComponent().showBusy(false);
        	
        } else if(code.equals("printxpe")) {
			
			String image = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()); 
			String data  = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString()); 
			String type  = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString()); 
			String paper = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param5").toString());
			
			if (image.equals("")) {
				image = "logo_dipo_white.png";
			}
			
			/*
			 * set data to print
			 */
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("body"  , data);
			hashMap.put("footer", type);
			hashMap.put("paper" , paper);
			((AppNikita)comp.getNikitaComponent().getActivity().getApplicationContext()).printer.setupPrintJob("DSF", hashMap);
			
			/*
			 * set logo to print
			 */
			try {
				InputStream is = comp.getNikitaComponent().getActivity().getAssets().open(image);
				Bitmap logo = BitmapFactory.decodeStream(is);
				((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.setLogo(logo);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
//			((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.checkBattery();
			((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.startPrinting();
			comp.getNikitaComponent().showBusy(true);
			
//			((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.setOnGetBatteryStatus(new OnGetBatteryStatus() {
//				@Override
//				public void OnGet(String status) {
//					Nset messageN = Nset.newObject();
//					messageN.setData("BATTERY_STATUS", status);
//					comp.getNikitaComponent().onResult(address, "BATTERY", messageN);
//					((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.startPrinting();
//				}
//			});
			
			((NikitaActivity)comp.getNikitaComponent().getActivity()).printer.setOnFinishPrint(new OnFinishPrint() {				
				@Override
				public void OnFinish(Boolean status, String message, String battery, String deviceName, String deviceMAC) {
					comp.getNikitaComponent().showBusy(false);
					/*
					 * untuk log
					 */
					if (status) {
						Nset messageN = Nset.newObject();
						messageN.setData("PRINT_RESULT", "PRINTED");
						messageN.setData("PRINTER_NAME", deviceName);
						messageN.setData("PRINTER_MAC", deviceMAC);
						comp.getNikitaComponent().onResult(address, "PRINTER", messageN);
					} else {
						Nset messageN = Nset.newObject();
						messageN.setData("PRINT_RESULT", message);
						messageN.setData("PRINTER_NAME", deviceName);
						messageN.setData("PRINTER_MAC", deviceMAC);						
						comp.getNikitaComponent().onResult(address, "WARNING_ERROR", messageN);
						Toast.makeText(comp.getNikitaComponent().getActivity(), message,Toast.LENGTH_SHORT).show();
					}
				}
			});
			
		} else if (code.equals("pssrint")) {
        	comp.getNikitaComponent().showBusy(true);
        	 
			Handler handler = new Handler( AppNikita.getInstance().getStreamHandler().getLooper() ){ 		
				public void handleMessage(final Message msg) {
					 
					switch (msg.what) {
		            case BluetoothService.MESSAGE_STATE_CHANGE:
		            	
		            	Log.i("BLUE", "MESSAGE_STATE_CHANGE");
		                switch (msg.arg1) {
		                case BluetoothService.STATE_CONNECTED: 
		                	Log.i("BLUE", "STATE_CONNECTED");
		                	comp.runOnUI(new Runnable() {
		        				public void run() {
		        					Toast.makeText(comp.getNikitaComponent().getActivity(), "STATE_CONNECTED",  Toast.LENGTH_SHORT).show();
		        				}
		        			});

		                	
		                	byte[] cmd = new byte[3];
		                    cmd[0] = 0x1b;
		                    cmd[1] = 0x21;
		                   
		                        cmd[2] |= 0x10;
		                        mService.write(cmd);        
		                        mService.sendMessage(data1, "GBK"); 
		                        cmd[2] &= 0xEF;
		                        mService.write(cmd);     
		                        mService.sendMessage(data2,"GBK");
		                    
		                	
		                	Log.i("BLUE+",Thread.currentThread().getName());
		                	/*
		                	try {
		                		if (image1.equals("") && image2.equals("")) {
		                			//mService.sendMessage(data1+data2, "GBK"); 
		                			OutputStream os = mSocket.getOutputStream();
		                			os.write((data1+data2).getBytes());
		                			os.flush();
		                			os.close();
		                			mService.stop();
		                			mService=null;
		                			//mService.write((data1+data2).getBytes());
		                			//mService.write(new byte[] {0});
								}else{
									OutputStream os = mSocket.getOutputStream();	
									if (!image1.equals("")) {
										os.write(dataWithImage(comp.getNikitaComponent(), image1, data1));
									}else if (!data1.equals("")) {
										os.write((data1).getBytes());
									}
									if (!image2.equals("")) {
										os.write(dataWithImage(comp.getNikitaComponent(), image2, data2));
									}else if (!data2.equals("")) {
										os.write((data2).getBytes());
									}											
		            				os.flush();
		            				os.close();
		            				mService.stop();
		                			mService=null;
		            				 
		         				}
		                		comp.getNikitaComponent().showBusy(false);				            	
		            		} catch (IOException e) { }
		            		*/
		                	
		                	mService.stop();
		                	 for (Thread t : Thread.getAllStackTraces().keySet()) {
				 			        if ( t.getName().equals("ConnectedThread")|| t.getName().equals("ConnectThread")||t.getName().equals("AcceptThread")) {
										try {
											//t.interrupt();
										} catch (Exception e) { }
									}
				   			      //  Log.i("BLUE", t.getName());
				   			    }
		                	
		                    break;
		                case BluetoothService.STATE_CONNECTING: 
		                	//Log.i("BLUE", "STATE_CONNECTING");
		                	comp.getNikitaComponent().showBusy(true);
		                case BluetoothService.STATE_LISTEN:	 
		                	comp.getNikitaComponent().showBusy(true);
		                	//Log.i("BLUE", "STATE_LISTEN");
		                case BluetoothService.STATE_NONE:
		                	//Log.i("BLUE", "STATE_NONE");
		                }         
		                break;
		            case BluetoothService.MESSAGE_CONNECTION_LOST:
		            	Log.i("BLUE", "MESSAGE_CONNECTION_LOST");
		            	comp.runOnUI(new Runnable() {
	        				public void run() {
	        					Toast.makeText(comp.getNikitaComponent().getActivity(), "MESSAGE_CONNECTION_LOST",  Toast.LENGTH_SHORT).show();
	        				}
	        			});
		            	
		            	mService.stop();
            			mService=null;
		            	//ConnectedThread
		            	//AcceptThread
		            	
		            	comp.getNikitaComponent().showBusy(false);

		                break;
		            case BluetoothService.MESSAGE_UNABLE_CONNECT: 
		            	Log.i("BLUE+",Thread.currentThread().getName());
		            	Log.i("BLUE", "MESSAGE_UNABLE_CONNECT");
		            	comp.runOnUI(new Runnable() {
	        				public void run() {
	        					Toast.makeText(comp.getNikitaComponent().getActivity(), "trying connection ",  Toast.LENGTH_SHORT).show();
	        				}
	        			});
		        
		            	//comp.getNikitaComponent().showBusy(false);
		            	
		            	break;
		            case BluetoothService.MESSAGE_WRITE: 		            
		            	Log.i("BLUE", "MESSAGE_WRITE");
		            	break;
		            case BluetoothService.MESSAGE_READ: 		            
		            	Log.i("BLUE", "MESSAGE_READ");
		            	break;
		            }
				}
			};
			
			 
			BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			mBluetoothAdapter.cancelDiscovery();
			
			if (mService==null) {
				mService = new BluetoothService( comp.getNikitaComponent().getActivity(),  handler){		    			 
	    			public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
	    				mSocket=socket;  mDeviceAdress=device.getAddress();
	    				Log.i("BLUE-",Thread.currentThread().getName());
	    				super.connected(socket, device);
	    			}
	    			
	    		};
			}					
			
		    		
			if ( mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
					try {	    					
						if (mService.getState()==BluetoothService.STATE_CONNECTED||mService.getState()==BluetoothService.STATE_CONNECTING) {
							if (mDeviceAdress!=null  && mDeviceAdress.equals(address)) {
								//mService.sendMessage(data1+data2, "GBK"); 
								 /*
								try {
									if (image1.equals("") && image2.equals("")) {
			                			//mService.sendMessage(data1+data2, "GBK"); 
			                			OutputStream os = mSocket.getOutputStream();
			                			os.write((data1+data2).getBytes());
			                			os.flush();
			                			os.close();
			                			//mSocket.close();
			                			//mService.write((data1+data2).getBytes());
			                			//mService.write(new byte[] {0});
									}else{
										OutputStream os = mSocket.getOutputStream();	
										if (!image1.equals("")) {
											os.write(dataWithImage(comp.getNikitaComponent(), image1, data1));
										}else if (!data1.equals("")) {
											os.write((data1).getBytes());
										}
										if (!image2.equals("")) {
											os.write(dataWithImage(comp.getNikitaComponent(), image2, data2));
										}else if (!data2.equals("")) {
											os.write((data2).getBytes());
										}											
			            				os.flush();
			            				os.close();  
			         				}
								} catch (Exception e) { }
								*/
								comp.getNikitaComponent().showBusy(false);	
	    					}else{
								//mService.connect(mService.getDevByMac(address));
							}	    						
						}else{
							 
							mService.connect(mService.getDevByMac(address));
							Log.i("BLUE?",Thread.currentThread().getName());
						}	    					
					} catch (RuntimeException e) { }
			} else{
				comp.getNikitaComponent().showBusy(false);
			}				
			   		
			
			 
			    for (Thread t : Thread.getAllStackTraces().keySet()) {
			        
			      //  Log.i("BLUE", t.getName());
			    }
			   
        }else  if (code.equals("show")) {
        	comp.getNikitaComponent().runOnUiThread(new Runnable() {
				public void run() {
					Intent intent = new Intent(comp.getNikitaComponent().getActivity(), DeviceListActivity.class); 
		        	comp.getNikitaComponent().getActivity().startActivityForResult(intent, RESULT_BLUETOOTH_SCAN);
				}
			});
        }
		return true;
	}
	
	private void printImage() {
	    byte[] sendData = null;
	    PrintPic pg = new PrintPic();
	    pg.initCanvas(384);     
	    pg.initPaint();
	    pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
	    sendData = pg.printDraw();
	    mService.write(sendData);   
	}
	
	    BluetoothAdapter mBluetoothAdapter;
	    BluetoothSocket mmSocket;
	    BluetoothDevice mmDevice;
	 
	    OutputStream mmOutputStream;
	    InputStream mmInputStream;
	    Thread workerThread;
	 
	  void openBT(String address) throws IOException {
	        try {
	        	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	        	Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
	                    .getBondedDevices();
	            if (pairedDevices.size() > 0) {
	                for (BluetoothDevice device : pairedDevices) {
	 
	                    // MP300 is the name of the bluetooth printer device
	                    if (device.getAddress().equals(address)) {
	                 
	                        mmDevice = device;
	                        break;
	                    }
	                }
	            }
	            
	            // Standard SerialPortService ID
	            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
	            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
	             
	            mmSocket.connect();
	            mmOutputStream = mmSocket.getOutputStream();
	            mmInputStream = mmSocket.getInputStream();
	 
	           // beginListenForData();
	 
	          
	        } catch (NullPointerException e) {
	            e.printStackTrace();
	            Log.i("BLUE-1",e.getMessage());
	            comp.runOnUI(new Runnable() {
    				public void run() {
    					Toast.makeText(comp.getNikitaComponent().getActivity(), "PRINTER IS BUSY",  Toast.LENGTH_SHORT).show();
    				}
    			});
	        } catch (Exception e) {
	            e.printStackTrace();
	            Log.i("BLUE-1",e.getMessage());
	            comp.runOnUI(new Runnable() {
    				public void run() {
    					Toast.makeText(comp.getNikitaComponent().getActivity(), "PRINTER IS BUSY",  Toast.LENGTH_SHORT).show();
    				}
    			});
	        }
	    }
	  void sendData(byte[] msg) throws IOException {
		  try {
	         	for (int i = 0; i < msg.length; i++) {
	        		 mmOutputStream.write(msg[i]);	  
	        		 Thread.sleep(1);
	        		 if (i%128==0) {
	        			 mmOutputStream.flush();
	        			 Thread.sleep(100);
					}
				}
	        	 
	            mmOutputStream.flush();
	            Thread.sleep(1000);
	        } catch (NullPointerException e) {
	            e.printStackTrace();
	            Log.i("BLUE@2",e.getMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	            Log.i("BLUE@2",e.getMessage());
	        }
	  }
	  
	  void closeBT() throws IOException {
	        try {
	            //stopWorker = true;
	            mmOutputStream.close();
	            mmInputStream.close();
	            mmSocket.close();
	       
	        } catch (NullPointerException e) {
	        	Log.i("BLUE@",e.getMessage());
	            e.printStackTrace();
	        } catch (Exception e) {
	            e.printStackTrace();
	            Log.i("BLUE@",e.getMessage());
	        }
	    }

	  private void sleep(){
		  try {
				Thread.sleep(1);
			} catch (Exception e) { } 
	  };
	  private void sleepFlush(){
		  try {
			Thread.sleep(1000);
		} catch (Exception e) { } 
	  };
	  public byte[] dataWithImage(OutputStream out, NikitaControler form , String image , String data2) {      
       
        try {
       	 InputStream is = form.getActivity().getAssets().open(image);//n.png
       	 Bitmap img0 =BitmapFactory.decodeStream(is);
       	 
            boolean[] dots = getBitmapMonoChromeWithScalePrinter(img0);              
            
          
    		
    		 
    		int threshold=127;
     
            int multiplier = 570; // this depends on your printer model. for Beiyang you should use 1000
            double scale = (double)(multiplier/(int)img0.getWidth());
            int iheight = (int)(img0.getHeight() * scale);
            int iwidth = (int)(img0.getWidth() * scale);
            
            
            
            int integer = iwidth;
            byte[] width = new byte[4];
            for (int i = 0; i < 4; i++) {
           	 width[i] = (byte)(integer >>> (i * 8));
            }
            byte[] widthmsb=  new byte[] {
                    (byte)(integer >>> 24),
                    (byte)(integer >>> 16),
                    (byte)(integer >>> 8),
                    (byte)integer};
            
            
            int offset = 0;
    
            
            out.write((char)0x1B);
            out.write('3');
            out.write((byte)0);
            
            sleep();
            
            while (offset < iheight){
           	 
           	 
           	 //raster error
           	 //out.write((char)0x1D);
           	 //out.write((char)0x76);        
           	 // out.write((char)0x30);   
           	 
           	 //bit image
           	 out.write((char)0x1B);
           	 out.write('*');         // bit-image mode
           	 out.write((byte)33);    // 24-dot double-density
           	 out.write(width[0]);  // width low byte
           	 out.write(width[1]);  // width high byte
           	 
            	sleep();
           	 
                for (int x = 0; x < iwidth; ++x)   {
                    for (int k = 0; k < 3; ++k)  {
                        byte slice = 0;
                       
                        for (int b = 0; b < 8; ++b) {
                            int y = (((offset / 8) + k) * 8) + b;
                            // Calculate the location of the pixel we want in the bit array.
                            // It'll be at (y * width) + x.
                            int i = (y * iwidth) + x;

                            // If the image is shorter than 24 dots, pad with zero.
                            boolean v = false;
                            if (i < dots.length) {
                                v = dots[i];
                            }
                            slice |= (byte)((v ? 1 : 0) << (7 - b));
                        }

                        out.write(slice);                        
                       
                    }
                    sleep(); 
                }
                offset += 24;
                out.write((char)0x0A);
                out.flush();
                sleepFlush();
            }
          
            
            //fort 12x24
            out.write((char)0x1B);
            out.write((char)0x4D);
            out.write((byte)0);

            sleep();
            // Restore the line spacing to the default of 30 dots.
            out.write((char)0x1B);
            out.write('2');
           //out.write((byte)32);

            sleep();
            out.write(data2.getBytes());;
            
            out.flush();
            sleepFlush();
            return new byte[0];
        } catch (Exception e) {  }
        return new byte[0];
   }

    
	  public byte[] dataWithImage(NikitaControler form , String image , String data2) {
        
       
        try {
       	 InputStream is = form.getActivity().getAssets().open(image);//n.png
       	 Bitmap img0 =BitmapFactory.decodeStream(is);
       	 
            boolean[] dots = getBitmapMonoChromeWithScalePrinter(img0);              
            ByteArrayOutputStream out= new ByteArrayOutputStream();
          
    		
    		 
    		int threshold=127;
     
            int multiplier = 570; // this depends on your printer model. for Beiyang you should use 1000
            double scale = (double)(multiplier/(int)img0.getWidth());
            int iheight = (int)(img0.getHeight() * scale);
            int iwidth = (int)(img0.getWidth() * scale);
            
            
            
            int integer = iwidth;
            byte[] width = new byte[4];
            for (int i = 0; i < 4; i++) {
           	 width[i] = (byte)(integer >>> (i * 8));
            }
            byte[] widthmsb=  new byte[] {
                    (byte)(integer >>> 24),
                    (byte)(integer >>> 16),
                    (byte)(integer >>> 8),
                    (byte)integer};
            
            
            int offset = 0;
    
            
            out.write((char)0x1B);
            out.write('3');
            out.write((byte)0);
            
            while (offset < iheight){
           	 
           	 
           	 //raster error
           	 //out.write((char)0x1D);
           	 //out.write((char)0x76);        
           	 // out.write((char)0x30);   
           	 
           	 //bit image
           	 out.write((char)0x1B);
           	 out.write('*');         // bit-image mode
           	 out.write((byte)33);    // 24-dot double-density
           	 out.write(width[0]);  // width low byte
           	 out.write(width[1]);  // width high byte

           	 
                for (int x = 0; x < iwidth; ++x)   {
                    for (int k = 0; k < 3; ++k)  {
                        byte slice = 0;
                       
                        for (int b = 0; b < 8; ++b) {
                            int y = (((offset / 8) + k) * 8) + b;
                            // Calculate the location of the pixel we want in the bit array.
                            // It'll be at (y * width) + x.
                            int i = (y * iwidth) + x;

                            // If the image is shorter than 24 dots, pad with zero.
                            boolean v = false;
                            if (i < dots.length) {
                                v = dots[i];
                            }
                            slice |= (byte)((v ? 1 : 0) << (7 - b));
                        }

                        out.write(slice);
                    }
                }
                offset += 24;
                out.write((char)0x0A);
            }
          
            
            //fort 12x24
            out.write((char)0x1B);
            out.write((char)0x4D);
            out.write((byte)0);
            
            // Restore the line spacing to the default of 30 dots.
            out.write((char)0x1B);
            out.write('2');
           //out.write((byte)32);
            
            out.write(data2.getBytes());;
           
            return out.toByteArray();
        } catch (Exception e) {  }
        return new byte[0];
   }

    
	public boolean[] getBitmapMonoChromeWithScalePrinter(Bitmap img0) {
		int height = img0.getHeight(); 
		int width = img0.getWidth(); 
		
		 
		int threshold=127;

       int multiplier = 570; // this depends on your printer model. for Beiyang you should use 1000
       double scale = (double)(multiplier/(int)width);
       int xheight = (int)(height * scale);
       int xwidth = (int)(width * scale);
       int dimensions = xwidth * xheight;
     
		
		int index=0;
		boolean[] dots= new boolean[dimensions];
		
		for (int y = 0; y < xheight; y++) {
           for (int x = 0; x < xwidth; x++)  {
               int _x = (int)(x / scale);
               int _y = (int)(y / scale);
               int rgb = img0.getPixel(_x, _y); 
               int r = (rgb >> 16) & 0xFF;
               int g = (rgb >> 8) & 0xFF;
               int b = (rgb & 0xFF);
               //int gray = (r + g + b) / 3;
               int luminance = (int)(r* 0.3 + g * 0.59 + b * 0.11);
               dots[index] =    (luminance < threshold);
               index++;
           }
       }
		return dots;
   }
	
	public static byte[] LogoAsset (NikitaControler form, String imageName){
		PrintGraphics pg = new PrintGraphics();
		pg.initCanvas(380);
		pg.initPaint();
		try {
			InputStream is = form.getActivity().getAssets().open("logo.jpg", AssetManager.ACCESS_BUFFER);
			pg.drawImage(0, 0, is);
		}catch (Exception e) { }		
		return pg.printDraw();
	}
	
	public static byte[] Logo (NikitaControler form, String imageName){
		if (imageName.length()>=3) {
			try {
				new File("/sdcard/logo.jpg").delete();
			} catch (Exception e) {}
			try {
				InputStream is = form.getActivity().getAssets().open("logo.jpg");
				//Utility.copyFile(is, "/sdcard/logo.jpg");
			} catch (Exception e) { }
		}		
		PrintGraphics pg = new PrintGraphics();
		pg.initCanvas(540);
		pg.initPaint();
		pg.drawImage(0, 0, "/sdcard/logo.jpg");
		pg.printPng();
		
		return pg.printDraw();
	}
}
