package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;
import java.util.Set;

import org.json.JSONObject;

import com.bixolon.printer.BixolonPrinter;
import com.nikita.mobile.finalphase.service.NikitaFuseLocation;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class BxlPrinter extends MobilePrinter {
	public static String deviceName;
	public static String deviceMAC;
	public static String firmwareVersion;
	public static String manufacturer;
	public static String printerModel;
	public static String codePage;
	public static String battery;
	private Context ctx;
	static BixolonPrinter bixolonPrinter;
	private int paper = 2;
	private String company;
	private Bitmap logo;
	private HashMap<String, String> printText;
	private boolean isConnected = false;
	private boolean testConn = false;
	private boolean testPrint = false;
	private boolean isThread = false;
	public BxlPrinter(Context ctx) {
		this.ctx = ctx;
		BxlPrinter.bixolonPrinter = new BixolonPrinter(ctx, mHandler, null);
	}

	private final Handler mHandler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case BixolonPrinter.MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case BixolonPrinter.STATE_CONNECTING:
					Log.i("Printer", deviceName + " is connecting");
					break;

				case BixolonPrinter.STATE_CONNECTED:
					// bixolonPrinter.getPrinterId(BixolonPrinter.PRINTER_ID_FIRMWARE_VERSION);
					// bixolonPrinter.getPrinterId(BixolonPrinter.PRINTER_ID_MANUFACTURER);
					// bixolonPrinter.getPrinterId(BixolonPrinter.PRINTER_ID_PRINTER_MODEL);
					// bixolonPrinter.getPrinterId(BixolonPrinter.PRINTER_ID_CODE_PAGE);
					bixolonPrinter.automateStatusBack(true);
					if(!isThread){
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								bixolonPrinter.getBatteryStatus();
							}
						},30*60*1000);
						isThread=true;
					}
					// bixolonPrinter.setPowerSavingMode(false, 3000000);
					isConnected = true;
					if (!testConn && !testPrint) {
						Toast.makeText(ctx, deviceName + " is connected", Toast.LENGTH_LONG).show();
						startPrinting();
					} else if(testConn){
						Toast.makeText(ctx, "Printer Connection Success", Toast.LENGTH_LONG).show();
						finishMessage(true, "Printer Connection Success", battery, deviceName, deviceMAC);
						// bixolonPrinter.printText("Printer Test Page",
						// BixolonPrinter.ALIGNMENT_CENTER,
						// BixolonPrinter.TEXT_ATTRIBUTE_FONT_B,
						// BixolonPrinter.TEXT_SIZE_HORIZONTAL1 |
						// BixolonPrinter.TEXT_SIZE_VERTICAL1, true);
						testConn = false;
						disconnect();
					}else if(testPrint){
						printTest();
					}
					break;

				case BixolonPrinter.STATE_NONE:
					// Toast.makeText(ctx, deviceName + " is disconnected",
					// Toast.LENGTH_SHORT).show();
					// finishMessage(false, deviceName + " is disconnected",
					// battery);
					isConnected = false;
					break;
				}
				return true;

			case BixolonPrinter.MESSAGE_DEVICE_NAME:
				deviceName = msg.getData().getString(BixolonPrinter.KEY_STRING_DEVICE_NAME);
				if (deviceName.contains("2")) {
					paper = 2;
				} else {
					paper = 3;
				}
				return true;

			case BixolonPrinter.MESSAGE_TOAST:
				// if (!testConn) {
				Toast.makeText(ctx, msg.getData().getString(BixolonPrinter.KEY_STRING_TOAST), Toast.LENGTH_SHORT)
						.show();
				// }
				return true;

			case BixolonPrinter.MESSAGE_READ:
				dispatchMessage(msg);
				return true;

			case BixolonPrinter.MESSAGE_PRINT_COMPLETE:
				finishMessage(true, "Print Complete", battery, deviceName, deviceMAC);
				// if (printText.get("footer").equalsIgnoreCase("ORIGINAL")) {
				// printText.put("footer", "CUSTOMER COPY");
				// printDipoText(true);
				// }
//				bixolonPrinter.disconnect();
				if(testPrint){
					testPrint=false;
					disconnect();
				}
				return true;

			case BixolonPrinter.MESSAGE_ERROR_INVALID_ARGUMENT:
				finishMessage(false, "Invalid Argument", battery, deviceName, deviceMAC);
				return true;

			case BixolonPrinter.MESSAGE_ERROR_OUT_OF_MEMORY:
				finishMessage(false, "Out of Memory", battery, deviceName, deviceMAC);
				return true;
			}
			return false;
			// return true;
		}
	});

	public void setupPrintJob(String company, HashMap<String, String> printText) {
		this.company = company;
		this.printText = printText;
		Log.i("PrintJob ", "for " + company);
	}

	public void startPrinting() {
		if (!isConnected) {
			String mac = this.getDeviceMAC();
			if(mac!=null && !mac.equals("")){
				bixolonPrinter.connect(mac);
//			}else{
//				bixolonPrinter.findBluetoothPrinters();
//			}
			}
		} else {
			if (printText.get("footer").equalsIgnoreCase("ORIGINAL&COPY")) {
				printText.put("footer", "ORIGINAL");
				printDipoText(true);
			} else {
				printDipoText(false);
			}

		}
	}

	public void checkBattery() {
		if (!isConnected) {
			String mac = this.getDeviceMAC();
			if(mac!=null && !mac.equals("")){
				bixolonPrinter.connect(mac);
			}
//			}else{
//				bixolonPrinter.findBluetoothPrinters();
//			}
		} else {
			BxlPrinter.bixolonPrinter.getBatteryStatus();
		}
	}

	private void dispatchMessage(Message msg) {
		switch (msg.arg1) {
		case BixolonPrinter.PROCESS_GET_PRINTER_ID:
			Bundle data = msg.getData();
			Toast.makeText(ctx, data.getString(BixolonPrinter.KEY_STRING_PRINTER_ID), Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_GET_BS_CODE_PAGE:
			data = msg.getData();
			Toast.makeText(ctx, data.getString(BixolonPrinter.KEY_STRING_CODE_PAGE), Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_AUTO_STATUS_BACK:
			StringBuffer buffer = new StringBuffer(0);
			if (msg.arg2 == BixolonPrinter.AUTO_STATUS_COVER_OPEN) {
				buffer.append("Cover is open.\n");
			}
			if (msg.arg2 == BixolonPrinter.AUTO_STATUS_NO_PAPER) {
				buffer.append("Paper end sensor: no paper present.\n");
			}

			if (buffer.capacity() > 0) {
				finishMessage(false, buffer.toString(), battery, deviceName, deviceMAC);
			} 
			break;

		case BixolonPrinter.PROCESS_GET_BATTERY_STATUS:
			String statusBatery="";
			switch (msg.arg2) {
				case BixolonPrinter.STATUS_BATTERY_FULL:
					statusBatery="Battery is full";
					break;
				case BixolonPrinter.STATUS_BATTERY_HIGH:
					statusBatery="Battery is high";
					break;
				case BixolonPrinter.STATUS_BATTERY_MIDDLE:
					statusBatery="Battery is middle";
					break;
				case BixolonPrinter.STATUS_BATTERY_LOW:
					statusBatery="Battery is low";
					break;
			}
			Intent filter= new Intent();
		    filter.setAction("com.nikita.generator");
		    filter.putExtra("uuid", "batterychanged");
		    filter.putExtra("provider", "printer");
		    Nset n = Nset.newObject();
		    n.setData("battery_level", statusBatery);
		    filter.putExtra("data", n.toJSON());
		    ctx.sendBroadcast(filter);
			break;

		case BixolonPrinter.PROCESS_GET_STATUS:
			if (msg.arg2 == BixolonPrinter.STATUS_NORMAL) {
				Toast.makeText(ctx, "No error", Toast.LENGTH_SHORT).show();
			} else {
				StringBuffer buffer1 = new StringBuffer();
				if ((msg.arg2 & BixolonPrinter.STATUS_COVER_OPEN) == BixolonPrinter.STATUS_COVER_OPEN) {
					buffer1.append("Cover is open.\n");
				}
				if ((msg.arg2 & BixolonPrinter.STATUS_PAPER_NOT_PRESENT) == BixolonPrinter.STATUS_PAPER_NOT_PRESENT) {
					buffer1.append("Paper end sensor: paper not present.\n");
				}

				if (buffer1.capacity() > 0) {
					finishMessage(false, buffer1.toString(), battery, deviceName, deviceMAC);
				} else {
					bixolonPrinter.getBatteryStatus();
				}
				// Toast.makeText(ctx, buffer1.toString(),
				// Toast.LENGTH_SHORT).show();
			}
			break;

		case BixolonPrinter.PROCESS_GET_PRINT_SPEED:
			switch (msg.arg2) {
			case BixolonPrinter.PRINT_SPEED_LOW:
				Toast.makeText(ctx, "Print speed: low", Toast.LENGTH_SHORT).show();
				break;
			case BixolonPrinter.PRINT_SPEED_MEDIUM:
				Toast.makeText(ctx, "Print speed: medium", Toast.LENGTH_SHORT).show();
				break;
			case BixolonPrinter.PRINT_SPEED_HIGH:
				Toast.makeText(ctx, "Print speed: high", Toast.LENGTH_SHORT).show();
				break;
			}
			break;

		case BixolonPrinter.PROCESS_GET_POWER_SAVING_MODE:
			String text = "Power saving mode: ";
			if (msg.arg2 == 0) {
				text += false;
			} else {
				text += true + "\n(Power saving time: " + msg.arg2 + ")";
			}
			Toast.makeText(ctx, text, Toast.LENGTH_SHORT).show();
			break;

		case BixolonPrinter.PROCESS_EXECUTE_DIRECT_IO:
			buffer = new StringBuffer();
			data = msg.getData();
			byte[] response = data.getByteArray(BixolonPrinter.KEY_STRING_DIRECT_IO);
			for (int i = 0; i < response.length && response[i] != 0; i++) {
				buffer.append(Integer.toHexString(response[i]) + " ");
			}

			Toast.makeText(ctx, buffer.toString(), Toast.LENGTH_SHORT).show();
			break;
		}
	}

	public void setLogo(Bitmap logo) {
		this.logo = logo;
	}

	public void printDipoText(boolean printCopy) {

		// TemplatePrinter template = new TemplatePrinter(ctx, company,
		// paper);
		// print logo
		bixolonPrinter.printBitmap(logo, BixolonPrinter.ALIGNMENT_CENTER, BixolonPrinter.BITMAP_WIDTH_FULL, 88, true,
				false, true);

		// print title
		int attribute = 0;
		// attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
		// attribute |= BixolonPrinter.TEXT_ATTRIBUTE_UNDERLINE1;
		int size = 0;
		// if(paper>2){
		// size = BixolonPrinter.TEXT_SIZE_HORIZONTAL2;
		// size |= BixolonPrinter.TEXT_SIZE_VERTICAL2;
		// }else{
		// size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		// size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		// }
		// bixolonPrinter.printText(template.getSJBTitle(),BixolonPrinter.ALIGNMENT_CENTER,attribute,size,false);

		// print content
		paper = Integer.valueOf(printText.get("paper"));
		String toPrint = printText.get("body");
		String[] printLines = toPrint.split("\n");
		toPrint = "";
		String title = "";
		String afterTitle = "";
		String beforeFooter = "";
		boolean printedBy = false;

		for (int i = 0; i < printLines.length; i++) {
			if (i > 2) {
				if (printLines[i].contains("Printed By")) {
					printedBy = true;
				}
				if (printedBy) {
					beforeFooter += printLines[i] + "\n";
				} else {
					toPrint += printLines[i] + "\n";
				}
			} else {
				if (i == 2) {
					afterTitle = printLines[i] + "\n";
				} else if (i == 0) {
					title = printLines[i];
				}
				// attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
				// attribute |= BixolonPrinter.TEXT_ATTRIBUTE_UNDERLINE1;
				// if (paper>2) {
				// size = BixolonPrinter.TEXT_SIZE_HORIZONTAL2;
				// size |= BixolonPrinter.TEXT_SIZE_VERTICAL2;
				// } else {
				// size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
				// size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
				// }
				// bixolonPrinter.printText(title,BixolonPrinter.ALIGNMENT_CENTER,attribute,size,false);
			}
		}

		TextView headerView = new TextView(activity);
		Typeface dipoFont = Typeface.createFromAsset(ctx.getAssets(), "dipo.ttf");
		headerView.layout(0, 0, 256, 15);
		headerView.setTypeface(dipoFont);
		headerView.setTextSize(11);
		headerView.setGravity(Gravity.CENTER_HORIZONTAL);
		headerView.setDrawingCacheEnabled(true);
		headerView.setText(title);
		bixolonPrinter.printBitmap(headerView.getDrawingCache(), BixolonPrinter.ALIGNMENT_CENTER,
				BixolonPrinter.BITMAP_WIDTH_FULL, 88, true, false, true);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(afterTitle, BixolonPrinter.ALIGNMENT_CENTER, attribute, size, false);

		attribute = 0;
		size = 0;
		if (paper == 3) {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		} else if (paper == 1) {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_C;
		} else {
			attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
		}
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(toPrint, BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_B;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		bixolonPrinter.printText(beforeFooter, BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);

		// print footer
		TextView footerView = new TextView(activity);
		// Typeface dipoFont = Typeface.createFromAsset(ctx.getAssets(),
		// "dipo.ttf");
		footerView.layout(0, 0, 256, 30);
		footerView.setTypeface(dipoFont);
		footerView.setTextSize(11);
		footerView.setGravity(Gravity.CENTER_HORIZONTAL);
		footerView.setDrawingCacheEnabled(true);
		footerView.setText("---" + printText.get("footer") + "---");
		bixolonPrinter.printBitmap(footerView.getDrawingCache(), BixolonPrinter.ALIGNMENT_CENTER,
				BixolonPrinter.BITMAP_WIDTH_FULL, 88, true, false, true);

		attribute = 0;
		size = 0;
		attribute |= BixolonPrinter.TEXT_ATTRIBUTE_FONT_A;
		size = BixolonPrinter.TEXT_SIZE_HORIZONTAL1;
		size |= BixolonPrinter.TEXT_SIZE_VERTICAL1;
		// bixolonPrinter.printText("------------------------------------------",
		// BixolonPrinter.ALIGNMENT_LEFT, attribute, size, false);
		bixolonPrinter.printText("------------------------------------------------", BixolonPrinter.ALIGNMENT_CENTER,
				attribute, size, false);

		bixolonPrinter.lineFeed(2, false);
		bixolonPrinter.cutPaper(true);

		if (printCopy == true) {
			printText.put("footer", "CUSTOMER COPY");
			printDipoText(false);
		}
	}

	public void testConnection() {
		// deviceName = mac;
		// deviceMAC = mac;
		String mac = this.getDeviceMAC();
		testConn = true;
		if (mac.equals("")) {
			bixolonPrinter.findBluetoothPrinters();
		} else {
			bixolonPrinter.connect(mac);
		}
	}

	public void disconnect() {
		if (isConnected) {
			isConnected = false;
			bixolonPrinter.automateStatusBack(false);
			bixolonPrinter.disconnect();
		}
	}

	public void printTest() {
		testPrint=true;
		if (!isConnected) {
			String mac = this.getDeviceMAC();
			if(mac!=null && !mac.equals("")){
				bixolonPrinter.connect(mac);
			}
//			}else{
//				bixolonPrinter.findBluetoothPrinters();
//			}
		 } else {
			 TestPage();
		 }
	}
	private void TestPage(){
		HashMap<String, String> dataToPrint = new HashMap<String, String>();
		dataToPrint.put("body", "PRINT A TEST PAGE \n Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in augue pharetra, vulputate felis eu, feugiat eros. Cras est tortor, sollicitudin ac tortor in, venenatis convallis felis. Etiam nisl neque, aliquam sed tincidunt a, interdum a est. Sed hendrerit congue est eu fermentum");
		dataToPrint.put("footer", "TEST PAGE");
		dataToPrint.put("paper", "2");
		this.setLogo(null);
		this.setupPrintJob("", dataToPrint);
		this.startPrinting();
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceMAC(String deviceMAC) {
		this.deviceMAC = deviceMAC;
	}

	public String getDeviceMAC() {
		return deviceMAC;
	}

}
