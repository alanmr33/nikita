package com.nikita.mobile.finalphase.connection;

public interface NikitaMultipartListener {
	public void onChangePart(String name, Object data);
}
