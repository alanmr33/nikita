package com.nikita.mobile.finalphase.adapter;

import android.view.View;

 

public interface AdapterInterface {
	public View onMappingColumn (int row, View v, Object data);
}
