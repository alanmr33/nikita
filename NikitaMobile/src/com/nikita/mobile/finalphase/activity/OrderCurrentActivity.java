package com.nikita.mobile.finalphase.activity;

import java.util.Vector;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.ui.ComboboxUI;
import com.nikita.mobile.finalphase.printer.BxlPrinter;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.NikitasetVector;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;

public class OrderCurrentActivity extends AndroidActivity{
	Nikitaset nikitaset;
	Nset nSetting = Nset.newObject();
	private String getTitle(String def){
		if (getIntent()!=null && getIntent().getStringExtra("title")!=null) {
			return getIntent().getStringExtra("title");
		}
		return def;
	}
	protected void onCreate(Bundle savedInstanceState) {		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultorder);		
		nSetting =  Nset.readJSON( Generator.getNikitaParameters().getData( "INIT-ORDER-ACTIVITY").toString());
		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));	
		UtilityAndroid.setButtonActivity(findViewById(R.id.lnrCnt), nSetting, new View.OnClickListener() {
			public void onClick(View v) {
				String tag = String.valueOf(v.getTag());
				Nset n = Nset.readJSON(tag);				
				Generator.setVirtual("@+CORE-ID", "");
				Generator.setVirtual("@+CORE-ACTIVITYID", "");
				Generator.setVirtual("@+CORE-ORDERID","");
				
				Generator.setVirtual("@+CORE-MENUID", "ORDER");
				Generator.setVirtual("@+CORE-STATUS", "0");										
				Generator.setVirtual("@+CORE-FORMID", n.getData("id").toString() );
				
				Generator.setVirtual("@+CORE-MENUCODE", getCode());
				if ( findViewById(R.id.txtTitle)instanceof TextView ) {							
					Generator.setVirtual("@+CORE-MENUTITLE", ((TextView)findViewById(R.id.txtTitle)).getText().toString());
				}else{
					Generator.setVirtual("@+CORE-MENUTITLE", "");
				}
				String name = n.getData("form").toString() ;
				
				Generator.clearNikitaForms();
				Generator.startNikitaForm(OrderCurrentActivity.this, name );
			}
		} );		
		
		((TextView)findViewById(R.id.txtTitle)).setText( getTitle( "TO DO LIST") );
		
		findViewById(R.id.frmCustom).setVisibility(View.GONE);
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {				 
				finish();
			}
		});
		
		((EditText)findViewById(R.id.txtFind)).setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					populate(v.getText().toString());
					
					return true;
				}
				return false;
			}
		});
				
		Spinner spin = (Spinner) findViewById(R.id.combobox);
		spin.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView arg0, View arg1, int position, long arg3) {
				multiordercurr=position;
				populate("");
			}
			public void onNothingSelected(AdapterView<?> arg0) { }
		});
		
		
		String multi = Generator.getNikitaParameters().getData("INIT-UI-ORDER-MULTI").toString();
		if (multi.equals("")) {
			multiordercurr=-1;
			populate("");
			findViewById(R.id.frmCustom).setVisibility(View.GONE);
			findViewById(R.id.frmCustomFind).setVisibility(View.VISIBLE);
		}else{
			findViewById(R.id.frmCustomFind).setVisibility(View.VISIBLE);
			Vector<String> list = new Vector<String>();
			Nset n = Nset.readJSON(multi);
			for (int i = 0; i < n.getArraySize(); i++) {
				list.add(n.getData(i).getData("text").toString());
			}
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);//simple_list_item_1
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//simple_spinner_dropdown_item
			spin.setAdapter(adapter);
		}
	}
	private Nset norderview = Nset.newArray();
	private Nset norderviewnewform = Nset.newArray();
	private int multiordercurr = -1;
	private int newformcount = -1;
	private String search = "";

    private void populate(String cari){
    	search = cari;
    
	   Messagebox.showBusyDialog(OrderCurrentActivity.this, new Runnable() {					
			public void run() {				
				NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
				
				String multi = Generator.getNikitaParameters().getData("INIT-UI-ORDER-MULTI").toString(); 
				if (multi.equals("")) {
					String sql = Generator.getNikitaParameters().getData("INIT-UI-ORDER-QUERY").toString();
					nikitaset = ncmobile.Query( sql.equals("")?"SELECT * FROM mobileorder":sql);
					
					norderview = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-UI-ORDER-LIST-ITEM").toString());
				}else{
					Nset n = Nset.readJSON(multi);
					String sql = n.getData(multiordercurr).getData("query").toString();
					nikitaset = ncmobile.Query( sql.equals("")?"SELECT * FROM mobileorder":sql);
					norderview = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-UI-ORDER-LIST-ITEM").toString());
				}
				
				//fitur search 24-06-2016
				//tambah search nama customer
				if(!search.equals("")){
					String sql = " SELECT * FROM mobileorder WHERE agreement_id LIKE '%"+search+"%'"+" OR "+"license_plate_no LIKE '%"
						+search+"%'";
					nikitaset = ncmobile.Query(sql);
					norderview = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-UI-ORDER-LIST-ITEM").toString());
				}
				
				StringBuffer sbactivity = new StringBuffer();				
				Nikitaset nact = ncmobile.Query(  "SELECT orderid FROM mobileactivity " );
				for (int i = 0; i < nact.getRows(); i++) {
//					sbactivity.append(nact.getText(i, 0)).append("|");
				}	
				
				//hide if activity exist
				String sactivity = sbactivity.toString();
				Vector<String> remove = new Vector<String>();
				for (int i = 0; i < nikitaset.getRows(); i++) {
					if (sactivity.contains( nikitaset.getText( i, "agreement_id") +"|") ) {
						remove.addElement(i+"");
					}
				}
				for (int j = 0; j < remove.size(); j++) {
					nikitaset.getDataAllVector().remove( Utility.getInt(remove.elementAt( (remove.size()-1-j ) ))  );
				}
								
				String newform = Generator.getNikitaParameters().getData("INIT-UI-ORDER-NEWFORM").toString();
				if (!newform.equals("")) {
					Nset n = Nset.readJSON(newform);//[black 1]
					if (n.getArraySize()>=1) {
						newformcount = n.getArraySize();
						Vector<Vector<String>> ndata =nikitaset.getDataAllVector();
						for (int i = 0; i < n.getArraySize(); i++) {
							Vector<String> newformrow = new Vector<String>();
							if (n.getData(i).isNsetArray()) {
								for (int j = 0; j < n.getData(i).getArraySize(); j++) {
									newformrow.addElement(n.getData(i).getData(j).toString());
								}
							}else{
								for (int j = 0; j < nikitaset.getDataAllHeader().size(); j++) {
									newformrow.addElement( n.getData(i).getData(nikitaset.getDataAllHeader().elementAt(j)).toString()   );
								}
							}							
							ndata.insertElementAt(newformrow, i);
						}
						nikitaset= new Nikitaset(nikitaset.getDataAllHeader(), ndata,nikitaset.getError(), nikitaset.getInfo());
						String s = Generator.getNikitaParameters().containsKey("INIT-UI-ORDER-NEWFORM-LIST-ITEM")? Generator.getNikitaParameters().getData("INIT-UI-ORDER-NEWFORM-LIST-ITEM").toString(): Generator.getNikitaParameters().getData("INIT-UI-ORDER-LIST-ITEM").toString();
						norderviewnewform = Nset.readJSON( s );
					}
				}
				
			}
		}, new Runnable() {
			public void run() {
				ListView listView = (ListView) findViewById(R.id.lstView);			
				
				ArrayAdapter<Vector<String>> arrayAdapter = new ArrayAdapter<Vector<String>>(OrderCurrentActivity.this, R.layout.defaultorderitem,
						nikitaset.getDataAllVector()) {
 					public View getView(final int position, final View convertView, final ViewGroup parent) {
						final View v = Utility.getInflater(OrderCurrentActivity.this,  R.layout.defaultorderitem);
						
						String agrId = nikitaset.getText(position, "agreement_id");
						//set icon priority
						NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
						String sql = Generator.getNikitaParameters().getData("INIT-UI-ORDER-PRIORITY-QUERY").toString();
						String sqlHobPrior = Generator.getNikitaParameters().getData("INIT-UI-ORDER-PRIORITY-HOB-QUERY").toString();
						try {							
							Nikitaset nsPrior = ncmobile.Query(sql.equals("") ? "SELECT priority FROM order_priority where orderid = '" + agrId + "'" : sql + agrId + "'");
							Nikitaset nsHobPrior = ncmobile.Query(sqlHobPrior.equals("") ?"SELECT hob_priority FROM mobileorder WHERE agreement_id='" + agrId + "'" : sqlHobPrior + agrId + "'");
														
//							String priority = nsPrior.getText(0, "priority");
							String priority = nsPrior.getText(0, 0);
							String hobPrior = nsHobPrior.getText(0, 0);
							
							if(hobPrior.equals("1"))
								((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.todoliststar);
							else
								if (priority.equals("1"))
									((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.d_todolist);
								else if (priority.equals("0"))
									((ImageView) v.findViewById(R.id.imageView1)).setImageResource(R.drawable.todolistbiru);
								else
									((ImageView) v.findViewById(R.id.imageView1)).setImageResource(R.drawable.todolisthitam);
						} catch (Exception e) {
							e.printStackTrace();
							((ImageView) v.findViewById(R.id.imageView1)).setImageResource(R.drawable.todolisthitam);
						}
						((TextView)v.findViewById(R.id.txtView)).setText(viewOrder(nikitaset, position) );
						UtilityAndroid.setListOrderIcon(position, v, nSetting, nikitaset.getText(position, "status"), new View.OnClickListener() {
							public void onClick(View v) {
								 								
							}
						});						
						return v;
					}
				};
				listView.setAdapter(arrayAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView arg0, View view,final int pos, long arg3) {
						
						NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
						String sql = "SELECT * FROM mobileactivity WHERE orderid="+"'"+nikitaset.getText(pos, "agreement_id")+"'"+" AND status=0";
						Nikitaset result = ncmobile.Query(sql);
						
						String name = Generator.getNikitaParameters().getData("INIT-ORDER-ACTIVITY-FORM").toString();
						
						if(result.getRows() > 0){
							
							Generator.setVirtual("@+CORE-ID", result.getText(0, "id"));
							Generator.setVirtual("@+CORE-ACTIVITYID", result.getText(0, "activityid"));
							Generator.setVirtual("@+CORE-ORDERID", result.getText(0, "orderid"));								
							Generator.setVirtual("@+CORE-ORDERROW", pos);
							Generator.setVirtual("@+CORE-ORDERDATA", "");
							
							Generator.setVirtual("@+CORE-MENUID", "DRAFT");
							Generator.setVirtual("@+CORE-VIEW",  result.getText(0, "read"));
							Generator.setVirtual("@+CORE-STATUS", result.getText(0, "status"));
							
							Generator.clearNikitaForms();
							String newBody = ncmobile.Query("SELECT body FROM mobileactivity WHERE activityid=? ", result.getText(0, "activityid")).getText(0, "body").toString();//nikitaset.getText(pos, "body")
							Generator.openStreamForms(Nset.readJSON(newBody) , name,  OrderCurrentActivity.this, "save");
							
						} else{
							
							Generator.setVirtual("@+CORE-ID", "");
							Generator.setVirtual("@+CORE-ACTIVITYID", "");
							Generator.setVirtual("@+CORE-ORDERID", nikitaset.getText(pos, "agreement_id"));
							Generator.setVirtual("@+CORE-ORDERROW", pos);
							Generator.setVirtual("@+CORE-ORDERDATA", nikitaset);
							
							Generator.setVirtual("@+CORE-MENUID", "ORDER");
							Generator.setVirtual("@+CORE-VIEW", viewOrder(nikitaset, pos));
							Generator.setVirtual("@+CORE-STATUS", "0");							
							
							Generator.clearNikitaForms();
							Generator.startNikitaForm(OrderCurrentActivity.this, name.equals("")?"nikita":name);
							
						}
						
						
					}
				});
				
				 
			}
		});			
   }
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//if (requestCode==1) {
			populate("");
		//}
	}
   public void reloadActivityData(){		
		populate("");
	}
	public void ListAdapter(ListView listView, Context context , Nikitaset vector){ 
		listView.setAdapter( new ArrayAdapter(context, R.layout.defaultorderitem, vector.getDataAllVector()){
			private Nikitaset nikitaset;
			private Context context;
			public ArrayAdapter get(Context context, Nikitaset nikitaset){
				this.nikitaset=nikitaset;
				this.context=context;
				return this;
			}
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = Utility.getInflater(context, R.layout.defaultorderitem);
				v.findViewById(R.id.frmBagde).setVisibility(View.GONE);

				//set drawable for order icon in 
				if (position>newformcount-1) {
					((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.n_order);	
					((TextView)v.findViewById(R.id.txtView)).setText(viewOrder(nikitaset, position) );
				}else{
					((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.n_order_blank);	
					((TextView)v.findViewById(R.id.txtView)).setText(viewOrder(nikitaset, position) );
				}
				
				return v;
			}
			
		}.get(context, vector));
	}
	
	private String viewOrder(Nikitaset nikitaset, int position){
		StringBuffer stringBuffer = new StringBuffer();
		boolean isnewform = !(position>newformcount-1);
		
		if (isnewform && norderviewnewform.getArraySize()>=1) {
			for (int i = 0; i < norderviewnewform.getArraySize(); i++) {
				if (norderviewnewform.getData(i).toString().startsWith("!")) {
					stringBuffer.append(  nikitaset.getText(position, norderviewnewform.getData(i).toString().substring(1) ));
				} else if (norderviewnewform.getData(i).toString().equals("@+ENTER")||norderviewnewform.getData(i).toString().equals("@+NEWLINE")) {
					stringBuffer.append("\r\n");
				} else{
					stringBuffer.append(  norderviewnewform.getData(i).toString() );
				}				
			}
		}else if (norderview.getArraySize()>=1) {
			for (int i = 0; i < norderview.getArraySize(); i++) {
				if (norderview.getData(i).toString().startsWith("!")) {
					stringBuffer.append(  nikitaset.getText(position, norderview.getData(i).toString().substring(1) ));
				} else if (norderview.getData(i).toString().equals("@+ENTER")||norderview.getData(i).toString().equals("@+NEWLINE")) {
					stringBuffer.append("\r\n");
				} else{
					stringBuffer.append(  norderview.getData(i).toString() );
				}				
			}
		}else{
			stringBuffer.append(nikitaset.getText(position, 0)).append("\r\n");
			stringBuffer.append(nikitaset.getText(position, 1)).append("\r\n");
			stringBuffer.append(nikitaset.getText(position, 2)).append("\r\n");
			stringBuffer.append(nikitaset.getText(position, 3)).append("\r\n");
			stringBuffer.append(nikitaset.getText(position, 4)).append("\r\n");
		}		
		return stringBuffer.toString();
	}
}
