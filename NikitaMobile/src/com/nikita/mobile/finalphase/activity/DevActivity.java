package com.nikita.mobile.finalphase.activity;
 
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NikitaEngine;
import com.nikita.mobile.finalphase.printer.GlobalPrinter;
import com.nikita.mobile.finalphase.printer.OnFinishPrint;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.service.NikitaRz;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity; 
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DevActivity  extends AndroidActivity {
	
	private String result = "";
	private static String codedebug = "";
	private String server = "";
	
	public GlobalPrinter printer;
	private String printerUrl = "";
	private Nset nprinter;
	private Activity activity;
	public static void saveListServer(Context context, String url){
		if (url.endsWith("/res/about/")) {		
			url=url.substring(0,url.length()-11);
		}
		
		Nset vtsave =  Nset.newArray();
		Nset n  = Nset.readJSON(Utility.getSetting(context, "LIST-SERVER", "[]"));
        if (n.isNsetArray()) {
            for (int i =  Math.max(0, n.getSize()- 50) ; i < n.getSize(); i++) {                           
                if (!n.getData(i).toString().trim().equalsIgnoreCase(url.trim())) {
                    vtsave.addData(n.getData(i).toString().trim());
                }
            }
            vtsave.addData(url);
        }else{
            vtsave.addData(url);
        }
        
        Utility.setSetting(context, "LIST-SERVER", vtsave.toJSON());
	}
	public static String[] getListServer(Context context){
		Nset n  = Nset.readJSON(Utility.getSetting(context, "LIST-SERVER", "[]"));
		String[] data =new String[n.getSize()];	
		for (int i = 0; i < n.getArraySize(); i++) {
			data[i] = n.getData(i).toString();
		}
		return data;
	}
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.devsetting);	
//		((EditText)findViewById(R.id.txtNikitaCode)).setText(Utility.getSetting(getApplicationContext(), "NIKITACODE", ""));
		
		((TextView)findViewById(R.id.txtTitle)).setText( "Nikita Settings" );
		((EditText)findViewById(R.id.txtServer)).setText( "Dipostar GPS Tracking" );
		
//		server = ((EditText)findViewById(R.id.txtServer)).getText().toString().trim();
//		server = "http://27.111.41.194/dipo";
		server = AppNikita.getInstance().getBaseUrlProduction();
		Utility.setSetting(getApplicationContext(), "BASE-URL", server);
		
		findViewById(R.id.imgRight).setVisibility(View.INVISIBLE);
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		findViewById(R.id.tblSave).setVisibility(View.GONE);
		
		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));	
		
		//add [auto complete]
//		AutoCompleteTextView autoCompleteTextView = ((AutoCompleteTextView)findViewById(R.id.txtServer));
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,  android.R.layout.simple_dropdown_item_1line, DevActivity.getListServer(getApplicationContext()));
//		autoCompleteTextView.setAdapter(adapter);
		
		if (AppNikita.NIKITA_DEV) {
			findViewById(R.id.txtServer).setVisibility(View.VISIBLE);
			findViewById(R.id.lblServer).setVisibility(View.VISIBLE);
		}

		if (AppNikita.PRODUCTION) {
			(findViewById(R.id.lblServer)).setVisibility(View.GONE);
			(findViewById(R.id.txtServer)).setVisibility(View.GONE);			
			(findViewById(R.id.tblSave)).setVisibility(View.GONE);
		}
		
		(findViewById(R.id.tblSave)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				server = ((EditText)findViewById(R.id.txtServer)).getText().toString().trim();
				if (server.toLowerCase().startsWith("http://")||server.toLowerCase().startsWith("https://")) {
				}else{
					server="http://"+server;
				}
				if (server.endsWith("/")) {		
					server=server.substring(0,server.length()-1);
				}
			 
				Utility.setSetting(getApplicationContext(), "BASE-URL", server);	
				//Utility.setSetting(getApplicationContext(), "NIKITACODE", ((EditText)findViewById(R.id.txtNikitaCode)).getText().toString());
				//Utility.setSetting(getApplicationContext(), "LIST-SERVER", "[]");//clear
				finish();
				//System.exit(0);
			}
		});
		
		(findViewById(R.id.tblSync)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				String server = ((EditText)findViewById(R.id.txtServer)).getText().toString().trim();
				String server = Utility.getSetting(getApplicationContext(), "BASE-URL", "");
				if (AppNikita.getInstance().getBaseUrl().trim().equals(server)) {
					SyncConfim();
				}else{
					Messagebox.showInfo(DevActivity.this, "Please, Save Before Synchronize");
				}
								
			}
		});
		
		(findViewById(R.id.tblConn)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (findViewById(R.id.txtNikitaCode) instanceof EditText) {
					String s = ((EditText)(findViewById(R.id.txtNikitaCode))).getText().toString().trim();
					if (s.equalsIgnoreCase("nikitawilly")) {
						AppNikita.PRODUCTION = false;
					}
				}
				Messagebox.showBusyDialog(DevActivity.this, new Runnable() {					
					public void run() {
//						server = ((EditText)findViewById(R.id.txtServer)).getText().toString().trim();
						if (server.toLowerCase().startsWith("http://")||server.toLowerCase().startsWith("https://")) {
						} else {
							server = "http://" + server;
						}
						if (server.endsWith("/")) {
							printerUrl = server + "mobile.mprinter";
							server = server + "mobile.mcheckuser";							
						} else {
							printerUrl = server + "/mobile.mprinter";
							server = server + "/mobile.mcheckuser";
						}
						try {							
							result = NikitaInternet.getString(NikitaInternet.postHttp(server)) ;
							Generator.out("Test", result);
						} catch (Exception e) { }
						Log.i("Server", server);
						
						Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
						try {							
							nprinter = Nset.readJSON(NikitaInternet.getString( NikitaInternet.postHttp( printerUrl, hashtable ) ) );														
						} catch (Exception e) { }
						Log.i("Printer Service URL", printerUrl);
					}
				}, new Runnable() {
					public void run() {
						if (result.contains("Hello Nikita")) {
							DevActivity.saveListServer(getApplicationContext(), server);
							
							if (nprinter.getData("status").toString().equalsIgnoreCase("OK") && nprinter.getData("error").toString().equals("")) {
								String printerId  = nprinter.getData("printer_id").toString();
								String brand 	  = nprinter.getData("brand").toString();
								String model 	  = nprinter.getData("model").toString();
								String macAddress = nprinter.getData("mac_address").toString();
								String serialNo   = nprinter.getData("serial_no").toString();
								String bluetooth  = nprinter.getData("bluetooth_name").toString();
								
								((EditText)findViewById(R.id.txtPrinterId)).setText(printerId);
								((EditText)findViewById(R.id.txtBrand)).setText(brand);
								((EditText)findViewById(R.id.txtModel)).setText(model);
								((EditText)findViewById(R.id.txtMacAddress)).setText(macAddress);
								((EditText)findViewById(R.id.txtSerialNo)).setText(serialNo);
								((EditText)findViewById(R.id.txtBluetooth)).setText(bluetooth);
//								Utility.setSetting(getApplicationContext(), "PRINTER-MODEL", model);
//								Utility.setSetting(getApplicationContext(), "PRINTER-MAC-ADDRESS", macAddress);
//								Utility.setSetting(getApplicationContext(), "PRINTER-SERIAL-NO", serialNo);
								findViewById(R.id.btnPrinterConn).setVisibility(View.VISIBLE);
								findViewById(R.id.btnPrintTest).setVisibility(View.VISIBLE);
								Log.i("Printer Service URL", printerUrl);
								
								Messagebox.showInfo(DevActivity.this, "Connection Success");
								findViewById(R.id.tblSync).setVisibility(View.VISIBLE);
							} else {
								Messagebox.showInfo(DevActivity.this, "Connection Error/Timeout");
							}			
						} else {
							Messagebox.showInfo(DevActivity.this, "Connection Error/Timeout");
						}
						Log.i("Server", server);
					}
				});
			}
		});
		
		TelephonyManager tm = (TelephonyManager) getSystemService(android.content.Context.TELEPHONY_SERVICE);
		String imei = tm.getDeviceId().toUpperCase();
 
		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			((EditText)findViewById(R.id.txtVersion)).setText(pInfo.versionName );
		} catch (Exception e) { }
		((EditText)findViewById(R.id.txtDevice)).setText(   Utility.insertString( imei , " ", 4)   );
		((EditText)findViewById(R.id.txtLogic)).setText(  Utility.getSetting(getApplicationContext(), "INIT-LOGIC-VERSION", ""));
		
//		((EditText)findViewById(R.id.txtModel)).setText(Utility.getSetting(getApplicationContext(), "PRINTER-MODEL", ""));
//		((EditText)findViewById(R.id.txtMacAddress)).setText(Utility.getSetting(getApplicationContext(), "PRINTER-MAC-ADDRESS", ""));
//		((EditText)findViewById(R.id.txtSerialNo)).setText(Utility.getSetting(getApplicationContext(), "PRINTER-SERIAL-NO", ""));
		
		codedebug="MOBILE-PLAY-"+imei;
		
//		Button btnTestPrinter = (Button)findViewById(R.id.btnTestPrint);
//		btnTestPrinter.setOnClickListener(new OnClickListener() {			
//			@Override
//			public void onClick(View v) {
//				//test connection only
//				String server = Utility.getSetting(getApplicationContext(), "BASE-URL", "");
//				printer.testConnection("");
//				//test print text
//				HashMap<String, String> dataToPrint=new HashMap<String, String>();
//				dataToPrint.put("body", "TEST\n ----TEST PRINTER----");
//				dataToPrint.put("footer", "TEST");
//				dataToPrint.put("paper", "3");
//				printer.setLogo(null);
//				printer.setupPrintJob("", dataToPrint);
//			}
//		});
		
		(findViewById(R.id.btnPrinterConn)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {				
				Messagebox.showBusyDialog(DevActivity.this, new Runnable() {
					//private String address = Utility.getSetting(getApplicationContext(), "PRINTER-MAC-ADDRESS", "");
					private String address = ((EditText)(findViewById(R.id.txtMacAddress))).getText().toString().trim();
					public void run() {
						try {
							printer.setDeviceMAC(address);
							printer.testConnection();
						} catch (Exception e) { }
					}
				}, new Runnable() {
					public void run() {
//						
					}
				});
			}
		});
		
		(findViewById(R.id.btnPrintTest)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {				
				Messagebox.showBusyDialog(DevActivity.this, new Runnable() {
					private String address = ((EditText)(findViewById(R.id.txtMacAddress))).getText().toString().trim();
					public void run() {
						try {
							printer.setActivity(activity);
							printer.setDeviceMAC(address);
							printer.printTest();
						} catch (Exception e) { }
					}
				}, new Runnable() {
					public void run() {
//						
					}
				});
			}
		});
	}
	public String getCurrentNikitaCode(){
//		return  ((EditText)findViewById(R.id.txtNikitaCode)).getText().toString();
		return "";
	}
	public static String getNikitaCode(){
		return  Utility.getSetting(AppNikita.getInstance().getApplicationContext(), "NIKITACODE", "");
	}
	public static void  setupNikita(Context context, String s){
		/*
			if (AppNikita.NIKITA_DEV) {
				TelephonyManager tm = (TelephonyManager)context. getSystemService(android.content.Context.TELEPHONY_SERVICE);
				String imei=tm.getDeviceId().toUpperCase();			 
				
				codedebug="MOBILE-PLAY-"+imei;
				Generator.out("DEBUG", codedebug);
				
				NotificationManager  notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE); 
				NotificationCompat.Builder noti = new NotificationCompat.Builder(context);
				noti.setContentTitle("Nikita Generator");
				noti.setContentText("Development Setting");
				noti.setSmallIcon(R.drawable.generator);
				noti.setAutoCancel(false);//cant cancle				
				
				Intent intent = new Intent(context, DevActivity.class);
				PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
				noti.setContentIntent(pIntent);
				
				Notification nc = noti.getNotification();
				nc.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
				notificationManager.notify(13, nc);
			}
			*/
	}
	
	
	private String initapp = "";
	private int count = 0;
	public void SyncConfim() {
			Builder dlg = new AlertDialog.Builder(this);
			dlg.setTitle("");
			dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();

					Messagebox.showBusyDialog(DevActivity.this, new Runnable() {					
						public void run() {
							result = "";//noerror
							Generator.synchronizeNikita(getApplicationContext()); 
														
							Nset param= Nset.readJSON(Utility.getSetting(DevActivity.this, "INIT", ""));  
							Utility.setSetting(getApplicationContext(), "INIT-LOGIC-VERSION", param.getData("INIT-LOGIC-VERSION").toString());
							initapp = param.getData("INIT-APP").toString();
							
							NikitaConnection nc = NikitaConnection.getConnection(NikitaConnection.LOGIC);
							
							if (nc.isNikitaConnection()) {
								reconfigurationsystem(initapp);
								result = "Tidak diperbolehkan Sync. menggunakan mode NIKITACONNECTION \r\n(ONLINE N-LOGIC)";
								return;
							}
							NikitaConnection nmobile = NikitaConnection.getConnection(NikitaConnection.MOBILE);
							 /*
							
							//new NV3
							//String sNv3 =Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl() + "/mobile.mlogic?mode=modulenv3&module="+Utility.urlEncode(param.getData("INIT-MODULE-NV3").toString())+"&recurse="+Utility.urlEncode(param.getData("INIT-MODULE-RECURSE").toString())  );
							//Nset nv3 = Nset.readJSON(sNv3);
							
													
						    
							//new Synchronize save to Nv3							
							String sV =Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl() + "/mobile.mlogic?mode=module&module="+Utility.urlEncode(param.getData("INIT-MODULE").toString())+"&recurse="+Utility.urlEncode(param.getData("INIT-MODULE-RECURSE").toString())  );
							final Nset nforms = Nset.readJSON(sV);
										
							if (nforms.getArraySize()>=1) {
								nc.Query("DELETE FROM web_form");
								nc.Query("DELETE FROM web_component");
								nc.Query("DELETE FROM web_route");
								
								Utility.deleteAllFileFolder(new File(Utility.getDefaultPath("nv3")));
							}
							for (int i = 0; i < nforms.getArraySize(); i++) {							
								sV =Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl() +"/mobile.mlogic?mode=frmcomplogic&nv3=true&formid="+nforms.getData(i).toString());//[formid..]
								Nset nsform =Nset.readJSON(sV);count=i+1;
								
								if (nsform.isNsetObject() && (nsform.getData("nfid").toString().equalsIgnoreCase("nv3")||nsform.getData("nfid").toString().equalsIgnoreCase("nv3-logicall")) ) {
									//nv3 true [support]
									NikitaEngine.compile(sV, nsform.getData("form").getData("data").getData(0).getData("formname").toString()  );
								}else{
									//old
									for (int j = 0; j < nsform.getArraySize(); j++) {									
										Nikitaset ns = new Nikitaset(nsform.getData(j));
										if (i==0) {
											createTable(nc, ns, j==0?"web_form":(j==1?"web_component":(j==2?"web_route":"")) );									
										}								
										if (j==0) {		
											insertTable(nc, ns, "web_form");
										}else if (j==1) {
											for (int k = 0; k < ns.getRows(); k++) {
												nc.Query("DELETE FROM web_route WHERE compid=?", ns.getText(k, "compid"));
											}
											insertTable(nc, ns, "web_component");
										}else if (j==2) {
											insertTable(nc, ns, "web_route");
										}
									}	
								}
								runOnUiThread(new Runnable() {
									public void run() {
										Messagebox.setProsesBarMsg(count+ " of " + nforms.getArraySize());											 
									}
								});
							}	
							*/ 
							if (!getCurrentNikitaCode().contains("nosynclogic")) {
								saveNikitaLogic(DevActivity.this,  nc, param, new OnProgresListener() {
									public void onProgres(int cnt, final int max, String msg) {
										count = cnt+1;
										runOnUiThread(new Runnable() {
											public void run() {
												Messagebox.setProsesBarMsg("N-Logic "+count+ " of " + max);											 
											}
										});
									}
								});
							}else{							 
								if (getCurrentNikitaCode().contains("logic:")) {
									String logicforms = getCurrentNikitaCode();
									logicforms = logicforms.substring(logicforms.indexOf("logic:")+6 );									
									if (logicforms.contains(";")) {
										logicforms = logicforms.substring(0, logicforms.indexOf(";"));									
									}
									
									saveNikitaLogic(DevActivity.this,  nc, param, new OnProgresListener() {
								public void onProgres(int cnt, final int max, String msg) {
											count = cnt+1;
									runOnUiThread(new Runnable() {
										public void run() {
													Messagebox.setProsesBarMsg("N-Logic "+count+ " of " + max);											 
										}
									});
										}
									},logicforms);
								}
							}
							if (!getCurrentNikitaCode().contains("nosynctable")) {
								saveSyncTable(DevActivity.this,  nmobile, param, new OnProgresListener() {
									public void onProgres(int cnt, final int max, String msg) {
										count = cnt+1;
										runOnUiThread(new Runnable() {
											public void run() {
												Messagebox.setProsesBarMsg("N-Table "+count+ " of " + max);											 
											}
							});
									}
								});
							}
							if (!getCurrentNikitaCode().contains("nosyncfile")) {
								saveSyncFile(DevActivity.this,  nmobile, param, new OnProgresListener() {
									public void onProgres(int cnt, final int max, String msg) {
										count = cnt+1;
										runOnUiThread(new Runnable() {
											public void run() {
												Messagebox.setProsesBarMsg("N-File "+count+ " of " + max);											 
											}
										});
									}
								});
							}
							if (!getCurrentNikitaCode().contains("nosynctablefile")) {
								saveSyncTableFile(DevActivity.this,  nmobile, param, new OnProgresListener() {
									public void onProgres(int cnt, final int max, String msg) {
										count = cnt+1;
										runOnUiThread(new Runnable() {
											public void run() {
												Messagebox.setProsesBarMsg("N-TableFile "+count+ " of " + max);											 
											}
										});
									}
								});
							}
							if (!getCurrentNikitaCode().contains("nosyncftable")) {
								saveSyncFT(DevActivity.this,  nmobile, param, new OnProgresListener() {
									private int ichild = 0;
									private int ichildMax = 0;
									private String pname = "";
									public void onProgres(int cnt, final int max,final String msg) {
										if (msg.equals("s")) {
											ichild = cnt;//child
											ichildMax = max;
											runOnUiThread(new Runnable() {
												public void run() {
													Messagebox.setProsesBarMsg(pname + " [" + (ichildMax-ichild)+ "]");											 
												}
											});
										}else{
											count = cnt+1;
											runOnUiThread(new Runnable() {
												public void run() {
													pname = "N-FTABLE "+count+ " of " + max;
													Messagebox.setProsesBarMsg(pname + " "+ msg);														
												}
											});
										}
									}
								});
							}
						}
					}, new Runnable() {
						public void run() {
							if (!result.equals("")) {
								Toast.makeText(DevActivity.this, result, Toast.LENGTH_LONG).show();
							}else	if (count>=1) {
								reconfigurationsystem(initapp);
								
								Toast.makeText(DevActivity.this, "Synchronize Completed", Toast.LENGTH_LONG).show();
								 
							}
						}
					});
					
				}
			});
		 		
			dlg.setCancelable(false);
			dlg.setTitle("Synchronize Nikita Logic");
			dlg.setMessage("Are you sure to Synchronize ?");
			dlg.create().show();
	 
	} 
	public static void reconfigurationsystem(String initapp){
		Generator.clearNikitaForms();
		
		NikitaRz.restartNikitaWebSocket(AppNikita.getInstance());
//		AlarmReceiver.restartAlarm(AppNikita.getInstance());
		Generator.initializeNikita(AppNikita.getInstance());	
		Nset param= Nset.readJSON(initapp);  
		//[{'class':'class', 'enable':'', shortcut:''}]
		for (int i = 0; i < param.getSize(); i++) {
			if (param.getData(i).containsKey("enable")) {
				if (param.getData(i).getData("class").toString().equalsIgnoreCase("mandalasales")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.MandalaSalesLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("mandalacollection")) {
					changeManifestIcon(AppNikita.getInstance(), "MandalaLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("wom")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.WomLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("mobile")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.IgloLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("setting")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.DevActivity", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("nikita")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.SplashScreen", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("perkasa")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.PerkasaLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("mandiri")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.MandiriLauncher", param.getData(i).getData("enable").toBoolean());
				}else if (param.getData(i).getData("class").toString().equalsIgnoreCase("dipo")) {
					changeManifestIcon(AppNikita.getInstance(), "com.nikita.mobile.finalphase.activity.launcher.DipoLauncher", param.getData(i).getData("enable").toBoolean());
				
				}else{
					changeManifestIcon(AppNikita.getInstance(), param.getData(i).getData("class").toString(), param.getData(i).getData("enable").toBoolean());
				}	
			}	
			if (param.getData(i).containsKey("shortcut")) {
				 
			}
		}//for (int i = 0; i < param.getSize(); i++) {
	}
	
	public interface OnProgresListener{
		public void onProgres(int count, int max, String msg);
	}	
	public static Nset checkSyncStatus(Nset nlog){
		Nset nresult = Nset.newObject();
		return checkSyncStatus(nresult, nlog);
	}
	public static Nset checkSyncStatus(Nset nresult, Nset nlog){
		if (nlog.getData("nfid").toString().equals("NikitaInternet")) {			
			nresult.setData("error", "Connection Error " + nlog.getData("error").toString());
		}else if (nlog.getData("error").toString().length()>=1) {
			nresult.setData("error", nlog.getData("error").toString());
		}else{			
			nresult.setData("error", "");
			nresult.setData("status", "OK");
		}	
		if (nlog.containsKey("state")) {
			nresult.setData("state", nlog.getData("state").toString());
		}
		return nresult;
	}
	public static boolean checkSyncStatusBreak(Nset nlog){
		if (nlog.getData("error").toString().length()>=1) {
			return true;
		}
		return false;
	}
	public static Nset saveNikitaLogic(Context context, NikitaConnection nc, Nset param, OnProgresListener progresListener ){
		 return saveNikitaLogic(context, nc, param, progresListener, null);
	}
	public static Nset saveNikitaLogic(Context context, NikitaConnection nc, Nset param, OnProgresListener progresListener, String array ){
		Nset nresult = checkSyncStatus( Nset.newObject() );
		Nset nforms = Nset.newArray();
		String sV = "";
		boolean nv3support = getNikitaCode().contains("enginenv3") || param.getData("INIT-ENGINE-NV3").toString().equalsIgnoreCase("true");  		
		if (array == null || nv3support == false) {
			Utility.deleteAllFileOnFolder(new File(Utility.getDefaultPath("nv3")));

			// Synchronize save to Nv3
			sV = Utility.getHttpConnection(AppNikita.getInstance().getBaseUrl() + "/mobile.mlogic?mode=module&module="
					+ Utility.urlEncode(param.getData("INIT-MODULE").toString()) + "&recurse=" + Utility.urlEncode(param.getData("INIT-MODULE-RECURSE").toString()));
			nforms = Nset.readJSON(sV);
			nresult = checkSyncStatus(nforms);
			if (checkSyncStatusBreak(nresult)) {
				return nresult;
			}

			if (nforms.getSize() >= 1) {
				nc.Query("DELETE FROM web_form");
				nc.Query("DELETE FROM web_component");
				nc.Query("DELETE FROM web_route");
			}
		}else{
			nforms = new Nset(Utility.splitVector(array,","));
		}
		int iNv3 = 0;
		//new Synchronize NV3
		if (param.containsKey("INIT-MODULE-NV3")) {
			Nset nv3 = Nset.readJSON(param.getData("INIT-MODULE-NV3").toString().trim());
			iNv3 = nv3.getSize();
			for (int i = 0; i < nv3.getSize(); i++) {
				String fname = nv3.getData(i).toString();
				String sV3 =Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl() + "/mobile.mlogic?mode=nv3&formid="+Utility.urlEncode( fname )+"&formname="+ Utility.urlEncode( fname )  );
				NikitaEngine.compile(sV3, fname );
				if (progresListener!=null) {
					progresListener.onProgres(i+1, iNv3, "");
				}
			}
		}
		
		
		for (int i = 0; i < nforms.getArraySize(); i++) {							
			//String usertable = Utility.getSetting(context.getApplicationContext(), "N-USER", "");
			TelephonyManager tm = (TelephonyManager)AppNikita.getInstance().getApplicationContext(). getSystemService(android.content.Context.TELEPHONY_SERVICE);
			String imeitable=tm.getDeviceId().toUpperCase();
			sV = Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl() +"/mobile.mlogic?mode=frmcomplogic"+(nv3support?"&nv3=true":"")+"&formid="+nforms.getData(i).toString()+"&imei="+imeitable);//[formid..]
			String bview = Utility.decodeBase64(sV);
			Nset nsform = Nset.readJSON(bview);
			nresult = checkSyncStatus(nforms);
			if (checkSyncStatusBreak(nresult)) {
				return nresult;
			}
			
			if (nsform.isNsetObject() && (nsform.getData("nfid").toString().equalsIgnoreCase("nv3")||nsform.getData("nfid").toString().equalsIgnoreCase("nv3-logicall")) ) {
				//nv3 true [support]
				NikitaEngine.compile(sV, new Nikitaset(nsform.getData("form")).getText(0, "formname") );
			}else{
				//old				 
				for (int j = 0; j < nsform.getSize(); j++) {									
					Nikitaset ns = new Nikitaset(nsform.getData(j));
					if (i==0) {
						createTable(nc, ns, j==0?"web_form":(j==1?"web_component":(j==2?"web_route":"")) );									
					}								
					if (j==0) {		
						insertTable(nc, ns, "web_form");
					}else if (j==1) {
						for (int k = 0; k < ns.getRows(); k++) {
							nc.Query("DELETE FROM web_route WHERE compid=?", ns.getText(k, "compid"));
						}
						insertTable(nc, ns, "web_component");
					}else if (j==2) {
						insertTable(nc, ns, "web_route");
					}
				}					 
			}
			if (progresListener!=null) {
				progresListener.onProgres(i+1+iNv3, nforms.getArraySize()+iNv3, "");
			}
		}
		return nresult;
	}
	public static Nset saveSyncTable(Context context,NikitaConnection ncmobile, Nset param, OnProgresListener progresListener ){
		Nset nresult = checkSyncStatus( Nset.newObject() );
		Nset n = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-DOWNLOAD-TABLE").toString() );
		int fcount = n.getArraySize();
		Nset tversion = Nset.newObject();
		if (fcount>=1) {
			tversion = Nset.readJSON( Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-TABLE-VERSION", "{}") );//{tablename:versi,...}
		}
		//add 17072016 
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
			hashtable.put("serverversion",  n.toString() );
			hashtable.put("localversion", tversion.toString() );
			try {
				Nset res = NikitaInternet.getNset( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mtable_user", hashtable ) );
				if (res.getData("status").toString().equalsIgnoreCase("OK")) {
					if (res.containsKey("serverversion")) {
						n = Nset.readJSON(res.getData("serverversion").toString());
					}
					if (res.containsKey("localversion")) {
						tversion =  Nset.readJSON(res.getData("localversion").toString());
					}
				}
			} catch (Exception e) { }
		}	
		for (int i = 0; i < n.getArraySize(); i++) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();
			hashtable = new Hashtable ();
			boolean downloadnow = false ; String savatablename = "";
			if ( n.getData(i).isNsetArray()) {
				hashtable.put("table",  n.getData(i).getData(0).toString() );//[table,version, save]
				if (n.getData(i).getData(1).toInteger()==0||n.getData(i).getData(1).toInteger()>tversion.getData(n.getData(i).getData(0).toString()).toInteger()) {
					tversion.setData(n.getData(i).getData(0).toString(), n.getData(i).getData(1).toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData(2).toString().equals("")?n.getData(i).getData(0).toString():n.getData(i).getData(2).toString();
				}
			}else if ( n.getData(i).isNsetObject()) {
				hashtable.put("table",  n.getData(i).getData("table").toString() );//{table:tablename, version:vernumber,save:tablename}
				if (n.getData(i).getData("version").toInteger()==0||n.getData(i).getData("version").toInteger()>tversion.getData(n.getData(i).getData("table").toString()).toInteger()) {
					tversion.setData(n.getData(i).getData("table").toString(), n.getData(i).getData("version").toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData("save").toString().equals("")?n.getData(i).getData("table").toString():n.getData(i).getData("save").toString();
				}
			}else{
				hashtable.put("table",  n.getData(i).toString() );//table
				savatablename = n.getData(i).toString();
				downloadnow=true;
			}
			if (downloadnow && savatablename.length()>=1) {
				Nset nv = Nset.readJSON(  NikitaInternet.getString( NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl()+"/mobile.mtable", hashtable ))  );					
				nresult = DevActivity.checkSyncStatus(nv);	
				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}
				Nikitaset ns = new Nikitaset( nv )   ;	
				DevActivity.dropTable(ncmobile,  savatablename);
				DevActivity.createTable(ncmobile, ns, savatablename);
				DevActivity.insertTable(ncmobile, ns,  savatablename);
			}				
			if (progresListener!=null) {
				progresListener.onProgres(i, fcount, "");
			}
		}
		if (fcount>=1) {
			 Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-TABLE-VERSION", tversion.toJSON() );	
		}
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();	
			hashtable.put("modeversion",  "after");
			try {
				NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mtable_user", hashtable );
			} catch (Exception e) { }
		}	
		return nresult;
	}
	public static Nset saveSyncFile(Context context, NikitaConnection nc, Nset param, OnProgresListener progresListener ){
		Nset nresult = checkSyncStatus( Nset.newObject() );
		Nset n = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-DOWNLOAD-FILE").toString() );
		int fcount = n.getArraySize();
		Nset tversion = Nset.newObject();
		if (fcount>=1) {
			tversion = Nset.readJSON( Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-FILE-VERSION", "{}") );//{tablename:versi,...}
		}
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
			hashtable.put("serverversion",  n.toString() );
			hashtable.put("localversion", tversion.toString() );
			try {
				Nset res = NikitaInternet.getNset( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mfile_user", hashtable ) );
				if (res.getData("status").toString().equalsIgnoreCase("OK")) {
					if (res.containsKey("serverversion")) {
						n = Nset.readJSON(res.getData("serverversion").toString());
					}
					if (res.containsKey("localversion")) {
						tversion =  Nset.readJSON(res.getData("localversion").toString());
					}
				}
			} catch (Exception e) { }
		}	
		for (int i = 0; i < n.getArraySize(); i++) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
			boolean downloadnow = false ; String savatablename = "";
			if ( n.getData(i).isNsetArray()) {
				hashtable.put("file",  n.getData(i).getData(0).toString() );//[table,version, save]
				if (n.getData(i).getData(1).toInteger()==0||n.getData(i).getData(1).toInteger()>tversion.getData(n.getData(i).getData(0).toString()).toInteger()) {
					tversion.setData(n.getData(i).getData(0).toString(), n.getData(i).getData(1).toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData(2).toString().equals("")?n.getData(i).getData(0).toString():n.getData(i).getData(2).toString();
				}
				hashtable.put("fileversion", tversion.getData(n.getData(i).getData(0).toString()).toInteger() );
			}else if ( n.getData(i).isNsetObject()) {
				hashtable.put("file",  n.getData(i).getData("table").toString() );//{table:tablename, version:vernumber,save:tablename}
				if (n.getData(i).getData("version").toInteger()==0||n.getData(i).getData("version").toInteger()>tversion.getData(n.getData(i).getData("table").toString()).toInteger()) {
					tversion.setData(n.getData(i).getData("table").toString(), n.getData(i).getData("version").toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData("save").toString().equals("")?n.getData(i).getData("table").toString():n.getData(i).getData("save").toString();
				}
				hashtable.put("fileversion", tversion.getData(n.getData(i).getData("table").toString()).toInteger() );
			}else{
				hashtable.put("file",  n.getData(i).toString() );//table
				hashtable.put("fileversion", 0 );
				savatablename = n.getData(i).toString();
				downloadnow=true;
			}
			if (progresListener!=null) {
				progresListener.onProgres(i, fcount, "downloading");
			}
			if (downloadnow && savatablename.length()>=1) {						
				HttpResponse httpResponse = NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mfile", hashtable )  ;					
				try {
					InputStream inputStream = httpResponse.getEntity().getContent();
					FileOutputStream fileOutputStream = new FileOutputStream(Utility.getDefaultPath(savatablename));
					int length = 0;byte[] buffer = new byte[1024];
					while ((length = inputStream.read(buffer)) != -1) {
						fileOutputStream.write(buffer, 0, length);
					}
					fileOutputStream.close();				
				} catch (Exception e) {
					nresult = DevActivity.checkSyncStatus(Nset.newObject().setData("error", e.getMessage()));	
					return nresult;	
				} 				  
			}				
			if (fcount>=1) {
				 Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-FILE-VERSION", tversion.toJSON() );	
			}			
		}//end foor	
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();	
			hashtable.put("modeversion",  "after");
			try {
				NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mfile_user", hashtable );
			} catch (Exception e) { }
		}	
		return nresult;
	}
	public static Nset saveSyncFT(Context context, NikitaConnection ncmobile, Nset param, OnProgresListener progresListener ){
		Nset nresult = checkSyncStatus( Nset.newObject() );
		Nset fretry = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-DOWNLOAD-FRETRY").toString() );
		if (fretry.containsKey("fretry")) {
			nresult.setData("state", fretry.getData("fretry").toString());
		}				
		Nset n = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-DOWNLOAD-FTABLE").toString() );
		int fcount = n.getArraySize();
		Nset tversion = Nset.newObject();
		if (fcount>=1) {
			tversion = Nset.readJSON( Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-FTABLE-VERSION", "{}") );//{tablename:versi,...}
		}
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
			hashtable.put("serverversion",  n.toString() );
			hashtable.put("localversion", tversion.toString() );
			try {
				Nset res = NikitaInternet.getNset( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mftable_user", hashtable ) );
				if (res.getData("status").toString().equalsIgnoreCase("OK")) {
					if (res.containsKey("serverversion")) {
						n = Nset.readJSON(res.getData("serverversion").toString());
					}
					if (res.containsKey("localversion")) {
						tversion =  Nset.readJSON(res.getData("localversion").toString());
					}
				}
			} catch (Exception e) { }
		}	
		if (!tversion.isNsetObject()) {
			tversion = Nset.newObject();
		}		
		for (int i = 0; i < n.getArraySize(); i++) {
			String fieldcase = "";
			Hashtable hashtable = AppNikita.getInstance().getArgsData();
			boolean downloadnow = false ; String savatablename = "";
			if ( n.getData(i).isNsetArray()) {
				fieldcase =  n.getData(i).getData(3).toString() ;
				hashtable.put("table",  n.getData(i).getData(0).toString() );//[table,version, save, fieldoption]
				if (n.getData(i).getData(1).toInteger()==0||n.getData(i).getData(1).toInteger()>tversion.getData(n.getData(i).getData(0).toString()).toInteger()) {
					tversion.setData(n.getData(i).getData(0).toString(), n.getData(i).getData(1).toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData(2).toString().equals("")?n.getData(i).getData(0).toString():n.getData(i).getData(2).toString();
				}
			}else if ( n.getData(i).isNsetObject()) {
				fieldcase =  n.getData(i).getData("field").toString() ;
				hashtable.put("table",  n.getData(i).getData("table").toString() );//{table:tablename, version:vernumber,save:connaame}
				if (n.getData(i).getData("version").toInteger()==0||n.getData(i).getData("version").toInteger()>tversion.getData(n.getData(i).getData("table").toString()).toInteger()) {
					tversion.setData(n.getData(i).getData("table").toString(), n.getData(i).getData("version").toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData("save").toString().equals("")?n.getData(i).getData("table").toString():n.getData(i).getData("save").toString();
				}
			}else{
				hashtable.put("table",  n.getData(i).toString() );//table
				savatablename = n.getData(i).toString();//not recom
				downloadnow=true;
			}
			hashtable.put("file",  hashtable.get("table") );//file=table
			int rows = -1;
			if (downloadnow && savatablename.length()>=1) {	
				try {	
					if (progresListener!=null) {
						progresListener.onProgres(i, fcount, "preparing");
					}
					HttpResponse response = NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mftable", hashtable );
					Header[] hdrs = response.getHeaders("rows");
					if (hdrs!=null) {
						for (int j = 0; j < hdrs.length; j++) {
							rows = Utility.getInt(String.valueOf(hdrs[j].getValue()));
						}
					}	
					Nset nrst = DevActivity.checkSyncStatus(rows>=0?Nset.newObject():Nset.newObject().setData("error", "Connection Error Row -1"));	
					nresult = DevActivity.checkSyncStatus(nresult, nrst);
					if (DevActivity.checkSyncStatusBreak(nrst)) {
						return nresult;
					}
					try {
						InputStream inputStream = response.getEntity().getContent();
						if (progresListener!=null) {
							progresListener.onProgres(i, fcount, "downloading");
						}
						FileOutputStream fileOutputStream = new FileOutputStream(Utility.getDefaultTempPath(savatablename));
						int length = 0;byte[] buffer = new byte[1024];
						while ((length = inputStream.read(buffer)) != -1) {
							fileOutputStream.write(buffer, 0, length);
						}
						fileOutputStream.close();
					}  catch (Exception e) { 
						nrst  =  NikitaInternet.getTimeoutMessage();	
						nresult = DevActivity.checkSyncStatus(nresult, nrst);
						return nresult;	
					}					
					//extrack from server
					FileInputStream fileInputStream = new FileInputStream(Utility.getDefaultTempPath(savatablename));
					DataInputStream dataInputStream = new DataInputStream(fileInputStream);
					
					/*===================================================================*/
					ncmobile = NikitaConnection.getConnection(savatablename.equals("")?NikitaConnection.MOBILE:savatablename);
					/*===================================================================*/
					
					int cur  = 0;
					
					String tname ="";
					Nikitaset ns = null;
					
					while(dataInputStream.available()>0) {			            
			            String currentRow = dataInputStream.readUTF();
			            Nset nv = Nset.readJSON(currentRow);
			            if (nv.isNsetObject()) {		            	
			            	/*[drop data]*/ 
			            	//rows = nv.getData("rows").toInteger();
			            	tname = nv.getData("tablename").toString();
							DevActivity.dropTable(ncmobile,  tname);
							
			            	/*[create data]*/							
							ns = new Nikitaset(nv.getData("nikitaset"));
							if (fieldcase.contains("flcase")||fieldcase.contains("fucase")||fieldcase.contains("fieldlcase")||fieldcase.contains("fieldlucase")) {
								Vector<String> hdr = new Vector<String>();
								for (int j = 0; j < ns.getDataAllHeader().size(); j++) {
									if (fieldcase.contains("flcase")||fieldcase.contains("fieldlcase")) {
										hdr.addElement(ns.getDataAllHeader().elementAt(j).toLowerCase());
									}else{
										hdr.addElement(ns.getDataAllHeader().elementAt(j).toUpperCase());
									}									
								}
								ns.setAllHeader(hdr);
							}
			            	DevActivity.createTable(ncmobile, ns , tname);
			            	
			            	/*[Check error]*/
			            	nrst = DevActivity.checkSyncStatus(nv.getData("nikitaset"));	
			            	nresult =  DevActivity.checkSyncStatus(nresult, nrst);
							if (DevActivity.checkSyncStatusBreak(nresult)) {
								fileInputStream.close();
								new File(Utility.getDefaultTempPath(savatablename)).delete();
								return nresult;
							}
						}else if (nv.isNsetArray() && ns!= null) {
							/*[insert data]*/							
							try {
								ns.setAllData((Vector<Vector<String>>)nv.getInternalObject());
								DevActivity.insertTable(ncmobile, ns,  tname);
							} catch (Exception e) { Generator.err("DEBUGx", e.getMessage()); }
						}						
			            if (progresListener!=null) {
							progresListener.onProgres(cur, Math.max(1, rows), "s");
						}
			            cur++;
			        }					
					fileInputStream.close();
					new File(Utility.getDefaultTempPath(savatablename)).delete();
				} catch (Exception e) { 
					Nset nrst = DevActivity.checkSyncStatus(Nset.newObject().setData("error", String.valueOf(e.getMessage())));	
					nresult =  DevActivity.checkSyncStatus(nresult, nrst);
					return nresult;				
				} 
			}				
			
			if (progresListener!=null) {
				progresListener.onProgres(i, fcount, "");
			}
			if (fcount>=1) {
				 Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-FTABLE-VERSION", tversion.toJSON() );	
			}			
		}//end foor
		
		//add 17072016 
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();	
			hashtable.put("modeversion",  "after");
			try {
				NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mftable_user", hashtable );
			} catch (Exception e) { }
		}	
		return nresult;
	}
	public static Nset saveSyncTableFile(Context context,NikitaConnection nc, Nset param, OnProgresListener progresListener ){
		Nset nresult = checkSyncStatus( Nset.newObject() );
		//download additional table-file [10]
		Nset n = Nset.readJSON( Generator.getNikitaParameters().getData("INIT-DOWNLOAD-TABLEFILE").toString() );
		int fcount = n.getArraySize();
		Nset tversion = Nset.newObject();
		if (fcount>=1) {
			tversion = Nset.readJSON( Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-TABLEFILE-VERSION", "{}") );//{tablename:versi,...}
		}
		//add 17072016 
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
			hashtable.put("serverversion",  n.toString() );
			hashtable.put("localversion", tversion.toString() );
			try {
				Nset res = NikitaInternet.getNset( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mtablefile_user", hashtable ) );
				if (res.getData("status").toString().equalsIgnoreCase("OK")) {
					if (res.containsKey("serverversion")) {
						n = Nset.readJSON(res.getData("serverversion").toString());
					}
					if (res.containsKey("localversion")) {
						tversion =  Nset.readJSON(res.getData("localversion").toString());
					}
				}
			} catch (Exception e) { }
		}	
		for (int i = 0; i < n.getArraySize(); i++) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();			
 
			
			boolean downloadnow = false ; String savatablename = "";
			if ( n.getData(i).isNsetArray()) {
				hashtable.put("table",  n.getData(i).getData(0).toString() );//[table,version, save]
				if (n.getData(i).getData(1).toInteger()==0||n.getData(i).getData(1).toInteger()>tversion.getData(n.getData(i).getData(0).toString()).toInteger()) {
					tversion.setData(n.getData(i).getData(0).toString(), n.getData(i).getData(1).toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData(2).toString().equals("")?n.getData(i).getData(0).toString():n.getData(i).getData(2).toString();
				}
			}else if ( n.getData(i).isNsetObject()) {
				hashtable.put("table",  n.getData(i).getData("table").toString() );//{table:tablename, version:vernumber,save:tablename}
				if (n.getData(i).getData("version").toInteger()==0||n.getData(i).getData("version").toInteger()>tversion.getData(n.getData(i).getData("table").toString()).toInteger()) {
					tversion.setData(n.getData(i).getData("table").toString(), n.getData(i).getData("version").toInteger());
					downloadnow=true;
					savatablename =  n.getData(i).getData("save").toString().equals("")?n.getData(i).getData("table").toString():n.getData(i).getData("save").toString();
				}
			}else{
				hashtable.put("table",  n.getData(i).toString() );//table
				savatablename = n.getData(i).toString();
				downloadnow=true;
			}
			hashtable.put("file",  hashtable.get("table") );//file=table
			
			
			if (downloadnow && savatablename.length()>=1) {						
				Nset nv  = Nset.readJSON(  NikitaInternet.getString( NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mtablefile/?", hashtable ))  );					
				nresult = DevActivity.checkSyncStatus(nv);	
				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}
				
				Nikitaset ns = new Nikitaset( nv )   ;	
				try {	
					FileOutputStream fileOutputStream = new FileOutputStream(Utility.getDefaultPath(savatablename));
					fileOutputStream.write(ns.toNset().toJSON().getBytes());
					fileOutputStream.close();
				} catch (Exception e) {
					nresult = DevActivity.checkSyncStatus(Nset.newObject().setData("error",  String.valueOf(e.getMessage())));	
					return nresult;	
				} 
			}				
			
			if (progresListener!=null) {
				progresListener.onProgres(i, fcount, "");
			}
		}
		if (fcount>=1) {
			 Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-TABLEFILE-VERSION", tversion.toJSON() );	
		}
		
		//add 17072016 
		if (Generator.getNikitaParameters().getData("INIT-DOWNLOAD-USERVERSION").toString().equals("true")) {
			Hashtable hashtable = AppNikita.getInstance().getArgsData();	
			hashtable.put("modeversion",  "after");
			try {
				NikitaInternet.postHttp( AppNikita.getInstance().getBaseUrl()+"/mobile.mtablefile_user", hashtable );
			} catch (Exception e) { }
		}	
		return nresult;
	}
	public static void deleteMobileOrder(NikitaConnection nc, Nikitaset ns, String table){
		StringBuffer sbuff = new StringBuffer();
		for (int row = 0; row < ns.getRows(); row++) {
			sbuff.append(row>=1?",":"");
			sbuff.append("'").append(StringEscapeUtils.escapeSql(  ns.getText(row, "orderid")  ) ).append("'");
		}
		
		deleteTable(nc, table, "orderid IN ["+sbuff.toString()+"]");
	}
	public static void deleteTable(NikitaConnection nc,  String tablename, String where){
		nc.Query("DELETE FROM "+tablename + " WHERE "+ where ) ;	
	}
	public static void makeMobileOrderlcase(Nikitaset ns){
		Vector<String> header = new Vector<String> ();
		for (int i = 0; i < ns.getCols(); i++) {
			if (ns.getHeader(i).toLowerCase().equals("orderid")) {
				header.addElement(ns.getHeader(i).toLowerCase());
			}else{
				header.addElement(ns.getHeader(i));
			}			
		}
		ns.setAliasName(header);
	}
	public static void dropTable(NikitaConnection nc,  String tablename){
		nc.Query("drop table "+tablename) ;	
	}
	public static void createTable(NikitaConnection nc, Nikitaset ns, String tablename){
		StringBuffer sbuff = new StringBuffer();
		sbuff.append("CREATE table ").append(tablename).append(" ( ");
	 
		for (int col = 0; col < ns.getCols(); col++) {
			String fname = new Nset(ns.getInfo()).getData("metadata").getData("type").getData(col).toString();
			fname=NikitaConnection.convertType(fname, NikitaConnection.CORE_SQLITE);
			fname=fname.equals("")?"TEXT":fname;
			sbuff.append(col>=1?",":"").append(ns.getHeader(col)).append("  ").append( fname );		
		}
		sbuff.append(" ) ");
		Generator.out("SQL", sbuff.toString());
		nc.Query(sbuff.toString()) ;
	}
	public static void insertTable(NikitaConnection nc, Nikitaset ns, String table){		
		StringBuffer sb = new StringBuffer("INSERT INTO "+ table + " (");		
		for (int col = 0; col < ns.getCols(); col++) {
			sb.append(col>=1?",":"");
			sb.append("'").append( StringEscapeUtils.escapeSql( ns.getHeader(col)) ).append("'") ;
		}
		sb.append(") VALUES ");
		
		for (int row = 0; row < ns.getRows(); row++) {
			try {
			StringBuffer va = new StringBuffer("(");
			for (int col = 0; col < ns.getCols(); col++) {
				va.append(col>=1?",":"");
				va.append("'").append( StringEscapeUtils.escapeSql( ns.getText(row, col)) ).append("'") ;
			}
			va.append(")");
				Generator.out("DEBUG", sb.toString()+ va.toString());
			nc.Query( sb.toString() + va.toString() );
				//Generator.out("DEBUGE", nst.getError());
			} catch (Exception e) { 
				Generator.err("DEBUGX", e.getMessage());  }
		} 
	}
	 
	public static void debug (){
		WifiManager wifi = (WifiManager)AppNikita.getInstance().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		//if (wifi!=null && wifi.isWifiEnabled()  ) {
			TelephonyManager tm = (TelephonyManager)AppNikita.getInstance().getApplicationContext(). getSystemService(android.content.Context.TELEPHONY_SERVICE);
			String imei=tm.getDeviceId().toUpperCase();
			String s =Utility.getHttpConnection( AppNikita.getInstance().getBaseUrl()+"/res/mobiledebug/?code=MOBILE-PLAY-"+imei);
			
			final Nikitaset ns = new Nikitaset(Nset.readJSON(s));
			if (ns.getRows()>=1) {
				
				
				final String formname = Nset.readJSON(ns.getText(0, 2)).getData("fid").toString();
								
				Intent   filter= new Intent();
			    filter.setAction("com.nikita.generator.start");
			    filter.putExtra("nikitaformname", formname);			   	    
			    AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 
				
				
				AppNikita.getInstance().getMainHandler().post(new Runnable() {
					public void run() {
						 Intent   intent= new Intent();
						 intent.putExtra("nikitaformname", formname);
						 //AppNikita.getInstance().getApplicationContext().startActivity(intent);
					}
				});				
			 
			}
		//}	if
	}
	public static void newDebugMode (String message){
		final String formname = Nset.readJSON(message).getData("fid").toString();
		
		Intent   filter= new Intent();
	    filter.setAction("com.nikita.generator.start");
	    filter.putExtra("nikitaformname", formname);			   	    
	    //AppNikita.getInstance().getApplicationContext().sendBroadcast(filter); 		
		Generator.clearNikitaForms();
		
		AppNikita.getInstance().getMainHandler().post(new Runnable() {
			public void run() {
				 Intent intent= new Intent(AppNikita.getInstance(), StarterActivity.class);
				 intent.putExtra("nikitaformname", formname);
				 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|intent.FLAG_ACTIVITY_CLEAR_TASK);
				 AppNikita.getInstance().getApplicationContext().startActivity(intent);
				 
			}
		});
	}
	public static void createShortcutIcon(Context context, String clsname, String name, int icon){
    	try {
            Intent shortcutIntent = new Intent(context.getApplicationContext(), Class.forName(clsname));
            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(context.getApplicationContext(), icon));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            context.getApplicationContext().sendBroadcast(addIntent);
		} catch (Exception e) { }
    }
	public static void removeShortcutIcon(Context context, String clsname, String name){
    	try {
            Intent shortcutIntent = new Intent(context.getApplicationContext(), Class.forName(clsname));
            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
            addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
            context.getApplicationContext().sendBroadcast(addIntent);
		} catch (Exception e) { }
    }
    
    public static void changeManifestIcon(Context context, String clasname, boolean enable){
    	 PackageManager pm = context.getApplicationContext().getPackageManager();
 
         try {
        	 if (enable) {       
            	 ComponentName componentName = new ComponentName(  context.getApplicationContext().getPackageName(), clasname);
            	 pm.setComponentEnabledSetting(componentName,
                         PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                         PackageManager.DONT_KILL_APP);
    		 }else{
    			 ComponentName componentName = new ComponentName(  context.getApplicationContext().getPackageName(), clasname);
    			 pm.setComponentEnabledSetting(componentName, 
               		 	PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                        PackageManager.DONT_KILL_APP);
    		 }  
		} catch (Exception e) { }
    }
    
    protected void onStart() {
    	super.onStart();    	
    	printer=AppNikita.getInstance().printer;
    	activity=this;
//   	 	printer.setOnFinishPrint(new OnFinishPrint() {
//			@Override
//			public void OnFinish(Boolean status, String message, String battery, String deviceName, String deviceMAC) {
//				// TODO Auto-generated method stub
//				if (status) {
//					
//				} else {
//					
//				}
//			}
//		});
    };
    protected void onStop() {
    	super.onStop();
	   	printer.disconnect();
	   	printer = null;
    }
					
}
