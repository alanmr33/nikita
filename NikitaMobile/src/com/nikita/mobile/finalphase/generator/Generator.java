package com.nikita.mobile.finalphase.generator;

import java.lang.reflect.Constructor;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.component.IExpression;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.database.SingleRecordset;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.stream.NfData;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class Generator {
	public static  boolean isClassExist(String className) {
        try  {
            Class.forName(className);
            return true;
        }  catch (final ClassNotFoundException e) {
            return false;
        }
    }
	
	private static Nset initparam =  Nset.newObject();
	public static Nset getNikitaParameters(){
		return initparam;
	}
	public static String getNikitaParameterValue(String key, String def){
		if (initparam.containsKey(key)) {
			return initparam.getData(key).toString();
		}
		return def;
	}
	public static int getNikitaParameterValueInt(String key, int def){
		if (initparam.containsKey(key)) {
			return initparam.getData(key).toInteger();
		}
		return def;
	}

	public static void clearNikitaForms(){
		AppNikita.getInstance().removeNikitaControlerAll();
	}
	
	public static void startNikitaForm(Activity caller, String formname, Nset args){
		if (formname.endsWith("Activity")) {
			if (Generator.isClassExist("com.nikita.mobile.finalphase.activity."+formname)) {
				try {
					Intent intent = new Intent(caller, Class.forName("com.nikita.mobile.finalphase.activity."+formname));	
					String[] keys = args.getObjectKeys();
					for (int i = 0; i < keys.length; i++) {
						intent.putExtra(keys[i], args.getData(keys[i]).toString());
					}	
					caller.startActivity(intent);
				} catch (Exception e) { }
			}
		}else{
			NikitaControler nikitaComponent =   NikitaControler.getInstance(formname); 
			nikitaComponent.internalstart("", args);
			nikitaComponent.startFromActivity(caller, "1");
		}
	}
	public static void startNikitaForm(Activity caller, String formname){
		startNikitaForm(caller, formname, Nset.newObject());
	}
	public static void startNikitaForm(Activity caller, Class<?> form) {
		startNikitaForm(caller, form, Nset.newObject());
	}
	public static void startNikitaForm(Activity caller, Class<?> form, Nset args) {
		try {
			NikitaControler nikitaComponent = (NikitaControler) Class.forName(form.getName()).newInstance();
			nikitaComponent.internalstart("", args);
			nikitaComponent.startFromActivity(caller, "1");
		} catch (Exception e) { 
			Log.e("startNikitaForm", e.getMessage());
		}		
	}
    
	 
	public static void synchronizeNikita(final Context appContext){
		Log.i("Nikita", "synchronizeNikita[Generator]");
		Utility.setAppContext(appContext);	
		
		String init =Utility.getHttpConnection(AppNikita.getInstance().getBaseUrl()+"/res/mobileparameter/init/");
		Nset n = Nset.readJSON(init);
		
		if (Nikitaset.isNikitaset(n)) {
			Nikitaset nikitaset = new Nikitaset(n);
			if (nikitaset.getError().length()<=1) {
				n = Nset.newObject();
				for (int i = 0; i < nikitaset.getRows(); i++) {
					n.setData(nikitaset.getText(i, "paramkey"), nikitaset.getText(i, "paramvalue"));
				}
				Utility.setSetting(appContext, "INIT", n.toJSON());
 
			}
		}		 
		//------------------------------------------------------------------------------------------//
		String dbcon =Utility.getHttpConnection(AppNikita.getInstance().getBaseUrl()+"/mobile.mlogic/?mode=connection");
		n = Nset.readJSON(dbcon);
		if (Nikitaset.isNikitaset(n)) {			
			Nikitaset nikitaset = new Nikitaset(n);
			if (nikitaset.getError().length()<=1) {
				Utility.setSetting(appContext, "CONN", dbcon);
			}
		}
		
		
		
		initializeNikita(appContext);
	}
	public static void synchronizeLogic(final Context appContext){
		
	}
	public static void initializeNikita(final Context appContext){
		Log.i("Nikita", "initializeNikita[Generator]");
		Utility.setAppContext(appContext);
		 
		Generator.closeAllConnection(); 
		NikitaConnection.setDefaultConnectionSetting(new NikitaProperty(""){
			public synchronized Nset read() {
				 Nset defLogin = Nset.newObject();	
				 initparam = Nset.readJSON(Utility.getSetting(appContext, "INIT", ""));
				 
				 Nset in = initparam ; Nset n = Nset.newObject();
                 n.setData("user" ,in.getData("INIT-CONNECTION-USER").toString() );
                 n.setData("pass" ,in.getData("INIT-CONNECTION-PASS").toString());
                 n.setData("class",in.getData("INIT-CONNECTION-CLASS").toString());
                 n.setData("url"  ,in.getData("INIT-CONNECTION-URL").toString() );
                 defLogin.setData("logic", n);
                 
				 Nikitaset nikitaset = new  Nikitaset( Nset.readJSON(Utility.getSetting(appContext, "CONN", ""))  ) ;
		           for (int i = 0; i < nikitaset.getRows(); i++) {
		               if (!nikitaset.getText(i, 0).equals("")) {
		                    n = Nset.newObject();
		                    n.setData("user",nikitaset.getText(i, "connusername").toString() );
		                    n.setData("pass",nikitaset.getText(i, "connpassword").toString() );
		                    n.setData("class",nikitaset.getText(i, "connclass").toString() );
		                    n.setData("url",nikitaset.getText(i, "connurl").toString() );
		                    defLogin.setData(nikitaset.getText(i, "connname").toString(), n);       
		               }    
		           }
		          
				return defLogin;
			};
			
		});	 
		//------------------------------------------------------------------------------------------//
	}
	
	private static Hashtable<String, NikitaConnection> ncarr = new Hashtable<String, NikitaConnection>();
	public static void closeAllConnection(){
        Enumeration<NikitaConnection> en = ncarr.elements();
        while (en.hasMoreElements()) {
            NikitaConnection nc = en.nextElement();
            nc.closeConnection();
        }
    } 
	     
    public static NikitaConnection getConnection(){        
        return ncarr.get(NikitaConnection.LOGIC);
    }
    public static NikitaConnection getConnection(String s){
        s=s.equals("")?NikitaConnection.DEFAULT:s;                
        NikitaConnection nc =  ncarr.get(s);
        if (nc==null) {
            nc = NikitaConnection.getConnection(s);
            ncarr.put(s, nc);   
            
        }else{
            if ( nc.isClosed() ) {
                nc = NikitaConnection.getConnection(s);
                ncarr.put(s, nc);   
            }
        }
        return nc;
    }
	public static void out(String tag, String msg){
		Log.i(tag, msg);
	}
	public static void err(String tag, String msg){
		Log.e(tag, msg);
	}
	public static String getVirtualString(String key) {
		if (getVirtual(key)!= null) {
			if (getVirtual(key)instanceof String) {
				return (String)getVirtual(key);
			}
			return getVirtual(key).toString();
		}
		return "";
	}
	public static Object getVirtual(String key) {
		return AppNikita.getInstance().getVirtual(key);
	}
	public static void setVirtual(String key,  Object data) {
		AppNikita.getInstance().setVirtual(key, data);
	}
	
	public static boolean runActionClass(Nset action, Component component){
        try {
                               
            String classname = "com.nikita.mobile.finalphase.generator.action."+action.get("class").toString();
            if (action.get("class").toString().equals("")) {
                return true; 
            }else if (action.get("class").toString().equals("com.")) {
                classname = action.get("class").toString();
            }     
            IAction action1 = (IAction) Class.forName(classname).newInstance();
            return action1.OnAction(component, action);
        } catch (Exception e) {   }
        return false;
    }   
    public static boolean runExpressionClass(Nset expression, Component component){
        try {           
            String classname = "com.nikita.mobile.finalphase.generator.expression."+expression.get("class").toString();
            if (expression.get("class").toString().equals("")) {
                return component.getNikitaComponent().expression; 
            }else if (expression.get("class").toString().equals("com.")) {
                classname = expression.get("class").toString();
            } 
            IExpression expression1 = (IExpression) Class.forName(classname).newInstance();
            return expression1.OnExpression(component, expression);
        } catch (Exception e) {    }
        return false;
    }   

    public static void loopdetect(Component component){
            try {
                int i = Utility.getInt(component.getNikitaComponent().getVirtualString("@+LOOP"))+1;
                if (i>=1000) {
                	component.getNikitaComponent().setInterupt(false);
                	Generator.out("loopdetect","Nikita Detect Looping without End");
                }
                component.getNikitaComponent().setVirtualRegistered("@+LOOP", i );
            } catch (Exception e) { }
    }
	
    public static void saveOpenForms() {
    	String[] keys = Utility.getObjectKeys(AppNikita.getInstance().getNikitaControlers());
		for (int i = 0; i < AppNikita.getInstance().getNikitaControlerCount() ; i++) {
			AppNikita.getInstance().getNikitaComponent(keys[i]).saveFileComponents();
		} 	
	}
    public static Nset getStreamFormsWithCurrentForm(String curform){
    	Nset result = getStreamForms();
    	if (!curform.trim().equals("")) {
    		result.getData(Utility.MD5("init").toLowerCase(Locale.ENGLISH)).setData("nikitageneratorcurrentform", curform);
		}     
    	return result;
    }
    public static Nset getStreamForms(){
    	return getStreamForms(new String[0]);
    }
    public static Nset getStreamForms(String...fname){
    	Nset result = Nset.newObject();Nset forms = Nset.newObject();
		if (fname!=null && fname.length>=1) {
			String[] keys = Utility.getObjectKeys(AppNikita.getInstance().getNikitaControlers());
			for (int i = 0; i < keys.length; i++) {
				for (int j = 0; j < fname.length; j++) {
					 if (keys[i].equals(fname[j])) {
						 result.setData(keys[i], AppNikita.getInstance().getNikitaComponent(keys[i]).getStreamComponents());
						 forms.setData(AppNikita.getInstance().getNikitaComponent(keys[i]).getNikitaFormName(), keys[i]);
						 break;
					}
				} 
			}		
		}else{
			String[] keys = Utility.getObjectKeys(AppNikita.getInstance().getNikitaControlers());
			for (int i = 0; i < AppNikita.getInstance().getNikitaControlerCount(); i++) {				
				result.setData(keys[i], AppNikita.getInstance().getNikitaComponent(keys[i]).getStreamComponents());
				forms.setData(AppNikita.getInstance().getNikitaComponent(keys[i]).getNikitaFormName(), keys[i]); 
			} 
		}
		//result.setData("nikitageneratorformname", forms);
		result.setData( Utility.MD5("init").toLowerCase(Locale.ENGLISH), Nset.newObject().setData("nikitageneratorcurrentform",AppNikita.getInstance().getCurrentNikitaControler().getNikitaFormName()));
		return result;
	}
    public static void openStreamForms(Nset data, Activity caller){ 
    	openStreamForms(data, caller, null);
    }
    public static void openStreamForms(Nset data, Activity caller, String request){ 
    	openStreamForms(data, null, caller, request);
    }
    public static void openStreamForms(Nset data, String startform, Activity caller, String request){ 
    	/*
    	if (AppNikita.getInstance().getActionHandler()!=null) {
			AppNikita.getInstance().getActionHandler().post(new Runnable() {
	    		Nset data;  Activity caller;
	    		public Runnable get (Nset data, Activity caller){
	    			this.caller=caller;
	    			this.data=data;
	    			return this;
	    		}
				public void run() {
					String currentform = openStreamForms(data);
			    	if (currentform!=null) {
			    		NikitaControler.getInstance(currentform).startFromActivity(caller);
					}				
				}
			}.get(data, caller));
		}
    	*/
    	String currentform = openStreamForms(data);
    	if (startform!=null && !startform.equals("")) {
    		currentform = startform;
		}
    	if (currentform!=null) {
    		NikitaControler.getInstance(currentform).startFromActivity(caller, request);
		}
		
    }
    public static void openStreamForms(Nset data, NikitaControler caller){  
    	caller.runOnActionThread(new Runnable() {
    		Nset data;
    		public Runnable get (Nset data){
    			this.data=data;
    			return this;
    		}
			public void run() {
				String currentform = openStreamForms(data);
		    	if (currentform!=null) {
		    		NikitaControler.getInstance(currentform).show();
				}				
			}
		}.get(data));
    	
    }
    private static String openStreamForms(Nset data){    	
    	String[] keys = data.getObjectKeys();String currentform = null;
    	for (int i = 0; i < keys.length; i++) {
    		if (keys[i].equalsIgnoreCase(Utility.MD5("init"))) {
    			currentform = data.getData(Utility.MD5("init")).getData("nikitageneratorcurrentform").toString();
			}else{
				if (AppNikita.getInstance().getNikitaComponent(keys[i])!=null) {
					AppNikita.getInstance().getNikitaComponent(keys[i]).close();
				}
				NikitaControler nc = NikitaControler.getInstance(keys[i]);				
				
				nc.addOnLoadListener( new Runnable() {
					Nset data;NikitaControler nc;
					public Runnable get(NikitaControler nc,Nset data){
						this.nc=nc;
						this.data=data;
						return this;
					}
					public void run() {
						nc.openStreamComponents(data);						
					}
				}.get(nc, data.getData(keys[i]))  );
				
				nc.start();//start noui		
			}
		}  
    	return currentform;
		}  
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 
	
	public static Activity curractivity;
	public static String currBufferedView;
	public static String currModel;
	public static String currApplication;
	public static String currApplicationName;
	public static String currModelActivityID;
	public static String currOrderCode;
	public static String currOrderTable;
	public static String currAppTheme;//new 05/05/2014
	public static String currAppMenu;
 
	
	public static Hashtable<String, String> virtual = new Hashtable<String, String>();
	public static Recordset currRecordset;
	public static String currResult;
	@SuppressWarnings("rawtypes")
	public static Class currHome;

	protected static void setCurrentActivity(Activity activity) {
		curractivity = activity;
	} 

	public static String getFileSeparator(){
		return System.getProperty("file.separator"); 
	}

	public static boolean onGeneratorNewExpression(Component comp, SingleRecordset data) {
		 
		return false;
	}

	public static boolean onGeneratorNewAction(Component comp, SingleRecordset data) {
		  
		return true;
	}
}
