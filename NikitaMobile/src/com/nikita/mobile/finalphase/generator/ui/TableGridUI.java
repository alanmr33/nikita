package com.nikita.mobile.finalphase.generator.ui;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class TableGridUI  extends Component{
	public TableGridUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	private TableLayout tableGrid;
	
	@Override
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.compgrid);
		tableGrid=(TableLayout)view.findViewById(R.id.tblGrid);
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		//view.findViewById(R.id.linegrid).getLayoutParams().width = view.getContext().getResources().getDisplayMetrics().widthPixels; 
		 
		//tableGrid.getLayoutParams().width=form.getActivity().getResources().getDisplayMetrics().widthPixels;
		 	
		
		view.findViewById(R.id.imgFirst).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {  
				prepareAction("page-first") ;
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRouteAction("page-first");							
					}
				});	
			}
		});
		view.findViewById(R.id.imgPrev).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {  
				prepareAction("page-prev") ;
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRouteAction("page-prev");							
					}
				});	
			}
		});
		
		view.findViewById(R.id.imgNext).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {  
				prepareAction("page-next") ;
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRouteAction("page-next");							
					}
				});	
			}
		});
		view.findViewById(R.id.imgLast).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {  
				prepareAction("page-last") ;
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						runRouteAction("page-last");							
					}
				});	
			}
		});
		
		brunFirst = false;
		
		final android.widget.Spinner spin = (android.widget.Spinner) view.findViewById(R.id.spnShowPage);
		ArrayAdapter<String> adapter = new ArrayAdapter(getNikitaComponent().getActivity(), android.R.layout.simple_spinner_item, new String[]{"10","50","100"});//simple_list_item_1
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);//simple_spinner_dropdown_item
		spin.setAdapter(adapter);
		spin.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView arg0, View arg1, int position, long arg3) {
				if (brunFirst) {						 
					prepareAction("page-view") ;
					rowperPage=Utility.getInt((String)spin.getSelectedItem());
					getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							runRouteAction("page-view");
						}
					});	
				}
				brunFirst=true;
			}
			public void onNothingSelected(AdapterView<?> arg0) { }
		});
		for (int i = 0; i < adapter.getCount(); i++) {
			if(rowperPage==Utility.getInt(adapter.getItem(i))){
				spin.setSelection(i);
			}
		}	
		view.findViewById(R.id.lnrGridShow).setVisibility(View.GONE);
		view.findViewById(R.id.incPaging).setVisibility(View.GONE);
		
		

		if (getStyle()==null) {
			setStyle(new Style().setStyle("n-width", "100%"));
			maxwidth = form.getActivity().getResources().getDisplayMetrics().widthPixels;
		}else{
			if (getStyle().getInternalObject().getData("style").getData("n-width").toString().length()>=1) {
				//getStyle().setStyle("n-width", "100dp");
				maxwidth  = form.getActivity().getResources().getDisplayMetrics().widthPixels;
			}else{
				getStyle().setStyle("n-width", "100%");
				maxwidth = form.getActivity().getResources().getDisplayMetrics().widthPixels;
			}
		}
		//mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		fillGridOnUI();
		
		return view;
	}
	private boolean brunFirst = false;
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	public void setData(Nset data) {
		super.setData(data);	
		 
		maxrows=data.getArraySize();
		
		fillGridOnNewThread();
	}
	
	private void fillGridOnNewThread(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					fillGridOnUI();
					refreshViewOnUI();
				}
			});
		} 
	}
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		v = view.findViewById(R.id.incLabels);
		if (v instanceof TextView) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.GONE);
		}
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public void fillGridOnUI(){		 
		if (tableGrid!=null) {			 
			tableGrid.removeAllViews();
			
			getPagingString(getCurrentPage(), getShowPerPage(), maxrows);
			//header
			TableRow tableRow =  createTableRow();
			if (showrowindex || showcheckbox) {
				TextView txt = new TextView(getNikitaComponent().getActivity());
				txt.setText("No");
				
				FrameLayout frameLayout = createFrameLayout();
	 
				frameLayout.addView(txt, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
				MarginLayoutParams mParams = (MarginLayoutParams)txt.getLayoutParams();
				mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
				((android.widget.FrameLayout.LayoutParams)txt.getLayoutParams()).gravity=Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL;
				
				tableRow.addView(frameLayout);   
            }
			for (int col = 0; col < headers.getArraySize(); col++) {				
				//tableGrid.setC
				
				TextView txt = new TextView(getNikitaComponent().getActivity());
				if (headers.getData(col).toString().startsWith("[") && col>=2 ) {
					txt.setText("");
				}else{
					txt.setText(headers.getData(col).toString());
				}
				
				
				FrameLayout frameLayout = createFrameLayout();
	 
				frameLayout.addView(txt, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
				MarginLayoutParams mParams = (MarginLayoutParams)txt.getLayoutParams();
				mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
				((android.widget.FrameLayout.LayoutParams)txt.getLayoutParams()).gravity=Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL;
				
				
				frameLayout.setVisibility(headers.getData(col).toString().equals("")?View.GONE:View.VISIBLE);
				tableRow.addView(frameLayout);
				 
				
				//tableGrid.setColumnShrinkable(col,true);
				
			}
			if (showactioncols) {
				TextView txt = new TextView(getNikitaComponent().getActivity());
				txt.setText("");
				
				FrameLayout frameLayout = createFrameLayout();
	 
				frameLayout.addView(txt, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
				MarginLayoutParams mParams = (MarginLayoutParams)txt.getLayoutParams();
				mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
				((android.widget.FrameLayout.LayoutParams)txt.getLayoutParams()).gravity=Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL;
				
				
				tableRow.addView(frameLayout);   
            }
			tableRow.setBackgroundColor(0xffb8d1f3);
			tableGrid.addView(tableRow);
			
			int width = tableRow.getMeasuredWidth();
            if (width>maxwidth) {
            	maxwidth=width;
			} 
			
			//data
			for (int row = 0; row < getData().getArraySize(); row++) {
				tableRow = createTableRow();
				width=0;//reset
				int absoulterow = 0;
	            if ( getCurrentPage()==-1 && getShowPerPage()==-1) {
	                absoulterow = row;
	            }else{
	                absoulterow = (row  + (getCurrentPage()-1)*getShowPerPage());
	            }
	            
	            if (showrowindex || showcheckbox) {
	                if (showcheckbox) {
	                    final String idx = "-R"+absoulterow+"C-1" ;
	                    
	                    CheckBox txt = new CheckBox(getNikitaComponent().getActivity());
	                    txt.setText((absoulterow+1)+"" );
	    				
	    				FrameLayout frameLayout = createFrameLayout();
	    	 
	    				frameLayout.addView(txt, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
	    				MarginLayoutParams mParams = (MarginLayoutParams)txt.getLayoutParams();
	    				mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
	    				((android.widget.FrameLayout.LayoutParams)txt.getLayoutParams()).gravity=Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL;
	    				
	    				tableRow.addView(frameLayout);
	    				
	                }else{
	                	TextView txt = new TextView(getNikitaComponent().getActivity());
	    				txt.setText((absoulterow+1)+"" );
	    				
	    				FrameLayout frameLayout = createFrameLayout();
	    	 
	    				frameLayout.addView(txt, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
	    				MarginLayoutParams mParams = (MarginLayoutParams)txt.getLayoutParams();
	    				mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
	    				((android.widget.FrameLayout.LayoutParams)txt.getLayoutParams()).gravity=Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL;
	    				
	    				tableRow.addView(frameLayout);
	                }
	            }
	           
	            
				for (int col = 0; col <  (headers.getArraySize()==0 ?getData().getData(row).getArraySize():headers.getArraySize())     +(showactioncols?1:0); col++) {
					String stext = getData().getData(row).getData(col).toString();
					Component vv = null;Component v = null;
	                if (adapterListener!=null) {
	                    vv =adapterListener.getViewItem(row, col, this, getData());
	                    if (vv!=null && vv.isVisible()!=true && (vv.getId().equals("")||vv.getName().equals(""))) {
	                        stext=vv.getText();
	                        vv=null;                        
	                    }else{
	                        v=(vv!=null)?vv:v;   
	                    }                               
	                }       
	                View view;     
	                if (v!=null) {    
	                	view=v.getView();
	                }else{
	                	Log.i("headers", headers.getData(col).toString());
	                	if (headers.getData(col).toString().startsWith("[#]") || headers.getData(col).toString().startsWith("[*]")|| headers.getData(col).toString().startsWith("[v]") ) { 
	                		TextView txt = new TextView(getNikitaComponent().getActivity());
							txt.setText(stext);
							view=txt;
	                	}  else if (headers.getData(col).toString().contains("[") && col>=2 ) {   
	                        Nset na = Nset.readsplitString("up,down,edit,add,delete,remove,copy,run,play,view,paste,new,find,search,clear",",");                        
	                        LinearLayout h1 = new LinearLayout(getNikitaComponent().getActivity());
	                        h1.setOrientation(LinearLayout.HORIZONTAL);
	                        for (int i = 0; i < na.getArraySize(); i++) {
	                            ImageView 
	                            img =new ImageView(getNikitaComponent().getActivity());
	                            img.setOnClickListener(new View.OnClickListener() {	                            	
									public void onClick(final View v) {
										prepareAction( (String)v.getTag() ) ;										
										getNikitaComponent().runOnActionThread(new Runnable() {
											public void run() {
												runRouteAction( (String)v.getTag() );					
											}
										});	
									}
								});
	                            img.setTag( "item-"+row+"-"+na.getData(i).toString() );
	                            setImageResource(img, na.getData(i).toString());
	                             
	                            int p = convertPixel("2dp");
	                            img.setPadding(p,p,p,p);
	                            if (headers.getData(col).toString().equals("")) {
	                                h1.addView(img,  new MarginLayoutParams(convertPixel("24dp") ,convertPixel("24dp")));
	                            }else if (headers.getData(col).toString().contains("["+na.getData(i).toString()+"]")) {
	                                h1.addView(img,  new MarginLayoutParams(convertPixel("24dp"),convertPixel("24dp")));
	                            }
	                            h1.setOnTouchListener(new View.OnTouchListener() {
									public boolean onTouch(View v, MotionEvent event) {
										// stop hire
										return true;
									}
								});
	                            //MarginLayoutParams mParams = (MarginLayoutParams)img.getLayoutParams();
	        					//mParams.setMargins(0, 0, convertPixel("3dp"), 0);
	                            //img.setPadding(0, 0, convertPixel("3dp"), 0);
	                        }  
	                        view=h1; 	
	                    }else{
	                    	TextView txt = new TextView(getNikitaComponent().getActivity());
							txt.setText(stext);
							view=txt;
	                    	 
	                    }
	                	
	                	
	                }
					
					if (view.getParent() instanceof ViewGroup) {
						( (ViewGroup)view.getParent()).removeView(view);
					}else if (view.getParent() instanceof FrameLayout) {
						( (FrameLayout)view.getParent()).removeView(view);
					}else if (view.getParent() instanceof LinearLayout) {
						( (LinearLayout)view.getParent()).removeView(view);	
					}
					
					FrameLayout frameLayout = createFrameLayout();		 
					frameLayout.addView(view, new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT,MarginLayoutParams.WRAP_CONTENT));		
					MarginLayoutParams mParams = (MarginLayoutParams)view.getLayoutParams();
					mParams.setMargins(convertPixel("6dp"), convertPixel("10dp"), convertPixel("6dp"), convertPixel("10dp"));
					 
					frameLayout.setVisibility(headers.getData(col).toString().equals("")?View.GONE:View.VISIBLE);
					tableRow.addView(frameLayout);		
					frameLayout.setLayoutParams(new TableRow.LayoutParams());
					//((TableRow.LayoutParams)frameLayout.getLayoutParams()).gravity=Gravity.CENTER;
					
					width =  width+frameLayout.getMeasuredWidth();
					Log.i("GRID-", frameLayout.getMeasuredWidth()+"");
							
				}
				
				//width =  width+tableRow.getMeasuredWidth();
				
	            if (width>maxwidth) {
	            	maxwidth=width;
				} 
				 
				tableRow.setTag(""+row);
				if (row%2==0) {					
					tableRow.setBackgroundColor(0xffd4d4d4);
				}else{
					tableRow.setBackgroundColor(0xffe8ecee);
				}
				//tableRow.setBackgroundResource(R.drawable.cellshape);
				tableGrid.addView(tableRow);
			
				
				tableRow.setOnTouchListener(new View.OnTouchListener() {
					public boolean onTouch(View v, MotionEvent event) {
						if (event.getAction() == MotionEvent.ACTION_DOWN) {
							v.setBackgroundColor(0xffb8d1f3);
						} else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
							if ( Utility.getInt((String)v.getTag())%2==0 ) {
								v.setBackgroundColor(0xffd4d4d4);
							}else{								 
								v.setBackgroundColor(0xffe8ecee);
							}
						}
						return false;
					}
				});
				tableRow.setOnClickListener(new View.OnClickListener() {
					public void onClick(final View v) {  
						prepareAction("item-"+ (Utility.getInt((String)v.getTag())));
						getNikitaComponent().runOnActionThread(new Runnable() {
							public void run() {
								runRouteAction("item-"+ (Utility.getInt((String)v.getTag())) );						
							}
						});	
					}
				});
			}
			
			tableGrid.post(new Runnable() {
			    @Override
			    public void run() {
			        int maxwidth = tableGrid.getMeasuredWidth();
			        Log.i("GRID", maxwidth+"");
			        if (maxwidth>getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels) {
						tableGrid.getLayoutParams().width=maxwidth;	
					}else{
						tableGrid.getLayoutParams().width=getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels-convertPixel("25px");
					}
			    }
			});
			
			
			tableGrid.setMinimumWidth(getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels); 
			//maxwidth=tableGrid.getMeasuredWidth();
			if (maxwidth>getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels) {
				tableGrid.getLayoutParams().width=maxwidth;	
			}else{
				//tableGrid.getLayoutParams().width=getNikitaComponent().getActivity().getResources().getDisplayMetrics().widthPixels-convertPixel("25px");
			}
			if (headers.getArraySize()>=1) {
			   tableGrid.setStretchAllColumns(true);
				//tableGrid.setShrinkAllColumns(true);
			}
			
		}	
	}
	
	private int minwidth = -1;
	private TableRow createTableRow(){
		TableRow tableRow = new TableRow(getNikitaComponent().getActivity()){
			protected void onDraw(Canvas canvas) {
		        super.onDraw(canvas);	
		        
		        Paint strokePaint = new Paint();
		        strokePaint.setARGB(255, 127, 127, 127);
		        strokePaint.setStyle(Paint.Style.STROKE);
		        strokePaint.setStrokeWidth(2);  
		        Rect r = canvas.getClipBounds() ;
		        Rect outline = new Rect(1, 1, r.right-1, r.bottom-1) ;
		        //canvas.drawRect(outline, strokePaint) ;		      
		        int w = 0;
		        for (int i = 0; i < getChildCount(); i++) {
		        	int iw = getChildAt(i).getMeasuredWidth();
		        	
		        	outline = new Rect(w, 0, w+iw,  r.bottom) ;
		        	w=w+iw;
			        canvas.drawRect(outline, strokePaint) ;		  
				}		        
		         
		    }					
			
		};
		tableRow.setBackgroundDrawable(new BitmapDrawable(){});
		return tableRow;
	}
	private FrameLayout createFrameLayout(){
		FrameLayout frameLayout = new FrameLayout(getNikitaComponent().getActivity()){
			protected void onDraw(Canvas canvas) {
		        super.onDraw(canvas);	
		        /*
		        Paint strokePaint = new Paint();
		        strokePaint.setARGB(255, 127, 127, 127);
		        strokePaint.setStyle(Paint.Style.STROKE);
		        strokePaint.setStrokeWidth(1);  
		        Rect r = canvas.getClipBounds() ;

		        Rect outline = new Rect(1, 1, r.right-1, r.bottom-1) ;
		        canvas.drawRect(outline, strokePaint) ;	
		        */	      
		    }						
		};
		//frameLayout.setBackgroundDrawable(new BitmapDrawable(){});
		return frameLayout;
	}
	
	
	public void getPagingString(int page, int rowperpage, int rows) {
		boolean isTablet = Utility.isTablet(getNikitaComponent().getActivity());
		 
		int maxbutton = (isTablet?6:4) /2;
	        int pages = (rows/rowperpage) + (rows%rowperpage<=0?0:1);
	        
	        view.findViewById(R.id.frmNext).setVisibility(isTablet?View.VISIBLE:View.GONE);
        	view.findViewById(R.id.frmPrev).setVisibility(isTablet?View.VISIBLE:View.GONE);
	        
	         
	        if (rows > 1) {
	            view.findViewById(R.id.imgFirst).setEnabled(page!=1);
	            view.findViewById(R.id.imgPrev).setEnabled(page!=1);
	            if (view.findViewById(R.id.imgFirst).isEnabled()) {
	            	((ImageView)view.findViewById(R.id.imgFirst)).setImageResource(R.drawable.page_active);
	            	((ImageView)view.findViewById(R.id.imgPrev)).setImageResource(R.drawable.page_active);
	            	((TextView)view.findViewById(R.id.txtFirst)).setTextColor(0xffff0000);
	            	((TextView)view.findViewById(R.id.txtPrev)).setTextColor(0xffff0000);
				}else{
					((ImageView)view.findViewById(R.id.imgFirst)).setImageResource(R.drawable.page_disable);
	            	((ImageView)view.findViewById(R.id.imgPrev)).setImageResource(R.drawable.page_disable);
	            	((TextView)view.findViewById(R.id.txtFirst)).setTextColor(0xff7f7f7f);
	            	((TextView)view.findViewById(R.id.txtPrev)).setTextColor(0xff7f7f7f);
				}
	            
	            
	            int low = (page-maxbutton)<=0?maxbutton-page:0;
	            int hig = (page+maxbutton)>=pages?(page+maxbutton)-pages:0;            
	            
	            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.lnrPages );
	            linearLayout.removeAllViews();
	            for (int i = Math.max(0, page-maxbutton-hig); i < Math.min(pages, page+maxbutton+low); i++) {
	               View v = Utility.getInflater(view.getContext(), R.layout.compgridpagingitem) ;
                   ((TextView) v.findViewById(R.id.txtPage)).setText((i+1)+"");
                   v.findViewById(R.id.imgPage).setTag((i+1)+"");
                   v.findViewById(R.id.imgPage).setOnClickListener(new View.OnClickListener() {
	           			public void onClick(final View v) {  
	           				prepareAction("page-"+Utility.getInt((String)v.getTag())) ;
	           				getNikitaComponent().runOnActionThread(new Runnable() {
	           					public void run() {
	           						runRouteAction("page-"+Utility.getInt((String)v.getTag()));							
	           					}	           					
	           				});	
	           			} 
	           		});
                    if (page==(i+1)) {
                    	((ImageView)v.findViewById(R.id.imgPage)).setImageResource(R.drawable.page_selected);
					}
                    linearLayout.addView(v);
	            } 
	            
	            view.findViewById(R.id.imgNext).setEnabled(page!=pages);
	            view.findViewById(R.id.imgLast).setEnabled(page!=pages);
	            
	            if (maxrows==0) {
	            	((TextView)view.findViewById(R.id.txtShowEntry)).setText(" no entries");
	            }else{
	            	((TextView)view.findViewById(R.id.txtShowEntry)).setText(" "+Utility.formatCurrency((getCurrentPage()-1)*getShowPerPage()+1) +" to "+ Utility.formatCurrency(((getCurrentPage()-1)*getShowPerPage()) +getData().getArraySize())+" of "+Utility.formatCurrency(maxrows)+" entries");
		            
	            }
	            if (rows<=1 || getCurrentPage()==-1) {
	            	
	            	view.findViewById(R.id.incPaging).setVisibility(View.GONE);
	            	view.findViewById(R.id.lnrGridShow).setVisibility(View.GONE);
				}else{
					view.findViewById(R.id.incPaging).setVisibility(View.VISIBLE);
	            	view.findViewById(R.id.lnrGridShow).setVisibility(View.VISIBLE);
				}
	             view.findViewById(R.id.linegrid).setVisibility(View.VISIBLE);
	            
	             
	            
	            if (view.findViewById(R.id.imgLast).isEnabled()) {
	            	((ImageView)view.findViewById(R.id.imgLast)).setImageResource(R.drawable.page_active);
	            	((ImageView)view.findViewById(R.id.imgNext)).setImageResource(R.drawable.page_active);
	            	((TextView)view.findViewById(R.id.txtLast)).setTextColor(0xffff0000);
	            	((TextView)view.findViewById(R.id.txtNext)).setTextColor(0xffff0000);
				}else{
					((ImageView)view.findViewById(R.id.imgLast)).setImageResource(R.drawable.page_disable);
	            	((ImageView)view.findViewById(R.id.imgNext)).setImageResource(R.drawable.page_disable);
	            	((TextView)view.findViewById(R.id.txtLast)).setTextColor(0xff7f7f7f);
	            	((TextView)view.findViewById(R.id.txtNext)).setTextColor(0xff7f7f7f);
				}
	        }
	        
	      
	}
	public void setOnFilterClickListener(int curpage, int rowperpage){
        setCurrentPage(curpage);//first
        rowperPage=rowperpage;//def
    }   
	public boolean prepareItemClick(String action){
        if ( action.startsWith("item-")) {
            String row = action.substring(5);               
            //restoreData(Utility.getInt(row));
            return true;                         
        }
        return false;
    }
	public void prepareAction(String action){
		getNikitaComponent().updateParameter("action", action);
		prepareFilter(action);
	}
	public boolean prepareFilter(String action){
        if (action.startsWith("page-first")) {
            setCurrentPage(1);
            return true;
        }else if (action.startsWith("page-back")) {
            setCurrentPage(getCurrentPage()-1);
            return true;
         }else if (action.startsWith("page-next")) { 
            setCurrentPage(getCurrentPage()+1);
            return true;
        }else if (action.startsWith("page-last")) { 
            setCurrentPage(999999999);
            return true;
        }else if (action.startsWith("page-view")) { 
            return true;
        }else if (action.startsWith("page-")) {     
            String page = action.substring(5);
            setCurrentPage(Utility.getInt(page));
            return true;
        }else if (action.startsWith("page-sort")) {     
            return true;
        }
        return false;
    }
  	private int currPage    =-1;
    private int rowperPage  =-1;
    private int maxrows     =0;
    private int maxwidth     =0;
    private int sortcol     =-1;
    public int getSortCol(){
        return sortcol;//?
    }
    
    public int getCurrentPage(){
        return currPage;
    }
    public int getShowPerPage(){
        return rowperPage;
    }
    public void setCurrentPage(int page){
        currPage=page;
    }
	public static final int TYPE_CHECKBOK = 1;
	private int row =-1;
	 
	
	public void setRowCounter(boolean b){
	    showrowcount=b;
	}	
	public void showRowIndex(boolean b){
	    showrowindex=b;
	}
	public void showRowIndex(boolean b, boolean check){
	    showrowindex=b;
	    showcheckbox=check;
	}
	public void showActionCols(boolean b){
	    showactioncols=b;
	}
	private boolean showrowcount = false;
	boolean showrowindex=false;
	boolean showactioncols=false;
	boolean showcheckbox=false;
	 
	 
	public int getSeletedRow(){
	    return row;
	}

	public void setColHide(int col, boolean b) {
		
	}
	public void setColType(int col, int b) {
		
	}
	
	public void setDataHeader(Nset data) {
		headers=data;
	}
	Nset headers = Nset.newArray();
	public void setData(Nikitaset data) {	
        maxrows=data.getDataAllVector().size();
 
        Nset info = new Nset( data.getInfo( ));
        if ( info.getData("nfid").toString().equals("Nset") && info.getData("mode").toString().equals("paging") ) {
        	maxrows=info.getData("rows").toInteger();
            setCurrentPage(info.getData("page").toInteger());
        }
        
        if (headers.getArraySize()<=0) {
            headers=new Nset(data.getDataAllHeader());
        }
        
        super.setData(new Nset(data.getDataAllVector()));  
        
        fillGridOnNewThread();
    }
    
	public String getText() {
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	public interface AdapterListener {
	    public Component getViewItem(int row, int col, Component parent, Nset data);
	}
	protected AdapterListener adapterListener;
	public void setAdapterListener(AdapterListener adapterListener){
		this.adapterListener=adapterListener;
    }  
	
	 
}
