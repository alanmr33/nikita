package com.nikita.mobile.finalphase.generator.action;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.NCore;
import com.rkrzmail.nikita.data.Nset;

public class ShowAlertAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, Nset currdata) {
		final String message = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("message").toString());
		comp.runOnUI(new Runnable() {
			public void run() {
				comp.getNikitaComponent().showDialog("", message, "", "", "", "" , Nset.newArray());
			}
		});
		return true;
	}
    
}
