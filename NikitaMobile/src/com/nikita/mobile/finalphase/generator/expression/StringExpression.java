package com.nikita.mobile.finalphase.generator.expression;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IExpression;
import com.rkrzmail.nikita.data.Nset;

public class StringExpression implements IExpression {

	public boolean OnExpression(Component comp, Nset currdata) {    
		String code = currdata.getData("code").toString().trim();
        String param1 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
        if (code.equals("regex")||code.equals("matches")) {
             String param2 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());  
            return param1.matches(param2);
        }else if (code.equals("containchars")) {
            String param2 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());  
            for (int i = 0; i < param1.length(); i++) {
                if (param2.contains(param1.substring(i,i+1))) {
                   return true;
                }                
            }
        }                
        return false;
    }
  
}
