package com.nikita.mobile.finalphase.utility;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

public class GPSTracker extends Service implements LocationListener{
	
	private Context context;
	private boolean isGPSEnable =  false;
	private boolean isOnline = false;
	private boolean canGetLocation = false;
	Location location = null;
	private double latitude;
	private double longitude;
	
	private long MIN_DISTANCE_CHANGE_FOR_UPDATES = 300; // radius of covered 10 meter
	private long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute 
	
	private LocationManager locationManager;
	
	public GPSTracker(Context context){
		this.context = context;
		getLocation();
	}
	
	public Location getLocation(){
		try {
			locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
			
			//GPS Status
			isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			
			//Networt Status
			isOnline = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			
			//jika GPS device aktif dan kondisi online
			if (isGPSEnable && isOnline) {
				
				//didapatkan lokasi
				this.canGetLocation =  true;
				
				if (isOnline) {
					
					//location manager mendapatkan lokasi dalam jarak/radius dan waktu tertentu
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
					
					Log.d("Online", "Online");
					
					//jika locationManager tidak null / dapat semua lokasi
					if (locationManager != null) {
						
						//mengisi lokasi kedalam location dari locationmanager
						location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						
						if (location != null) {
							
							//mendapatkan lattitude dan longitude
							latitude = location.getLatitude();
							longitude = location.getLongitude();
						}
					}
					
				}
				
				if (isGPSEnable) {
					if (location == null) {
						locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
						
						if (location != null) {							
							//location manager mendapatkan lokasi berdasarkan GPS dalam jarak/radius dan waktu tertentu
							locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
							Log.d("GPS", "Ok");
							
							//jika locationManager tidak null / dapat semua lokasi
							if (locationManager != null) {
								
								//mengisi lokasi berdasarkan GPS kedalam location dari locationmanager
								location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
								
								if (location != null) {
									
									//mendapatkan lattitude dan longitude
									latitude = location.getLatitude();
									longitude = location.getLongitude();
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
		}
		return location;
	}

	public void stopUsingGPS(){
		if (locationManager != null) {
			locationManager.removeUpdates(GPSTracker.this);
		}
	}
	
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
	}

	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}
		return longitude;
	}
	
	public boolean canGetLocation(){		
		return this.canGetLocation;
	}
	
	public  boolean isGpsEnable () {
		return this.isGPSEnable;
	}
	
	public void showSettingGPS(){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("GPS Setting");
		alert.setMessage("GPS is not enable. Do you want go to Setting menu ?");
		
		alert.setPositiveButton("Setting", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				startActivity(intent);
			}
		});
		
		alert.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();				
			}
		});
		
		alert.show();
	}
	
	@Override
	public void onLocationChanged(Location location) {		
	}

	@Override
	public void onProviderDisabled(String provider) {		
	}

	@Override
	public void onProviderEnabled(String provider) {		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {		
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
