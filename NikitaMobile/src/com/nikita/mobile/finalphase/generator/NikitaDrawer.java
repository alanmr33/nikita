package com.nikita.mobile.finalphase.generator;

import java.util.Vector;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.action.BluetoothPrintAction;
import com.nikita.mobile.finalphase.generator.action.SystemAction;
import com.nikita.mobile.finalphase.generator.ui.FileUploadUI;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class NikitaDrawer extends NikitaActivity{
	private ActionBarDrawerToggle actionBarDrawerToggle;
   private DrawerLayout drawerLayout;
	
	public void setContentView(View view) {
		super.setContentView(R.layout.nformdrawer);//must super
		FrameLayout parent = (FrameLayout) findViewById(R.id.drawer_content_frame);
		
		//View view = Utility.getInflater(this, resource);
		parent.addView(view, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.clear, R.string.icon, R.string.icon) {
			public void onDrawerClosed(View drawerView) {
				NikitaDrawer.this.invalidateOptionsMenu();
			}
			public void onDrawerOpened(View drawerView) {
				NikitaDrawer.this.invalidateOptionsMenu();
			}
		};
		drawerLayout.setDrawerListener(actionBarDrawerToggle);
		
		
	}

	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (actionBarDrawerToggle!=null) {
			actionBarDrawerToggle.syncState();
		}
	}
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (actionBarDrawerToggle!=null) {
			actionBarDrawerToggle.onConfigurationChanged(newConfig);
		}
	}
	public void openListDrawer() {
		if (drawerLayout!=null) {
			drawerLayout.openDrawer(Gravity.LEFT);
		}
	}
	 public void setListDrawer( int resource,  View vlist) {	
		 View v = findViewById(R.id.drawer_list);	;
		 if (v instanceof ListView) {
			 ListView lstMenu = (ListView) v;	 Vector data = new Vector(); data.addElement("");
			 lstMenu.setAdapter(new ArrayAdapter(this, 0 , data){
				View vlist;
				public ArrayAdapter get (View vlist){
					this.vlist= vlist;;
					return this;
				}
				public View getView(int position, View convertView, ViewGroup parent) {
					if (vlist.getParent() instanceof ViewGroup) {
						((ViewGroup)vlist.getParent()).removeView(vlist);
					}
					FrameLayout layout = new FrameLayout(vlist.getContext());
					layout.addView(this.vlist);
					
					return layout;
				}
			 }.get(vlist));
		 }
	 }
}
