package com.nikita.mobile.finalphase.generator.ui.layout;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class VerticalLayout  extends ComponentGroup{

	public VerticalLayout(NikitaControler form) {
		super(form);
	}


	private LinearLayout view;
 
	public View onCreate(NikitaControler form) {
		view=new LinearLayout(form.getActivity());
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		((LinearLayout)view).setOrientation(LinearLayout.VERTICAL);
			    
		 
		LinearLayout container = view;
		if (getStyle()!=null && getStyle().getViewStyle().contains("n-scroll:true")) {
			ScrollView hScrollView = new ScrollView(form.getActivity());
			hScrollView.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			
			container =new LinearLayout(form.getActivity());
			container.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			((LinearLayout)container).setOrientation(LinearLayout.VERTICAL);
			
			hScrollView.addView(container, Utility.getMarginLayoutParams(hScrollView) );
			view.addView(hScrollView, Utility.getMarginLayoutParams(hScrollView) );
		}
		
		for (int i = 0; i < getComponentCount(); i++) {
			View child = getComponent(i).startCreateUI(form);			
			container.addView(child, Utility.getMarginLayoutParams(child) );			
		}
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
	}
}
