package com.nikita.mobile.finalphase.generator.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class DateFormatAction implements IAction{

	public boolean OnAction(Component comp, Nset data) {
        data =data.getData("args");        
        String param = comp.getNikitaComponent().getVirtualString(data.get("param").toString());
        String format =  comp.getNikitaComponent().getVirtualString(data.get("format").toString());
        
        String result = FormatDate(Utility.getInt(format), param, "");        
        comp.getNikitaComponent().setVirtual(data.get("result").toString(), result);
        return true;
    }
    
    public static String FormatDate(int code, String date, String time) {
            //- to yyyy-mm-dd hh:nn:ss
            Nset n = Nset.readJSON("['yyyy-mm-dd','dd/mm/yyyy','dd/mm/yy','dd/mmm/yyyy','dd/mmm/yy','mm/dd/yyyy','mm/dd/yy','mmm/dd/yyyy','mmm/dd/yy','']", true);
        
            String sDate = "" ;
            SimpleDateFormat dateFormatOfStringInDB = new SimpleDateFormat(code<=0?n.getData(code*-1).toString():"yyyy-mm-dd");
            try {
                Date d1 = dateFormatOfStringInDB.parse(date);
                SimpleDateFormat dateFormatYouWant = new SimpleDateFormat(code<=0?"yyyy-mm-dd":n.getData(code).toString());
                sDate = dateFormatYouWant.format(d1);
            } catch (Exception e) {  }    
             
          
            time=(time!=null)?time:"";            
            return sDate+(time.equals("")?"":" ")+time;
	}
 

    
}
