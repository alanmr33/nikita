package com.nikita.mobile.finalphase.generator.ui;

import java.io.File;
import java.io.InputStream;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NFormActivity;
import com.nikita.mobile.finalphase.generator.NikitaActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;


public class FileUploadUI extends Component{
	public static int RESULT_CAMERA_CAPTURE = 1321341135;
	public static int RESULT_FILE_BROWSER   = 132144445;
	
	
	static final int STARTDATE = 1;
	
	@Override
	public boolean isFileComponent() {
		return true;
	}
	public void saveFileComponent() {
		if (getText().endsWith(".tmp")) {
			String fname = AppNikita.getInstance().getDeviceId() + "." + getId() + "." + Long.toHexString(System.currentTimeMillis()) +"."+Long.toHexString(System.nanoTime())+".cam";
			Utility.copyFile( Utility.getDefaultPath(getText()), Utility.getDefaultPath(fname) );
			setText(fname);
		}
	}
	 
	
	public FileUploadUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	 
	private int requestcode   ; 
	@Override
	public View onCreate(NikitaControler form) {
		requestcode=(requestcode!=0?requestcode:hashCode());
		
		view=Utility.getInflater(form.getActivity(), R.layout.compfile);
		view.findViewById(R.id.imgText).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (FileUploadUI.this.getStyle()==null) {
					FileUploadUI.this.setStyle(new Style());
				}
				String mimeaccept = FileUploadUI.this.getStyle().getInternalObject().getData("attr").getData("n-accept").toString();
				String action = FileUploadUI.this.getStyle().getInternalObject().getData("attr").getData("n-action").toString();
				
				if (action.toLowerCase().trim().equals("camera")) {
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					File photo = new File(Environment.getExternalStorageDirectory(), "image");
					intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
					getNikitaComponent().getActivity().startActivityForResult(intent, requestcode);//RESULT_CAMERA_CAPTURE
				}else{
					Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					if (mimeaccept.equals("")) {
						intent.setType("file/*");//intent.setType("file/*");
					}else{
						intent.setType(mimeaccept);
					}		
					try {
						getNikitaComponent().getActivity().startActivityForResult(intent, requestcode+1);//RESULT_FILE_BROWSER
					} catch(ActivityNotFoundException exp){
		                Toast.makeText(getNikitaComponent().getActivity(), "No File Manager)",5000).show();
		            }					
				}				
			}
		});
		
		if (form.getActivity() instanceof NikitaActivity) {
			((NikitaActivity) form.getActivity()).addOnActivityResultListener(new NikitaActivity.OnActivityResultListener() {
				public void onActivityResult(int requestCode, int resultCode, Intent data) {
					if (requestCode==requestcode && resultCode==Activity.RESULT_OK) {
						File photo = new File(Environment.getExternalStorageDirectory(), "image");
						String fname = getId() +".tmp";
						Utility.copyFile( photo.getAbsolutePath(), Utility.getDefaultPath(fname) );
						setText(fname);	
						 
						viewImage( "Camera Capture");
						 
					}	else if (requestCode==(requestcode+1) && resultCode==Activity.RESULT_OK) {
						 
						Uri uri = data.getData();
				        String   file = getFilename(getNikitaComponent().getActivity(), data);
				         
						 
						String fname = getId() +".tmp";
						//Utility.copyFile( file, Utility.getDefaultPath(fname) );
						setText(file);	 setText(fname);	
						 
						viewImage( file);
						
						 
					}	
					
				}
			});
		}
		
		
		
		
		setImage(); 
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	private String getFilename(Activity form, Intent intent)  {
		String fileName = "noname";
		try {			
		    Context context=form.getApplicationContext();
		    String scheme = intent.getData().getScheme();
		    if (scheme.contains("file")) {
		        fileName = intent.getData().getLastPathSegment();
		        String FilePath = intent.getData().getPath();
		        String FileName = intent.getData().getLastPathSegment();
		        int lastPos = FilePath.length() - FileName.length();
		        String Folder = FilePath.substring(0, lastPos);
		        fileName= FilePath;
		    }else if (scheme.equals("content")) {
		        String[] proj = { MediaStore.Video.Media.TITLE };
		        Uri contentUri = null;
		        Cursor cursor = context.getContentResolver().query(intent.getData(), proj, null, null, null);
		        if (cursor != null && cursor.getCount() != 0) {
		            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE);
		            cursor.moveToFirst();
		            fileName = cursor.getString(columnIndex);
		        }
		    }
		    return fileName!=null?fileName:"1";
		} catch (Exception e) { }
		return "2";
	}
	public View getView() {		 
		return view;
	}
	
	public void setLabel(String label) {
		super.setLabel(label);		
		refreshView();
	}
	@Override
	public void setText(String text) {
		super.setText(text);		
		refreshView();
	}

	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
	
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshStyleOnUI();
				}
			});
		} 
	}
	
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		View v  = view.findViewById(R.id.editText1);
		if (v instanceof EditText) {
			((EditText)v).setText(getText());
		}
		
		v  = view.findViewById(R.id.txtText);
		if (v instanceof Button) {
			//((Button)v).setText(getText());
		}
		
		v  = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
		 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}
	public String getText() {
		if (view!=null) {
			//View v = view.findViewById(R.id.txtText);
			//if (v instanceof EditText) {
			//	return ((EditText)v).getText().toString();
			//}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			super.setText(getText()); 
		}			
		//view=null;//reset
	}
	
	private void setImage(){
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			//Utility.OpenImage(img, Utility.getDefaultTempPath(getText()), R.drawable.ic_launcher);
			viewImage(  Utility.getDefaultTempPath(getText()));
			view.findViewById(R.id.imgClear).setVisibility(View.VISIBLE);
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			//Utility.OpenImage(img, Utility.getDefaultPath(getText()), R.drawable.ic_launcher);
			viewImage( Utility.getDefaultPath(getText()));
			view.findViewById(R.id.imgClear).setVisibility(View.VISIBLE);
		}	
	}
	
	private String getImagePath(){
		if (getText().trim().equals("")) {
		}else  if (new File(Utility.getDefaultTempPath(getText())).exists()) {
			return Utility.getDefaultTempPath(getText());
		}else if (new File(Utility.getDefaultPath(getText())).exists()) {
			return Utility.getDefaultPath(getText());
		}	
		return "";
	}
	
	public void viewImage( String absolutePath){
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText) {
			if (absolutePath.contains("/")) {
				absolutePath=absolutePath.substring(absolutePath.lastIndexOf("/")+1);
			}
			
			((EditText)v).setText(absolutePath);
		}
	}
	
}
