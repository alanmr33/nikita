package com.nikita.mobile.finalphase.activity.launcher;

import com.nikita.mobile.finalphase.activity.DevActivity;
import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.activity.SettingActivity;
import com.nikita.mobile.finalphase.activity.SplashScreen;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.service.NikitaService;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

public class MandiriLauncher extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.launcherwom);
		
		
		Utility.setAppContext(getApplicationContext());
		AppNikita.getInstance().setVirtual("@+SETTING+LAUNCHERICONID", R.drawable.mandiri_launcher2+"");
		Generator.initializeNikita(getApplicationContext());
		
		if (findViewById(R.id.progressBar1)!=null) {
			findViewById(R.id.progressBar1).setVisibility(View.GONE);
		}
 
		if (AppNikita.PRODUCTION && Utility.getSetting(getApplicationContext(), "INIT" , "").trim().equals("")) {
			test();
			Toast.makeText(this, "Connection to Server", Toast.LENGTH_SHORT).show();			
		}else if (Generator.getNikitaParameters().getData("INIT-SPLASH").toInteger()>=1) {
			new Handler().postDelayed(new Runnable() {
				public void run() {
					Intent intent = new Intent(MandiriLauncher.this, LoginActivity.class);	
					intent.putExtra("iconid", R.drawable.mandiri_launcher2);
					startActivity(intent);
					finish();
				}
			}, Generator.getNikitaParameters().getData("INIT-SPLASH").toInteger() * 1000);			
		}else {
			Intent intent = new Intent(MandiriLauncher.this, LoginActivity.class);	
			intent.putExtra("iconid", R.drawable.mandiri_launcher2);
			startActivity(intent);
			finish();
		}
		
	}
	public void test(){
		AppNikita.getInstance().getActionHandler().post(new Runnable() {
			public void run() {
				if (findViewById(R.id.progressBar1)!=null) {
					findViewById(R.id.progressBar1).setVisibility(View.VISIBLE);
				}
				Generator.synchronizeNikita(getApplicationContext());//2016-01-27 Sync Reload Setting
				if (MandiriLauncher.this!=null) {
					MandiriLauncher.this.runOnUiThread(new Runnable() {
						public void run() {
							if (Utility.getSetting(getApplicationContext(), "INIT" , "").trim().equals("")) {
								if (findViewById(R.id.progressBar1)!=null) {
									findViewById(R.id.progressBar1).setVisibility(View.GONE);
								}
								Toast.makeText(MandiriLauncher.this, "Error Connection", Toast.LENGTH_LONG).show();	
							}	else{
								Intent intent = new Intent(MandiriLauncher.this, LoginActivity.class);	
								intent.putExtra("iconid", R.drawable.mandiri_launcher2);
								startActivity(intent);
								finish();
							}
						}
					});
				}
			}
		});
	}

}
