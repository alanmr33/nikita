/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nikita.mobile.finalphase;
 
 

import com.google.android.gcm.GCMRegistrar;
import com.nikita.mobile.finalphase.utility.Utility;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtilities {
	  static String SERVER_URL = "";
	 

	    /**
	     * Tag used on log messages.
	     */
	    static final String TAG = "GCMDemo";

	    /**
	     * Intent used to display a message in the screen.
	     */
	    public static final String DISPLAY_MESSAGE_ACTION =
	            "DISPLAY_MESSAGE";

	    /**
	     * Intent's extra that contains the message to be displayed.
	     */
	    public static final String EXTRA_MESSAGE = "message";
    private static final int MAX_ATTEMPTS = 1;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();

    /**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    
    public static boolean register(final Context context, final String regId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
        SERVER_URL = Utility.getSetting(context.getApplicationContext(), "BASE-URL", "");
        
        
        String serverUrl = SERVER_URL + "mobile.pushmessage";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        params.put("mode", "register");
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        Log.i(TAG, "1");
        
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
            	Log.i(TAG, "2");
                displayMessage(context, "server_registering");
                Log.i(TAG, "2a"+serverUrl);
                post(serverUrl, params);
                Log.i(TAG, "3");
                GCMRegistrar.setRegisteredOnServer(context, true);
                Log.i(TAG, "4");
                String message = "server_registered";
                displayMessage(context, message);
                return true;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        String message = "server_register_error";
        displayMessage(context, message);
        return false;
    }

    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        SERVER_URL = Utility.getSetting(context.getApplicationContext(), "BASE-URL", "");
        
        String serverUrl = SERVER_URL + "mobile.pushmessage";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        params.put("mode", "unregister");
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
            String message = "server_unregistered";
            displayMessage(context, message);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = "server_unregister_error";
            displayMessage(context, message);
        }
    }

    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(String endpoint, Map<String, String> params) throws IOException {
        URL url;
        Log.i(TAG, "post-1");
        try {
            url = new URL(endpoint);
            Log.i(TAG, "post-2");
        } catch (MalformedURLException e) {
        	Log.i(TAG, "post-3");
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        Log.i(TAG, "post-4");
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
      }
    
    
    static void displayMessage(Context context, String message) {
        //Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        //intent.putExtra(EXTRA_MESSAGE, message);
        //context.sendBroadcast(intent);
    }
}
