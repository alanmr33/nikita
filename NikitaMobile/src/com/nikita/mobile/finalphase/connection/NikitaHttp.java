package com.nikita.mobile.finalphase.connection;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import com.nikita.mobile.finalphase.ssl.EasySSLSocketFactory;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nset;

public class NikitaHttp {
	public static NikitaMultipart getHttp(NikitaHttpParam param){
		return getHttp( param.toString() );
	}
	public static NikitaMultipart getHttp(String url, Hashtable<String, String> arg){
		return getHttp( NikitaHttpParam.create(arg).add(url)  );
	}
	public static NikitaMultipart postHttp(NikitaHttpParam param){
		return postHttp(param.getURL(), param.get());
	}
	
	public static NikitaMultipart postHttp(String url, Hashtable<String, String> arg){
		return postHttp(url, arg);
	}
	public static NikitaMultipart sendpostImage(String url, Hashtable<String, String> arg, String img){
		try {			 
			String fname = img;
			while (fname.contains("/")) {
				fname=fname.substring(fname.indexOf("/")+1);				
			}
			return sendpostImage(url, arg, fname, new FileInputStream(img));
		} catch (Exception e) { }
		return new NikitaMultipart();
	}
	
	public static NikitaMultipart sendpostImage(String url, Hashtable<String, String> arg, String fname, InputStream img){
		return postHttp(url, arg, fname, img);
	}	
	
	
 
	public static NikitaMultipart getHttp(String url)  {
		try{
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);	
	
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			DefaultHttpClient httpClient = new DefaultHttpClient(cm, params);
	
			
		    HttpGet httpget = new HttpGet(url);
		    httpget.setParams(params);
		    HttpResponse response;
		    response = httpClient.execute(httpget);
		    
		    return new NikitaMultipart( response.getEntity().getContent() );
	    
		}catch (Exception e) {
	    	return new NikitaMultipart();
		}	 
	} 
	
	private static NikitaMultipart postHttp(String url, Hashtable<String, String> arg, String filename, InputStream file) {
		String strResponse = null;
		try {
			HttpClient client = new DefaultHttpClient();
			
			//13kcrazy
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			
			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			client = new DefaultHttpClient(cm, params);
			//13kcrazy
			
			HttpPost post = new HttpPost(url);	

 
			if (file!=null) {
				//multipart/form-data
				MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				Nset nset = new Nset(arg) ;
				String[] keys =  nset.getObjectKeys();
				for (int i = 0; i < keys.length; i++) {
					reqEntity.addPart(keys[i], new StringBody( nset.getData(keys[i]).toString() ) );
				}					
				reqEntity.addPart("image", new InputStreamBody(file, filename) );
				 
			 
				
				post.setEntity(reqEntity);
			
			}else{
				//application/x-www-form-urlencoded
				Nset nset = new Nset(arg) ;
				String[] keys =  nset.getObjectKeys();
				
				ArrayList<NameValuePair> postparam = new ArrayList<NameValuePair>();
				for (int i = 0; i < keys.length; i++) {
					postparam.add(new BasicNameValuePair(keys[i], nset.getData(keys[i]).toString() ));
				}	
				
				post.setEntity(new UrlEncodedFormEntity(postparam) ); 
			}
			 

			HttpResponse response = client.execute(post);
		 
			return new NikitaMultipart( response.getEntity().getContent() );
			    
		}catch (Exception e) {
	    	return new NikitaMultipart();
		}	
	}
}
