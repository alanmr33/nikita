package com.nikita.mobile.finalphase.generator.ui;

import java.util.Locale;
import java.util.Vector;

import android.graphics.Rect;
import android.text.Editable;
import android.text.Editable.Factory;
import android.text.InputFilter;
import android.text.InputFilter.AllCaps;
import android.text.InputFilter.LengthFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class TextboxUI extends Component{
	public TextboxUI(NikitaControler form) {
		super(form);
	}
	
	private View view;
	private boolean onchange =  true; 
    
 
  
	public View onCreate(NikitaControler form) {
		view=Utility.getInflater(form.getActivity(), R.layout.comptextbox);
		
		view.findViewById(R.id.imgText).setVisibility(View.GONE);		 
		view.findViewById(R.id.imgText).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				getNikitaComponent().runOnActionThread(new Runnable() {
					public void run() {
						if (bbarcode) {
							runRouteAction("barcode");
							  
						}else{
							runRouteAction("finder");	
						}												
					}
				});			
			}
		});
		
		
		((EditText)view.findViewById(R.id.txtText)).setOnEditorActionListener(new EditText.OnEditorActionListener() {
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_GO) {
		        	getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							runRouteAction("enter");							
						}
					});	 
		            return true;
		        }
		        return false;
		    }

			 
		});
		 
		((EditText)view.findViewById(R.id.txtText)).setHint(getHint());
		setText(super.getText());		
				
		if (getStyle()!=null) {
			if (getStyle().getViewStyle().contains("n-finder:true")) {
				bfinder=true;
			}
			if (getStyle().getViewStyle().contains("n-barcode:true")) {
				bbarcode=true;
			}
			if (getStyle().getViewStyle().contains("n-searchicon:true")) {
				((ImageView)view.findViewById(R.id.imgText)).setImageResource(R.drawable.search);
				view.findViewById(R.id.imgText).setVisibility(View.VISIBLE);
			}
		}
		
		
		if (bfinder||bbarcode) {
			view.findViewById(R.id.imgText).setVisibility(View.VISIBLE);
			if (bbarcode) {
				((ImageView)view.findViewById(R.id.imgText)).setImageResource(R.drawable.barcode);
			}
			if (bfinder) {
				((ImageView)view.findViewById(R.id.imgText)).setImageResource(R.drawable.find);
			}
		}
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
				
		refreshViewOnUI();
		refreshStyleOnUI();
		
		
		
			 	
		if (getStyle().getInternalStyle().containsKey("n-event-change")) {
			((EditText)view.findViewById(R.id.txtText)).addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {		}
				String old = "";
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
					old = s.toString();
				}
				final int live = 1000;
				long curr = System.currentTimeMillis();
				public void afterTextChanged(Editable s) {
					if (!old.equals(s.toString()) && onchange && ((EditText)view.findViewById(R.id.txtText)).isFocused()) {	
						curr = System.currentTimeMillis();						
						AppNikita.getInstance().getMainHandler().postDelayed(new Runnable() {
							public void run() {
								if (Math.abs(System.currentTimeMillis()-curr)>=live) {
									getNikitaComponent().runOnActionThread(new Runnable() {
										public void run() {
											runRouteAction("change");							
										}
									});	 
								}							
							}
						}, live);
					}					
				}
			});	
			  
		}
		EditText editText = ((EditText)view.findViewById(R.id.txtText));			
		setFilter(editText, getStyle().getInternalStyle().getData("n-char-ucase").toString().equals("true"), getStyle().getInternalStyle().getData("n-maxlength").toInteger(), getStyle().getInternalStyle().getData("n-char-accept").toString(), getStyle().getInternalStyle().getData("n-lock").toString().equals("true") );
		
		
		if (getStyle().getInternalStyle().containsKey("n-currency-format") && getStyle().getInternalStyle().getData("n-currency-format").toString().equalsIgnoreCase("true")) {
			((EditText)view.findViewById(R.id.txtText)).addTextChangedListener(new TextWatcher() {
				boolean busy = false;
				public void onTextChanged(CharSequence s, int start, int before, int count) {					
				}
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				}
				public void afterTextChanged(Editable s) { 	
					if (!false) {
						busy = true;
						EditText inputValue  = ((EditText)view.findViewById(R.id.txtText));
						inputValue.removeTextChangedListener(this);
											 
					    int sel = inputValue.getSelectionStart();
					    int csel = inputValue.getText().toString().length() - sel; 
					 
					    String cur = s.toString();
					   
					    cur = cur.replace(",", "");
					    cur = Utility.formatCurrency(cur);
					    /*StringBuilder stringBuilder = new StringBuilder();
					    if (cur.contains(".")) {
					    	int count = 0;
					    	int il = cur.indexOf(".");
					    	for (int i = 0; i < il; i++) {
					    		if (count>=3) {
									count = 0;
									stringBuilder.append(",");
								}
					    		count ++ ;
					    		stringBuilder.append(cur.substring((il-1)-i, (il-1)-i+1));			
							}
					    	stringBuilder.reverse(); 
					    	stringBuilder.append(cur.substring(il));
					    	stringBuilder.append(".");
					    	count = 0;
					    	for (int i = il+1; i < cur.length(); i++) {
					    		if (count>=3) {
									count = 0;
									stringBuilder.append(",");
								}
					    		count ++ ;
								stringBuilder.append(cur.substring(i, i+1));								
							}
						}else{
							int count = 0; int il = cur.length();
							for (int i = 0; i < il; i++) {								
								if (count>=3) {
									count = 0;
									stringBuilder.append(",");
								}
								count ++ ;
								stringBuilder.append(cur.substring((il-1)-i, (il-1)-i+1));								
							}
							stringBuilder.reverse();
						}
					    cur = stringBuilder.toString();*/
	 				    
					  
					    csel = cur.length() - csel; 	
	                    inputValue.setText(cur);
	                    if (csel >= inputValue.length()) {
	                    	inputValue.setSelection(inputValue.length());
	                    }else if (csel > cur.length()) {
	                    	inputValue.setSelection(cur.length());
						}else{
							inputValue.setSelection(csel, csel);
						}
	                    inputValue.addTextChangedListener(this);
					}
					busy = false;					
				}
			});	
		}
 
		return view;
	}
		
	
	private void setPasswordMask(EditText editText, boolean password){
		if (password) {
			/*
			editText.setTransformationMethod(new TransformationMethod() {
				public void onFocusChanged(View view, CharSequence sourceText,	boolean focused, int direction, Rect previouslyFocusedRect) { }
				public CharSequence getTransformation(CharSequence source, View view) {
					return "*********************************************************";
				}
			}); 
			*/
			editText.setTransformationMethod(PasswordTransformationMethod.getInstance()); 			
		}else{
			editText.setTransformationMethod(null); 
		}
	}
	private void setFilter(EditText editText,  boolean ucase, int length, String accept, boolean lock){
		InputFilter[] filters = new InputFilter[4];
		if (ucase) {
			filters[0]= new InputFilter.AllCaps();
		}else{
			filters[0]= new InputFilterOriginal();
		}
		if (length>=1) {
			filters[1]= new InputFilter.LengthFilter(length);
		}else{ 
			filters[1]= new InputFilterOriginal();
		}
		if (accept.trim().length()>=1) {
			filters[2]= new InputFilterAccept(accept.trim());
		}else{
			filters[2]= new InputFilterOriginal();
		}
		if (lock) {
			filters[3]= new InputFilterLock();
		}else{
			filters[3]= new InputFilterOriginal();
		}
		editText.setFilters(filters);
	} 
	 
	private class InputFilterLock  implements InputFilter{
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
			return source.length() < 1 ? dest.subSequence(dstart,dend):"";
		}
	}
	private class InputFilterOriginal implements InputFilter{
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				return null;// keep original
		}
	}
	private class InputFilterAccept implements InputFilter{
		String regexAccept = "";
		public InputFilterAccept(String reString) {
			regexAccept = reString!=null ? reString.trim() :"" ;
        }
		public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				for (int i = start; i < end; i++) { 				
					String ch = String.valueOf(source.charAt(i));
					if (regexAccept.equals("")) {
						return null;// keep original
					}else if (ch.matches(regexAccept)) {
						return null;// keep original
					}else{
						return ""; 
					}		             
				} 
				return null;// keep original
		}
	}
	 
	public void setText(String text) {
		super.setText(text);	
 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					View v = view.findViewById(R.id.txtText);
					if (v instanceof EditText) {
						EditText editText = ((EditText)v);
						onchange=false;
						String text = getSuperText();
						if ( !text.equals( editText.getText().toString() )) {
							InputFilter[] f = editText.getFilters();
							editText.setFilters(new InputFilter[0]);							
							editText.setText( text );
								if (editText.isFocused()) {
									editText.setSelection(text.length());
								}
								editText.setFilters(f);
						}				
						onchange=true;
						editText.setEnabled(getEnable());	
					}
				}
			});
		} 
	}
	 private boolean isPassword( ){
	      if (getStyle()!=null) {
	         if (getStyle().getInternalObject().getData("style").getData("n-password").toString().toLowerCase().equals("true")) {
	             return true;
	         }
	     }
	     return false;
	}
	
	  
	public View getView() {		 
		return view;
	}
	private boolean bfinder=false;
	private boolean bbarcode=false;
	public void showFinder(boolean b){
		bfinder=b;
	}
	 
 
	public Style getStyle() {
		if (super.getStyle()!=null) {			
			return super.getStyle();
		} 
		super.setStyle(Style.createStyle());		
		return super.getStyle();
	}
	public void setStyle(Style style) {
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					
					refreshStyleOnUI();
					String val =  getStyle().getInternalStyle().getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
					if (val.equals("true")) {
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittexterror);
					}else{
						view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittext);
					}
					
					if (getStyle().getInternalObject().getData("style").getData("n-lock").toString().toLowerCase().equals("true")) {
						
		            }
					
					//ucase,length,accept
					EditText editText = ((EditText)view.findViewById(R.id.txtText));			
					setFilter(editText, getStyle().getInternalStyle().getData("n-char-ucase").toString().equals("true"), getStyle().getInternalStyle().getData("n-maxlength").toInteger(), getStyle().getInternalStyle().getData("n-char-accept").toString(), getStyle().getInternalStyle().getData("n-lock").toString().equals("true") );
 
					
				}
			});
		} 
	}
	@Override
	public void onChangeViewUi() {	 
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
	 
		
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText) {	
			/*
			if (!super.getText() .equals( ((EditText)v).getText().toString() )) {
				InputFilter[] f = ((EditText)v).getFilters();
				((EditText)v).setFilters(new InputFilter[0]);
					((EditText)v).setText( super.getText() );//?
				((EditText)v).setFilters(f);
			}				
			*/
			((EditText)v).setEnabled(getEnable());	
		}
		
		v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
		
		v = view.findViewById(R.id.lblMandatory);
		if (v != null ) {			 
			v.setVisibility(isMandatory()?View.VISIBLE:View.GONE);
		}
		
		
	}
	private boolean isEditable(){
        if (getStyle()!=null) {
            if (getStyle().getInternalObject().getData("style").getData("n-lock").toString().toLowerCase().equals("true")) {
                return false;
            }
        }
        return isEnable();
    }
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
	 
		
		View v = view.findViewById(R.id.txtText);
		if (v instanceof EditText && getStyle()!=null) {
			validateGravity(((EditText)v), getStyle().getInternalObject().getData("style"));
		}
		
		Nset style = Nset.newObject();
		if (getStyle()!=null) {
			style = getStyle().getInternalObject().getData("style");
		}
		
		String val =  style.getData("n-error").toString().trim().toLowerCase(Locale.ENGLISH);
		if (val.equals("true")) {
			view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittexterror);
		}else{
			view.findViewById(R.id.txtText).setBackgroundResource(R.drawable.nfedittext);
		}	
			
		if (isPassword() && false) {
			((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			((EditText)view.findViewById(R.id.txtText)).setTransformationMethod(PasswordTransformationMethod.getInstance());
		}else{
			if (getStyle()!=null) {								  
				val =  style.getData("n-input").toString().toLowerCase();
				if (val.length()>=1) {
					if (val.contains("number")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_NUMBER);
					}	else if (val.contains("decimal")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
					}	else if (val.contains("numbersign")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
					}	else if (val.contains("decimalsign")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
					}	else if (val.contains("email")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
					}	else if (val.contains("ucase")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
					}	else if (val.contains("phone")) {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_PHONE);
					}else {
						((EditText)view.findViewById(R.id.txtText)).setInputType(InputType.TYPE_CLASS_TEXT);
					}
				}
			}			
		}
		EditText editText = ((EditText)view.findViewById(R.id.txtText));		
		if (isPassword()||getStyle().getInternalObject().getData("style").getData("n-password-mask").toString().equalsIgnoreCase("true")) {
			setPasswordMask(editText, true);
		}else{
			setPasswordMask(editText, false);
		}	
		
		Vector<InputFilter> filters = new Vector<InputFilter>();
		if (!isEditable()) {
			//filters.addElement(  readOnlyFilter()  ); 
		}
		if (getStyle()!=null) {						
			 			
			val =  style.getData("n-maxlength").toString().toLowerCase();
			if (val.length()>=1) {
				filters.addElement(  new InputFilter.LengthFilter(style.getData("n-maxlength").toInteger())  ); 
			}
			//getStyle().getInternalObject().getData("style").setData("n-max-length",  style.getData("n-maxlength").toInteger());
		}
		
		
		if (filters.size()>=1) {
			InputFilter[] f = new InputFilter[filters.size()];
			for (int i = 0; i < f.length; i++) {
				f[i]=filters.elementAt(i);
			}
			//((EditText)view.findViewById(R.id.txtText)).setFilters(f);
		}
		
		
	}
	
	private InputFilter readOnlyFiltera(){
		return new InputFilter() {
		    public CharSequence filter(CharSequence source, int start, int end,Spanned dest, int dstart, int dend) {
				
		    	return source.length() < 1 ? dest.subSequence(dstart, dend) : "";
			}
		 };
	}

	public String getSuperText() {
		return super.getText();
	}
	public String getText() {
		if (view!=null) {
			View v = view.findViewById(R.id.txtText);
			if (v instanceof EditText) {
				return ((EditText)v).getText().toString();
			}
			
		}
		return super.getText();
	}
	public void onSaveState() {
		if (view!=null) {
			
			super.setText(getText()); 
		}	
		
		//view=null;//reset
	}
	
}
