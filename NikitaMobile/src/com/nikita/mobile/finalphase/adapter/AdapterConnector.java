package com.nikita.mobile.finalphase.adapter;

import java.util.Vector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@SuppressWarnings("rawtypes")
public class AdapterConnector {

	public static void notifyDataSetChanged(ListView lstView) {

		try {
			((ArrayAdapter) lstView.getAdapter()).notifyDataSetChanged();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void connector(ListView lstView, Vector data, int layout, AdapterInterface iAdapter) {
		lstView.setAdapter(new ProtoAdapter(lstView.getContext(), layout, data, iAdapter));

	}

	public static void setConnector(ListView lstView, Vector data, int layout, AdapterInterface iAdapter) {
		new AdapterConnector().connector(lstView, data, layout, iAdapter);
	}

	public static View getInflater(Context context, int layoutprototipe) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(layoutprototipe, null, false);
		return rowView;
	}

	private class ProtoAdapter extends ArrayAdapter<Vector> {

		private Context context;
		private AdapterInterface iAdapter;
		private Vector vData;
		private int layoutprototipe;

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(layoutprototipe, parent, false);

			return iAdapter.onMappingColumn(position, rowView, vData.elementAt(position));

		}

		@SuppressWarnings("unchecked")
		public ProtoAdapter(Context context, int textViewResourceId, Vector vData, AdapterInterface iAdapter) {
			super(context, textViewResourceId, vData);
			this.context = context;
			this.iAdapter = iAdapter;
			layoutprototipe = textViewResourceId;
			this.vData = vData;
			notifyDataSetChanged();

		}

	}

}
