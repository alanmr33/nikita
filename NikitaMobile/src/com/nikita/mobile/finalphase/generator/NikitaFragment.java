package com.nikita.mobile.finalphase.generator;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nikita.mobile.finalphase.generator.action.BluetoothPrintAction;
import com.nikita.mobile.finalphase.generator.action.SystemAction;
import com.nikita.mobile.finalphase.generator.ui.FileUploadUI;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class NikitaFragment extends Fragment{
	private boolean isgenerator =false;
	private NikitaControler nikitaComponent;
	View vRoot ;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
		vRoot = inflater.inflate(R.layout.nformfragment, container, false);
		
		if (getArguments().getString("nfid")!=null && getArguments().getString("nfid").equals("nikitaform")) {
			String formname = savedInstanceState.getString("nikitaformname");
			nikitaComponent = AppNikita.getInstance().getNikitaComponent(formname);
		}
				
		
		if (vRoot.findViewById(R.id.frmBusy)!=null) {
			vRoot.findViewById(R.id.frmBusy).setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {}
			});
		}
		if (nikitaComponent!=null) {
			nikitaComponent.setActivity(getActivity(), vRoot);
			AppNikita.getInstance().setCurrentNikitaControler(nikitaComponent);//mybe fia showform
			
			showBusy(true);			
			if (nikitaComponent.isLoaded()) {
				if (nikitaComponent.isBusy()) {
					showBusy(true);
				}else{
					showBusy(false);
				}
				nikitaComponent.onCreateUI();				 
			}else{
				nikitaComponent.setOnLoadListener(new Runnable() {					
					public void run() {	
						
						if (nikitaComponent.isBusy()) {
							showBusy(true);
						}else{
							showBusy(false);
						}						
						nikitaComponent.onCreateUI();	
					}
				});			 				
			}
		}
		
		return vRoot;
	}
	
	public void showBusy(boolean show){
		vRoot.findViewById(R.id.frmBusy).setVisibility(show?View.VISIBLE:View.GONE);
	}
	public void onActivityResult(int requestCode,final int resultCode, Intent data) {
		
		String sRequesCode = requestCode+"";
		if (requestCode==SystemAction.RESULT_NATIVE_CALL) {
			sRequesCode = "NATIVE";
		}else if (requestCode==BluetoothPrintAction.RESULT_BLUETOOTH_SCAN) {
			sRequesCode = "BLUETOOTH";
		}else if (requestCode==FileUploadUI.RESULT_CAMERA_CAPTURE) {
			sRequesCode = "CAMERA";
		}else if (requestCode==FileUploadUI.RESULT_FILE_BROWSER) {
			sRequesCode = "FILE";
		}
		final Nset nResult = Nset.newObject();
		
		if (data.getExtras()!=null) {			
			String[] keys = data.getExtras().keySet().toArray(new String[data.getExtras().keySet().size()]);
			for (int i = 0; i < keys.length; i++) {
				if (data.getExtras().get(keys[i]) instanceof String) {
					nResult.setData(keys[i], data.getExtras().getString(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Integer) {
					nResult.setData(keys[i], data.getExtras().getInt(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Long) {
					nResult.setData(keys[i], data.getExtras().getLong(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Boolean) {
					nResult.setData(keys[i], data.getExtras().getBoolean(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Double) {
					nResult.setData(keys[i], data.getExtras().getDouble(keys[i]) );
				}else{
				
				}				
			}			
		}
		
		final String requestcode = sRequesCode;
		if (nikitaComponent!=null) {
			nikitaComponent.runOnActionThread(new Runnable() {
				public void run() {
					nikitaComponent.onResult(requestcode, resultCode+"", nResult);	
				}
			});
		}
	}
	
	 
	public void onSaveInstanceState(final Bundle outState) {
		if (nikitaComponent!=null) {
			nikitaComponent.runOnActionThread(new Runnable() {
				public void run() {
					nikitaComponent.onSaveState();	
				}
			});
		}
	}
	
	
}
