package com.nikita.mobile.finalphase.utility;

import java.io.IOException;

import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hipmob.gifanimationdrawable.GifAnimationDrawable;
import com.nikita.mobile.finalphase.R;

public class BusyDialog extends Dialog {
	
 
private ImageView iv;
	
public BusyDialog(Context context) {
	super(context, R.style.TransparentProgressDialog);
    	WindowManager.LayoutParams wlmp = getWindow().getAttributes();
    	wlmp.gravity = Gravity.CENTER_HORIZONTAL;
    	getWindow().setAttributes(wlmp);
		setTitle(null);
		setCancelable(false);
		setOnCancelListener(null);
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		iv = new ImageView(context);
		
		iv.setImageResource(R.drawable.busy);
		//layout.addView(iv, params);
		
		ProgressBar progressBar = new ProgressBar(context);
		layout.addView(progressBar, params);
		
		textView = new TextView(context);
		textView.setGravity(Gravity.CENTER);
		layout.addView(textView, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		
		addContentView(layout, params);
	}
	private TextView textView ;
	public void setMessage(String s){
		if (textView!=null) {
			textView.setText(s);
		}
	}
	public void showa() {
		super.show();
		//RotateAnimation anim = new RotateAnimation(0.0f, 360.0f , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
		//anim.setInterpolator(new LinearInterpolator());
		//anim.setRepeatCount(Animation.INFINITE);
		//anim.setDuration(3000);
		//iv.setAnimation(anim);
		//iv.startAnimation(anim);
		
		try {
			GifAnimationDrawable little = new GifAnimationDrawable( getContext().getResources().openRawResource(R.drawable.busy));
 
			iv.setImageDrawable(little);
			little.setVisible(true, true);
		} catch (NotFoundException e) { 
		} catch (Exception e) {	}		
	}
}
