package com.nikita.mobile.finalphase.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.IllegalFormatCodePointException;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.AlertDialog.Builder;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.dt;
import com.google.android.gms.internal.ft;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.action.MobileAction;
import com.nikita.mobile.finalphase.service.AlarmReceiver;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.NikitaProperty;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

public class LoginActivity extends AndroidActivity {
	public static boolean pause = false;

	private Nset nresult;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultlogin);
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		findViewById(R.id.imgRight).setVisibility(View.INVISIBLE);
		findViewById(R.id.tblLogin2).setVisibility(View.GONE);// 2016-02-23 new
																// request
		((TextView) findViewById(R.id.txtTitle)).setText("Login");

		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));

		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			((TextView) findViewById(R.id.txtVersion))
					.setText("Version " + pInfo.versionName + " : " + Utility.getSetting(getApplicationContext(), "INIT-LOGIC-VERSION", ""));
		} catch (Exception e) {
		}

		Nset lactivity = Nset.readJSON(Generator.getNikitaParameterValue("INIT-LOGIN-ACTIVITY", ""));

		((ImageView) findViewById(R.id.imgLoginIcon)).setImageResource(R.drawable.generator);
		try {
			if (getIntent() != null && getIntent().getIntExtra("iconid", 0) >= 1) {
				((ImageView) findViewById(R.id.imgLoginIcon)).setImageResource(getIntent().getIntExtra("iconid", 0));
			}
			if (pause) {

			}
			int i = Utility.getInt(AppNikita.getInstance().getVirtual("@+SETTING+LAUNCHERICONID") + "");
			((ImageView) findViewById(R.id.imgLoginIcon)).setImageResource(i);
		} catch (Exception e) {
		}

		// ((EditText)findViewById(R.id.txtUsername)).setText( "DKAMAL" );
		// ((EditText)findViewById(R.id.txtPassword)).setText( "1234" );

		if (lactivity.containsKey("userid")) {
			if (lactivity.getData("userid").containsKey("label")) {
				((TextView) findViewById(R.id.lblUserid)).setText(lactivity.getData("userid").getData("label").toString());
			}
			if (lactivity.getData("userid").containsKey("visible")) {
				((TextView) findViewById(R.id.txtPassword)).setText(lactivity.getData("userid").getData("visible").toString().equals("true") ? View.VISIBLE : View.INVISIBLE);
			}

		}
		if (lactivity.containsKey("password")) {
			if (lactivity.getData("password").containsKey("label")) {
				((TextView) findViewById(R.id.lblUserid)).setText(lactivity.getData("password").getData("label").toString());
			}
			if (lactivity.getData("password").containsKey("visible")) {
				((TextView) findViewById(R.id.txtPassword))
						.setVisibility(lactivity.getData("password").getData("visible").toString().equals("true") ? View.VISIBLE : View.INVISIBLE);
			}
		}
		if (lactivity.containsKey("caption")) {
			if (lactivity.getData("caption").containsKey("label")) {
				((TextView) findViewById(R.id.lblCaption)).setText(lactivity.getData("caption").getData("label").toString());
			}
			if (lactivity.getData("caption").containsKey("visible")) {
				((TextView) findViewById(R.id.lblCaption))
						.setVisibility(lactivity.getData("caption").getData("visible").toString().equals("true") ? View.VISIBLE : View.INVISIBLE);
			}
		}
		if (lactivity.containsKey("image")) {
			if (lactivity.getData("image").containsKey("visible")) {
				(findViewById(R.id.imgLoginIcon)).setVisibility(lactivity.getData("image").getData("visible").toString().equals("true") ? View.VISIBLE : View.INVISIBLE);
			}
		}

		if (lactivity.containsKey("TITLE")) {
			((TextView) findViewById(R.id.txtTitle)).setText(lactivity.getData("TITLE").toString());
		}

		if (lactivity.containsKey("remember")) {
			if (lactivity.getData("remember").getData("visible").toString().equalsIgnoreCase("true")) {
				((CheckBox) findViewById(R.id.cRemember)).setVisibility(View.VISIBLE);
			} else if (lactivity.getData("remember").getData("visible").toString().equalsIgnoreCase("gone")) {
				((CheckBox) findViewById(R.id.cRemember)).setVisibility(View.GONE);
			} else {
				((CheckBox) findViewById(R.id.cRemember)).setVisibility(View.INVISIBLE);
			}
			if (lactivity.getData("remember").containsKey("checked")) {
				((CheckBox) findViewById(R.id.cRemember)).setChecked(lactivity.getData("remember").getData("checked").toString().equalsIgnoreCase("true"));
			}

			if (lactivity.getData("remember").containsKey("text")) {
				((CheckBox) findViewById(R.id.cRemember)).setText(lactivity.getData("remember").getData("text").toString());
			}
		}
		if (lactivity.containsKey("setting")) {
			if (lactivity.getData("setting").getData("visible").toString().equalsIgnoreCase("true")) {
				(findViewById(R.id.tblSetting)).setVisibility(View.VISIBLE);
			} else if (lactivity.getData("setting").getData("visible").toString().equalsIgnoreCase("gone")) {
				(findViewById(R.id.tblSetting)).setVisibility(View.GONE);
			} else {
				(findViewById(R.id.tblSetting)).setVisibility(View.INVISIBLE);
			}
			if (lactivity.getData("setting").containsKey("text")) {
				((Button) findViewById(R.id.tblSetting)).setText(lactivity.getData("setting").getData("text").toString());
			}
		}
		if (lactivity.containsKey("copyright")) {
			if (lactivity.getData("copyright").containsKey("text")) {
				((TextView) findViewById(R.id.txtCopyRight)).setText(lactivity.getData("copyright").getData("text").toString());
			}
		} else {
			(findViewById(R.id.txtCopyRight)).setVisibility(View.GONE);
		}

		if (lactivity.containsKey("login2")) {
			if (lactivity.getData("login2").getData("visible").toString().equalsIgnoreCase("true")) {
				(findViewById(R.id.tblLogin)).setVisibility(View.GONE);
				(findViewById(R.id.tblLogin2)).setVisibility(View.VISIBLE);
			}
			if (lactivity.getData("login2").containsKey("text")) {
				((Button) findViewById(R.id.tblLogin2)).setText(lactivity.getData("login2").getData("text").toString());
			}
		}
		if (lactivity.containsKey("login")) {
			if (lactivity.getData("login").containsKey("text")) {
				((Button) findViewById(R.id.tblLogin)).setText(lactivity.getData("login").getData("text").toString());
			}
		}

		((CheckBox) findViewById(R.id.cRemember)).setChecked(Utility.getSetting(getApplicationContext(), "REMEMBER", "").equals("true") ? true : false);
		if (((CheckBox) findViewById(R.id.cRemember)).isChecked()) {
			((EditText) findViewById(R.id.txtUsername)).setText(Utility.getSetting(getApplicationContext(), "N-USER", ""));
			;

		}
		if (lactivity.containsKey("showpassword")) {
			if (lactivity.getData("showpassword").getData("visible").toString().equalsIgnoreCase("true")) {
				((CheckBox) findViewById(R.id.cShowPassword)).setVisibility(View.VISIBLE);
			} else if (lactivity.getData("showpassword").getData("visible").toString().equalsIgnoreCase("gone")) {
				((CheckBox) findViewById(R.id.cShowPassword)).setVisibility(View.GONE);
			} else {
				((CheckBox) findViewById(R.id.cShowPassword)).setVisibility(View.INVISIBLE);
			}

			if (lactivity.getData("showpassword").containsKey("text")) {
				((CheckBox) findViewById(R.id.cShowPassword)).setText(lactivity.getData("showpassword").getData("text").toString());
			}
		}

		((CheckBox) findViewById(R.id.cShowPassword)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!((CheckBox) findViewById(R.id.cShowPassword)).isChecked()) {
					((EditText) findViewById(R.id.txtPassword)).setTransformationMethod(PasswordTransformationMethod.getInstance());
				} else {
					((EditText) findViewById(R.id.txtPassword)).setTransformationMethod(null);
				}
			}
		});

		View.OnClickListener onClickListener = new View.OnClickListener() {
			public void onClick(View v) {
				viewProgressMsg(LoginActivity.this, "Login", 0);
				Utility.setSetting(getApplicationContext(), "REMEMBER", ((CheckBox) findViewById(R.id.cRemember)).isChecked() ? "true" : "");

				if (getCurrentNikitaCode().contains("remember")) {
					Utility.setSetting(getApplicationContext(), "N-USER-NC", ((EditText) findViewById(R.id.txtUsername)).getText().toString());
					Utility.setSetting(getApplicationContext(), "N-PASS-NC", ((EditText) findViewById(R.id.txtPassword)).getText().toString());
				}
				
				//copy semua file dalam satu folder ke folder lain
				String paramFlagCopy = "1"; //diganti jdi parameter yg diambil dr engine
				
				File Chkfile = new File("/storage/emulated/0/Android/data/com.nikita.mobile.finalphase/files/mobile_old.db");
				
				if(!Chkfile.exists())  {
				if (paramFlagCopy.equals("1")) {
					File srcDir = new File("/storage/emulated/0/Android/data/com.nikita.mobile.fullphase/files/mobile.db");
					File destDir = new File("/storage/emulated/0/Android/data/com.nikita.mobile.finalphase/files/mobile.db");
					File destDirOld = new File("/storage/emulated/0/Android/data/com.nikita.mobile.finalphase/files/mobile_old.db");
					File srcDirImg = new File("/storage/emulated/0/Android/data/com.nikita.mobile.fullphase/files/image/");
					File destDirImg = new File("/storage/emulated/0/Android/data/com.nikita.mobile.finalphase/files/image/");
					try {
						FileUtils.copyFile(srcDir, destDir);
						FileUtils.copyFile(srcDir, destDirOld);
						FileUtils.copyDirectory(srcDirImg, destDirImg);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}}

				onClickLogin();
			}
		};
		(findViewById(R.id.tblLogin)).setOnClickListener(onClickListener);
		(findViewById(R.id.tblLogin2)).setOnClickListener(onClickListener);

		(findViewById(R.id.tblSetting)).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, SettingActivity.class);
				startActivity(intent);
			}
		});

		if (getIntent() != null && getIntent().getStringExtra("logon") != null && getIntent().getStringExtra("logon").equalsIgnoreCase("clear")) {
			Utility.setSetting(getApplicationContext(), "N-LOGON", "");
		}

		// trigger to choose what is the next page, Login or MenuActivity
		if (Utility.getSetting(getApplicationContext(), "N-LOGON", "").length() < 10) {
		} else if (Utility.getSetting(getApplicationContext(), "N-LOGON", "").substring(0, 10).equals(Utility.Now().substring(0, 10))) {
			Nset afterlogin = Nset.readJSON(Generator.getNikitaParameters().getData("INIT-LOGIN-ACTIVITY").toString());
			if (afterlogin.containsKey("afterlogin")) {
				Generator.startNikitaForm(LoginActivity.this, afterlogin.getData("afterlogin").toString());
			} else {
				Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
				startActivity(intent);
			}
			finish();
		}
		if (getCurrentNikitaCode().contains("remember")) {
			((EditText) findViewById(R.id.txtUsername)).setText(Utility.getSetting(getApplicationContext(), "N-USER-NC", ""));
			;
			((EditText) findViewById(R.id.txtPassword)).setText(Utility.getSetting(getApplicationContext(), "N-PASS-NC", ""));
			;
		}
		// NForm.startFormWithBusy("nikita", LoginActivity.this);
	}

	private void onClickLogin() {
		Messagebox.showBusyDialog(LoginActivity.this, new Runnable() {
			public void run() {
				Nset afterlogin = Nset.readJSON(Generator.getNikitaParameters().getData("INIT-LOGIN-ACTIVITY").toString());
				String userreq = "";
				String mstvreq = afterlogin.getData("downloadversion").toString();
				if (afterlogin.containsKey("userreq") && afterlogin.getData("userreq").toString().equalsIgnoreCase("true")) {
					userreq = ((EditText) findViewById(R.id.txtPassword)).getText().toString();
				}
				String NewPassword = Utility.MD5(((EditText) findViewById(R.id.txtPassword)).getText().toString())
						+Utility.MD5(((EditText) findViewById(R.id.txtUsername)).getText().toString()).substring(0, 10);
				nresult = onLogin(LoginActivity.this, ((EditText) findViewById(R.id.txtUsername)).getText().toString(),
						NewPassword, userreq, mstvreq);
				if (nresult.containsKey("mobileaction")) {
				}
			}
		}, new Runnable() {
			public void run() {
				if (nresult.getData("error").toString().equals("") && nresult.getData("status").toString().equals("OK")) {
					if (isAppExpired(LoginActivity.this)) {
						showDialog("App. Expired", "App. is Expired", "Back", "Download", "", "expired", Nset.newObject());
					} else {
						if (nresult.getData("mode").toString().equals("offline")) {
							Toast.makeText(LoginActivity.this, "offline mode", Toast.LENGTH_LONG).show();
						}
						if (nresult.getData("message").toString().length() >= 1) {
							Toast.makeText(LoginActivity.this, nresult.getData("message").toString(), Toast.LENGTH_LONG).show();
						}
						Nset afterlogin = Nset.readJSON(Generator.getNikitaParameters().getData("INIT-LOGIN-ACTIVITY").toString());
						Utility.setSetting(getApplicationContext(), "N-LOGON", Utility.Now());
						Utility.setSetting(getApplicationContext(), "N-ONLINE", Utility.Now());
						if (afterlogin.containsKey("afterlogin")) {
							Generator.startNikitaForm(LoginActivity.this, afterlogin.getData("afterlogin").toString());
						} else {
							Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
							startActivity(intent);
						}

						//if (!Utility.getSetting(getApplicationContext(), "TIME-LOGOUT", "").isEmpty()
						//		&& !(Utility.getSetting(LoginActivity.this, "TIME-LOGOUT", "").substring(0, 10).equals(Utility.Now().substring(0, 10)))) {
						//	Connection.DBinsert("historyactivity", "activity=logout", "datetime=" + Utility.getSetting(getApplicationContext(), "TIME-LOGOUT", ""),
						//			"status=0");
						//}

						int days = Generator.getNikitaParameters().getData("INIT-DELETE-HISTORY-DAY").toInteger();
						if (Generator.getNikitaParameters().getData("INIT-DELETE-HISTORY").toString().equalsIgnoreCase("1")) {
							if (days == 0) {
								deleteHistoryActivity();
							} else {
								NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
								Nikitaset n = ncmobile.Query("SELECT * FROM historyactivity WHERE status = '1'");

								for (int a = 0; a < n.getRows(); a++) {
									SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									Calendar now = Calendar.getInstance();
									Date createdate = null;
									try {
										createdate = dateformat.parse(n.getText(a, "datetime"));
									} catch (ParseException e) {
									}

									if (Utility.selisihDateTime(now.getTime(), createdate) >= days) {
										ncmobile.Query("DELETE FROM historyactivity WHERE id = '" + n.getText(a, "id") + "'");
									}

								}
							}
						}

						insertHistoryLogin();

						AlarmReceiver.restartAlarm(AppNikita.getInstance());
						Utility.setSetting(getApplicationContext(), "TIME-LOGOUT", "");
						finish();
					}
				} else if (nresult.getData("action").toString().equals("expired")) {
					showDialog("App. Expired", nresult.getData("error").toString(), "Back", "Download", "", "expired", Nset.newObject());
				} else if (nresult.getData("action").toString().equals("showdialog")) {
					Toast.makeText(LoginActivity.this, nresult.toJSON(), Toast.LENGTH_LONG).show();
					;
					showDialog(nresult.getData("title").toString(), nresult.getData("message").toString(), nresult.getData("button1").toString(),
							nresult.getData("button2").toString(), "", "showdialog", nresult.getData("dialogresult"));
				} else if (nresult.getData("action").toString().equals("freeze")) {
				} else if (nresult.getData("state").toString().equals("fretry")) {
					fretry();
				} else if (nresult.getData("state").toString().equals("nikita")) {
					onClickLogin();
				} else {
					showDialog("Login Gagal", nresult.getData("error").toString(), "OK", "", "", "", Nset.newObject());
				}
			}
		});
	}

	private static boolean isAppExpired(Context context) {
		int nverapp = Utility.parseVersion(Generator.getNikitaParameters().getData("INIT-APP-VERSION").toString());
		int currapp = 0;
		try {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			currapp = Utility.parseVersion(pInfo.versionName);
		} catch (Exception e) {
		}
		if (nverapp > currapp) {
			return true;
		}
		return false;
	}

	private static void viewProgressMsg(Activity context, final String message, final int persen) {
		context.runOnUiThread(new Runnable() {
			public void run() {
				lastprogres = persen;
				Messagebox.setProsesBarMsg("[" + message + "]\r\n\r\n" + "Please wait " + persen + "%");
			}
		});
	}

	public String getCurrentNikitaCode() {
		return Utility.getSetting(getApplicationContext(), "NIKITACODE", "");
	}

	private void preparenikitaaction(Context context, Nset nikitaaction, String key) {
		if (nikitaaction.containsKey(key)) {
			Utility.setSetting(context.getApplicationContext(), key, nikitaaction.getData(key).toString());
		}
	}

	public Nset onLogin(final Activity context, String username, String password, String npassword, String dwonloadmaster) {
		// sync init dahulu

		Generator.closeAllConnection();
		Generator.synchronizeNikita(context.getApplicationContext());// 2016-01-27
																		// Sync
																		// Reload
																		// Setting
		// Generator.initializeNikita(context.getApplicationContext());

		createPrinterLog();
		DevActivity.reconfigurationsystem(Generator.getNikitaParameterValue("INIT-APP", ""));

		NikitaConnection nc = NikitaConnection.getConnection(NikitaConnection.LOGIC);
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);

		if (context instanceof Activity) {
			if (isAppExpired(context)) {
				return Nset.readJSON("{'action':'expired','error':'App. is Expired','status':''}", true);
			}
		}

		Utility.setSetting(context.getApplicationContext(), "N-IMEI",
				((TelephonyManager) context.getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase());

		String version = "";
		try {
			PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = pInfo.versionName;
		} catch (NameNotFoundException e) {
		}

		LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		// login
		Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();
		hashtable.put("gps",
				manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ? "GPS" : (manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) ? "NETWORK" : "OFF"));
		hashtable.put("wifi", ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).isWifiEnabled() ? "ON" : "OFF");
		hashtable.put("bluetooth", BluetoothAdapter.getDefaultAdapter().isEnabled() ? "ON" : "OFF");
		hashtable.put("userid", username);
		hashtable.put("password", password);
		hashtable.put("userreq", npassword);
		hashtable.put("mocklocation", UtilityAndroid.isMockSettingsON(context) ? "ON" : "OFF");
		hashtable.put("lastuserid", Utility.getSetting(context.getApplicationContext(), "N-USER", ""));
		hashtable.put("queuecount", ncmobile.Query("SELECT * FROM mobileactivity WHERE status = 0 OR status = 1").getRows() + "");
		hashtable.put("imei", ((TelephonyManager) context.getSystemService(android.content.Context.TELEPHONY_SERVICE)).getDeviceId().toUpperCase());
		hashtable.put("version", version);
		hashtable.put("logicversion", Utility.getSetting(context.getApplicationContext(), "INIT-LOGIC-VERSION", ""));

		// 17072016
		if (dwonloadmaster.equalsIgnoreCase("true")) {
			hashtable.put("tableversion", Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-TABLE-VERSION", "{}"));
			hashtable.put("ftableversion", Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-FTABLE-VERSION", "{}"));
			hashtable.put("fileversion", Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-FILE-VERSION", "{}"));
			hashtable.put("tablefileversion", Utility.getSetting(context.getApplicationContext(), "DOWNLOAD-TABLEFILE-VERSION", "{}"));
		}
		// resumesupport
		hashtable.put("resumesupport", "");

		// hapus semua isi table dan gambar yg terkirim tiap ganti bulan
		Calendar now = Calendar.getInstance();
		Date dtNow = now.getTime();

		// if(dtNow.getDate() == 1 ){
		// Utility.deleteFileAll(Utility.getDefaultPath("image"));
		//
		// // truncate table
		// String sql = "DELETE FROM mobileorder";
		// String sql1 = "DELETE FROM mobileactivity";
		// String sql2 = "DELETE FROM order_priority";
		// ncmobile.Query(sql);
		// ncmobile.Query(sql1);
		// ncmobile.Query(sql2);
		//
		// }

		// 27 - 09 - 2016
		String query = "SELECT * FROM mobileactivity WHERE status = 0 OR status = 2";
		Nikitaset dataSent = ncmobile.Query(query);

		if (dataSent.getRows() > 0) {
			for (int i = 0; i < dataSent.getRows(); i++) {
				if (dataSent.getText(i, "createdate").length() != 0) {
					SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					// Calendar now = Calendar.getInstance();
					Date createdate = null;
					try {
						createdate = dateformat.parse(dataSent.getText(i, "createdate"));
					} catch (ParseException e) {
					}

					//05-01-2017 : housekeeping photo, data bulan dan tahun kemarin tidak terhapus, pdhl sudah bulan 1 tahun 2017
					
					if ( createdate.getMonth() < dtNow.getMonth() || (createdate.getMonth() > dtNow.getMonth() && createdate.getYear() < dtNow.getYear()) ) {
						// delete data sent
						// String newBody = ncmobile.Query("SELECT body FROM
						// mobileactivity WHERE activityid=?",
						// dataSent.getText(0, "activityid")).getText(0,
						// "body").toString() ;
						// Nset a = Nset.readJSON(newBody);
						// String foto1 =
						// a.getData("dipo_mtodolist_child").getData("foto_1").getData(0).toString();
						// String foto2 =
						// a.getData("dipo_mtodolist_child").getData("foto_2").getData(0).toString();
						//
						// new
						// File(Utility.getDefaultImagePath(foto1)).delete();
						// if(foto2.equals(".cam")) {
						// new
						// File(Utility.getDefaultImagePath(foto2)).delete();
						//// Utility.deleteAllFileFolder(new
						// File(Utility.getDefaultImagePath(foto2)));
						// }

						ncmobile.Query("DELETE FROM order_priority WHERE orderid='" + dataSent.getText(i, "orderid") + "'");
						MobileAction.deleteMobileActivity(context, dataSent, i);
					}
				}
			}
		}

		Nset nlog = Nset.readJSON(NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mlogin", hashtable)));
		nresult = nlog;

		viewProgressMsg(context, "Synchronize N-Logic", 10);
		if (nlog.getData("error").toString().equals("") && nlog.getData("status").toString().equals("OK")) {
			// login true
			Utility.setSetting(context.getApplicationContext(), "N-AUTH", nlog.getData("nauth").toString());
			Utility.setSetting(context.getApplicationContext(), "N-BATCH", nlog.getData("batch").toString());
			Utility.setSetting(context.getApplicationContext(), "N-SESSION", nlog.getData("session").toString());
			
			String NewPassword = Utility.MD5(Utility.MD5(password)+Utility.MD5(username).substring(0, 10));
			Utility.setSetting(context.getApplicationContext(), "N-USER", username);
			Utility.setSetting(context.getApplicationContext(), "N-PASS", NewPassword);
			/*
			 * Utility.setSetting(context.getApplicationContext(), "N-LOGON",
			 * Utility.Now());
			 */
			/*
			 * Utility.setSetting(context.getApplicationContext(), "N-ONLINE",
			 * Utility.Now());
			 */
			Utility.setSetting(context.getApplicationContext(), "N-USER-BRANCH", nlog.getData("branchid").toString()); // get
																														// branch
																														// id
																														// user

			Utility.setSetting(context.getApplicationContext(), "N-PROFILE", nlog.getData("profile").toJSON());

			Utility.setSetting(context.getApplicationContext(), "@+SESSION-USERNAME", username);
			Utility.setSetting(context.getApplicationContext(), "@+SESSION-AUTH", nlog.getData("nauth").toString());
			Utility.setSetting(context.getApplicationContext(), "@+SESSION-BATCH", nlog.getData("batch").toString());
			Utility.setSetting(context.getApplicationContext(), "@+SESSION-SESSION", nlog.getData("session").toString());
			Utility.setSetting(context.getApplicationContext(), "@+SESSION-PROFILE", nlog.getData("profile").toJSON());

			Utility.setSetting(context.getApplicationContext(), "N-ACCESS", nlog.getData("menuaccess").toString());
			Utility.setSetting(context.getApplicationContext(), "N-AUTOLOCK", nlog.getData("autolock").toString());

			// perintah change 17072016
			if (nresult.containsKey("nikitaaction")) {
				Nset nikitaaction = nresult.getData("nikitaaction");
				preparenikitaaction(context, nikitaaction, "INIT-LOGIC-VERSION");
				preparenikitaaction(context, nikitaaction, "INIT-TABLE-VERSION");

				preparenikitaaction(context, nikitaaction, "DOWNLOAD-TABLE-VERSION");
				preparenikitaaction(context, nikitaaction, "DOWNLOAD-FTABLE-VERSION");
				preparenikitaaction(context, nikitaaction, "DOWNLOAD-FILE-VERSION");
				preparenikitaaction(context, nikitaaction, "DOWNLOAD-TABLEFILE-VERSION");
				if (nikitaaction.containsKey("CLEAR-LOGIC")) {
					Utility.setSetting(context.getApplicationContext(), "INIT-LOGIC-VERSION", "");
				}
				if (nikitaaction.containsKey("CLEAR-TABLE")) {
					Utility.setSetting(context.getApplicationContext(), "INIT-TABLE-VERSION", "");
				}
				if (nikitaaction.containsKey("CLEAR-DOWNLOAD-VERSION")) {
					Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-TABLE-VERSION", "{}");
					Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-FTABLE-VERSION", "{}");
					Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-FILE-VERSION", "{}");
					Utility.setSetting(context.getApplicationContext(), "DOWNLOAD-TABLEFILE-VERSION", "{}");
				}
			}

			// download logic master [50]
			int nverlogic = Utility.parseVersion(Generator.getNikitaParameters().getData("INIT-LOGIC-VERSION").toString());
			int nvertable = Utility.parseVersion(Generator.getNikitaParameters().getData("INIT-TABLE-VERSION").toString());
			if (nverlogic > Utility.parseVersion(Utility.getSetting(context.getApplicationContext(), "INIT-LOGIC-VERSION", "")) || nverlogic == 0) {
				String sV = "";
				int fcount = 0;

				if (nc.isNikitaConnection()) {
					context.runOnUiThread(new Runnable() {
						public void run() {
							String result = "Tidak diperbolehkan Sync. menggunakan mode NIKITACONNECTION \r\n(ONLINE N-LOGIC)";
							Toast.makeText(LoginActivity.this, result, Toast.LENGTH_LONG).show();
						}
					});
				} else {
					/*
					 * //getallformsfrommodule sV = NikitaInternet.getString(
					 * NikitaInternet.postHttp(AppNikita.getInstance().
					 * getBaseUrl()+"/mobile.mlogic?mode=module&module="+Utility
					 * .urlEncode(Generator.getNikitaParameters().getData(
					 * "INIT-MODULE").toString())+"&recurse="+Utility.urlEncode(
					 * Generator.getNikitaParameters().getData(
					 * "INIT-MODULE-RECURSE").toString()) )); final Nset nforms
					 * = Nset.readJSON(sV); //formcomponentlogic fcount =
					 * nforms.getArraySize(); for (int i = 0; i <
					 * nforms.getArraySize(); i++) { if
					 * (nc.isNikitaConnection()) {
					 * Utility.setSetting(context.getApplicationContext(),
					 * "INIT-LOGIC-VERSION", "ONLINE"); break;// return
					 * Nset.readJSON(
					 * "{'error':'Tidak diperbolehkan Sync. menggunakan mode NIKITACONNECTION'}"
					 * , true); } sV =NikitaInternet.getString(
					 * NikitaInternet.postHttp(AppNikita.getInstance().
					 * getBaseUrl()+"/mobile.mlogic?mode=frmcomplogic&formid="+
					 * nforms.getData(i).toString()));//[formid..] Nset nsform
					 * =Nset.readJSON(sV); for (int j = 0; j <
					 * nsform.getArraySize(); j++) { Nikitaset ns = new
					 * Nikitaset(nsform.getData(j)); if (i==0) { if (j==0) {
					 * DevActivity.createTable(nc, ns, "web_form"); }else if
					 * (j==1) { DevActivity.createTable(nc, ns,
					 * "web_component"); }else if (j==2) {
					 * DevActivity.createTable(nc, ns, "web_route"); } } if
					 * (j==0) { nc.Query("DELETE FROM web_form WHERE formid=?",
					 * nforms.getData(i).toString()); nc.Query(
					 * "DELETE FROM web_form WHERE formname=?", ns.getText(0,
					 * "formname")); DevActivity.insertTable(nc, ns,
					 * "web_form"); }else if (j==1) { nc.Query(
					 * "DELETE FROM web_component WHERE formid=?",
					 * nforms.getData(i).toString()); for (int k = 0; k <
					 * ns.getRows(); k++) { nc.Query(
					 * "DELETE FROM web_route WHERE compid=?", ns.getText(k,
					 * "compid")); } DevActivity.insertTable(nc, ns,
					 * "web_component"); }else if (j==2) {
					 * DevActivity.insertTable(nc, ns, "web_route"); }
					 * 
					 * } viewProgressMsg(context, "Synchronize N-Logic",
					 * 10+((i+1)*50/fcount)); }
					 */
					nlog = DevActivity.saveNikitaLogic(context, nc, Generator.getNikitaParameters(), new DevActivity.OnProgresListener() {
						public void onProgres(final int count, final int max, String msg) {
							viewProgressMsg(context, "Synchronize N-Logic", 10 + ((count + 1) * 50 / max));
						}
					});
					nresult = DevActivity.checkSyncStatus(nresult, nlog);
					if (DevActivity.checkSyncStatusBreak(nresult)) {
						// download additional file [5]
						return nresult;
					}

					Utility.setSetting(context.getApplicationContext(), "INIT-LOGIC-VERSION", Generator.getNikitaParameters().getData("INIT-LOGIC-VERSION").toString());
				}
			}

			if (nvertable > Utility.parseVersion(Utility.getSetting(context.getApplicationContext(), "INIT-TABLE-VERSION", "")) || nvertable == 0) {
				// download additional file [5]
				nlog = DevActivity.saveSyncFile(context, ncmobile, Generator.getNikitaParameters(), new DevActivity.OnProgresListener() {
					public void onProgres(final int count, final int max, String msg) {

						viewProgressMsg(context, "Synchronize File", 60 + ((count + 1) * 20 / max));
					}
				});
				nresult = DevActivity.checkSyncStatus(nresult, nlog);
				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}

				// download additional table-file [10]

				nlog = DevActivity.saveSyncTableFile(context, ncmobile, Generator.getNikitaParameters(), new DevActivity.OnProgresListener() {
					public void onProgres(final int count, final int max, String msg) {

						viewProgressMsg(context, "Synchronize TableFile", 65 + ((count + 1) * 20 / max));
					}
				});
				nresult = DevActivity.checkSyncStatus(nresult, nlog);
				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}

				// download additional table [15]
				nlog = DevActivity.saveSyncTable(context, ncmobile, Generator.getNikitaParameters(), new DevActivity.OnProgresListener() {
					public void onProgres(final int count, final int max, String msg) {
						viewProgressMsg(context, "Synchronize Table", 73 + ((count + 1) * 20 / max));
					}
				});
				nresult = DevActivity.checkSyncStatus(nresult, nlog);

				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}

				bufferProgresPersen = 0;
				bufferProgresMessage = "";
				nlog = DevActivity.saveSyncFT(context, ncmobile, Generator.getNikitaParameters(), new DevActivity.OnProgresListener() {
					public void onProgres(final int count, final int max, String msg) {
						if (msg.equals("s")) {
							viewProgressMsg(context, "Synchronize FTable " + bufferProgresMessage + "(" + (max - count) + ")", bufferProgresPersen);
						} else {
							bufferProgresPersen = 78 + ((count + 1) * 20 / max);
							bufferProgresMessage = "(" + (count + 1) + "/" + max + ")";
							viewProgressMsg(context, "Synchronize FTable " + bufferProgresMessage + " " + msg, bufferProgresPersen);
						}

					}
				});
				nresult = DevActivity.checkSyncStatus(nresult, nlog);
				if (DevActivity.checkSyncStatusBreak(nresult)) {
					return nresult;
				}
				Utility.setSetting(context.getApplicationContext(), "INIT-TABLE-VERSION", Generator.getNikitaParameters().getData("INIT-TABLE-VERSION").toString());

			}

			if (nlog.getData("action").toString().equals("clearactivity")) {
				MobileAction.drop(ncmobile);
				MobileAction.createifneed(ncmobile);
			}

			// download order [20]
			viewProgressMsg(context, "Synchronize Order", 80);
			downloadTable();
			nlog = downloadorder(context, false);
			nresult = DevActivity.checkSyncStatus(nresult, nlog);
			if (DevActivity.checkSyncStatusBreak(nresult)) {
				return nresult;
			}
		} else if (nlog.getData("nfid").toString().equals("NikitaInternet")) {
			// Generator.out("LOGIN", "1");
			// type name = new type(arguments);
			if (Utility.getSetting(context.getApplicationContext(), "N-ONLINE", "").length() < 10) {
				// Generator.out("LOGIN", "2");
			} else if (Utility.getSetting(context.getApplicationContext(), "N-ONLINE", "").substring(0, 10).equals(Utility.Now().substring(0, 10))) {
				// Generator.out("LOGIN", "3");
				if (getCurrentNikitaCode().contains("nooffline")) {
				} else if (Utility.getSetting(context.getApplicationContext(), "N-USER", "").equals(username)
						&& Utility.getSetting(context.getApplicationContext(), "N-PASS", "").equals(Utility.MD5(Utility.MD5(password)+Utility.MD5(username).substring(0, 10)))) {
					Generator.out("LOGIN", "4");
					nlog.setData("error", "");
					nlog.setData("status", "OK");
					nlog.setData("mode", "offline");
				}
			}
		}
		
		return nresult;
	}

	private int bufferProgresPersen = 0;
	private String bufferProgresMessage = "";

	private void downloadTable() {
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		String usertable = Utility.getSetting(getApplicationContext(), "N-USER", "");
		TelephonyManager tm = (TelephonyManager)AppNikita.getInstance().getApplicationContext(). getSystemService(android.content.Context.TELEPHONY_SERVICE);
		String imeitable=tm.getDeviceId().toUpperCase();
		
		String mm = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=1"+"&userid="+usertable+"&imei="+imeitable));
		String mview = Utility.decodeBase64(mm);
		String oo = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=2"+"&userid="+usertable+"&imei="+imeitable));
		String oview = Utility.decodeBase64(oo);
		String qq = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=3"+"&userid="+usertable+"&imei="+imeitable));
		String qview = Utility.decodeBase64(qq);
		
		//Nset m = Nset.readJSON(NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=order_priority")));
		//Nset o = Nset
		//		.readJSON(NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=dsf_bertemu_dengan")));
		Nset m = Nset.readJSON(mview);
		Nset o = Nset.readJSON(oview);
		Nset q = Nset.readJSON(qview);
		//Nset q = Nset
		//		.readJSON(NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadtable" + "?table=dsf_status_kunjungan")));

		//Generator.out("downloadpriority", Utility.decodeBase64(m.toJSON()));
		//Generator.out("download bertemu dengan", Utility.decodeBase64(o.toJSON()));
		//Generator.out("download visit status", Utility.decodeBase64(q.toJSON()));
		
		Generator.out("downloadpriority", m.toJSON());
		Generator.out("download bertemu dengan", o.toJSON());
		Generator.out("download visit status", q.toJSON());

		if (m.getArraySize() < 0) {
			Nikitaset ns = new Nikitaset(m.getData());
			DevActivity.makeMobileOrderlcase(ns);
			DevActivity.createTable(ncmobile, ns, "order_priority");
			DevActivity.insertTable(ncmobile, ns, "order_priority");
		}

		// Recordset rs = Connection.DBquery("SELECT * FROM
		// dsf_bertemu_dengan");
		Nikitaset nset1 = ncmobile.Query("SELECT * FROM dsf_bertemu_dengan");

		if (o.getArraySize() < 0) {
			Nikitaset ns = new Nikitaset(o.getData());
			if (nset1.getRows() != o.getArraySize()) {
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "dsf_bertemu_dengan");
				Connection.DBquery("DELETE FROM dsf_bertemu_dengan");
				DevActivity.insertTable(ncmobile, ns, "dsf_bertemu_dengan");
			} else {
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "dsf_bertemu_dengan");
				DevActivity.insertTable(ncmobile, ns, "dsf_bertemu_dengan");
			}

		}

		// Recordset rs1 = Connection.DBquery("SELECT * FROM
		// dsf_status_kunjungan");
		Nikitaset nset2 = ncmobile.Query("SELECT * FROM dsf_status_kunjungan");

		if (q.getArraySize() < 0) {
			Nikitaset ns = new Nikitaset(q.getData());
			if (nset2.getRows() != q.getArraySize()) {
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "dsf_status_kunjungan");
				Connection.DBquery("DELETE FROM dsf_status_kunjungan");
				DevActivity.insertTable(ncmobile, ns, "dsf_status_kunjungan");
			} else {
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "dsf_status_kunjungan");
				DevActivity.insertTable(ncmobile, ns, "dsf_status_kunjungan");
			}
		}
	}

	public static synchronized Nset downloadorder(Context context, boolean service) {
		Nset nresult = DevActivity.checkSyncStatus(Nset.newObject());
		Nset nset = Nset.newArray();
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		// Nikitaset nikitaset = ncmobile.Query("SELECT orderid FROM
		// mobileorder");
		String sql = Generator.getNikitaParameters().getData("INIT-DOWNLOAD-ORDER-QUERY").toString();
		Nikitaset nikitaset = ncmobile.Query(sql);
		for (int i = 0; i < nikitaset.getRows(); i++) {
			nset.addData(nikitaset.getText(i, 0));
		}
		// {branchid=1, session=, action=download, service=false, nauth=,
		// userid=field.coll3, logicversion=2.0.352, imei=354599060368910,
		// username=field.coll3, version=2.3.166, localdate=2016-11-21 11:36:26,
		// batch=, auth=, orderid=[]}
		Hashtable<String, String> hashtable = AppNikita.getInstance().getArgsData();// new
																					// Hashtable<String,
																					// String>();
		hashtable.put("orderid", nset.toJSON());
		hashtable.put("service", service ? "true" : "false");
		hashtable.put("action", "download");
		// hashtable.put("action", "RECEIVED" );
		hashtable.put("nauth", Utility.getSetting(context.getApplicationContext(), "N-AUTH", ""));
		hashtable.put("userid", Utility.getSetting(context.getApplicationContext(), "N-USER", ""));
		
		String nn = NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadorder", hashtable));
		//String nview = Utility.decodeBase64(nn);
		Nset n = Nset.readJSON(nn);
		// Nset m = Nset.readJSON( NikitaInternet.getString(
		// NikitaInternet.postHttp(
		// AppNikita.getInstance().getBaseUrl()+"/mobile.mdownloadtable"+"?table=order_priority"
		// )) );
		// Nset o = Nset.readJSON( NikitaInternet.getString(
		// NikitaInternet.postHttp(
		// AppNikita.getInstance().getBaseUrl()+"/mobile.mdownloadtable"+"?table=dsf_bertemu_dengan"
		// )) );
		// Nset q = Nset.readJSON( NikitaInternet.getString(
		// NikitaInternet.postHttp(
		// AppNikita.getInstance().getBaseUrl()+"/mobile.mdownloadtable"+"?table=dsf_status_kunjungan"
		// )) );

		Generator.out("downloadorder", n.toJSON());
		// Generator.out("downloadpriority", m.toJSON());
		// Generator.out("download bertemu dengan", o.toJSON());
		// Generator.out("download visit status", q.toJSON());

		int fcount = n.getArraySize();
		for (int i = 0; i < n.getArraySize(); i++) {

			if (n.getData(i).containsKey("truncate")) {
				Nikitaset ns = new Nikitaset(n.getData(i).getData("truncate"));
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.dropTable(ncmobile, "mobileorder");
				DevActivity.createTable(ncmobile, ns, "mobileorder");
				DevActivity.insertTable(ncmobile, ns, "mobileorder");
			} else if (n.getData(i).containsKey("insert")) {
				Nikitaset ns = new Nikitaset(n.getData(i).getData("insert"));
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "mobileorder");
				DevActivity.insertTable(ncmobile, ns, "mobileorder");
			} else if (n.getData(i).containsKey("update")) {
				Nikitaset ns = new Nikitaset(n.getData(i).getData("update"));
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.createTable(ncmobile, ns, "mobileorder");
				DevActivity.deleteMobileOrder(ncmobile, ns, "mobileorder");

				DevActivity.insertTable(ncmobile, ns, "mobileorder");
			} else if (n.getData(i).containsKey("delete")) {
				Nikitaset ns = new Nikitaset(n.getData(i).getData("delete"));
				DevActivity.makeMobileOrderlcase(ns);
				DevActivity.deleteMobileOrder(ncmobile, ns, "mobileorder");
			}

			if (context instanceof Activity) {
				viewProgressMsg((Activity) context, "Saving... Order", 60 + ((i + 1) * 20 / fcount));
			}

		}

		nset = Nset.newArray();
		nikitaset = ncmobile.Query("SELECT orderid FROM mobileorder");
		for (int i = 0; i < nikitaset.getRows(); i++) {
			nset.addData(nikitaset.getText(i, 0));
		}
		hashtable = AppNikita.getInstance().getArgsData();// new
															// Hashtable<String,
															// String>();
		hashtable.put("orderid", nset.toJSON());
		hashtable.put("service", service ? "true" : "false");
		hashtable.put("action", "received");
		hashtable.put("nauth", Utility.getSetting(context.getApplicationContext(), "N-AUTH", ""));
		hashtable.put("userid", Utility.getSetting(context.getApplicationContext(), "N-USER", ""));
		n = Nset.readJSON(NikitaInternet.getString(NikitaInternet.postHttp(AppNikita.getInstance().getBaseUrl() + "/mobile.mdownloadorder", hashtable)));
		return nresult;
	}

	private AlertDialog alertDialog = null;

	protected void hideMessage() {
		if (alertDialog != null) {
			if (alertDialog.isShowing()) {
				alertDialog.dismiss();
			}
		}
	}

	private static int fretrycount = 0;
	private static int lastprogres = 0;

	private void fretry() {
		fretrycount++;
		Nset fretry = Nset.readJSON(Generator.getNikitaParameters().getData("INIT-DOWNLOAD-FRETRY").toString());
		showDialog(fretry.containsValue("title") ? fretry.getData("title").toString() : "Download Gagal",
				fretry.containsValue("message") ? fretry.getData("message").toString() : "Ulang Download ?",
				fretry.containsValue("button1") ? fretry.getData("button1").toString() : "OK",
				fretry.containsValue("button2") ? fretry.getData("button2").toString() : "Cancel", "", "fretry", Nset.newObject());
	}

	public void showDialog(String title, String msg, String button1, String button2, String button3, String requestcode, Nset result) {
		hideMessage();

		final Nset data = Nset.newObject();
		data.setData("title", title);
		data.setData("msg", msg);
		data.setData("button1", button1);
		data.setData("button2", button2);
		data.setData("button3", button3);
		data.setData("requestcode", requestcode);
		data.setData("result", result);

		Builder dlg = new AlertDialog.Builder(this);
		dlg.setTitle("");
		if (!data.getData("button1").toString().equals("")) {
			dlg.setPositiveButton(data.getData("button1").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					if (data.getData("requestcode").toString().equals("showdialog")) {

					} else if (data.getData("requestcode").toString().equals("fretry")) {
						Messagebox.showBusyDialog(LoginActivity.this, new Runnable() {
							public void run() {
								viewProgressMsg(LoginActivity.this, "Connecting ...", lastprogres);
								try {
									Random randomGenerator = new Random(new Date().getTime());
									int ts = 1000 * randomGenerator.nextInt(60);
									Thread.sleep(ts <= 15000 ? 15000 : ts);
								} catch (Exception e) {
								}
							};
						}, new Runnable() {
							public void run() {
								Random randomGenerator = new Random(new Date().getTime());
								if (getCurrentNikitaCode().contains("nikita")) {
									fretrycount = 0;
									onClickLogin();
								} else if ((fretrycount >= 3 && randomGenerator.nextInt(60) >= 15) || fretrycount >= 9) {
									fretrycount = 0;
									onClickLogin();
								} else {
									fretry();
								}
							};
						});
					}
				}
			});
		}
		if (!data.getData("button2").toString().equals("")) {
			dlg.setNeutralButton(data.getData("button2").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					if (data.getData("requestcode").toString().equals("showdialog")) {
						if (data.getData("url").toString().startsWith("intent://")) {
							Intent intent = new Intent(data.getData("url").toString().substring(9));
							startActivity(intent);
						} else if (data.getData("url").toString().startsWith("nform://")) {
							Generator.startNikitaForm(LoginActivity.this, data.getData("url").toString().substring(8));
						} else if (data.getData("url").toString().length() >= 3) {
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getData("url").toString()));
							startActivity(intent);
						}
						finish();
					} else if (data.getData("requestcode").toString().equals("expired")) {
						// Intent intent = new Intent(Intent.ACTION_VIEW,
						// Uri.parse(Generator.getNikitaParameters().getData("INIT-APP-DOWNLOAD").toString()));
						// startActivity(intent);
						String appver = Generator.getNikitaParameters().getData("INIT-APP-VERSION").toString();
						DownloadManager.Request r = new DownloadManager.Request(Uri.parse(Generator.getNikitaParameters().getData("INIT-APP-DOWNLOAD").toString()));
						r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Dipo Star Finance" + appver + ".apk");
						r.setMimeType("application/vnd.android.package-archive");
						r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
						DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
						dm.enqueue(r);
					}

				}
			});
		}
		if (!data.getData("button3").toString().equals("")) {
			dlg.setNegativeButton(data.getData("button3").toString(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		}
		dlg.setCancelable(false);
		dlg.setTitle(data.getData("title").toString());
		dlg.setMessage(data.getData("msg").toString());
		alertDialog = dlg.create();
		alertDialog.show();
	}

	private void insertHistoryLogin() {
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		// insert activity 'login' to table
		Nikitaset nset = ncmobile.Query("SELECT name FROM sqlite_master WHERE type='table' AND name='historyactivity'");
		//check table in database
		if (nset.getRows() == 0) {
			ncmobile.Query("CREATE TABLE historyactivity (id INTEGER NOT NULL," + "activity TEXT NOT NULL, " + "datetime TEXT NOT NULL, " + "status TEXT NOT NULL,"
					+ "PRIMARY KEY (id) );");
		}

		String datetime = Utility.getSetting(getApplicationContext(), "TIME-LOGIN", "");
		//if (datetime.equals(null) || datetime == "" || datetime.isEmpty()) {
			ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) VALUES ('login','" + Utility.Now() + "','0')");
			Utility.setSetting(getApplicationContext(), "TIME-LOGIN", Utility.Now());
		//} else {
		//	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//	Calendar now = Calendar.getInstance();
		//	Date lastlogin = new Date();
		//	try {
		//		lastlogin = dateformat.parse(datetime);
		//	} catch (ParseException e) {
		//}

			// if date and month is same
			//if (lastlogin.getDate() == now.getTime().getDate() && (lastlogin.getMonth() + 1) == (now.getTime().getMonth() + 1)) {
				// not insert
			//} else {
				//insert logout activity; data from shared pref
			//	if (Utility.getSetting(getApplicationContext(), "TIME-LOGOUT", "").isEmpty()) {
			//		String dateLogout = (lastlogin.getYear() + 1900) + "-" + (lastlogin.getMonth() + 1) + "-" + lastlogin.getDate() + " 23:59:00";
			//		ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) " + "VALUES ('logout','" + dateLogout + "','0')");
			//	}
			//	ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) VALUES ('login','" + Utility.Now() + "','0')");
			//	Utility.setSetting(getApplicationContext(), "TIME-LOGIN", Utility.Now());
			//}
		//}

	}

	// A year y is represented by the integer y - 1900.
	// A month is represented by an integer from 0 to 11; 0 is January, 1 is
	// February, and so forth; thus 11 is December.
	// A date (day of month) is represented by an integer from 1 to 31 in the
	// usual manner.

	private void deleteHistoryActivity() {
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		// WHERE datetime NOT LIKE '%2016-11-23%' AND status = '1'
		String dateNow = Utility.Now().substring(0, 10);
		ncmobile.Query("DELETE FROM historyactivity WHERE datetime NOT LIKE '%" + dateNow + "%' AND status = '1'");
	}

	private void createPrinterLog() {
		NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
		
		Nikitaset nset = ncmobile.Query("SELECT name FROM sqlite_master WHERE type='table' AND name='printerlog'");
		//check table in database
		if (nset.getRows() == 0) {
			Nikitaset printerlog = ncmobile.Query("CREATE TABLE printerlog ("
					+ "id INTEGER NOT NULL," 
					+ "print_type TEXT NOT NULL, "
					+ "printer_model TEXT NOT NULL, "
					+ "mac_address TEXT NOT NULL, "										
					+ "onoff_status TEXT NOT NULL, "
					+ "battery_status TEXT NOT NULL, "
					+ "remark TEXT NOT NULL, "
					+ "status TEXT NOT NULL,"
					+ "user_name TEXT NOT NULL, "
					+ "datetime TEXT NOT NULL, "
					+ "PRIMARY KEY (id) );");
		}

//		String datetime = Utility.getSetting(getApplicationContext(), "TIME-LOGIN", "");
//		ncmobile.Query("INSERT INTO historyactivity (activity, datetime, status) VALUES ('login','" + Utility.Now() + "','0')");
//		Utility.setSetting(getApplicationContext(), "TIME-LOGIN", Utility.Now());		
	}
}
