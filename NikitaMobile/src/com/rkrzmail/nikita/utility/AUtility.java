package com.rkrzmail.nikita.utility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

 /**
 * created by 13k.mail@gmail.com
 */
public class AUtility {
	 
	public static int KeepRatioW;

	public static String getDate() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

	
 
	public static String getDefaultPath() {
		 return null;
	}

	 

	public static String getDefaultPath(String fname) {
		return getDefaultPath() + fname;
	}

	public static void createFolderAll(String folder) {
		folder = folder.replace("//", "//");
		String[] s = split(folder, "/");
		String path = "";
		for (int i = 0; i < s.length - ((folder.endsWith("/") ? 0 : 1)); i++) {
			if (!s[i].equals("")) {
				path = path + s[i] + "/";
				createFolder(path);
			}
		}
	}

	public static Vector<Vector<String>> splitVector(String original, String separatorcol, String separatorrow) {
		Vector<Vector<String>> nodes = new Vector<Vector<String>>();
		int index = original.indexOf(separatorrow);
		while (index >= 0) {
			nodes.addElement(splitVector(original.substring(0, index), separatorcol));
			original = original.substring(index + separatorrow.length());
			index = original.indexOf(separatorrow);
		}
		nodes.addElement(splitVector(original, separatorcol));
		return nodes;
	}

	public static Vector<String> splitVector(String original, String separator) {
		Vector<String> nodes = new Vector<String>();
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		nodes.addElement(original);
		return nodes;
	}

        public static String concatVector(Vector<String> original, String separator, int start) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = start; i < original.size(); i++) {
                stringBuffer.append(separator).append(original.elementAt(i));
            }
            return stringBuffer.toString();
        }
	public static String[] split(String original, String separator) {
		Vector<String> nodes = splitVector(original, separator);
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);
			}
		}
		nodes.removeAllElements();
		return result;
	}

	 
	public static String getHttpConnection(String stringURL) {
		 
		StringBuffer data = new StringBuffer();
		try {
			URL url = new URL(stringURL);
			URLConnection ucon = url.openConnection();
			ucon.setConnectTimeout(25000);
			InputStream is = ucon.getInputStream();

			int current = 0;
			while ((current = is.read()) != -1) {
				data.append((char) current);
			}

		} catch (Exception e) {
			data.append("Connection Timeout");
		}
		 
		return data.toString();
	}

	public static String repeat(String sString, int iTimes) {
		String output = "";
		for (int i = 0; i < iTimes; i++)
			output = output + sString;
		return output;
	}

	@SuppressWarnings("deprecation")
	public static String getURLenc(String... get) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < get.length; i++) {
			buffer.append((i % 2) == 0 ? get[i] : URLEncoder.encode(get[i]));
		}
		return buffer.toString();
	}

	 
  
	public static final String MD5(final String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return "";
	}

	   

	public static int getInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return 0;
		}
	}

	public static long getLong(String s) {
		try {
			return Long.parseLong(s);
		} catch (Exception e) {
			return 0;
		}
	}

	public static double getDouble(String s) {
		try {
			return Double.parseDouble(s);
		} catch (Exception e) {
			return 0;
		}
	}
  

	public static void createFolder(String folder) {
		buildResource(new String[] { "mkdir", folder });
	}

	private static void buildResource(String[] str) {
		try {
			Process ps = Runtime.getRuntime().exec(str);
			try {
				ps.waitFor();
				 
			} catch (InterruptedException e) {
				 
			}
		} catch (IOException e) {
			 
		}
	}

	public static void removeFolder(String folder) {
		try {
			Process ps = Runtime.getRuntime().exec("rm -rf " + folder);
			try {
				ps.waitFor();
				 
			} catch (InterruptedException e) { }
		} catch (Exception e) {
		}

	}

	public static void deleteFileAll(String folder) {
		deleteFolderAll(new File(folder));
	}

	public static void deleteFolderAll(File dir) {
		try {
			for (File file : dir.listFiles()) {
				if (file.isFile()) {
					file.delete();
				} else {
					deleteFolderAll(file);
					file.delete();
				}
			}
		} catch (Exception e) {
		}
		dir.delete();
	}
        public static String readFile(InputStream is) {
             try {
			byte[] buffer = new byte[1024];
			int length;StringBuffer sb = new StringBuffer();
			while ((length = is.read(buffer)) > 0) {
				sb.append(new String(buffer, 0, length));
			}
                        return sb.toString();
            } catch (Exception e) { }
            return "";
        }
	public static void copyFile(String origin, String destination) {
		try {
			copyFile(new FileInputStream(origin), destination);
		} catch (Exception e) {
		}
	}

	public static void copyFile(InputStream is, String destination) {
		try {
			OutputStream os = new FileOutputStream(destination);

			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {
		}
	}
        public static String getString(String s, String chars) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			if (chars.indexOf(s.charAt(i)) != -1) {
				buf.append(s.charAt(i));
			}
		}
		try {
			return buf.toString();
		} catch (Exception e) {
		}
		return "";
	}
	public static int getNumber(String s) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			if ("01234567890".indexOf(s.charAt(i)) != -1) {
				buf.append(s.charAt(i));
			}
		}
		try {
			return Integer.parseInt(buf.toString());
		} catch (Exception e) {
		}
		return 0;
	}

	public static String insertString(String original, String sInsert, int igroup) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < original.length(); i++) {
			if ((i % igroup) == 0 && igroup != 0 && i != 0) {
				sb.append(sInsert + original.substring(i, i + 1));
			} else {
				sb.append(original.substring(i, i + 1));
			}
		}
		return sb.toString();
	}

	public static String Now() {
		Calendar calendar = Calendar.getInstance();

		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
	}

	public static String formatCurrency(String original) {
		return insertStringMax(insertStringRev(original, ",", 3), " ", 0);
	}

	public static String insertStringMax(String original, String sInsert, int max) {
		StringBuffer sb = new StringBuffer();
		for (int i = original.length(); i < max; i++) {
			sb.append(sInsert);
		}
		sb.append(original);
		return sb.toString();
	}

	public static String insertStringRev(String original, String sInsert, int igroup) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < original.length(); i++) {

			if (((original.length() - i) % igroup) == 0 && igroup != 0 && i != 0) {
				sb.append(sInsert + original.substring(i, i + 1));
			} else {
				sb.append(original.substring(i, i + 1));
			}
		}
		return sb.toString();
	}

	public static String notNull(String s) {
		if (s != null) {
			return s;
		}
		return "";
	}

	public static boolean isExit;

	public static void setExit(boolean isExit) {
		AUtility.isExit = isExit;
	}

	public static boolean isExit() {
		return isExit;
	}

	public static long converDateToLong(String date) {

		SimpleDateFormat df = null;

		if (date.length() == 10) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy");
			}
		} else if (date.length() == 16) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			}
		} else if (date.length() == 19) {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			}
		} else {
			if (date.contains("-")) {
				df = new SimpleDateFormat("dd-MM-yyyy");
			} else {
				df = new SimpleDateFormat("dd/MM/yyyy");
			}
		}

		try {
			Date dt = df.parse(date);
			long hasil = dt.getTime();
			return hasil;
		} catch (Exception e) {
		}

		return 0;
	}

	 
	public static boolean containsChar(String kalimat, char character) {
		boolean isContain = false;
		String charLower = kalimat.toLowerCase();
		char[] charArray = charLower.toCharArray();
		for (int i = 0; i < charLower.length(); i++) {
			if (charArray[i] == character) {
				isContain = true;
				return isContain;
			}
		}
		return isContain;
	}

	 

	 
	public static boolean isContainImages(String path) {
		boolean isThere = false;
		File file = new File(path);
		if (file.exists()) {
			isThere = true;
		} else {
			isThere = false;
		}
		return isThere;
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static void i(String tag, String msg) {
		 
	}

	/**
	 * checking contex application if null application close by system
	 * 
	 * @param activity
	 */
	  
	public static void imagePost(String URL, String fileName) {

	}

	 
	
	public static Vector<LinkedHashMap<String, String>> getRecordWithSeparate(Vector<String> data, String separate){
		Vector<LinkedHashMap<String, String>> v = new Vector<LinkedHashMap<String,String>>();
		for (int i = 0; i < data.size(); i++) {
			if (data.elementAt(i).contains(separate)) {
				LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
				map.put(data.elementAt(i).substring(0, data.elementAt(i).indexOf(separate)), data.elementAt(i).substring(data.elementAt(i).indexOf(separate) + 1));
				v.add(map);
			}
		}
		return v;
	}
	
	public static boolean isNumeric(String str) {
          return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
        }

         
        public static String formatsizeKByte(long i) {
            String sRes=(i/1024+(i==0?0:1))+"";        
            return  formatCurrency(sRes);
        }
        public static String replace(String _text, String _searchStr, String _replacementStr)     {
            StringBuffer sb = new StringBuffer();
            int searchStringPos = _text.indexOf(_searchStr);
            int startPos = 0;
            int searchStringLength = _searchStr.length();
            while (searchStringPos != -1) {
                sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
                startPos = searchStringPos + searchStringLength;
                searchStringPos = _text.indexOf(_searchStr, startPos);
            }
            sb.append(_text.substring(startPos,_text.length()));
            return sb.toString();
        } 
         
        public static HttpResponse doGet(String url)  {
            try{
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				//schemeRegistry.register(new Scheme("https", getSslSocketFactory(), 443));//EasySSLSocketFactory
				 
				HttpParams params = new BasicHttpParams();
				params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
				params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
				params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				
		
				
				params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
				HttpConnectionParams.setConnectionTimeout(params, 0);
				HttpConnectionParams.setSoTimeout(params, 0);
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				 
				ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
				DefaultHttpClient httpClient = new DefaultHttpClient(cm, params);
		
				
			    HttpGet httpget = new HttpGet(url);
			    httpget.setParams(params);
			    HttpResponse response;
			    response = httpClient.execute(httpget);
			    return response;
			}catch (Exception e) {
		    	return null;
			}
	 
        }
        public static HttpResponse doPost(String url, String param)  {
            try{
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				//schemeRegistry.register(new Scheme("https", getSslSocketFactory(), 443));//EasySSLSocketFactory
				 
				HttpParams params = new BasicHttpParams();
				params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
				params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
				params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				
		
				
				params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
				HttpConnectionParams.setConnectionTimeout(params, 0);
				HttpConnectionParams.setSoTimeout(params, 0);
				HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
				 
				ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
				DefaultHttpClient httpClient = new DefaultHttpClient(cm, params);
		
				
			    HttpGet httpget = new HttpGet(url);
			    httpget.setParams(params);
			    HttpResponse response;
			    response = httpClient.execute(httpget);
			    return response;
			}catch (Exception e) {
		    	return null;
			}
	 
        }
}
