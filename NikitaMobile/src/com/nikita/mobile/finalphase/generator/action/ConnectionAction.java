package com.nikita.mobile.finalphase.generator.action;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.ui.DateUI;
import com.nikita.mobile.finalphase.generator.ui.TableGridUI;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class ConnectionAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        String conn = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
        String xsql = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()); //query 
        String page = currdata.getData("args").getData("param4").toString();  
        
        String data = currdata.getData("args").getData("param3").toString();  
        String result = currdata.getData("args").getData("result").toString();  
        
        String x = xsql;
        
        Nset ns =   ConnectionAction.parseNikitaConnection(comp,  xsql);
        xsql= ns.getData("query").toString();
        conn= ns.getData("conn").toString();    
        data= ns.getData("args").toString();  
         
        //System.out.println(data);
        
        if (code.equals("query")) {
            NikitaConnection nc = NikitaConnection.getConnection(conn.equals("")?"default":conn);
            
            Nset nargs   =  Nset.readJSON(data);
            String[] arg = new String[nargs.getArraySize()<=0?0:nargs.getArraySize()];
            for (int i = 0; i < arg.length; i++) {
                arg[i] = nargs.getData(i).toString();
            }
            
            Nikitaset n;              
            Component c = comp.getNikitaComponent().getComponent(page);
            page = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString());
            if (c instanceof TableGridUI) {
                    if (comp.getNikitaComponent().getParameter("component").equals(c.getId())) {
                        //((TableGridUI)c).prepareFilter(comp.getNikitaComponent().getParameter("action"));
                    }                    
                    ((TableGridUI)c).setOnFilterClickListener(  ((TableGridUI)c).getCurrentPage()!=-1?((TableGridUI)c).getCurrentPage():1, ((TableGridUI)c).getShowPerPage()>=1? ((TableGridUI)c).getShowPerPage():10);
                     n = nc.QueryPage( ((TableGridUI)c).getCurrentPage() ,((TableGridUI)c).getShowPerPage(), xsql, arg.length!=0?arg:null);
                    //n = nc.QueryPage( 0, 0, xsql, arg.length!=0?arg:null);
                    
                    comp.getNikitaComponent().refreshComponent(c);
            }else if(!page.trim().equals("")){
                
                if (page.contains(",")) {
                    n = nc.QueryPage( Utility.getInt(page.substring(0, page.indexOf(","))),Utility.getInt(page.substring(page.indexOf(",")+1)), xsql, arg.length!=0?arg:null);
                }else if (Utility.isNumeric(page)) {
                    n = nc.QueryPage(1, Utility.getInt(page) , xsql, arg.length!=0?arg:null);
                }else{
                    n = nc.QueryPage(1,10, xsql, arg.length!=0?arg:null);
                }
            }else{
                n = nc.Query(xsql, arg.length!=0?arg:null);
                if (c instanceof TableGridUI) {
                     //((TableGridUI)c).setOnFilterClickListener(null,-1,-1);
                     //comp.getNikitaComponent().refreshComponent(c);
                }
            }
            
            //add for database            
            for (int i = 0; i < n.getCols(); i++) {
            	comp.getNikitaComponent().setVirtual("!"+n.getHeader(i), n);                
            }
           //26-06-2016
            if (n.getInfo() instanceof Nset) {
                Nset vn = ((Nset)n.getInfo()) ;                    
                if (vn.containsKey("metadata") && vn.getData("metadata").containsKey("name")) {
                    Nset nAS = vn.getData("metadata").getData("name");
                    for (int i = 0; i < nAS.getSize(); i++) {
                        String name = nAS.getData(i).toString();
                        if (!name.equals("")) {
                        	comp.getNikitaComponent().setVirtual("!"+name, n);
                        }                        
                    }                     
                }               
             }               
            
            comp.getNikitaComponent().setVirtual(result, n);
        }  else if (code.equals("rollback")) {           
        	NikitaConnection.getConnection(conn).rollback();
        }  else if (code.equals("begin")||code.equals("setsavepoint")||code.equals("savepoint")||code.equals("setpoint")) {
        	NikitaConnection.getConnection(conn).setSavepoint();
        }  else if (code.equals("commit")) {  
        	NikitaConnection.getConnection(conn).commit();
        }  else if (code.equals("close")) {  
        	NikitaConnection.getConnection(conn).closeConnection();    
        }
			 
		return true;
	}

	private int getQueryArgs(String sql) { 
        int count = 0;
        for(int i=0;i<sql.length();i++) {
            if(sql.charAt(i)=='?'){
                count++;                            
            }
        }
        return count;              
    }
    
    public static Nset parseNikitaConnection(Component comp, String nc){
        if (nc.startsWith("{")) {
            return parseNikitaConnection(comp, Nset.readJSON(nc));
        }else{
            Nset nret =  Nset.newObject();
            nret.setData("sql", nc);
            nret.setData("query", nc);
            nret.setData("conn", "default");        
            return nret;
        }
    }
    public static Nset parseNikitaConnection(Component comp, Nset n){
    	Nset nret =  Nset.newObject();
        Nset narg =  Nset.newArray();
        Nset nals =  Nset.newArray();
        StringBuffer sb = new StringBuffer();
 
        if (n.getData("dbmode").toString().equals("select")) {   
            Nset f = Nset.readJSON( n.getData("fields").toString());
            Nset order = Nset.readJSON( n.getData("orderby").getData("orderbys").toString());
            
            sb.append("SELECT ");
            for (int i = 0; i < f.getArraySize(); i++) {
                 sb.append(i>=1?",":"").append(f.getData(i).toString());
            }
            sb.append(f.getArraySize()>=1?"":"*");
            sb.append(" FROM ").append(n.getData("tbl").toString()).append(" ");      
            
            if (n.getData("where").getData("type").toString().equals("1")) {
                if (!n.getData("where").getData("logic").toString().equals("0")) {
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    sb.append(n.getData("where").getData("logic").toString().equals("1")?" OR ":" AND ").append(   n.getData("where").getData("param").getData("parameter2").toString() ).append("=? ");

                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter2").toString()) );
                    
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter1").toString());
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter2").toString());
                }else{
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter1").toString());
                 }
            }else if (n.getData("where").getData("type").toString().equals("2")) {
                sb.append(" WHERE ").append(n.getData("where").getData("sqlwhere").toString());
                int i = n.getData("argswhere").getObjectKeys().length;
                for (int j = 0; j < i; j++) {
                     narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                     nals.addData(  ( n.getData("argswhere").getData(""+j).toString() ) );
                }
            }
            
            if(!n.getData("orderby").getData("conditionorders").toString().equals("3")){
                if(order.getArraySize() >= 1){
                    sb.append(" ORDER BY ");
                    for (int i = 0; i < order.getArraySize(); i++) {
                         sb.append(i>=1?",":"").append(order.getData(i).toString());
                    }                
                }
                if(n.getData("orderby").getData("conditionorders").toString().equals("1"))
                    sb.append(" ASC ");
                if(n.getData("orderby").getData("conditionorders").toString().equals("2"))
                    sb.append(" DESC ");
            }else
                sb.append(" ORDER BY ").append(n.getData("orderby").getData("customs").toString());
                
            
            nret.setData("sql", sb.toString());
            nret.setData("query", sb.toString());
            nret.setData("args", narg);
            nret.setData("argsname", nals);
        }else if (n.getData("dbmode").toString().equals("delete")) {
            Nset f = Nset.readJSON( n.getData("fields").toString());
            sb.append("DELETE FROM ").append(n.getData("tbl").toString()).append(" ");      
            
            if (n.getData("where").getData("type").toString().equals("1")) {
                if (!n.getData("where").getData("logic").toString().equals("0")) {
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    sb.append(n.getData("where").getData("logic").toString().equals("1")?" OR ":" AND ").append(   n.getData("where").getData("param").getData("parameter2").toString() ).append("=? ");

                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter2").toString()) );
                    
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter1").toString());
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter2").toString());
                }else{
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData( "parameter1" ).toString()) );
                    
                    nals.addData(n.getData("where").getData("paramargs").getData("parameter1").toString());
                }
            }else if (n.getData("where").getData("type").toString().equals("2")) {
                sb.append(" WHERE ").append(n.getData("where").getData("sqlwhere").toString());
                int i = n.getData("argswhere").getObjectKeys().length;
                for (int j = 0; j < i; j++) {
                    narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                    nals.addData( ( n.getData("argswhere").getData(""+j).toString() ) );
                }
            }
        
            nret.setData("sql", sb.toString());
            nret.setData("query", sb.toString());
            nret.setData("args", narg);
            nret.setData("argsname", nals);
        }else if (n.getData("dbmode").toString().equals("insert")) {
            sb.append("INSERT INTO ").append(n.getData("tbl").toString()).append(" ( ");    
            String[] keys = n.getData("args").getObjectKeys();
            
            for (int j = 0; j < keys.length; j++) {
                 sb.append(j>=1?",":"").append(keys[j]).append("");
            }
            
             sb.append(" )VALUES ( ");    
             for (int j = 0; j < keys.length; j++) {
                if (  n.getData("args").getData(keys[j]).toString().endsWith("(todate)")) {
                    sb.append(j>=1?",":"").append("TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss')");
                }else{
                    sb.append(j>=1?",":"").append("?");
                }                
                if (  n.getData("args").getData(keys[j]).toString().startsWith("$") &&  !n.getData("args").getData(keys[j]).toString().contains(")") &&  (comp.getNikitaComponent().getComponent(  n.getData("args").getData(keys[j]).toString() ) instanceof DateUI)  ) {
                    narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("args").getData (keys[j]).toString()+"(datetime)" ) );
                    nals.addData(  comp.getNikitaComponent().getVirtualString( n.getData("args").getData (keys[j]).toString()+"(datetime)" ) );
                }else{ 
                    narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("args").getData(keys[j]).toString() ) );
                    nals.addData(  ( n.getData("args").getData(keys[j]).toString() ) );
                }
            }
             sb.append(" ) ");  
             
 
            
            nret.setData("sql", sb.toString());
            nret.setData("query", sb.toString());
            nret.setData("args", narg);
            nret.setData("argsname", nals);
        }else if (n.getData("dbmode").toString().equals("update")) {
         
            sb.append("UPDATE ").append(n.getData("tbl").toString()).append(" ");    
            String[] keys = n.getData("args").getObjectKeys();
            
            for (int j = 0; j < keys.length; j++) {
                if (  n.getData("args").getData(keys[j]).toString().endsWith("(todate)")) {
                    sb.append(j>=1?",":"SET ").append(keys[j]).append("=TO_DATE(?, 'yyyy/mm/dd hh24:mi:ss') ");
                }else{
                    sb.append(j>=1?",":"SET ").append(keys[j]).append("=? ");
                } 
                if (  n.getData("args").getData(keys[j]).toString().startsWith("$") &&  !n.getData("args").getData(keys[j]).toString().contains(")") &&  (comp.getNikitaComponent().getComponent(  n.getData("args").getData(keys[j]).toString() ) instanceof DateUI)  ) {
                    narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("args").getData (keys[j]).toString()+"(datetime)" ) );
                    nals.addData(  ( n.getData("args").getData (keys[j]).toString()+"(datetime)" ) );
                }else{ 
                    narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("args").getData(keys[j]).toString() ) );
                    nals.addData(  ( n.getData("args").getData(keys[j]).toString() ) );
                }
            }
            
            if (n.getData("where").getData("type").toString().equals("1")) {
                if (!n.getData("where").getData("logic").toString().equals("0")) {
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    sb.append(n.getData("where").getData("logic").toString().equals("1")?" OR ":" AND ").append(   n.getData("where").getData("param").getData("parameter2").toString() ).append("=? ");

                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter2").toString()) );
                    
                    nals.addData( ( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    nals.addData( ( n.getData("where").getData("paramargs").getData("parameter2").toString()) );
                }else{
                    sb.append(" WHERE  ").append(   n.getData("where").getData("param").getData("parameter1").toString() ).append("=? ");
                    narg.addData( comp.getNikitaComponent().getVirtualString( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    
                    nals.addData( ( n.getData("where").getData("paramargs").getData("parameter1").toString()) );
                    
                }
            }else if (n.getData("where").getData("type").toString().equals("2")) {
                sb.append(" WHERE ").append(n.getData("where").getData("sqlwhere").toString());
                int i = n.getData("argswhere").getObjectKeys().length;
                for (int j = 0; j < i; j++) {
                     narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                     nals.addData(  ( n.getData("argswhere").getData(""+j).toString() ) );
                }
            }
     
            
            nret.setData("sql", sb.toString());
            nret.setData("query", sb.toString());
            nret.setData("args", narg);
            nret.setData("argsname", nals);
        }else if (n.getData("dbmode").toString().equals("query")) {
            if (n.getData("sql").toString().equals("?")) {
                int i = 1;// n.getData("argswhere").getObjectKeys().length;
                for (int j = 0; j < i; j++) {
                     narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                    nret.setData("sql",  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() )   );
                    nret.setData("query",  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );   
                    nret.setData("args", narg);
                    nret.setData("argsname", nals);
                }
            }else if (n.getData("sql").toString().startsWith("@")) {
                nret.setData("sql", comp.getNikitaComponent().getVirtualString(  n.getData("sql").toString()  ));
                nret.setData("query", comp.getNikitaComponent().getVirtualString(   n.getData("sql").toString() ));
                nret.setData("args", narg);
                nret.setData("argsname", nals);
            }else{  
                int i = n.getData("argswhere").getObjectKeys().length;
                for (int j = 0; j < i; j++) {
                     narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                     nals.addData(  ( n.getData("argswhere").getData(""+j).toString() ) );
                }


                nret.setData("sql",  n.getData("sql").toString());
                nret.setData("query",  n.getData("sql").toString());
                nret.setData("args", narg);
                nret.setData("argsname", nals);
            }           
        }else if (n.getData("dbmode").toString().equals("call")) {
        
            int i = n.getData("argswhere").getObjectKeys().length;
            for (int j = 0; j < i; j++) {
                 narg.addData(  comp.getNikitaComponent().getVirtualString( n.getData("argswhere").getData(""+j).toString() ) );
                 nals.addData(  ( n.getData("argswhere").getData(""+j).toString() ) );
            }
//            System.err.println( n.getData("callz").toString());
            nret.setData("callz",  n.getData("callz").toString());
            nret.setData("call",  n.getData("callz").toString());
            nret.setData("args", narg);
            nret.setData("argsname", nals);
        }else{//query
            nret.setData("sql", n.getData("sql").toString());
            nret.setData("query",  n.getData("sql").toString());
        }
        nret.setData("conn", n.getData("conn").toString());
       
          //System.out.println( n.getData("sql").toString());
          //  System.out.println(narg.toJSON()); 
       
        return nret;
    }

}
