package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;

import com.nikita.mobile.finalphase.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

public class TemplatePrinter {
    private String company;
    private String template;
    private Activity activity;
    private int paper;
    public TemplatePrinter(Activity activity,String company,int paper){
        this.company=company;
        this.activity=activity;
        this.paper=paper;
        if(paper>2){
        	this.template="SURAT JANJI BAYAR\n"
        			+ " Yang bertanda tangan dibawah\n"+
        			" ini : \n"+
                    " Nama         : [nama]\n"+
                    " No.Perjanjian: [noper]\n"+
                    " Jumlah Bayar : [jumlah]\n"+
                    " Angsuran-ke  : [angsuran] \n"+
                    " Tanggal Bayar: [tglbayar]\n"+
                    " Menyatakan dengan sebenar\n"+
                    " -benarnya hal-hal berikut\n"+
                    " dibawah ini : \n"+
                    " 1.Bahwa saya akan melakukan \n"+
                    "    pembayaran Sewa/angsuran\n"+
                    "    kendaran sejumlah tersebut \n"+
                    "    diatas selambat-lambatnya \n"+
                    "    pada tanggal \n"+ 
                    "    [tgltempo]\n"+
                    " 2.Bahwa bila saya tidak dapat \n"+ 
                    "    melakukan pembayaran sampai\n"+ 
                    "    dengan tanggal tersebut \n"+
                    "    maka saya akan menyerahkan\n"+
                    "    kendaraan kepada \n"+
                    "    [company] \n"+
                    "    atau karyawannya yang \n"+
                    "    ditugaskan untuk mengambil\n"+
                    "    kendaraan. \n"+
                    "  Demikian surat pernyataan ini\n"+
                    "  saya buat dengan sebenarnya\n"+ 
                    "  tanpa paksaan ataupun tekanan\n"+
                    "  dari pihak manapun. \n"+
                    " [tempat] , [tgl] \n"+
                    "\n"+
                    " [konsumen] Printed By [fc]\n"+
                    " |bottomsp| [branch]\n";
    	}else{
    		this.template="SURAT JANJI BAYAR\n"
    				+ "Yang bertanda tangan dibawah ini : \n"+
                    "Nama         : [nama]\n"+
                    "No.Perjanjian: [noper]\n"+
                    "Jumlah Bayar : [jumlah] \n"+
                    "Angsuran-ke  : [angsuran] \n"+
                    "Tanggal Bayar: [tglbayar]\n"+
                    "Menyatakan dengan sebenar-benarnya hal-\n"+ 
                    "hal berikut dibawah ini : \n"+
                    "1. Bahwa saya akan melakukan pembayaran\n"+
                    "   Sewa/angsuran kendaran sejumlah\n" +
                    "   tersebut diatas selambat-lambatnya\n" +
                    "   pada tanggal [tgltempo]\n"+
                    "2. Bahwa bila saya tidak dapat melakukan\n" +
                    "   pembayaran sampai dengan tanggal \n" +
                    "   tersebut maka saya akan menyerahkan \n" +
                    "   kendaraan kepada [company] \n" +
                    "   atau karyawannya yang ditugaskan untuk\n" +
                    "   mengambil kendaraan. \n \n"+
                    "Demikian surat pernyataan ini saya buat dengan\n " +
                    "sebenarnya tanpa paksaan ataupun tekanan dari \n"
                    + "pihak manapun. \n"+
                    "[tempat] , [tgl] \n"+
                    "\n"
                    + "\n"
                    + "\n"+
                    "[konsumen]     Printed By [fc]\n"+
                    "|bottomsp|		[branch]\n";
    	}
    }
    public String getSJBTitle(){
    	return "Surat Janji Bayar\n";
    }
    public String generateSJBResult(HashMap<String, String> params){
        if(params.containsKey("nama")){
        	template=template.replace("[nama]", params.get("nama"));
        }else{
        	template=template.replace("[nama]", "-");
        }
        if(params.containsKey("noper")){
        	template=template.replace("[noper]", params.get("noper"));
        }else{
        	template=template.replace("[noper]", "-");
        }
        if(params.containsKey("jumlah")){
        	template=template.replace("[jumlah]", params.get("jumlah"));
        }else{
        	template=template.replace("[jumlah]", "-");
        }
        if(params.containsKey("angsuran")){
        	template=template.replace("[angsuran]", params.get("angsuran"));
        }else{
        	template=template.replace("[angsuran]", "-");
        }
        if(params.containsKey("tglbayar")){
        	template=template.replace("[tglbayar]", params.get("tglbayar"));
        }else{
        	template=template.replace("[tglbayar]", "-");
        }
        if(params.containsKey("tgltempo")){
        	template=template.replace("[tgltempo]", params.get("tgltempo"));
        }else{
        	template=template.replace("[tgltempo]", "-");
        }
        if(params.containsKey("tgl")){
        	template=template.replace("[tgl]", params.get("tgl"));
        }else{
        	template=template.replace("[tgl]", "-");
        }
        if(params.containsKey("tempat")){
        	template=template.replace("[tempat]", params.get("tempat"));
        }else{
        	template=template.replace("[tempat]", "-");
        }
        if(params.containsKey("fc")){
        	template=template.replace("[fc]", params.get("fc"));
        }else{
        	template=template.replace("[fc]", "-");
        }
        int konsumenLength=10;
        if(params.containsKey("konsumen")){
        	konsumenLength=params.get("konsumen").length();
        	template=template.replace("[konsumen]", params.get("konsumen"));
        }else{
        	konsumenLength=1;
        	template=template.replace("[konsumen]", "-");
        }
        String spaceBottom="";
        for(int i=0;i<konsumenLength;i++){
        	spaceBottom+=" ";
        }
        template=template.replace("[company]", company);
        template=template.replace("|bottomsp|", spaceBottom);
        if(params.containsKey("branch")){
        	template=template.replace("[branch]", params.get("branch"));
        }else{
        	template=template.replace("[branch]", "-");
        }
        return template;
    }
}
