package com.nikita.mobile.finalphase.generator;

import java.util.Vector;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.action.BluetoothPrintAction;
import com.nikita.mobile.finalphase.generator.action.SystemAction;
import com.nikita.mobile.finalphase.generator.ui.FileUploadUI;
import com.nikita.mobile.finalphase.generator.ui.ReceiverUI;
import com.nikita.mobile.finalphase.printer.GlobalPrinter;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class NikitaActivity extends Activity{
	private boolean isgenerator =false;
	private NikitaControler nikitaComponent;
	private View vRoot ;
	public GlobalPrinter printer;
	private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
        	//Log.i("NikitaActivity-BroadcastReceiver-onReceive", "onReceive");
        	 
        	if (nikitaComponent!=null && nikitaComponent.getContent()!=null) {
        		for (int i = 0; i < nikitaComponent.getContent().getComponentCount(); i++) {
					if (nikitaComponent.getContent().getComponent(i) instanceof ReceiverUI) {
						String receiver = nikitaComponent.getContent().getComponent(i).getText();
						String action = intent.getAction()!=null?intent.getAction():"";
						String uuid = intent.getStringExtra("uuid")!=null?intent.getStringExtra("uuid"):"";
						String provider = intent.getStringExtra("provider")!=null?intent.getStringExtra("provider"):"";
						String message = intent.getStringExtra("message")!=null?intent.getStringExtra("message"):"";
						String status = intent.getStringExtra("status")!=null?intent.getStringExtra("status"):"";
						String data = intent.getStringExtra("data")!=null?intent.getStringExtra("data"):"";
						if (receiver.equals(action)||receiver.equals(uuid)||receiver.equals(provider)||receiver.equals(action)) {
							Nset result = Nset.newObject();
							result.setData("uuid", uuid);
							result.setData("provider", provider);
							result.setData("action", action);
							result.setData("message",message);
							result.setData("status",status);
							result.setData("data", data);							
							nikitaComponent.setVirtual("@+RESULT", result);
							nikitaComponent.runOnActionThread(new Runnable() {
								Component component;
								public Runnable get(Component component){
									this.component=component;
									return this;
								}
								public void run() {
									component.runRoute();
								}
							}.get(nikitaComponent.getContent().getComponent(i)));							 
						}
					}
				}
        		
        		
			}
            //Toast.makeText(getApplicationContext(), intent.getStringExtra("data")!=null?intent.getStringExtra("data"):"", Toast.LENGTH_SHORT).show();
        }
    }; 
    
    public boolean dispatchKeyEvent(android.view.KeyEvent event) {
    	AppNikita.setUserInteraction();
    	return super.dispatchKeyEvent(event);
    }
    public boolean dispatchTouchEvent(MotionEvent ev) {
    	AppNikita.setUserInteraction();
    	return super.dispatchTouchEvent(ev);
    }
    private Dialog dialog; 
    public void showDialog(Dialog dialog){
    	if (this.dialog!=null && this.dialog.isShowing()) {
    		this.dialog.dismiss();
		}
    	this.dialog=dialog;
    	this.dialog.show();
    }
    protected void onStart() {
		 IntentFilter filter = new IntentFilter();
	     filter.addAction("com.nikita.generator");
	     filter.addAction("com.nikita.nikitanr");
	     registerReceiver(receiver, filter);        
	     super.onStart();    	
    };
   
   protected void onStop() {
		unregisterReceiver(receiver);
	   	super.onStop();
	   	if (dialog!=null && dialog.isShowing()) {
	   		dialog.dismiss();
		}
   } 
   protected void onDestroy() {
	   super.onDestroy();
   } 
	protected void onResume() {
		if (nikitaComponent!=null && getIntent()!=null && nikitaComponent.isNikitaActivityOwner(getIntent().getStringExtra("uuid"))) {
		}else{
			finish(); 
		}
		super.onResume();
	}
	
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);//uithread
		vRoot = Utility.getInflater(this, R.layout.nformactivity);
		setContentView(vRoot);		
		AppNikita.getInstance().setCurrentNikitaActivity(this);
		printer=AppNikita.getInstance().printer;		
		printer.setActivity(this);
		if (getIntent()!=null && getIntent().getStringExtra("nfid")!=null && getIntent().getStringExtra("nfid").equals("nikitaform")) {
			String formname = getIntent().getStringExtra("nikitaformname");
			nikitaComponent = AppNikita.getInstance().getNikitaComponent(formname);
			if (nikitaComponent!=null) {
				nikitaComponent.setNikitaControlerID(getIntent().getStringExtra("uuid"));
			}
		}
			
		if (vRoot.findViewById(R.id.frmBusy)!=null) {
			vRoot.findViewById(R.id.frmBusy).setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {}
			});
		}
		
		

		if (nikitaComponent!=null) {			 
			nikitaComponent.setActivity(this, vRoot);
			AppNikita.getInstance().setCurrentNikitaControler(nikitaComponent);//mybe fia showform
			
			showBusy(true);			
			if (nikitaComponent.isLoaded()) {
				 
				if (nikitaComponent.isBusy()) {
					showBusy(true);
				}else{
					showBusy(false);
				}
				 
				nikitaComponent.onCreateUI();	
				UtilityAndroid.setHeaderBackground(nikitaComponent.getActivity(), findViewById(R.id.frmHeaderBG));	
				
			}else{
				nikitaComponent.setOnLoadListener(new Runnable() {
					public void run() {							 
						if (nikitaComponent.isBusy()) {
							showBusy(true);
						}else{
							showBusy(false);
						}				
						 
						nikitaComponent.onCreateUI();	
						UtilityAndroid.setHeaderBackground(nikitaComponent.getActivity(), findViewById(R.id.frmHeaderBG));	
						
					}
				});			 				
			}
		}
		
		//getActionBar().setTitle("ss");
	}	
	
	 
	
	public void showBusy(boolean show){
		vRoot.findViewById(R.id.frmBusy).setVisibility(show?View.VISIBLE:View.GONE);
	}
	public Activity getActivity(){
		return this;//framgment support
	}
	public NikitaControler getNikitaComponent(){
		return nikitaComponent; 
	}
	public void setNikitaComponent(NikitaControler nikitaComponent){
		this.nikitaComponent=nikitaComponent; 
	}
	
	public interface OnActivityResultListener{
		public void onActivityResult(int requestCode, int resultCode, Intent data) ;
	}
	private Vector<OnActivityResultListener> activityResultListeners = new Vector<OnActivityResultListener>();
	public void addOnActivityResultListener(OnActivityResultListener activityResultListener){
		activityResultListeners.addElement(activityResultListener);
	}
	
	protected void onActivityResult(int requestCode,final  int resultCode, Intent data) {
		for (int i = 0; i < activityResultListeners.size(); i++) {
			if (activityResultListeners.elementAt(i)!=null) {
				activityResultListeners.elementAt(i).onActivityResult(requestCode, resultCode, data);
			}
		}
		
		String sRequesCode = requestCode+"";
		if (requestCode==SystemAction.RESULT_NATIVE_CALL) {
			sRequesCode = "NATIVE";
		}else if (requestCode==BluetoothPrintAction.RESULT_BLUETOOTH_SCAN) {
			sRequesCode = "BLUETOOTH";
		}else if (requestCode==FileUploadUI.RESULT_CAMERA_CAPTURE) {
			sRequesCode = "CAMERA";
		}else if (requestCode==FileUploadUI.RESULT_FILE_BROWSER) {
			sRequesCode = "FILE";
		}
		Nset v = Nset.newObject();
		
		AppNikita.getInstance().setVirtual("@+NATIVE-REQUESTCODE", requestCode);
		if (data!=null) {
			AppNikita.getInstance().setVirtual("@+NATIVE-RESULT", data);
		}else{
			AppNikita.getInstance().removeVirtual("@+NATIVE-RESULT");
		}
		
	 
		if (data!=null && data.getExtras()!=null) {			
			String[] keys = data.getExtras().keySet().toArray(new String[data.getExtras().keySet().size()]);
			for (int i = 0; i < keys.length; i++) {
				if (data.getExtras().get(keys[i]) instanceof String) {
					v.setData(keys[i], data.getExtras().getString(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Integer) {
					v.setData(keys[i], data.getExtras().getInt(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Long) {
					v.setData(keys[i], data.getExtras().getLong(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Boolean) {
					v.setData(keys[i], data.getExtras().getBoolean(keys[i]) );
				}else if (data.getExtras().get(keys[i]) instanceof Double) {
					v.setData(keys[i], data.getExtras().getDouble(keys[i]) );
				}else{
				
				}				
			}			
		}		
		final Nset nResult = v;
		final String requestcode = sRequesCode;
		if (nikitaComponent!=null) {
			nikitaComponent.runOnActionThread(new Runnable() {
				public void run() {
					nikitaComponent.onResult(requestcode, resultCode+"", nResult);	
				}
			});
		}
	 
		
	}
		
	public void onBackPressed() {
		if (nikitaComponent!=null) {
			nikitaComponent.runOnActionThread(new Runnable() {
				public void run() {
					nikitaComponent.onBack();			
				}
			});
		}else{
			super.onBackPressed();
		}		
	}
   
	protected void onSaveInstanceState(final Bundle outState) {
		if (nikitaComponent!=null) {
			nikitaComponent.runOnActionThread(new Runnable() {
				public void run() {
					nikitaComponent.onSaveState();			
				}
			});
		}
	}

	public Component getComponentGenerator(String key) {
		if (nikitaComponent!=null) {
			return nikitaComponent.getComponent(key);
		}
		return null;
	}
	 
}
