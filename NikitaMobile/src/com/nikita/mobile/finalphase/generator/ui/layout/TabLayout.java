package com.nikita.mobile.finalphase.generator.ui.layout;

 
 
 
 
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
 

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent; 
 
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.nikita.mobile.finalphase.component.ComponentGroup;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NFormActivity;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.service.FlowLayout;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;
import com.viewpagerindicator.LinePageIndicator;
import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.TitlePageIndicator;

public class TabLayout extends ComponentGroup{

	public TabLayout(NikitaControler form) {
		super(form);
	}


	private View view;
	 
	public View onCreate(NikitaControler form) {
		
		return onCreateFragment(form);
	}
	
	public View onCreateFlow(NikitaControler form) {		
		view=Utility.getInflater(form.getActivity(), R.layout.comptab);
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		//view.findViewById(R.id.lnrtabcnt).getLayoutParams().width = view.getContext().getResources().getDisplayMetrics().widthPixels; 
		FlowLayout tbl = (FlowLayout) view.findViewById(R.id.lnrtabtbl);
		LinearLayout cnt = (LinearLayout) view.findViewById(R.id.lnrtabcnt);
		for (int i = 0; i < getComponentCount(); i++) {
			View child = getComponent(i).onCreate(getNikitaComponent());	
			child.setVisibility(View.GONE);
			cnt.addView(child, Utility.getMarginLayoutParams(child) );	
			
			
			Button btn = new Button(form.getActivity());
			btn.setText(getComponent(i).getText());
			btn.setLayoutParams(new FlowLayout.LayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
			tbl.addView(btn );
			
			btn.setTag(i+"");
			btn.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) { 
					try {
						for (int j = 0; j < getComponentCount(); j++) {
							getComponent(j).getView().setVisibility(View.GONE);
						}
						
						getComponent(Utility.getInt( (String)v.getTag() )  ).getView().setVisibility(View.VISIBLE);
					} catch (Exception e) { }
				}
			});
		}
		
		
		
		
		
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		 
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}
	 
	public View onCreateFragment(NikitaControler form) {			
		view=Utility.getInflater(form.getActivity(), R.layout.comptabfragment);
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, convertPixel("240dp")));
		   
		android.widget.FrameLayout frameLayout = (android.widget.FrameLayout) view.findViewById(R.id.indicator);		
		
		// Instantiate a ViewPager and a PagerAdapter.
		ViewPager mPager = (ViewPager) view.findViewById(R.id.pager);
		PagerAdapter mPagerAdapter = new android.support.v13.app.FragmentPagerAdapter(form.getActivity().getFragmentManager()) {
			
			public int getCount() {				
				return TabLayout.this.getComponentCount();
			}


			public Fragment getItem(final int arg0) {
				return new Fragment(){
					public Fragment get(int p){
						pos=p;								 
						return this;
					}
					private int pos ;					
					public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
						View child = getComponent(pos).onCreate(getNikitaComponent());	
						return child;
					}					   
			
	     			public void onDestroyView() {
	     				getComponent(pos).onSaveState();	     				
	     				try {
	     					if (getActivity()!=null && !getActivity().isFinishing()) {
	     						if (getFragmentManager()!=null) {
	     							getFragmentManager().beginTransaction().remove(this).commit();
								}
							}
						} catch (Exception e) { }	     				
	     				super.onDestroyView();
	     				
	     			}
				}.get(arg0);				 
			}
			
			public CharSequence getPageTitle(int position) {
				return getComponent(position).getText();
			}
		}; 

				
        mPager.setAdapter(mPagerAdapter);
     
        TabPageIndicator tabPageIndicator =new TabPageIndicator(view.getContext());
        frameLayout.addView(tabPageIndicator);
        tabPageIndicator.setViewPager(mPager);	
                
        
        mPager.setCurrentItem(Utility.getInt(super.getText()));	
		
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		 
		
		refreshViewOnUI();
		refreshStyleOnUI();
		return view;
	}

 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		
		View v = view.findViewById(R.id.lblLabel);
		if (v instanceof TextView) {
			((TextView)v).setText(getLabel());
		}
 
		v = view.findViewById(R.id.incLabels);
		if (v instanceof View) {
			v.setVisibility(getLabel().equals("")?View.GONE:View.VISIBLE);
		}
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
	}

	public void onSaveState() {
		for (int i = 0; i < getComponentCount(); i++) {
			getComponent(i).onSaveState();
		}
		
		if (view!=null) {
			View v = view.findViewById(R.id.pager);
			if (v instanceof ViewPager ) {
				ViewPager mPager = (ViewPager)v;
				super.setText(mPager.getCurrentItem()+"");
			}		
			
		}
	}
	
 

}
