package com.nikita.mobile.finalphase.component;

import java.util.Vector;

import android.view.View;

import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.rkrzmail.nikita.data.Nset;

public class ComponentGroup extends Component{

	public ComponentGroup(NikitaControler form) {
		super(form);
		 
	}
	

	protected Vector<Component> components = new Vector<Component>();
    

    
    public void restoreData(Nset data) {
        
    }   
 
   
    public void addComponent(Component component){
        component.setParent(this);        
        components.addElement(component);
        if (getForm()!=null) {
            component.setForm(getForm());
        }
    }
    
    public Component getComponent(int i){
        return components.elementAt(i);
    }
    
    public int getComponentCount(){
        return components.size();
    }

    public void removeComponent(int i){
        try {
            components.removeElementAt(i);
        } catch (Exception e) {}
    }
    public void setComponentAt(Component comp, int i){
        try {
            components.setElementAt(comp, i);
        } catch (Exception e) {}
    }
    public void removeAllComponents(){
         components.removeAllElements();
    }
    
    @Override
    public void setForm(Component formid) {
        super.setForm(formid);
        for (int i = 0; i < getComponentCount(); i++) {
             getComponent(i).setForm(formid);
        }
    }
    public void setInstanceId(String newinst) {
        super.setInstanceId(newinst);
        for (int i = 0; i < getComponentCount(); i++) {
             getComponent(i).setInstanceId(newinst);
        }
    }
    public void setZindex(String newId) {
        for (int i = 0; i < getComponentCount(); i++) {
            //??
            if (components.elementAt(i) instanceof ComponentGroup) {                
                 ((ComponentGroup)components.elementAt(i)).setZindex(newId);
            }
        }
    }
    public Component findComponentbyName(String name){
        if (getName().equals(name)) {
          return this;
        }
        for (int j = 0; j < components.size(); j++) {
            if (components.elementAt(j).getName().equals(name)) {
                return components.elementAt(j);
            }      
            if (components.elementAt(j) instanceof ComponentGroup) {                
                Component component = ((ComponentGroup)components.elementAt(j)).findComponentbyName(name);
                if (component!=null) {
                    return component;
                }
            }          
        }
        return  null;
    }
    public Component replaceComponentbyId(String id, Component comp){
    	if (getId().equals(id)) {
            return this;
          }
          for (int j = 0; j < components.size(); j++) {
              if (components.elementAt(j).getId().equals(id)) {
                  return components.set(j, comp);
                  
              }   
              if (components.elementAt(j) instanceof ComponentGroup) {
                  Component component = ((ComponentGroup)components.elementAt(j)).replaceComponentbyId(id, comp);
                  if (component!=null) {
                      return component;
                  }
              }              
          }
    	
    	return null;
    }
    public Component findComponentbyId(String id){
        if (getId().equals(id)) {
          return this;
        }
        for (int j = 0; j < components.size(); j++) {
            if (components.elementAt(j).getId().equals(id)) {
                return components.elementAt(j);
            }   
            if (components.elementAt(j) instanceof ComponentGroup) {
                Component component = ((ComponentGroup)components.elementAt(j)).findComponentbyId(id);
                if (component!=null) {
                    return component;
                }
            }              
        }
        return null;
    }
    
    public Component getComponentbyId(String id){
        Component component = findComponentbyId(id);
        if (component!=null) {
            return component;
        }
        return new Component(null);
    }                                                                                                                           

}
