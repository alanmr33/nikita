package com.nikita.mobile.finalphase.service;

import java.util.Set;

import com.nikita.mobile.finalphase.activity.LoginActivity;
import com.nikita.mobile.finalphase.database.Connection;
import com.nikita.mobile.finalphase.database.Recordset;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.action.MobileAction;
import com.nikita.mobile.finalphase.utility.Utility;

import android.app.IntentService;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
 

 
public class NikitaService extends IntentService { 
	public NikitaService() {
		super("Nikita Service");	
	}
	
	public NikitaService(String name) {
		super("Nikita Service");	
	}

	protected void onHandleIntent(Intent intent) {
    	Log.i("NikitaService", Thread.currentThread().getName()+" : "+Looper.myLooper().getThread().getName());
	    Log.i("NikitaService", "action:"+intent.getAction()+" : "+ Utility.Now() );
    	
    	Intent  filter = new Intent();
	    filter.setAction("com.nikita.activity.menuactivity");

	    String action = intent.getAction()!=null?intent.getAction():"";
    	if (action.equals("com.nikita.generator")) {
    		String uuid = intent.getStringExtra("uuid")!=null?intent.getStringExtra("uuid"):"";
    		if (uuid.equals("pushmessage")) {
							
			}
    	}else if (action.equals("order")) {
    		LoginActivity.downloadorder(getApplicationContext(), true);
    	}else if (action.equals("pending")) {
    		//send pending activity order
    		Recordset rs = Connection.DBquery("SELECT * FROM mobileactivity WHERE status = '1'");
    		if(rs.getRows() > 0) {
    			MobileAction.sendMobileActivityData(getApplicationContext());
    		}
    		
    		Recordset rs1 = Connection.DBquery("SELECT * FROM historyactivity WHERE status = '0'");
    		if( rs1.getRows() > 0 ) {
    			MobileAction.sendHistoryActivity(getApplicationContext());
    		}
    		
    	}else if (action.equals("nikitarz")) {
    		Set<Thread> th = Thread.getAllStackTraces().keySet();
    		Thread[] ath = th.toArray(new Thread[th.size()]);
            for (int i = 0; i < ath.length; i++) {
            	Log.i("Nikita NikitaRz:", ath[i].getName());
    			if (ath[i].getName().equalsIgnoreCase("IntentService[NikitaRz]")) {
					return;
				}
            }
    		NikitaRz.startNikitaWebSocket(getApplicationContext());
		}else if (action.equals("background")) {
			
			//Intent   intent2= new Intent();
			//intent2.setAction("com.nikita.generator");
		    //intent2.putExtra("nikitaformname",intent .getStringExtra("nikitaformname") ;			
			//Generator.startNikitaForm(this, formname);
		}
	}
}
