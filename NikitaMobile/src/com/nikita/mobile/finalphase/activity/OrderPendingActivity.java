package com.nikita.mobile.finalphase.activity;

import java.util.Hashtable;
import java.util.Vector;

import com.nikita.mobile.finalphase.connection.NikitaConnection;
import com.nikita.mobile.finalphase.connection.NikitaInternet;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.utility.Messagebox;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.utility.UtilityAndroid;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class OrderPendingActivity  extends AndroidActivity{
	Nikitaset nikitaset;
	Nset nSetting = Nset.newObject();
	
	private Nset norderviewnewform = Nset.newArray();
	private String getTitle(String def){
		if (getIntent()!=null && getIntent().getStringExtra("title")!=null) {
			return getIntent().getStringExtra("title");
		}
		return def;
	}
	protected void onCreate(Bundle savedInstanceState) {		 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.defaultorder);	
		
		nSetting =  Nset.readJSON( Generator.getNikitaParameters().getData( "INIT-PENDING-ACTIVITY").toString());
		UtilityAndroid.setHeaderBackground(this, findViewById(R.id.frmHeaderBG));	
		UtilityAndroid.setButtonActivity(findViewById(R.id.lnrCnt),  nSetting , new View.OnClickListener() {
			public void onClick(View v) {
				String tag = String.valueOf(v.getTag());
				Nset n = Nset.readJSON(tag);				
				Generator.setVirtual("@+CORE-ID", "");
				Generator.setVirtual("@+CORE-ACTIVITYID", "");
				Generator.setVirtual("@+CORE-ORDERID","");
				
				Generator.setVirtual("@+CORE-MENUID", "PENDING");
				Generator.setVirtual("@+CORE-STATUS", "0");										
				Generator.setVirtual("@+CORE-FORMID", n.getData("id").toString() );
				
				Generator.setVirtual("@+CORE-MENUCODE", getCode());
				if ( findViewById(R.id.txtTitle)instanceof TextView ) {							
					Generator.setVirtual("@+CORE-MENUTITLE", ((TextView)findViewById(R.id.txtTitle)).getText().toString());
				}else{
					Generator.setVirtual("@+CORE-MENUTITLE", "");
				}
				String name = n.getData("form").toString() ;
				
				Generator.clearNikitaForms();
				Generator.startNikitaForm(OrderPendingActivity.this, name );
			}
		} );	
		
		((TextView)findViewById(R.id.txtTitle)).setText( getTitle("Data Tertunda") );
		findViewById(R.id.textView1).setVisibility(View.GONE);
		findViewById(R.id.frmCustom).setVisibility(View.GONE);
		findViewById(R.id.frmCustomFind).setVisibility(View.GONE);
		findViewById(R.id.imgLeft).setVisibility(View.INVISIBLE);
		findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {				 
				finish();
			}
		});
		
		populate();
	}
	private void populate(){
		Messagebox.showBusyDialog(OrderPendingActivity.this, new Runnable() {					
			public void run() {
				NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
				nikitaset = ncmobile.Query(nSetting.containsKey("query")? nSetting.getData("query").toString():"SELECT * FROM mobileactivity WHERE status=1");
				norderviewnewform = Nset.readJSON(  Generator.getNikitaParameters().getData("INIT-UI-PENDING-LIST-ITEM").toString() );
				
			}
		}, new Runnable() {
			public void run() {
				ListView listView = (ListView) findViewById(R.id.lstView);
				
				ArrayAdapter<Vector<String>>  arrayAdapter = new ArrayAdapter<Vector<String>> (OrderPendingActivity.this,  R.layout.defaultorderitem, nikitaset.getDataAllVector()){
 					public View getView(final int position, final View convertView, final ViewGroup parent) {
						final View v = Utility.getInflater(OrderPendingActivity.this,  R.layout.defaultorderitem);
						
						//text
						((TextView)v.findViewById(R.id.txtView)).setText( viewDraft(norderviewnewform, nikitaset.getText(position, "body"), nikitaset.getText(position, "read")) );
						((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.n_pending_red);
						UtilityAndroid.setListOrderIcon(position, v, nSetting, nikitaset.getText(position, "status"), new View.OnClickListener() {
							public void onClick(View v) {
								 listDeleteAction(Nset.readJSON(String.valueOf(v.getTag())), nSetting, position, nikitaset);
							}
						});						
						return v;
					}
				};
				listView.setAdapter(arrayAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView arg0, View view, int pos, long arg3) {
						Generator.setVirtual("@+CORE-ID", nikitaset.getText(pos, "id"));
						Generator.setVirtual("@+CORE-ACTIVITYID", nikitaset.getText(pos, "activityid"));
						Generator.setVirtual("@+CORE-ORDERID", nikitaset.getText(pos, "orderid"));
						Generator.setVirtual("@+CORE-ORDERROW", pos);
						Generator.setVirtual("@+CORE-ORDERDATA", "");
						 
						
						Generator.setVirtual("@+CORE-MENUID", "PENDING");
						Generator.setVirtual("@+CORE-VIEW", nikitaset.getText(pos, "read"));
						Generator.setVirtual("@+CORE-STATUS", nikitaset.getText(pos, "status"));
						
						Generator.setVirtual("@+CORE-MENUCODE", getCode());
						if ( findViewById(R.id.txtTitle)instanceof TextView ) {							
							Generator.setVirtual("@+CORE-MENUTITLE", ((TextView)findViewById(R.id.txtTitle)).getText().toString());
						}else{
							Generator.setVirtual("@+CORE-MENUTITLE", "");
						}
						//NCore.removeAllForm();
						//NCore.setMobileActivityActive(Nset.readJSON(nikitaset.getText(pos, "body")));
						//NForm.startFormWithBusy("nikita", OrderDraftActivity.this);
						Generator.clearNikitaForms();
						//Generator.openStreamForms(Nset.readJSON(nikitaset.getText(pos, "body")) ,  OrderPendingActivity.this);
						String name = Generator.getNikitaParameters().getData("INIT-PENDING-ACTIVITY-FORM").toString();
						
						
						NikitaConnection ncmobile = Generator.getConnection(NikitaConnection.MOBILE);
						String newBody = ncmobile.Query("SELECT body FROM mobileactivity WHERE activityid=? ", nikitaset.getText(pos, "activityid")).getText(0, "body");//nikitaset.getText(pos, "body")
						Generator.openStreamForms(Nset.readJSON(newBody) ,name,  OrderPendingActivity.this, "pending");
					}
				});
			}
		});	
	}
	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 populate();
	}
	 public void reloadActivityData(){		
			populate();
		}
	/*public void ListAdapter(ListView listView, Context context , Nikitaset vector){ 
		listView.setAdapter( new ArrayAdapter(context, R.layout.defaultorderitem, vector.getDataAllVector()){
			private Nikitaset nikitaset;
			private Context context;
			public ArrayAdapter get(Context context, Nikitaset nikitaset){
				this.nikitaset=nikitaset;
				this.context=context;
				return this;
			}
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = Utility.getInflater(context, R.layout.defaultorderitem);
				v.findViewById(R.id.frmBagde).setVisibility(View.GONE);
				
				if (nikitaset.getText(position, "status").equals("1")) {
					((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.n_pending);
				}else{
					((ImageView)v.findViewById(R.id.imageView1)).setImageResource(R.drawable.n_draft);
				}	
				
				
				
				((TextView)v.findViewById(R.id.txtView)).setText( viewDraft(norderviewnewform, nikitaset.getText(position, "body"), nikitaset.getText(position, "read")) );
				
				
				return v;
			}
			
		}.get(context, vector));
	}*/
	
	private String viewDraft( Nset norderviewnewform, String activity, String readview){
		StringBuffer stringBuffer = new StringBuffer();
		 
		if (  norderviewnewform.getArraySize()>=1  ) {
			Nset n = Nset.readJSON(activity);
			for (int i = 0; i < norderviewnewform.getArraySize(); i++) {
				if (norderviewnewform.getData(i).toString().startsWith("[")) {
					stringBuffer.append(  n.get(norderviewnewform.getData(i).toString()).toString()    );
				} else if (norderviewnewform.getData(i).toString().equals("@+ENTER")||norderviewnewform.getData(i).toString().equals("@+NEWLINE")) {
					stringBuffer.append("\r\n");
				} else{
					stringBuffer.append(  norderviewnewform.getData(i).toString() );
				}				
			}		 
		}else{
			stringBuffer.append(readview);
		}		
		return stringBuffer.toString();
	}
	
}
