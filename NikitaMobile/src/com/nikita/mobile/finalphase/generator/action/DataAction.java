package com.nikita.mobile.finalphase.generator.action;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.commons.lang.StringEscapeUtils;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.ui.ReceiverUI;
import com.nikita.mobile.finalphase.service.NikitaRz;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class DataAction implements IAction{
	@Override
	public boolean OnAction(final Component comp, final  Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        String param1 = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString());  
        String param2 = currdata.getData("args").getData("param2").toString();
        
        if (code.equals("geterror")) {
            Object obj = comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param1").toString());
            if (obj instanceof Nikitaset) {

            	comp.getNikitaComponent().setVirtual(param2,  ((Nikitaset)obj).getError() );
            }else if (obj instanceof Nset) {
                
            }
        }else if (code.equals("setresult")) {
             
            Object o = comp.getNikitaComponent().getVirtual(param2);
            if (o instanceof Nset) {
            	comp.getNikitaComponent().setResult(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()), new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString())));
            }else{
            	comp.getNikitaComponent().setResult(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()), Nset.readJSON(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString())));
            }
        }else if (code.equals("md5")) {
        	comp.getNikitaComponent().setVirtual(param2,  Utility.MD5(param1) );
        }else if (code.equals("urlencode")) {
        	comp.getNikitaComponent().setVirtual(param2, URLEncoder.encode(param1) );
        }else if (code.equals("urldecode")) {
        	comp.getNikitaComponent().setVirtual(param2, URLDecoder.decode(param1) );
        }else if (code.equals("escapehtml")) {
        	comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeHtml(param1) );
        }else if (code.equals("unescapehtml")) {
        	comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.unescapeHtml(param1) );
         }else if (code.equals("escapesql")) {
        	 comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeSql(param1) );
         }else if (code.equals("escapecsv")) {
        	 comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeCsv(param1) );
        }else if (code.equals("unescapecsv")) {
        	comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.unescapeCsv(param1) );
        }else if (code.equals("nikitaset")) {
            Object o = comp.getNikitaComponent().getVirtual(param2);
 
            if (o instanceof Nikitaset) {
                Nikitaset ns = (Nikitaset)o;
                 
                String row = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString());  
                String col = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString());  
             
                if (row.equals("") && col.equals("")) {
                	comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(), new Nset(ns.getDataAllVector()).toJSON() );
                } else if (row.equals("")) {
                    //all rows with col
                    
                    String[] s = Utility.split(col, ",");
                    int[] cols =new int[s.length];
                    for (int i = 0; i < s.length; i++) {
                        cols[i] = Utility.getInt(s[i]);                        
                    }                    
                    comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(), new Nset(ns.copyDataAllVectorAtCol(cols)).toJSON() );
                } else if (col.equals("")) {
                    //all cols on current row    
                	try {
                		comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(),  new Nset(ns.getDataAllVector().elementAt(Utility.getInt(row))).toJSON() );
					} catch (Exception e) { }
                }else if (Utility.isNumeric(col)) {                    
                	comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(), ns.getText(Utility.getInt(row), Utility.getInt(col))  );
                }else{
                	comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(), ns.getText(Utility.getInt(row), col)  );
                }
               
                String nset = currdata.getData("args").getData("param6").toString(); 
                if (nset.startsWith("@")) {
                	comp.getNikitaComponent().setVirtual( nset, new Nset(ns.getDataAllVector()) );
                }
 
            }   
        }else if (code.equals("nikitasetadd")) {
            Object o = comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("result").toString()); 
            if (o instanceof Nikitaset) {
                Nikitaset ns = (Nikitaset)o;
                
                Vector<String> col = new Vector<String>();
                for (int i = 0; i < ns.getCols(); i++) {
                    col.addElement( comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param"+i).toString()) );
                }
                ns.getDataAllVector().addElement(col);
                                 
            }
        }else if (code.equals("nikitasetupd")||code.equals("nikitasetreplace")) {
            Object o = comp.getNikitaComponent().getVirtual(param2); 
            if (o instanceof Nikitaset) {
                Nikitaset ns = (Nikitaset)o;
                
                String row = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param3").toString());  
                String col = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString());  
                String newdata = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param5").toString());  
                
                
                if (!Utility.isNumeric(col)) {
                    for (int i = 0; i < ns.getCols(); i++) {
                        if (ns.getHeader(i).equals(col)) {
                            col=i+"";
                            break;
                        }
                    }
                }
                
                int r = Utility.getInt(row);
                int c = Utility.getInt(col);
                
                Vector<Vector<String>> vector = ns.getDataAllVector();
                if (vector.size()>r) {
                    if (vector.elementAt(r).size()>c) {
                        vector.elementAt(r).setElementAt(newdata, c);
                    }  
                }                
            }
        }else if (code.equals("newnikitaset")) {                      
                    
            Nset n = Nset.newObject();
            n.setData("header", new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param1").toString())));
            Nset data = new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param2").toString()));
            if (data.isNsetArray()) {
                n.setData("data", data);
            }else{
                n.setData("data", Nset.newArray());
            }            
            n.setData("info", new Nset(comp.getNikitaComponent().getVirtual(currdata.getData("args").getData("param3").toString())));
            n.setData("error", comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param4").toString()));
            Nikitaset ns = new Nikitaset(n);
            
            comp.getNikitaComponent().setVirtual( currdata.getData("args").getData("result").toString(), ns  );
         
        }else if (code.equals("broadcast")) {             //local
            Object o = comp.getNikitaComponent().getVirtual(param2);
            String recever = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()) ;
            comp.getNikitaComponent().setVirtualRegistered("@+REQUESTCODE", "");
        	comp.getNikitaComponent().setVirtualRegistered("@+RESPONSECODE", "");
        	
            if (o instanceof Nset) {            	
            }else{
            	o = Nset.readJSON(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()));
            }
            String[] nckeys =  AppNikita.getInstance().getNikitaControlerNames();
            for (int i = 0; i < nckeys.length; i++) {
            	NikitaControler nikitaControler =  AppNikita.getInstance().getNikitaComponent(nckeys[i]);
            	for (int j = 0; j < nikitaControler.getContent().getComponentCount(); j++) {
					if (nikitaControler.getContent().getComponent(j) instanceof ReceiverUI) {
						if (nikitaControler.getContent().getComponent(j).getText().equals(recever)) {
							nikitaControler.setVirtualRegistered("@+RESULT", o);
							nikitaControler.getContent().getComponent(j).runRoute();
						}
					}
				}
			}            
         }else if (code.equals("broadcastglobal")) {             
            Object o = comp.getNikitaComponent().getVirtual(param2);
            if (o instanceof Nset) {            	
            }else{
            	o = Nset.readJSON(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()));
            }
             Nset r =Nset.newObject();
	       	 r.setData("action", "broadcast");
	       	 r.setData("receiver", comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()));
	       	 r.setData("message", ((Nset)o).toJSON());
	       	 NikitaRz.send(r.toJSON());
        }else if (code.equals("broadcastlistener")) {             
            Object o = comp.getNikitaComponent().getVirtual(param2);
            if (o instanceof Nset) {            	
            }else{
            	o = Nset.readJSON(comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()));
            }
             Nset r =Nset.newObject();
	       	 r.setData("action", "listener");
	       	 r.setData("receiver", comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()));
	       	 r.setData("message", ((Nset)o).toJSON());
	       	 NikitaRz.send(r.toJSON());
        }else if (code.equals("broadcastlisteneradd")) {   
        	 Nset r =Nset.newObject();
        	 r.setData("action", "addlistener");
        	 r.setData("receiver", comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()));
        	 NikitaRz.send(r.toJSON());
        }else if (code.equals("broadcastlistenerremove")) {     
        	Nset r =Nset.newObject();
        	r.setData("action", "removelistener");
       	 	r.setData("receiver", comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param1").toString()));
       	 	NikitaRz.send(r.toJSON());
        }else if (code.equals("md5")) {
            comp.getNikitaComponent().setVirtual(param2,  Utility.MD5(param1) );
        }else if (code.equals("sha1")) {
            comp.getNikitaComponent().setVirtual(param2,  Utility.SHA1(param1) );
        }else if (code.equals("urlencode")) {
            comp.getNikitaComponent().setVirtual(param2, URLEncoder.encode(param1) );
        }else if (code.equals("urldecode")) {
            comp.getNikitaComponent().setVirtual(param2, URLDecoder.decode(param1) );
        }else if (code.equals("escapehtml")) {
            comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeHtml(param1) );
        }else if (code.equals("unescapehtml")) {
            comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.unescapeHtml(param1) );
         }else if (code.equals("escapesql")) {
            comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeSql(param1) );
         }else if (code.equals("escapecsv")) {
            comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.escapeCsv(param1) );
        }else if (code.equals("unescapecsv")) {
            comp.getNikitaComponent().setVirtual(param2, StringEscapeUtils.unescapeCsv(param1) );
        } 
       
        return true;
    }
}
