package com.nikita.mobile.finalphase.generator.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.generator.NForm;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.Style;
import com.nikita.mobile.finalphase.service.ButtonN;
import com.nikita.mobile.finalphase.service.IconButton;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class ButtonUI extends Component{

	public ButtonUI(NikitaControler form) {
		super(form);  
	}
	
	private View view;
 
	public View onCreate(NikitaControler nikitaComponent) {
		view=Utility.getInflater(nikitaComponent.getActivity(), R.layout.compbutton);
		view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.WRAP_CONTENT, MarginLayoutParams.WRAP_CONTENT));
		
		view.findViewById(R.id.imgView).setVisibility(View.GONE);
		view.findViewById(R.id.tblButton).setPadding(0, 0, 0, 0);
		 
		((ButtonN)view.findViewById(R.id.tblButton)).setIcon(view.findViewById(R.id.imgView), convertPixel("24dp"));
		
		view.findViewById(R.id.tblButton).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (getOnClickListener()!=null) {
					getOnClickListener().OnClick(ButtonUI.this);
				}else{
					getNikitaComponent().runOnActionThread(new Runnable() {
						public void run() {
							runRoute();							
						}
					});		
				}					
			}
		});
		mobiledefWidth();
		mobileLabelOrientation(view.findViewById(R.id.incLabels));
		
		 
		
		refreshViewOnUI();
		refreshStyleOnUI();
		
		return view;
	}
	 
	 
	public View getView() {		 
		return view;
	}
	private void refreshView(){
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					refreshViewOnUI();
				}
			});
		} 
	}
	private String getIcon( ){
        if (getStyle()!=null) {
            if (getStyle().getInternalObject().getData("style").containsKey("n-icon")) {
                return getBaseUrl(getStyle().getInternalObject().getData("style").getData("n-icon").toString());  
            }
        }
        return "";
    } 
 
	private void refreshViewOnUI(){
		view.setEnabled(getEnable());	
		view.setVisibility(getVisible()?View.VISIBLE:View.GONE);
		
		
		View v = view.findViewById(R.id.tblButton);
		if (v instanceof Button) {
			((Button)v).setText(getText());
			v.setEnabled(getEnable());	
		}

		String s = getIcon();
		if (s.length()>=3) {
			view.findViewById(R.id.imgView).setVisibility(View.VISIBLE);
			view.findViewById(R.id.tblButton).setPadding(convertPixel("32dp"), 0, 0, 0);
			
			if (s.contains("/")) {
				s=s.substring(s.lastIndexOf("/"));
			}
			setImageResource( (ImageView)view.findViewById(R.id.imgView), s);
		}else{
			view.findViewById(R.id.imgView).setVisibility(View.GONE);
			view.findViewById(R.id.tblButton).setPadding(0, 0, 0, 0);			 
		}
		
		validateGravity((TextView)view.findViewById(R.id.tblButton), getStyle().getInternalStyle());
		validateFontStyle(view.findViewById(R.id.tblButton),  getStyle().getInternalStyle() );
	}
	@Override
	public void setStyle(Style style) {		 
		super.setStyle(style);
		
		if (view!=null) {
			runOnUI(new Runnable() {
				public void run() {
					//refreshStyleOnUI();
					validateGravity((TextView)view.findViewById(R.id.tblButton), getStyle().getInternalStyle());
					validateFontStyle(view.findViewById(R.id.tblButton),  getStyle().getInternalStyle() );
				}
			});
		} 
	}
	private void refreshStyleOnUI(){
		super.StyleOnUI(view);
		
		validateGravity((TextView)view.findViewById(R.id.tblButton), getStyle().getInternalStyle());
		validateFontStyle(view.findViewById(R.id.tblButton),  getStyle().getInternalStyle() );
	}
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		refreshView();
	}

	public void setEnable(boolean enable) {
		super.setEnable(enable);
		refreshView();
	}
}
