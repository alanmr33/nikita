package com.nikita.mobile.finalphase.printer;

import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;

public class GlobalPrinter implements MobilePrinterInterface {
	private MobilePrinter mobilePrinter;
	public GlobalPrinter(Context ctx){
		this.mobilePrinter = new BxlPrinter(ctx);
	}
	@Override
	public void setOnGetBatteryStatus(OnGetBatteryStatus onGetBatteryStatus) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setOnGetBatteryStatus(onGetBatteryStatus);
	}
	@Override
	public void setOnFinishPrint(OnFinishPrint onFinishPrint) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setOnFinishPrint(onFinishPrint);		
	}
	@Override
	public void finishMessage(Boolean status, String message) {
		// TODO Auto-generated method stub
		this.mobilePrinter.finishMessage(status, message);
	}
	@Override
	public void setupPrintJob(String company, HashMap<String, String> printText) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setupPrintJob(company, printText);
	}
	@Override
	public void startPrinting() {
		// TODO Auto-generated method stub
		this.mobilePrinter.startPrinting();
	}
	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		this.mobilePrinter.disconnect();
	}
	@Override
	public void setLogo(Bitmap logo) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setLogo(logo);
	}	
	@Override
	public void setBatteryStatus(String message) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setBatteryStatus(message);
	}
	@Override
	public void checkBattery() {
		// TODO Auto-generated method stub
		this.mobilePrinter.checkBattery();
	}
	public void testConnection() {
		// TODO Auto-generated method stub
		this.mobilePrinter.testConnection();
	}
	public void printTest() {
		// TODO Auto-generated method stub
		this.mobilePrinter.printTest();
	}
	public void setDeviceName(String deviceName){
		this.mobilePrinter.setDeviceName(deviceName);
	}
	public String getDeviceName() {
		return this.mobilePrinter.getDeviceName();
	}
	public void setDeviceMAC(String deviceMAC){
		this.mobilePrinter.setDeviceMAC(deviceMAC);
	}
	public String getDeviceMAC() {
		return this.mobilePrinter.getDeviceMAC();
	}
	@Override
	public void setActivity(Activity activity) {
		// TODO Auto-generated method stub
		this.mobilePrinter.setActivity(activity);
	}
}
