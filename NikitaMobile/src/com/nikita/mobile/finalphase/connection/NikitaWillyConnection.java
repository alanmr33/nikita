 
package com.nikita.mobile.finalphase.connection;
/**
 * created by 13k.mail@gmail.com
 */
import android.util.Log;

import com.google.android.gms.ads.a;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.Generator;
import com.nikita.mobile.finalphase.generator.NCore;
import com.nikita.mobile.finalphase.utility.Utility;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringEscapeUtils;
 

/**
 * created by 13k.mail@gmail.com
 */
 
public class NikitaWillyConnection extends NikitaConnection{
    private String urlConnection = "";
    private String usrConnection = "";
    private  String pwdConnection = "";
    
    public NikitaConnection openConnection(String cname, String url, String user, String pass){
        pwdConnection=pass;usrConnection=user;urlConnection=url;
        return this;
    }
    
    public Nikitaset QueryPage(String sql, Nset array, int page, int rowperpage) {
    	return runQuery(sql, array, page, rowperpage);
    }    

    public Nikitaset QueryPage(int page, int rowperpage, String sql, String...param){
    	return runQuery(page, rowperpage, sql, param);
    }
    
    private Nikitaset runQuery(String sql, Nset array, int page, int rowperpage) {    			
		Hashtable<String, String> args= new Hashtable<String, String> ();
        args.put("sql", Nset.newObject().setData("nfid", "nikitaconnection").setData("sql", sql).setData("arg", array).toJSON());
		
        if (rowperpage==-1 && page==-1) {
		}else{
			args.put("paging",  page+","+rowperpage);
		}		
		return sendQuery(args);
    }
    private Nikitaset runQuery(int page, int rowperpage, String s, String... param) {   
        StringBuffer sb = new StringBuffer();
        int index = s.indexOf("?");int i=0;
		while (index >= 0) {
			String v =s.substring(0, index);
			if (v.endsWith("@")) {
				sb.append(v.substring(0,v.length()-1));
				s = s.substring(index + 1);
				index = s.indexOf("?");			
				
				sb.append(param[i]);i++;
			}else{
				sb.append(v);
				s = s.substring(index + 1);
				index = s.indexOf("?");			
				
				sb.append("'").append(StringEscapeUtils.escapeSql(param[i])).append("'");i++;
			}
		}
		sb.append(s);
		
		
		Hashtable<String, String> args= AppNikita.getInstance().getArgsData(); //new Hashtable<String, String> ();
        args.put("sql", sb.toString());
		
        if (rowperpage==-1 && page==-1) {
		}else{
			args.put("paging",  page+","+rowperpage);
		}
		
		return sendQuery(args);
    }
    	
    	
 
    private Nikitaset sendQuery(Hashtable<String, String> args) {
    	long start = System.currentTimeMillis();
    	args.put("username",  usrConnection);
    	args.put("password",  pwdConnection);
        try {
        	Nikitaset ns ;
        	if (Generator.getNikitaParameters().getData("INIT-CONNECTION-CONTENT-TYPE").toString().equalsIgnoreCase("base64")) {
        		ns =  new Nikitaset( Nset.readJSON(NikitaInternet.postHttpData(urlConnection, "application/nikitaconnection; nfid=base64", Utility.encodeBase64( new Nset(args).toJSON())  ).getEntity().getContent()) );
        	}else if (Generator.getNikitaParameters().getData("INIT-CONNECTION-CONTENT-TYPE").toString().equalsIgnoreCase("nikita")) {
        		ns =  new Nikitaset( Nset.readJSON(NikitaInternet.postHttpData(urlConnection, "application/nikitaconnection",  new Nset(args).toJSON()   ).getEntity().getContent()) );
			} else{
				ns =  new Nikitaset( Nset.readJSON(NikitaInternet.postHttp(urlConnection,args).getEntity().getContent()) );
			}
			Nset n = (new Nset(ns.getInfo()));
			if (!n.getData("core").toString().equals("")) {
		         core=n.getData("core").toInteger();
			}       			 

			//13lk overide time nikitaconnection
			ns.setInfo(new Nset(ns.getInfo()).setData("time", System.currentTimeMillis() - start  ));
			
			return  ns; 
        }catch (Exception ex) {
            System.out.println("NikitaWillyConnection");
            ex.printStackTrace();
        }         
        return  new Nikitaset( "Connection Problem" );
    }
    
    
    /*
    public static DefaultHttpClient getThreadSafeClient()  {

        DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams params = client.getParams();
        client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, 
                mgr.getSchemeRegistry()), params);
        return client;
    }
    */
    
    public boolean isNikitaConnection() {
        return true; 
    }
}
