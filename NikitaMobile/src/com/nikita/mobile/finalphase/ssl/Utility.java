package com.nikita.mobile.finalphase.ssl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
 

public class Utility {

	
	
	public static final String BOLD_FONT_PATH = "fonts/Montserrat-Bold.ttf";
	public static final String REGULAR_FONT_PATH = "fonts/Montserrat-Regular.ttf";
	public static Typeface regularFont, boldFont;
	
	
	public static final long APP_ID = 0xf46f5a7867d69ff0L;
	public final static String version = "2.8";
	public final static String applicationId = "SURVEYOR";
	
	public static boolean login = false;
	public static Activity activity;
	public static String imeiNumber;
	public static int photoCompress;
	public static String urls;
	public static int scheduller;
	public static String userId;
	public static int headerSize;
	public static Hashtable<String, String> formNumber;
	
	public static String oldPassword;
	
	public static String SEND_MODE =""; 
	

	public static String getOldPassword() {
		return oldPassword;
	}

	public static void setOldPassword(String oldPassword) {
		Utility.oldPassword = oldPassword;
	}

	public static int getPhotoCompress() {
		return photoCompress;
	}

	public static void setPhotoCompress(int photoCompress) {
		Utility.photoCompress = photoCompress;
	}

	public static int getScheduller() {
		return scheduller;
	}

	public static void setScheduller(int scheduller) {
		Utility.scheduller = scheduller;
	}

	public static boolean isLogin() {
		return Utility.login;
	}

	public static void setLogin(boolean login) {
		Utility.login = login;
	}


	public static Activity getActivity() {
		return activity;
	}

	public static void setActivity(Activity activity) {
		Utility.activity = activity;
	}

	public static String getImeiNumber() {
		return imeiNumber;
	}

	public static void setImeiNumber(String imeiNumber) {
		Utility.imeiNumber = imeiNumber;
	}

	

	public static String getUrls() {
		return urls;
	}

	public static void setUrls(String urls) {
		Utility.urls = urls;
	}

	

	public static String getUserId() {
		return userId;
	}

	public static void setUserId(String userId) {
		Utility.userId = userId;
	}

	public static int getHeaderSize() {
		return headerSize;
	}

	public static void setHeaderSize(int headerSize) {
		Utility.headerSize = headerSize;
	}

	

	public static ArrayList vectorToArrayList(Vector vector) {
		if (vector == null) {
			return null;
		}
		return new ArrayList<Object>(vector);
	}

	 
	public static void setFontAllView(ViewGroup vg) {

		for (int i = 0; i < vg.getChildCount(); ++i) {

			View child = vg.getChildAt(i);

			if (child instanceof ViewGroup) {

				setFontAllView((ViewGroup) child);

			} else if (child != null) {
				Typeface face;
				if (child.getTag() != null
						&& child.getTag().toString().toLowerCase()
								.equals("bold")) {
					face = boldFont;
				} else {
					face = regularFont;
				}
				if (child instanceof TextView) {
					TextView textView = (TextView) child;
					textView.setTypeface(face);
				} else if (child instanceof EditText) {
					EditText editView = (EditText) child;
					editView.setTypeface(face);
				} else if (child instanceof RadioButton) {
					RadioButton radioView = (RadioButton) child;
					radioView.setTypeface(face);
				} else if (child instanceof CheckBox) {
					CheckBox checkboxView = (CheckBox) child;
					checkboxView.setTypeface(face);
				}

			}

		}
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static Bitmap GetImageFromAssets(Context context, String imagePath) {
		Bitmap bmp = null;
		try {
			InputStream bitmap = context.getAssets().open(imagePath);
			bmp = BitmapFactory.decodeStream(bitmap);

		} catch (Exception e1) {
			Log.d("Application Find", e1.getMessage());
		}
		return bmp;
	}

	private static byte[] stringToByte(String s) {
		char[] sa = s.toCharArray();
		byte[] ba = new byte[sa.length];

		for (int i = 0; i < ba.length; i++) {
			ba[i] = (byte) (sa[i] & 0xFF);
		}

		return ba;
	}

	public void resourceAsStream() {
		InputStream is = this.getClass().getResourceAsStream("/English.res");
	}

	public static String encode(String s, String enc) throws IOException {
		ByteArrayOutputStream bOut = new ByteArrayOutputStream();
		DataOutputStream dOut = new DataOutputStream(bOut);
		StringBuffer ret = new StringBuffer(); // return value
		dOut.writeUTF(s);
		ByteArrayInputStream bIn = new ByteArrayInputStream(bOut.toByteArray());
		bIn.read();
		bIn.read();
		int c = bIn.read();
		while (c >= 0) {
			if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
					|| (c >= '0' && c <= '9') || c == '.' || c == '-'
					|| c == '*' || c == '_') {
				ret.append((char) c);
			} else if (c == ' ') {
				ret.append('+');
			} else {
				if (c < 128) {
					appendHex(c, ret);
				} else if (c < 224) {
					appendHex(c, ret);
					appendHex(bIn.read(), ret);
				} else if (c < 240) {
					appendHex(c, ret);
					appendHex(bIn.read(), ret);
					appendHex(bIn.read(), ret);
				}
			}
			c = bIn.read();
		}
		return ret.toString();
	}

	private static void appendHex(int arg0, StringBuffer buff) {
		buff.append('%');
		if (arg0 < 16) {
			buff.append('0');
		}
		buff.append(Integer.toHexString(arg0));
	}

	static public String urlEncode(String sUrl) {
		StringBuffer urlOK = new StringBuffer();
		for (int i = 0; i < sUrl.length(); i++) {
			char ch = sUrl.charAt(i);
			switch (ch) {
			case '<':
				urlOK.append("%3C");
				break;
			case '>':
				urlOK.append("%3E");
				break;
			case '/':
				urlOK.append("%2F");
				break;
			case ' ':
				urlOK.append("%20");
				break;
			case ':':
				urlOK.append("%3A");
				break;
			case '-':
				urlOK.append("%2D");
				break;
			default:
				urlOK.append(ch);
				break;
			}
		}
		return urlOK.toString();
	}

	/**
	 * Split string into vector based on string separator
	 * 
	 * @param data
	 *            String
	 * @param dataSeparator
	 *            Separator
	 * @return Vector
	 */
	public static Vector splitString(String data, String dataSeparator) {
		data = data + dataSeparator;
		Vector splitData = new Vector();
		int idx;

		do {
			idx = data.indexOf(dataSeparator);
			if (idx != -1) {
				splitData.addElement(data.substring(0, idx));
				data = data.substring(idx + 1);
			}
		} while (idx != -1);

		return splitData;
	}

	public static Hashtable stringToHastable(String data) {
		Hashtable hashdata = new Hashtable();

		String docid = "";
		Vector rawdata = splitString(data, "|");
		Vector contentdata;
		Vector detaildata;
		for (int i = 0; i < rawdata.size(); i++) {

			if (!rawdata.elementAt(i).toString().equals("")) {
				contentdata = splitString(rawdata.elementAt(i).toString(), "#");
				docid = contentdata.elementAt(0).toString();
				detaildata = splitString(contentdata.elementAt(1).toString(),
						";");
				hashdata.put(docid, detaildata);
			}

		}

		return hashdata;
	}

	// always verify the host - dont check for certificate
	public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	public static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection
					.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
