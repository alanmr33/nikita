package com.nikita.mobile.finalphase.stream;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import com.nikita.mobile.finalphase.ssl.EasySSLSocketFactory;
import com.nikita.mobile.finalphase.utility.Utility;

import android.util.Log;

public class ImagePost {
	public static String postHttpConnectionWithPicture(String urlString, Hashtable<String, String> param, String fname, String fnamefullpath) {
		String result = "";
		
		urlString = Utility.nikitaUrl(urlString);
		
		HttpEntity resEntity;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		Log.i("postHttpConnectionWithPicture", urlString);
		
		if (fnamefullpath!=null) {
			try {
				FileInputStream fis = new FileInputStream(fnamefullpath);

				byte[] buffer = new byte[1024] ;int len =0 ;
				while ( (len=fis.read(buffer))>=0 ) {
					bos.write(buffer, 0, len);
				}	
				fis.close();
			} catch (Exception e) { }
		}		
		ByteArrayBody dataimage = new ByteArrayBody(bos.toByteArray(), (fname!=null?fname:"") );

		try {
			HttpClient client = new DefaultHttpClient();
			
			//13kcrazy
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
			 
			HttpParams params = new BasicHttpParams();
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			

			
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpConnectionParams.setConnectionTimeout(params, 0);
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			 
			ClientConnectionManager cm = new SingleClientConnManager(params, schemeRegistry);
			client = new DefaultHttpClient(cm, params);
			//13kcrazy
			
			HttpPost post = new HttpPost( urlString );

			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			if (param!=null) {
				Enumeration<String> arg = param.keys();
				while (arg.hasMoreElements()) {
					String key = arg.nextElement();
					String dat = param.get(key);
					 
					reqEntity.addPart(key, new StringBody(dat));
				}
			}
			if (fnamefullpath!=null) {
				reqEntity.addPart("image", dataimage);
			}

			post.setEntity(reqEntity);
			HttpResponse response = client.execute(post);
			resEntity = response.getEntity();
			result = EntityUtils.toString(resEntity);
			
			if (resEntity != null) {
				Log.i("postHttpConnectionWithPicture", result);

			}
		} catch (Exception ex) {
			Log.e("postHttpConnectionWithPicture", "error: " + ex.getMessage(), ex);
			return "Connection Timeout";
		}
		Log.i("postHttpConnectionWithPicture", result);
		return result!=null?result: "Connection Timeout";
	}
	
	 
}
