package com.nikita.mobile.finalphase.generator.action;

import android.util.Log;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.component.IAction;
import com.nikita.mobile.finalphase.generator.Style;
import com.rkrzmail.nikita.data.Nikitaset;
import com.rkrzmail.nikita.data.Nset;
import com.rkrzmail.nikita.utility.AUtility;

public class StyleAction implements IAction {
	@Override
	public boolean OnAction(Component comp, Nset currdata) {
		String code = comp.getNikitaComponent().getVirtualString(currdata.getData("code").toString());  
        String param1 = currdata.getData("args").getData("param1").toString();         
        if (code.equals("style") && param1.startsWith("$")) {
            Component com = comp.getNikitaComponent().getComponent(param1);
            
            String s = comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString());
            if (s.equals("")) {
            	com.setStyle(Style.createStyle());            
			}else{
				Style currStyle = Style.createStyle(com.getStyle());
	            Style style = Style.createStyle( comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString()));
	            
	            String[] 
        		keys = style.getInternalStyle().getObjectKeys();
	            for (int i = 0; i < style.getInternalStyle().getSize(); i++) {
	            	currStyle.getInternalStyle().setData(keys[i],  style.getInternalStyle().getData( keys[i] )   );
				}
	            keys = style.getInternalAttr().getObjectKeys();
	            for (int i = 0; i < style.getInternalAttr().getSize(); i++) {
	            	currStyle.getInternalAttr().setData(keys[i],  style.getInternalAttr().getData( keys[i] )   );
				}
	           	            
	            com.setStyle(currStyle);
			}
            //com.setStyle(Style.createStyle( comp.getNikitaComponent().getVirtualString(currdata.getData("args").getData("param2").toString())));
            comp.getNikitaComponent().refreshComponent(com);
        }
        return true;
	}
}
