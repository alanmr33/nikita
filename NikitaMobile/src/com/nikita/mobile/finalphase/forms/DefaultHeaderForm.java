package com.nikita.mobile.finalphase.forms;

import java.util.List;
import java.util.Vector;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListPopupWindow;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.nikita.mobile.finalphase.component.Component;
import com.nikita.mobile.finalphase.generator.AppNikita;
import com.nikita.mobile.finalphase.generator.NikitaControler;
import com.nikita.mobile.finalphase.generator.ui.layout.NikitaForm;
import com.nikita.mobile.finalphase.utility.Utility;
import com.nikita.mobile.finalphase.R;
import com.rkrzmail.nikita.data.Nset;

public class DefaultHeaderForm extends NikitaControler{
 
	public void onCreateUI() {		
		
		super.onCreateUI();
	}
 
	public void initialize() {
		 
	}
	 public void popup(Context context, View view){
		 //Creating the instance of PopupMenu  
         PopupMenu popup = new PopupMenu(context, view);  
         //Inflating the Popup using xml file  
       //  popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());  
        
         //registering popup with OnMenuItemClickListener  
         popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {  
		      public boolean onMenuItemClick(MenuItem item) {  
		        return true;  
		      }  
         });  

         popup.show();//showing popup men
         
         
         
         
	 }
	 class popupListItem extends ArrayAdapter{
		Context context ;
		Vector vector ;
		public popupListItem(Context context, Vector objects) {
			super(context, 0, objects);	
			this.context=context;
			this.vector=objects;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = convertView;
			TextView view = new TextView(getContext());
//			View v = inflater.inflate(R.layout.popuplistitem, parent, false); 
//			TextView view = (TextView)v.findViewById(R.id.txtPopUp);
			view.setText(String.valueOf(vector.elementAt(position)));
			return v;
		}
		 
	 }
	 public void popupList(Context context, View view){
		 Vector vector = new Vector();
		 vector.addElement("aaaaaaa");
		 vector.addElement("bbbbbbb");
		 vector.addElement("ccccccc");
		 
		 
		ListAdapter listAdapter = new popupListItem(context, vector);
		final ListPopupWindow popupWindow = new ListPopupWindow(context);
		popupWindow.setAnchorView(view);
	    popupWindow.setAdapter( listAdapter );
	    //popupWindow.setWidth(400); // note: don't use pixels, use a dimen resource
	    //popupWindow.setWidth(Utility.convertDptoPx(context, 120));
	    popupWindow.setContentWidth(measureContentWidth(context, listAdapter ));
	    popupWindow.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				popupWindow.dismiss();			
			}	    	
		});	 
	    popupWindow.show();
	 }
	 private int measureContentWidth(Context context, ListAdapter listAdapter) {
		    ViewGroup mMeasureParent = null;
		    int maxWidth = 0;
		    View itemView = null;
		    int itemType = 0;

		    final ListAdapter adapter = listAdapter;
		    final int widthMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		    final int heightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		    final int count = adapter.getCount();
		    for (int i = 0; i < count; i++) {
		        final int positionType = adapter.getItemViewType(i);
		        if (positionType != itemType) {
		            itemType = positionType;
		            itemView = null;
		        }

		        if (mMeasureParent == null) {
		            mMeasureParent = new FrameLayout(context);
		        }

		        itemView = adapter.getView(i, itemView, mMeasureParent);
		        itemView.measure(widthMeasureSpec, heightMeasureSpec);

		        final int itemWidth = itemView.getMeasuredWidth();

		        if (itemWidth > maxWidth) {
		            maxWidth = itemWidth;
		        }
		    }

		    return maxWidth;
		}
	public void onLoad() {
		NikitaForm nf = new NikitaForm(this){
			View view;
			public View onCreate(NikitaControler form) {
				view = Utility.getInflater(getNikitaComponent().getActivity(), R.layout.header);
				view.setLayoutParams(new MarginLayoutParams(MarginLayoutParams.MATCH_PARENT, MarginLayoutParams.WRAP_CONTENT));
							
								
				view.findViewById(R.id.imgRight).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						final NikitaControler caller = AppNikita.getInstance().getNikitaComponent(DefaultHeaderForm.this.getCallerInstance())	;	
						if (caller!=null) {
							runOnActionThread(new Runnable() {
								public void run() {
									caller.onBack();									
								}
							});							
						}
					}
				});
				view.findViewById(R.id.imgLeft).setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						final NikitaControler caller = AppNikita.getInstance().getNikitaComponent(DefaultHeaderForm.this.getCallerInstance())	;	
						if (caller!=null) {
							runOnActionThread(new Runnable() {
								public void run() {
									caller.onResult("header", "lefticon", Nset.newObject());								
								}
							});							
						}
					}
				});
				
				
				((TextView)view.findViewById(R.id.txtTitle) ).setText(getText());				
//				view.findViewById(R.id.imgLeft).setVisibility(  form.getComponent("$lefticon").isVisible()?View.VISIBLE:View.GONE  );
				view.findViewById(R.id.imgLeft).setVisibility(  View.INVISIBLE );
				view.findViewById(R.id.imgRight).setVisibility(  form.getComponent("$righticon").isVisible()?View.VISIBLE:View.GONE  );
				
				return view;
			}

			public View getView() {
				return view;
			}
	  };	 
	  Component
	  component = new Component(this){ 
		public void setVisible(boolean visible) {
			super.setVisible(visible);
			runOnUiThread(new Runnable() {
				public void run() {
					if (getContent().getView()!=null && getContent().getView().findViewById(R.id.imgLeft)!=null) {
						getContent().getView().findViewById(R.id.imgLeft).setVisibility( getVisible()? View.VISIBLE: View.GONE);
					}
				}
			});
		}
	  };
	  component.setName("lefticon");
	  nf.addComponent(component);
	  
	  
	  component = new Component(this){ 
	  public void setVisible(boolean visible) {
			super.setVisible(visible);
			runOnUiThread(new Runnable() {
				public void run() {
					if (getContent().getView()!=null && getContent().getView().findViewById(R.id.imgRight)!=null) {
						getContent().getView().findViewById(R.id.imgRight).setVisibility( getVisible()? View.VISIBLE: View.GONE);
					}
				}
			});
		}
	  };
	  component.setName("righticon");
	  nf.addComponent(component);
	  
	  component = new Component(this){ 
		public void setVisible(boolean visible) {
			super.setVisible(visible);
			runOnUiThread(new Runnable() {
				public void run() {
					if (getContent().getView()!=null && getContent().getView().findViewById(R.id.txtTitle)!=null) {
						getContent().getView().findViewById(R.id.txtTitle).setVisibility( getVisible()? View.VISIBLE: View.GONE);
					}
				}
			});
		}
 
		public void setText(String text) {			 
			super.setText(text);
			runOnUiThread(new Runnable() {
				public void run() {
					if (getContent().getView()!=null && getContent().getView().findViewById(R.id.txtTitle)!=null) {
						((TextView)getContent().getView().findViewById(R.id.txtTitle)).setText(getText()); 
					}
				}
			});
		}
	  };
	  component.setName("title");
	  nf.addComponent(component);	  
	  	  
	   
	  setContent(nf);
	}
}
